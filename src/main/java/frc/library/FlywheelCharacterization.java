/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.BaseTalon;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.math.Time;
import org.growingstems.math.Time.TimeUnit;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;

public class FlywheelCharacterization {

    public enum State {
        STOPPED,
        INIT,
        START_DEADBAND,
        DEADBAND,
        START_POWER_MAP,
        POWER_MAP,
        WRAPUP
    }

    public static class Measurement {
        public double inputPower;
        public double measuredVelocity;

        private Measurement(double inputPower, double measuredVelocity) {
            this.inputPower = inputPower;
            this.measuredVelocity = measuredVelocity;
        }
    }

    private BaseTalon m_controller;
    private State m_state = State.STOPPED;
    private FileWriter m_log = null;
    private final String m_name;

    private static double k_maxTalonDacOutput = 1023.0;
    private double m_arbFF = 0.0;
    private double m_fGain = 0.0;
    private double m_maxVelocity = 0.0;

    // Deadband test variables and constants
    private static double k_deadbandPowerIncrement = 0.001;
    private static Time k_deadbandIncrementDelay = new Time(1.0, TimeUnit.SECONDS);
    private static double k_deadbandMaxReasonablePower = 0.1;
    private WpiTimer m_deadbandTimer = new WpiTimer();
    private double m_deadbandCurrentPower = 0.0;
    private double m_deadbandPower = 0.0;

    // Output Power Mapping test variables and constants
    private static double k_powerMapPowerIncrement = 0.02;
    private static Time k_powerMapIncrementDelay = new Time(6.0, TimeUnit.SECONDS);
    private WpiTimer m_powerMapTimer = new WpiTimer();
    private double m_powerMapCurrentPower = 0.0;
    private Vector<Measurement> m_powerMapTable = new Vector<Measurement>();

    public FlywheelCharacterization(String name, BaseTalon controller) {
        m_controller = controller;
        m_name = name;
    }

    public void start() {
        m_state = State.INIT;
    }

    public void stop() {
        m_state = State.WRAPUP;
    }

    public boolean isDone() {
        return m_state == State.STOPPED;
    }

    public double getCalcDeadband() {
        return m_arbFF;
    }

    public double getCalcFGain() {
        return m_fGain;
    }

    public double getCalcMaxVelocity() {
        return m_maxVelocity;
    }

    public void tick() {
        switch (m_state) {
            case STOPPED:
                return;
            case INIT:
                init();
                m_state = State.START_POWER_MAP;
                return;
            case START_DEADBAND:
                configureDeadbandTest();
                m_state = State.DEADBAND;
                return;
            case DEADBAND:
                if (runDeadbandTest()) {
                    m_state = State.START_POWER_MAP;
                }
                return;
            case START_POWER_MAP:
                configurePowerMapTest();
                m_state = State.POWER_MAP;
                return;
            case POWER_MAP:
                if (runPowerMapTest()) {
                    m_state = State.WRAPUP;
                }
                return;
            case WRAPUP:
                wrapup();
                m_state = State.STOPPED;
                return;
            default:
                m_state = State.STOPPED;
        }
    }

    private void init() {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        File file = new File(Filesystem.getDeployDirectory(), "../FlywheelChar_" + m_name + "_" + dateFormat.format(date) + ".log");

        try {
            m_log = new FileWriter(file);
        } catch (IOException e) {
            m_log = null;
            e.printStackTrace();
            DriverStation.reportError(e.toString(), true);
        }
    }

    private void configureDeadbandTest() {
        m_deadbandCurrentPower = 0.0;

        m_deadbandTimer.reset();
        m_deadbandTimer.start();
    }

    private boolean runDeadbandTest() {
        if (!m_deadbandTimer.isRunning()) {
            m_deadbandTimer.reset();
            m_deadbandTimer.start();
            m_deadbandCurrentPower = k_deadbandPowerIncrement;
            m_controller.set(ControlMode.PercentOutput, m_deadbandCurrentPower);
        }

        if (m_deadbandCurrentPower <= k_deadbandMaxReasonablePower) {
            if (m_deadbandTimer.hasElapsed(k_deadbandIncrementDelay)) {
                double velocity = m_controller.getSelectedSensorVelocity();
                if (velocity > 100.0) {
                    printInfo("Deadband - Found deadband at: " + m_deadbandCurrentPower + " Velocity: " + velocity);
                    m_deadbandPower = m_deadbandCurrentPower;
                    m_deadbandCurrentPower = 0.0;
                    m_deadbandTimer.stop();
                    m_controller.neutralOutput();
                    return true;
                } else if (velocity < 0.0) {
                    DriverStation.reportError("Sensor reported negative numbers. Configure motor controller before using Flywheel Characterizer.", false);
                    m_state = State.WRAPUP;
                }

                printInfo("Deadband - Tested " + m_deadbandCurrentPower + " - Velocity: " + velocity);
                m_deadbandCurrentPower += k_deadbandPowerIncrement;
                m_controller.set(ControlMode.PercentOutput, m_deadbandCurrentPower);
                m_deadbandTimer.reset();
            }
        } else {
            // We couldn't find the deadband. Sensor could be setup incorrectly.
            printInfo("Deadband - Hit max reasonable power (" + m_deadbandCurrentPower + ")! Ending test.");
            m_deadbandPower = Double.NaN;
            m_deadbandCurrentPower = 0.0;
            m_deadbandTimer.stop();
            m_controller.neutralOutput();
            return true;
        }

        return false;
    }

    private void configurePowerMapTest() {
        m_powerMapCurrentPower = 0.0;
        m_powerMapTimer.reset();
        m_powerMapTimer.start();
    }

    private boolean runPowerMapTest() {
        if (!m_powerMapTimer.isRunning()) {
            m_powerMapTimer.reset();
            m_powerMapTimer.start();
            m_powerMapCurrentPower = k_powerMapPowerIncrement;
            m_controller.set(ControlMode.PercentOutput, m_powerMapCurrentPower);
        }

        if (m_powerMapTimer.hasElapsed(k_powerMapIncrementDelay)) {
            if (m_powerMapCurrentPower < 1.0) {
                double velocity = m_controller.getSelectedSensorVelocity();

                if (velocity < 0.0) {
                    DriverStation.reportError("Sensor reported negative numbers. Configure motor controller before using Flywheel Characterizer.", false);
                    m_state = State.WRAPUP;
                }

                printInfo("Power Map - Measuring at: " + m_powerMapCurrentPower + " - Velocity: " + velocity);
                m_powerMapTable.add(new Measurement(m_powerMapCurrentPower, velocity));
                m_powerMapCurrentPower += k_powerMapPowerIncrement;
                if (m_powerMapCurrentPower > 1.0) {
                    m_powerMapCurrentPower = 1.0;
                }
                m_controller.set(ControlMode.PercentOutput, m_powerMapCurrentPower);
                m_powerMapTimer.reset();
            } else {
                double velocity = m_controller.getSelectedSensorVelocity();
                printInfo("Power Map - Measuring at: " + m_powerMapCurrentPower + " - Velocity: " + velocity);
                m_powerMapTable.add(new Measurement(m_powerMapCurrentPower, velocity));
                printInfo("Power Map - Finished");
                m_powerMapCurrentPower = 0.0;
                m_powerMapTimer.stop();
                m_powerMapTimer.reset();
                m_controller.neutralOutput();
                calculatePowerMapResults();
                return true;
            }
        }

        return false;
    }

    /**
     * Linear Regression Equation
     * Y = Bx + A
     * A = (Sum(Y) * Sum(X^2) - Sum(X) * Sum(X * Y)) /
     *     (n * Sum(X^2) - Sum(X)^2)
     *
     * B = (Sum(X * Y) - Sum(X) * Sum(Y)) /
     *     (n * Sum(X^2) - Sum(X)^2)
     */
    private void calculatePowerMapResults() {
        double sumX = 0.0;
        double sumY = 0.0;
        double sumXY = 0.0;
        double sumXSquared = 0.0;
        int total = 0;

        // Calculate Linear Regression Values
        for (Measurement powerMeas : m_powerMapTable) {
            if (powerMeas.measuredVelocity > 1.0e-9) {
                sumX += powerMeas.inputPower;
                sumY += powerMeas.measuredVelocity;
                sumXY += powerMeas.inputPower * powerMeas.measuredVelocity;
                sumXSquared += powerMeas.inputPower * powerMeas.inputPower;
                total++;
            }
        }

        double denom = total * sumXSquared - sumX * sumX;

        // Y = Bx + A
        double a = ((sumY * sumXSquared) - (sumX * sumXY)) / denom;
        double b = ((total * sumXY) - (sumX * sumY)) / denom;

        // Calculate TalonSRX specific values
        // Finds where the fitted line intercepts the X-Axis
        m_arbFF = (-1.0 * a) / b;
        // Find the fitted line's max velocity at a power of 1.0
        m_maxVelocity = a + b;
        // First calcs the new max power after offset, multiplies by the DAC's max power
        // because that's how the Talon's PID works for some reason, and then divides by
        // max velocity. The result is the adjusted F-Gain to better match the system's
        // behaviour.
        m_fGain = ((1.0 - m_arbFF) * k_maxTalonDacOutput) / m_maxVelocity;

        printInfo("Calc'd sumX - " + sumX);
        printInfo("Calc'd sumY - " + sumY);
        printInfo("Calc'd sumXY - " + sumXY);
        printInfo("Calc'd sumXSquared - " + sumXSquared);
        printInfo("Calc'd total - " + total);
        printInfo("Calc'd a - " + a);
        printInfo("Calc'd b - " + b);
        printInfo("Calc'd Deadband - " + m_arbFF);
        printInfo("Calc'd F-Gain - " + m_fGain);
        printInfo("Calc'd Max Velocity - " + m_maxVelocity);
    }

    private void wrapup() {
        m_controller.neutralOutput();
        printInfo("Results");
        printInfo("Deadband = " + m_deadbandPower);
        for (Measurement measurement : m_powerMapTable) {
            printInfo("Power Map = " + measurement.inputPower + " - " + measurement.measuredVelocity);
        }

        if (m_log != null) {
            try {
                m_log.close();
            } catch (IOException e) {
                e.printStackTrace();
                DriverStation.reportError(e.toString(), true);
            }
        }
        m_log = null;
    }

    private void printInfo(String line) {
        String local_line = m_name + ": " + line;
        System.out.println(local_line);
        if (m_log != null) {
            try {
                m_log.write(local_line + "\n");
            } catch (IOException e) {
                e.printStackTrace();
                DriverStation.reportError(e.toString(), true);
            }
        }
    }
}
