/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import edu.wpi.first.wpilibj.I2C.Port;
import edu.wpi.first.wpilibj.util.Color;

import com.revrobotics.ColorSensorV3;

public class RevColorSensor {
    private final Port m_i2cPort;
    private ColorSensorV3 m_colorSensor;

    private final ColorSensorV3.ProximitySensorResolution k_proxRes = ColorSensorV3.ProximitySensorResolution.kProxRes11bit;
    private final ColorSensorV3.ProximitySensorMeasurementRate k_proxRate = ColorSensorV3.ProximitySensorMeasurementRate.kProxRate12ms;
    private final ColorSensorV3.ColorSensorResolution k_colorRes = ColorSensorV3.ColorSensorResolution.kColorSensorRes16bit;
    private final ColorSensorV3.ColorSensorMeasurementRate k_colorRate = ColorSensorV3.ColorSensorMeasurementRate.kColorRate25ms;
    private final ColorSensorV3.GainFactor k_gainF = ColorSensorV3.GainFactor.kGain18x;
    private final ColorSensorV3.LEDPulseFrequency k_ledFreq = ColorSensorV3.LEDPulseFrequency.kFreq60kHz;
    private final ColorSensorV3.LEDCurrent k_ledCurrent = ColorSensorV3.LEDCurrent.kPulse100mA;
    private final int k_ledPulses = 64;

    public static class Hsv {
        public final double hue;
        public final double saturation;
        public final double value;

        public Hsv(double hues, double saturations, double values) {
            hue = hues;
            saturation = saturations;
            value = values;
        }
    }

    private static final int k_maxRawColor = (int) Math.pow(2, 16) - 1;
    private static final int k_maxRawProximity = (int) Math.pow(2, 11) - 1;

    public RevColorSensor(Port i2cPort) {
        m_i2cPort = i2cPort;
        init();
    }

    private void init() {
        // REV's ColorSensorV3 doesn't have a public init function, so we need to construct every time we want to re-initialize device.
        m_colorSensor = new ColorSensorV3(m_i2cPort);
        m_colorSensor.configureColorSensor(k_colorRes, k_colorRate, k_gainF);
        m_colorSensor.configureProximitySensor(k_proxRes, k_proxRate);
        m_colorSensor.configureProximitySensorLED(k_ledFreq, k_ledCurrent, k_ledPulses);
    }

    public Hsv getHSV() {
        double red = getRed();
        double green = getGreen();
        double blue = getBlue();
        double colorMax = Math.max(red, Math.max(green, blue));
        double colorMin = Math.min(red, Math.min(green, blue));
        double hue = 0;
        double saturation = 0;
        double value = 0;
        double delta = colorMax - colorMin;

        if (red > blue && red > green) {
            hue = (60.0 * ((green - blue) / delta) + 360.0) % 360.0;
        } else if (green > blue && green > red) {
            hue = (60.0 * ((blue - red) / delta) + 120.0) % 360.0;
        } else {
            hue = (60 * ((red - green) / delta) + 240.0) % 360.0;
        }

        saturation = delta / colorMax * 100.0;
        value = colorMax * 100.0;
        return new Hsv(hue, saturation, value);
    }

    public Color getColor() {
        return new Color(getRed(), getGreen(), getBlue());
    }

    public double getRed() {
        return (double)getRawRed() / (double)k_maxRawColor;
    }

    public double getGreen() {
        return (double)getRawGreen() / (double)k_maxRawColor;
    }

    public double getBlue() {
        return (double)getRawBlue() / (double)k_maxRawColor;
    }

    public double getIR() {
        return (double)getRawIR() * (double)k_maxRawColor;
    }

    public double getProximity() {
        return (double)getRawProximity() * (double)k_maxRawProximity;
    }

    public int getRawRed() {
        return m_colorSensor.getRed();
    }

    public int getRawGreen() {
        return m_colorSensor.getGreen();
    }

    public int getRawBlue() {
        return m_colorSensor.getBlue();
    }

    public int getRawProximity() {
        return m_colorSensor.getProximity();
    }

    public int getRawIR() {
        return m_colorSensor.getIR();
    }

    public void update() {
        if (m_colorSensor.hasReset()) {
            init();
        }
    }
}
