/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.drivers;

import com.ctre.phoenix.ErrorCode;
import com.ctre.phoenix.sensors.CANCoder;
import com.ctre.phoenix.sensors.CANCoderStatusFrame;

import edu.wpi.first.wpilibj.DriverStation;

public class CANCoderUtils {
    /**
     * Timeout that is used for hardware calls that are configuring the device.
     * These calls can take longer and should be given a little more time to occur.
     */
    public final static int k_cfgTimeout_ms = 200;

    /**
     * The max period that the hardware will accept when setting status rates. Any
     * value larger than this will just wrap.
     */
    public final static int k_maxPeriod_ms = 255;

    private final static int k_defaultSensorDataPeriod_ms = 10;

    /**
     * Creates a CANCoder Object with FRC Team 836 The RoboBees's preferred default
     * settings.
     *
     * @param canId CAN Device ID that represents the Device.
     * @return A newly created CANCoder object with default settings.
     */
    public static CANCoder createDefaultCANCoder(int canId) {
        return createDefaultCANCoder(canId, "");
    }

    /**
     * Creates a CANCoder Object with FRC Team 836 The RoboBees's preferred default
     * settings.
     *
     * @param canId  CAN Device ID that represents the Device.
     * @param canbus CAN Bus to be used.
     * @return A newly created CANCoder object with default settings.
     */
    public static CANCoder createDefaultCANCoder(int canId, String canbus) {
        CANCoder canCoder = new CANCoder(canId, canbus);
        ErrorCode error = ErrorCode.OK;

        error = canCoder.configFactoryDefault(k_cfgTimeout_ms);
        checkError(error, "configFactoryDefault");

        // Position
        // Velocity
        // Absolute Position - Default Period 10ms
        error = canCoder.setStatusFramePeriod(CANCoderStatusFrame.SensorData, k_defaultSensorDataPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "getStatusFramePeriod_SensorData");

        // This is undocumented in Phoenix Documents
        error = canCoder.setStatusFramePeriod(CANCoderStatusFrame.VbatAndFaults, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "getStatusFramePeriod_VbatAndFaults");

        return canCoder;
    }

    /**
     * Method for checking CANCoder related errors.
     *
     * @param error        Error code returned from CANCoder HAL call.
     * @param functionName String representing the function that returned the error.
     */
    public static void checkError(ErrorCode error, String functionName) {
        checkError(error, functionName, true);
    }

    /**
     * Method for checking CANCoder related errors.
     *
     * @param error         Error code returned from CANCoder HAL call.
     * @param functionName  String representing the function that returned the error.
     * @param printTrace    If true, append stack trace to error string.
     */
    public static void checkError(ErrorCode error, String functionName, boolean printTrace) {
        if (error != ErrorCode.OK) {
            DriverStation.reportError("CANCoder error code (" + error + ") found while running: " + functionName,
                    printTrace);
        }
    }
}
