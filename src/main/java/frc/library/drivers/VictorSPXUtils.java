/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.drivers;

import com.ctre.phoenix.ErrorCode;
import com.ctre.phoenix.motorcontrol.StatusFrame;
import com.ctre.phoenix.motorcontrol.VictorSPXControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.DriverStation;

/**
 * VictorSPXUtils is a class that contains static methods that return variously
 * configured VictorSPX objects.
 */
public class VictorSPXUtils {
    /**
     * Timeout that is used for regular set hardware calls. These calls are
     * generally quick and don't need a long timeout.
     */
    public final static int k_setTimeout_ms = 20;
    /**
     * Timeout that is used for hardware calls that are configuring the device.
     * These calls can take longer and should be given a little more time to occur.
     */
    public final static int k_cfgTimeout_ms = 200;
    /**
     * The max period that the hardware will accept when setting status rates. Any
     * value larger than this will just wrap.
     */
    public final static int k_maxPeriod_ms = 255;
    /** Default value used for Neutral Deadband setting in milliseconds. */
    public final static double k_defaultNeutralDeadband = 0.04;
    /** Default value used for Voltage Compensation Enable setting. */
    public final static boolean k_defaultVoltCompEnable = true;
    /** Default value used for Voltage Compensation setting in volts. */
    public final static double k_defaultVoltComp_V = 12.0;
    /** Default value used for General Status Period setting for a Master device in milliseconds. */
    public final static int k_defaultMasterGeneralPeriod_ms = 10;
    /** Default value used for Feedback 0 Period setting for a Master device in milliseconds. */
    public final static int k_defaultMasterFeedback0Period_ms = 20;

    /**
     * Creates a VictorSPX Object with FRC Team 836 The RoboBees's preferred default
     * settings.
     *
     * @param canId      CAN Device ID that represents the Device.
     * @param isFollower True if this VictorSPX is a follower VictorSPX.
     * @return A newly created VictorSPX object with default settings.
     */
    public static VictorSPX createDefaultVictorSPX(int canId, boolean isFollower) {
        VictorSPX victor = new VictorSPX(canId);
        ErrorCode error = ErrorCode.OK;

        // Start by reseting the device to Factory Default
        error = victor.configFactoryDefault(k_cfgTimeout_ms);
        checkError(error, "configFactoryDefault");

        // Disable Motor
        victor.set(VictorSPXControlMode.Disabled, 0.0);

        // Set Neutal Deadband
        error = victor.configNeutralDeadband(k_defaultNeutralDeadband, k_cfgTimeout_ms);
        checkError(error, "configNeutralDeadband");

        // Configure Voltage Compenstation
        victor.enableVoltageCompensation(k_defaultVoltCompEnable);
        error = victor.configVoltageCompSaturation(k_defaultVoltComp_V, k_cfgTimeout_ms);
        checkError(error, "configVoltageCompSaturation");

        // Set all the status frames to default, or max if defaulted to unused.
        if (isFollower) {
            error = victor.setStatusFramePeriod(StatusFrame.Status_1_General, k_maxPeriod_ms, k_cfgTimeout_ms);
            checkError(error, "setStatusFramePeriod");
            error = victor.setStatusFramePeriod(StatusFrame.Status_2_Feedback0, k_maxPeriod_ms, k_cfgTimeout_ms);
            checkError(error, "setStatusFramePeriod");
        } else {
            error = victor.setStatusFramePeriod(StatusFrame.Status_1_General, k_defaultMasterGeneralPeriod_ms,
                    k_cfgTimeout_ms);
            checkError(error, "setStatusFramePeriod");
            error = victor.setStatusFramePeriod(StatusFrame.Status_2_Feedback0, k_defaultMasterFeedback0Period_ms,
                    k_cfgTimeout_ms);
            checkError(error, "setStatusFramePeriod");
        }
        error = victor.setStatusFramePeriod(StatusFrame.Status_4_AinTempVbat, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        // Status Frame 5 is missing
        error = victor.setStatusFramePeriod(StatusFrame.Status_6_Misc, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = victor.setStatusFramePeriod(StatusFrame.Status_7_CommStatus, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = victor.setStatusFramePeriod(StatusFrame.Status_9_MotProfBuffer, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = victor.setStatusFramePeriod(StatusFrame.Status_10_Targets, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = victor.setStatusFramePeriod(StatusFrame.Status_12_Feedback1, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = victor.setStatusFramePeriod(StatusFrame.Status_13_Base_PIDF0, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = victor.setStatusFramePeriod(StatusFrame.Status_14_Turn_PIDF1, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = victor.setStatusFramePeriod(StatusFrame.Status_15_FirmwareApiStatus, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");

        return victor;
    }

    /**
     * Method for checking VictorSPX related errors.
     *
     * @param error        Error code returned from TalonSRX HAL call.
     * @param functionName String representing the function that returned the error.
     */
    public static void checkError(ErrorCode error, String functionName) {
        checkError(error, functionName, true);
    }

    /**
     * Method for checking VictorSPX related errors.
     *
     * @param error        Error code returned from TalonSRX HAL call.
     * @param functionName String representing the function that returned the error.
     * @param printTrace   If true, append stack trace to error string.
     */
    public static void checkError(ErrorCode error, String functionName, boolean printTrace) {
        if (error != ErrorCode.OK) {
            DriverStation.reportError("VictorSPX error code (" + error + ") found while running: " + functionName,
                    printTrace);
        }
    }
}
