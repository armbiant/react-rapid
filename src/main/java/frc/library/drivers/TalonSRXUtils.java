/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.drivers;

import com.ctre.phoenix.ErrorCode;
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;
import com.ctre.phoenix.motorcontrol.TalonSRXControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

import edu.wpi.first.wpilibj.DriverStation;

/**
 * TalonSRXUtils is a class that contains static methods that return variously
 * configured TalonSRX objects.
 */
public class TalonSRXUtils {
    /**
     * Timeout that is used for regular set hardware calls. These calls are
     * generally quick and don't need a long timeout.
     */
    public final static int k_setTimeout_ms = 20;
    /**
     * Timeout that is used for hardware calls that are configuring the device.
     * These calls can take longer and should be given a little more time to occur.
     */
    public final static int k_cfgTimeout_ms = 200;

    /**
     * The max period that the hardware will accept when setting status rates. Any
     * value larger than this will just wrap.
     */
    public final static int k_maxPeriod_ms = 255;

    /** Default value used for Current Limit Enable setting. */
    public final static boolean k_defaultCurrentLimitEnable = true;
    /** Default value used for Continuous Current Limit setting in amps. */
    public final static int k_defaultContCurrLimit_A = 10;
    /** Default value used for Peak Current Limit setting in amps. */
    public final static int k_defaultPeakCurrLimit_A = 40;
    /** Default value used for Peak Current Duration setting in milliseconds. */
    public final static int k_defaultPeakCurrDuration_ms = 100;

    /** Default value used for Neutral Deadband setting. */
    public final static double k_defaultNeutralDeadband = 0.04;
    /** Default value used for Voltage Compensation Enable setting. */
    public final static boolean k_defaultVoltCompEnable = true;
    /** Default value used for Voltage Compensation setting in volts. */
    public final static double k_defaultVoltComp_V = 12.0;
    /** Default value used for General Status Period setting for a Master device in milliseconds. */
    public final static int k_defaultMasterGeneralPeriod_ms = 10;
    /** Default value used for Feedback 0 Period setting for a Master device in milliseconds. */
    public final static int k_defaultMasterFeedback0Period_ms = 20;

    /**
     * Creates a TalonSRX Object with FRC Team 836 The RoboBees's preferred default
     * settings.
     *
     * @param canId      CAN Device ID that represents the Device.
     * @param isFollower True if this TalonSRX is a follower TalonSRX.
     * @return A newly created TalonSRX object with default settings.
     */
    public static TalonSRX createDefaultTalonSRX(int canId, boolean isFollower) {
        TalonSRX talon = new TalonSRX(canId);
        ErrorCode error = ErrorCode.OK;

        // Start by reseting the device to Factory Default
        error = talon.configFactoryDefault(k_cfgTimeout_ms);
        checkError(error, "configFactoryDefault");

        // Disable Motor
        talon.set(TalonSRXControlMode.Disabled, 0.0);

        // Set Neutal Deadband
        error = talon.configNeutralDeadband(k_defaultNeutralDeadband, k_cfgTimeout_ms);
        checkError(error, "configNeutralDeadband");

        // Configure Default Current Limit Settings
        talon.enableCurrentLimit(k_defaultCurrentLimitEnable);
        error = talon.configContinuousCurrentLimit(k_defaultContCurrLimit_A, k_cfgTimeout_ms);
        checkError(error, "enableCurrentLimit");
        error = talon.configPeakCurrentLimit(k_defaultPeakCurrLimit_A, k_cfgTimeout_ms);
        checkError(error, "configPeakCurrentLimit");
        error = talon.configPeakCurrentDuration(k_defaultPeakCurrDuration_ms, k_cfgTimeout_ms);
        checkError(error, "configPeakCurrentDuration");

        // Configure Voltage Compenstation
        talon.enableVoltageCompensation(k_defaultVoltCompEnable);
        error = talon.configVoltageCompSaturation(k_defaultVoltComp_V, k_cfgTimeout_ms);
        checkError(error, "configVoltageCompSaturation");

        // Set all the status frames to default, or max if defaulted to unused.
        if (isFollower) {
            error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_1_General, k_maxPeriod_ms, k_cfgTimeout_ms);
            checkError(error, "setStatusFramePeriod");
            error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0, k_maxPeriod_ms, k_cfgTimeout_ms);
            checkError(error, "setStatusFramePeriod");
        } else {
            error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_1_General, k_defaultMasterGeneralPeriod_ms,
                    k_cfgTimeout_ms);
            checkError(error, "setStatusFramePeriod");
            error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0,
                    k_defaultMasterFeedback0Period_ms, k_cfgTimeout_ms);
            checkError(error, "setStatusFramePeriod");
        }
        error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_3_Quadrature, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_4_AinTempVbat, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        // Status Frame 5 is missing
        error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_6_Misc, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_7_CommStatus, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_8_PulseWidth, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_9_MotProfBuffer, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_10_Targets, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_11_UartGadgeteer, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_12_Feedback1, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_13_Base_PIDF0, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_14_Turn_PIDF1, k_maxPeriod_ms, k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");
        error = talon.setStatusFramePeriod(StatusFrameEnhanced.Status_15_FirmwareApiStatus, k_maxPeriod_ms,
                k_cfgTimeout_ms);
        checkError(error, "setStatusFramePeriod");

        return talon;
    }

    /**
     * Method for checking TalonSRX related errors.
     *
     * @param error        Error code returned from TalonSRX HAL call.
     * @param functionName String representing the function that returned the error.
     */
    public static void checkError(ErrorCode error, String functionName) {
        checkError(error, functionName, true);
    }

    /**
     * Method for checking TalonSRX related errors.
     *
     * @param error         Error code returned from TalonSRX HAL call.
     * @param functionName  String representing the function that returned the error.
     * @param printTrace    If true, append stack trace to error string.
     */
    public static void checkError(ErrorCode error, String functionName, boolean printTrace) {
        if (error != ErrorCode.OK) {
            DriverStation.reportError("TalonSRX error code (" + error + ") found while running: " + functionName,
                    printTrace);
        }
    }
}
