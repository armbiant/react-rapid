/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.drivers;

import com.ctre.phoenix.ErrorCode;
import com.ctre.phoenix.sensors.BasePigeon;
import com.ctre.phoenix.sensors.PigeonIMU_StatusFrame;

import edu.wpi.first.wpilibj.DriverStation;

public class PigeonUtils {
    /**
     * Timeout that is used for hardware calls that are configuring the device.
     * These calls can take longer and should be given a little more time to occur.
     */
    public final static int k_cfgTimeout_ms = 200;

    /**
     * The max period that the hardware will accept when setting status rates. Any
     * value larger than this will just wrap.
     */
    public final static int k_maxPeriod_ms = 255;

    private final static int k_defaultSixDegYprPeriod_ms = 10;

    /**
     * Takes in either a Pigeon or Pigeon2 as a BasePigeon Object, and sets it up
     * with FRC Team 836 The RoboBees's preferred default settings.
     *
     * @param basePigeon Either a Pigeon or a Pigeon2.
     */
    public static void configAsDefaultPigeon(BasePigeon basePigeon) {
        ErrorCode error = ErrorCode.OK;

        error = basePigeon.configFactoryDefault(k_maxPeriod_ms);
        checkError(error, "configFactoryDefault");

        // Calibration Status
        // IMU Temperature - Default Period >100ms
        error = basePigeon.setStatusFramePeriod(PigeonIMU_StatusFrame.CondStatus_1_General, k_maxPeriod_ms);
        checkError(error, "setStatusFramePeriod_CondStatus_1_General");

        // Six degree fused Yaw, Pitch, Roll - Default Period 10ms
        error = basePigeon.setStatusFramePeriod(PigeonIMU_StatusFrame.CondStatus_9_SixDeg_YPR, k_defaultSixDegYprPeriod_ms);
        checkError(error, "setStatusFramePeriod_CondStatus_9_SixDeg_YPR");

        // Nine degree fused Yaw, Pitch, Roll (requires magnetometer calibration). - Default Period 10ms
        error = basePigeon.setStatusFramePeriod(PigeonIMU_StatusFrame.CondStatus_6_SensorFusion, k_maxPeriod_ms);
        checkError(error, "setStatusFramePeriod_CondStatus_6_SensorFusion");

        // Accumulated Gyro Angles - Default Period 20ms
        error = basePigeon.setStatusFramePeriod(PigeonIMU_StatusFrame.CondStatus_11_GyroAccum, k_maxPeriod_ms);
        checkError(error, "setStatusFramePeriod_CondStatus_11_GyroAccum");

        // Accelerometer derived angles - Default Period >100m
        error = basePigeon.setStatusFramePeriod(PigeonIMU_StatusFrame.CondStatus_3_GeneralAccel, k_maxPeriod_ms);
        checkError(error, "setStatusFramePeriod_CondStatus_3_GeneralAccel");

        // Six degree fused Quaternion - Default Period >100ms
        error = basePigeon.setStatusFramePeriod(PigeonIMU_StatusFrame.CondStatus_10_SixDeg_Quat, k_maxPeriod_ms);
        checkError(error, "setStatusFramePeriod_CondStatus_10_SixDeg_Quat");

        // Unprocessed magnetometer values (x,y,z) - Default Period 20ms
        error = basePigeon.setStatusFramePeriod(PigeonIMU_StatusFrame.BiasedStatus_4_Mag, k_maxPeriod_ms);
        checkError(error, "setStatusFramePeriod_BiasedStatus_4_Mag");

        // Biased gyro values (x,y,z) - Default Period >100ms
        error = basePigeon.setStatusFramePeriod(PigeonIMU_StatusFrame.BiasedStatus_2_Gyro, k_maxPeriod_ms);
        checkError(error, "setStatusFramePeriod_BiasedStatus_2_Gyro");

        // Biased accelerometer values (x,y,z) - Default Period >100ms
        error = basePigeon.setStatusFramePeriod(PigeonIMU_StatusFrame.BiasedStatus_6_Accel, k_maxPeriod_ms);
        checkError(error, "setStatusFramePeriod_BiasedStatus_6_Accel");

        // This is undocumented in Phoenix Documents
        error = basePigeon.setStatusFramePeriod(PigeonIMU_StatusFrame.CondStatus_2_GeneralCompass, k_maxPeriod_ms);
        checkError(error, "setStatusFramePeriod_CondStatus_2_GeneralCompass");

        // This is undocumented in Phoenix Documents
        error = basePigeon.setStatusFramePeriod(PigeonIMU_StatusFrame.RawStatus_4_Mag, k_maxPeriod_ms);
        checkError(error, "setStatusFramePeriod_RawStatus_4_Mag");
    }

    /**
     * Method for checking Pigeon related errors.
     *
     * @param error        Error code returned from Pigeon HAL call.
     * @param functionName String representing the function that returned the error.
     */
    public static void checkError(ErrorCode error, String functionName) {
        checkError(error, functionName, true);
    }

    /**
     * Method for checking Pigeon related errors.
     *
     * @param error        Error code returned from Pigeon HAL call.
     * @param functionName String representing the function that returned the error.
     * @param printTrace   If true, append stack trace to error string.
     */
    public static void checkError(ErrorCode error, String functionName, boolean printTrace) {
        if (error != ErrorCode.OK) {
            DriverStation.reportError("Pigeon error code (" + error + ") found while running: " + functionName,
                    printTrace);
        }
    }
}
