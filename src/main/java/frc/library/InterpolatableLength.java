/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import frc.library.Length.LengthUnit;

import org.growingstems.util.interpolation.InterpolatableValue;
import org.growingstems.util.interpolation.InverseInterpolatable;

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * InterpolatableLength is a container class for a length value that allows for
 * it to be both linearly interpolated, linearly extrapolated and inverse
 * linearly interpolated.
 */
public class InterpolatableLength
    extends InterpolatableValue<InterpolatableLength, Length> implements InverseInterpolatable<InterpolatableLength> {
    public InterpolatableLength(Length value) {
        super(value);
    }

    @Override
    public double linearInverseInterpolate(InterpolatableLength upper, InterpolatableLength other) {
        double range = upper.m_value.subtract(this.m_value).getValue(LengthUnit.INCHES);
        double otherShifted = other.m_value.subtract(this.m_value).getValue(LengthUnit.INCHES);

        return otherShifted / range;
    }

    @Override
    public InterpolatableLength linearInterpolate(InterpolatableLength upper, double x) {
        Length range = upper.m_value.subtract(this.m_value);

        return new InterpolatableLength((range.scale(x)).add(this.m_value));
    }

    @Override
    public InterpolatableLength linearExtrapolate(InterpolatableLength upper, double x) {
        /**
         * Extrapolate uses the interpolate method because the interpolate
         * implementation can handle extrapolation for this class.
         */
        return linearInterpolate(upper, x);
    }

    @Override
    public int compareTo(InterpolatableLength other) {
        if (this.m_value.getValue(LengthUnit.INCHES) > other.m_value.getValue(LengthUnit.INCHES)) {
            return 1;
        } else if (this.m_value.getValue(LengthUnit.INCHES) < other.m_value.getValue(LengthUnit.INCHES)) {
            return -1;
        } else {
            return 0;
        }
    }
}
