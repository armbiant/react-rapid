/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.math.Time;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.signals.api.SignalModifier;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class RateLimiter implements SignalModifier<Double, Double> {
    private static final double k_defaultDriveGain = 1.6;
    private final NetworkTableEntry m_driveGainEntry = SmartDashboard.getEntry("driveGain");
    private WpiTimer timer = new WpiTimer();
    private Time timeSinceLast = null;
    private double currentValue;

    public RateLimiter() {
        m_driveGainEntry.setNumber(k_defaultDriveGain);
        timer.start();
    }

    @Override
    public Double update(Double inputStick) {
        if (timeSinceLast == null) {
            currentValue = inputStick;
            timeSinceLast = timer.get();
            return currentValue;
        }
        var gain = m_driveGainEntry.getDouble(k_defaultDriveGain);
        var currentTime = timer.get();
        var delta = currentTime.subtract(timeSinceLast);
        timeSinceLast = currentTime;
        var maxChange = gain * delta.getValue(TimeUnit.SECONDS);
        if (currentValue < inputStick) {
            currentValue += maxChange;
            currentValue = Math.min(currentValue, inputStick);
        } else {
            currentValue -= maxChange;
            currentValue = Math.max(currentValue, inputStick);
        }

        return currentValue;
    }
}
