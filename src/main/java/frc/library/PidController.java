/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.math.Range;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.signals.api.SignalModifier;

// TODO: Javadocs
public class PidController implements SignalModifier<Double, Double> {
    private final Range m_iRange;

    private WpiTimer m_timer;

    private double m_pGain;
    private double m_dGain;
    private double m_iGain;

    private double m_error = 0.0;
    private double m_lastError = 0.0;
    private double m_time = 0.0;
    private double m_lastTime = 0.0;

    private double m_pVal = 0.0;
    private double m_dVal = 0.0;
    private double m_iVal = 0.0;

    private boolean m_firstFrame;

    private double m_setPoint = 0.0;

    public PidController(double pGain, double iGain, double dGain) {
        this(pGain, iGain, dGain, null);
    }

    public PidController(double pGain, double iGain, double dGain, Range iRange) {
        m_iRange = iRange;
        m_timer = new WpiTimer();
        resetPID();
        m_pGain = pGain;
        m_dGain = dGain;
        m_iGain = iGain;
        m_timer.start();
    }

    public void setSetPoint(double setPoint) {
        m_setPoint = setPoint;
    }

    @Override
    public Double update(Double in) {
        m_lastError = m_error;
        m_lastTime = m_time;
        m_time = m_timer.get().getValue(TimeUnit.SECONDS);
        m_error = m_setPoint - in;
        if (m_firstFrame) {
            m_lastError = m_error;
        } else if (m_iRange == null || (m_iRange.getLow() < m_error && m_iRange.getHigh() > m_error)) {
            m_iVal += m_iGain * (m_lastError + m_error) * (0.5) * (m_time - m_lastError);
        } else {
            m_iVal = 0.0;
        }
        m_pVal = m_pGain * m_error;
        m_dVal = m_dGain * (m_error - m_lastError) / (m_time - m_lastTime);
        m_firstFrame = false;
        return getPID();
    }

    public void resetPID() {
        resetPID(0.0);
    }

    public void resetPID(double startingIValue) {
        m_timer.reset();
        m_iVal = startingIValue;
        m_firstFrame = true;
    }

    private Double getPID() {
        return m_pVal + m_dVal + m_iVal;
    }
}
