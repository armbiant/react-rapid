/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

public enum LogEntryPriority {
    HIGH(0),
    MEDIUM(1),
    LOW(2),
    LOWEST(3);

    public final int value;

    LogEntryPriority(int value) {
        this.value = value;
    }
}

