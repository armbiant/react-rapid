/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import edu.wpi.first.wpilibj2.command.Subsystem;

import org.growingstems.util.statemachine.State;
import org.growingstems.util.statemachine.StateMachine;

public class ScheduledStateMachine extends StateMachine implements Subsystem {
    public ScheduledStateMachine(State initialState) {
        super(initialState);
        register();
    }

    @Override
    public void periodic() {
        step();
    }
}
