/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import org.growingstems.math.Unit;

/**
 * A mathematical representation of a Length which handles unit conversion.
 *
 * This representation of rate is defined in units / time, and is not
 * particularly
 * concerned with the non-time aspect of the rate.
 *
 * Construction of a non-zero Length requires a unit be specified. Retrieval
 * of the value also requires a unit to be specified.
 */
public class Length implements Unit<Length> {
    /**
     * Pre-defined units supported by the Length class
     */
    public static enum LengthUnit {
        METERS(1.0),
        CENTIMETERS(1.0e-2),
        MILLIMETERS(1.0e-3),
        MICROMETERS(1.0e-6),
        NANOMETERS(1.0e-9),
        KILOMETERS(1.0e3),
        INCHES(0.0254),
        FEET(0.3048),
        YARDS(0.9144);

        private double m_multiplier;

        private LengthUnit(double multiplier) {
            m_multiplier = multiplier;
        }

        private double getMultiplier() {
            return m_multiplier;
        }
    }

    public static Length meters(double value) {
        return new Length(value, LengthUnit.METERS);
    }

    public static Length centimeters(double value) {
        return new Length(value, LengthUnit.CENTIMETERS);
    }

    public static Length millimeters(double value) {
        return new Length(value, LengthUnit.MILLIMETERS);
    }

    public static Length micrometers(double value) {
        return new Length(value, LengthUnit.MICROMETERS);
    }

    public static Length nanometers(double value) {
        return new Length(value, LengthUnit.NANOMETERS);
    }

    public static Length kilometers(double value) {
        return new Length(value, LengthUnit.KILOMETERS);
    }

    public static Length inches(double value) {
        return new Length(value, LengthUnit.INCHES);
    }

    public static Length feet(double value) {
        return new Length(value, LengthUnit.FEET);
    }

    public static Length yards(double value) {
        return new Length(value, LengthUnit.YARDS);
    }

    private final double m_length_m;

    /**
     * Construct a Length of zero meters
     */
    public Length() {
        this(0.0);
    }

    /**
     * Private constructor that assumes the passed in value is in the
     * default units for efficiency
     *
     * @param value the value of the rate in meters
     */
    private Length(double value) {
        m_length_m = value;
    }

    /**
     * Construct a Length with a non-zero magnitude
     *
     * @param value the value of the rate
     * @param unit  the unit of the value provided
     */
    public Length(double value, LengthUnit unit) {
        m_length_m = value * unit.getMultiplier();
    }

    /**
     * Generate a String representation of the Length in inches.
     *
     * @return A String representation of the Length
     */
    @Override
    public String toString() {
        return toString(LengthUnit.INCHES);
    }

    /**
     * Standard toString method with configurable unit
     *
     * @param unit The unit to use for display
     * @return A String representation of the Length
     */
    public String toString(LengthUnit unit) {
        return getValue(unit) + " " + unit.toString();
    }

    /**
     * Get the value of the Length
     *
     * @param unit the unit of the value to provide
     * @return the value of the rate in the provided unit
     */
    public double getValue(LengthUnit unit) {
        return m_length_m / unit.getMultiplier();
    }

    @Override
    public Length clone() {
        return new Length(m_length_m);
    }

    @Override
    public Length add(Length other) {
        return new Length(m_length_m + other.m_length_m);
    }

    @Override
    public Length subtract(Length other) {
        return new Length(m_length_m - other.m_length_m);
    }

    @Override
    public Length scale(double s) {
        return new Length(m_length_m * s);
    }

    /**
     * Calculates the absolute value of the Length
     *
     * @return a new resulting positive rate
     */
    public Length abs() {
        return new Length(Math.abs(m_length_m));
    }
}
