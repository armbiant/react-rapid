/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import org.growingstems.drive.SwerveModule;
import org.growingstems.math.Angle;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.math.Range;
import org.growingstems.math.Vector2d;

/**
 * Represents a swerve module, which is a physical module on a robot that
 * contains a single wheel that can be both powered to create lateral movement
 * along the ground and steered to control the direction of the lateral motion.
 * <p>
 * It is important that the swerve module can be steered using closed loop
 * control utilizing a sensor to sense its steer angle.
 * </p>
 * <p>
 * It is optional, but also very helpful that the wheel itself has a sensor in
 * order to measure the wheel's rotation. This is utilized to predict the
 * individual modules position along the ground.
 * </p>
 */
public abstract class SwerveModuleBase implements SwerveModule {
    private final String m_name;
    private final Vector2d m_modulePosition;
    private final Range k_powerRange = new Range(-1.0, 1.0);

    protected double m_neutralDeadband = 1.0e-9; // [0.0, 1.0]
    protected double m_currentSteerPower = 0.0; // [-1.0, 1.0]
    protected double m_currentDrivePower = 0.0; // [-1.0, 1.0]
    protected double m_currentDriveVelocityGoal_inps = 0.0; // Inches/Second
    protected double m_prevDrivePosition_in = 0.0;
    protected double m_currentDrivePosition_in = 0.0;
    protected Angle m_prevSteerAngle = new Angle();
    protected Angle m_currentSteerAngle = new Angle();
    protected Angle m_steerAngleOffset = new Angle();
    protected Angle m_absSteerAngleOffset = new Angle();
    protected double m_driveInchesPerSensorUnit = 0.0;
    protected double m_steerRadiansPerSensorUnit = 0.0;

    public String getName() {
        return m_name;
    }

    /**
     * Constructs a Swerve Module with a position relative to the drivetrain's
     * origin.
     *
     * @param name                      The name of the module
     * @param modulePos                 Position of the swerve module relative to
     *                                  the drivetrain's origin.
     * @param driveInchesPerSensorUnit  Conversion factor for the drive wheel in
     *                                  Inches/SU.
     * @param steerRadiansPerSensorUnit Conversion factor for the steer angle of the
     *                                  module in Radians/SU.
     */
    public SwerveModuleBase(String name, Vector2d modulePos, double driveInchesPerSensorUnit, double steerRadiansPerSensorUnit) {
        m_name = name;
        m_modulePosition = modulePos;
        m_driveInchesPerSensorUnit = driveInchesPerSensorUnit;
        m_steerRadiansPerSensorUnit = steerRadiansPerSensorUnit;
    }

    /**
     * Commands the Swerve Module to stop all drive power and holds the the steering
     * angle at the last commanded position.
     */
    public void stop() {
        setDriveOpenLoop(0.0);
        m_currentDrivePower = 0.0;
    }

    @Override
    public void setConversionFactors(double driveInchesPerSensorUnit, double steerRadiansPerSensorUnit) {
        m_driveInchesPerSensorUnit = driveInchesPerSensorUnit;
        m_steerRadiansPerSensorUnit = steerRadiansPerSensorUnit;
    }

    @Override
    public void setVelocityClosedLoop(double velocity_inps, Angle steerAngle) {
        // TODO Might want to throw an exception instead of just forcing incoming values
        // to be positive only.
        velocity_inps = Math.abs(velocity_inps);
        boolean invertMotor = setSteerAngle(steerAngle, true);

        if (invertMotor) {
            m_currentDriveVelocityGoal_inps = velocity_inps;
        } else {
            m_currentDriveVelocityGoal_inps = velocity_inps * -1;
        }

        setDriveVelocityClosedLoop(m_currentDriveVelocityGoal_inps / m_driveInchesPerSensorUnit);
    }

    @Override
    public void setOpenLoop(double power, Angle steerAngle) {
        // If the given power is too low, don't adjust steer and set drive power to zero
        if (power < m_neutralDeadband) {
            m_currentDrivePower = 0.0;
            setDriveOpenLoop(m_currentDrivePower);
            return;
        }

        boolean invertMotor = setSteerAngle(steerAngle, true);

        if (invertMotor) {
            m_currentDrivePower = power * -1;
        } else {
            m_currentDrivePower = power;
        }

        m_currentDrivePower = k_powerRange.coerceValue(m_currentDrivePower);

        setDriveOpenLoop(m_currentDrivePower);
    }

    @Override
    public void setTotalOpenLoop(double drivePower, double steerPower) {
        m_currentDrivePower = k_powerRange.coerceValue(drivePower);
        m_currentSteerPower = k_powerRange.coerceValue(steerPower);
        setDriveOpenLoop(m_currentDrivePower);
        setSteerOpenLoop(m_currentSteerPower);
    }

    @Override
    public Vector2d getModuleLocation() {
        return m_modulePosition;
    }

    @Override
    public Vector2d getForwardKinematics() {
        m_prevDrivePosition_in = m_currentDrivePosition_in;
        m_prevSteerAngle = m_currentSteerAngle;

        m_currentDrivePosition_in = getDrivePosition_in();
        m_currentSteerAngle = getSteerAngle();

        Vector2d vector = new Vector2d(m_currentDrivePosition_in - m_prevDrivePosition_in, 0.0);
        return vector.rotate(m_currentSteerAngle);
    }

    @Override
    public Vector2d getVelocityVector() {
        return new Vector2d(getDriveVelocity_inps(), getSteerAngle());
    }

    /**
     * Set's the angle of driver wheel towards the given direction. 0.0 is pointing
     * forward in the +X direction, positive values are counter-clockwise.
     *
     * @param steerGoal       Goal to steer the drive wheel's direction. [-Pi, Pi)
     * @param reverseOptimize Reverses the drive wheel instead of rotating 180
     *                        degrees.
     * @return True if the drive wheel's direction should be inverted.
     */
    private boolean setSteerAngle(Angle steerGoal, boolean reverseOptimize) {
        boolean invertMotor = false;
        Angle currentAngle = getSteerAngle();

        Angle amountToTurn = steerGoal.difference(currentAngle);
        Angle positionGoal = currentAngle.add(amountToTurn);
        if (reverseOptimize) {
            if (amountToTurn.abs().getValue(AngleUnit.DEGREES) > 90.0) {
                // Optimized logic to reduce approaching infinty.
                Angle pi = new Angle(Math.PI, AngleUnit.RADIANS);
                if (amountToTurn.getValue(AngleUnit.RADIANS) > 0.0) {
                    positionGoal = positionGoal.subtract(pi);
                } else {
                    positionGoal = positionGoal.add(pi);
                }
                invertMotor = true;
            } else {
                invertMotor = false;
            }
        } else {
            invertMotor = false;
        }

        setSteerPositionClosedLoop(positionGoal.subtract(m_steerAngleOffset).getValue(AngleUnit.RADIANS) / m_steerRadiansPerSensorUnit);

        return invertMotor;
    }

    public double getDrivePosition_in() {
        return getRawDrivePosition_su() * m_driveInchesPerSensorUnit;
    }

    public double getDriveVelocity_inps() {
        return getRawDriveVelocity_sups() * m_driveInchesPerSensorUnit;
    }

    public Angle getSteerAngle() {
        return new Angle(getRawSteerAngle_su() * m_steerRadiansPerSensorUnit, AngleUnit.RADIANS).add(m_steerAngleOffset);
    }

    public void setAbsSteerAngleOffset(Angle offsetAngle) {
        m_absSteerAngleOffset = offsetAngle;
    }

    /**
     * Logic to determine whether or not the drive wheel should be inverted in order
     * to optimize steer angle commands.
     *
     * @param commandAngle_rad Angle that the module should go to in radians.
     * @param currentAngle_rad Angle that the module is currently at in radians.
     * @return True if the drive wheel should be reversed.
     */
    protected static boolean shouldReverse(double commandAngle_rad, double currentAngle_rad) {
        double delta = currentAngle_rad - commandAngle_rad;
        return (Math.abs(delta) > Math.PI / 2.0);
    }

    /**
     * Set's the Minumum power that will be applied to the drive wheel. Any value
     * lower will be considered zero power and the heading of the module will be
     * maintained.<br>
     * <br>
     * This helps prevent the wheel from spinning wildly at lower vector magnitudes.
     *
     * @param neutralDeadband Neutral deadband setting.
     */
    public void setNeutralDeadband(double neutralDeadband) {
        m_neutralDeadband = neutralDeadband;
    }

    /**
     * Gets the current deadband setting used to reduce the module's drive power to
     * zero.
     *
     * @return Neutral deadband value.
     */
    public double getNeutralDeadband() {
        return m_neutralDeadband;
    }

    public abstract void calibrateOffset();

    /**
     * Puts the drive wheel controller of the drive wheel into open loop control.
     *
     * @param power The power setting for the drive wheel controller. [-1.0, 1.0]
     * @return True if the extending class implements this function.
     */
    protected abstract boolean setDriveOpenLoop(double power);

    /**
     * Puts the drive wheel of the module into velocity closed loop control and sets
     * the module's drive wheel velocity goal.
     *
     * @param velocity_sups Velocity goal to be used for velocity closed loop in
     *                      sensor units per second. (-inf, inf)
     * @return True if the extending class implements this function.
     */
    protected abstract boolean setDriveVelocityClosedLoop(double velocity_sups);

    /**
     * Puts the drive wheel of the module into position closed loop control and sets
     * the module's drive wheel position goal.
     *
     * @param position_su Position goal to be used for position closed loop in
     *                    sensor units. (-inf, inf)
     * @return True if the extending class implements this function.
     */
    protected abstract boolean setDrivePositionClosedLoop(double position_su);

    /**
     * Puts the steer controller of the module into open loop control.
     *
     * @param power the power setting for the steer controller. [-1.0, 1.0]
     * @return True if the extending class implements this function.
     */
    protected abstract boolean setSteerOpenLoop(double power);

    /**
     * Puts the steer angle of the module into position closed loop control and sets
     * the module's steer angle goal.
     *
     * @param position_su position goal to be used for position closed loop in
     *                    sensor units. (-inf, inf)
     * @return True if the extending class implements this function.
     */
    protected abstract boolean setSteerPositionClosedLoop(double position_su);

    /**
     * Gets the swerve module's current steer angle directly from the module's
     * sensor. Generally makes a direct call to the hardware to retrieve sensor
     * data.
     *
     * @return steer angle in raw sensor units. (-inf, inf)
     */
    protected abstract double getRawSteerAngle_su();

    /**
     * Gets the swerve module's current steer velocity directly from the module's
     * sensor. Generally makes a direct call to the hardware to retrieve sensor
     * data.
     *
     * @return steer velocity in raw sensor units per second. (-inf, inf)
     */
    protected abstract double getRawSteerVelocity_sups();

    /**
     * Gets the swerve module's current drive position directly from the module's
     * sensor. Generally makes a direct call to the hardware to retrieve sensor
     * data.
     *
     * @return drive wheel position in raw sensor units. (-inf, inf)
     */
    protected abstract double getRawDrivePosition_su();

    /**
     * Gets the swerve module's current drive wheel velocity directly from the
     * module's sensor. Generally makes a direct call to the hardware to retrieve
     * sensor data.
     *
     * @return drive wheel velocity in raw sensor units per second. (-inf, inf)
     */
    protected abstract double getRawDriveVelocity_sups();

    /**
     * Modulates a robot's velocity given a magnitude and angle through a Vector2d.
     * This method, using SwerveDrive, controls which direction the robot translates
     * towards and
     * how fast it translates.
     *
     * @param velocityVector_inps - Vector2d composed of a magnitude in inps and a
     *                            direction the robot will translate towards
     *                            relative to the field
     */
    protected abstract void velocityVectorControl(Vector2d velocityVector_inps);
}
