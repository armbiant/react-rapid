/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import org.growingstems.util.interpolation.InterpolatableValue;

import edu.wpi.first.math.Pair;

public class InterpolatableDoublePair extends InterpolatableValue<InterpolatableDoublePair, Pair<Double, Double>> {

    public InterpolatableDoublePair(Pair<Double, Double> value) {
        super(value);
    }

    @Override
    public InterpolatableDoublePair linearInterpolate(InterpolatableDoublePair upper, double x) {
        double primaryRange = upper.m_value.getFirst() - this.m_value.getFirst();
        double secondaryRange = upper.m_value.getSecond() - this.m_value.getSecond();

        double first = primaryRange * x + this.m_value.getFirst();
        double second = secondaryRange * x + this.m_value.getSecond();
        return new InterpolatableDoublePair(new Pair<Double, Double>(first, second));
    }

    @Override
    public InterpolatableDoublePair linearExtrapolate(InterpolatableDoublePair upper, double x) {
        /**
         * Extrapolate uses the interpolate method because the interpolate
         * implementation can handle extrapolation for this class.
         */
        return linearInterpolate(upper, x);
    }

    @Override
    public int compareTo(InterpolatableDoublePair other) {
        if (this.m_value.getFirst() < other.m_value.getFirst()) {
            return -1;
        } else if (this.m_value.getFirst() > other.m_value.getFirst()) {
            return 1;
        } else if (this.m_value.getSecond() < other.m_value.getSecond()) {
            return -1;
        }  else if (this.m_value.getSecond() > other.m_value.getSecond()) {
            return 1;
        } else {
            return 0;
        }
    }
}
