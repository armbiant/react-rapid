/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.Robot;

import java.util.concurrent.atomic.AtomicBoolean;

import org.growingstems.util.statemachine.DynamicTransition;
import org.growingstems.util.statemachine.State;
import org.growingstems.util.statemachine.StateMachine;
import org.growingstems.util.statemachine.StaticTransition;

public abstract class GStemsSubsystem extends SubsystemBase implements Disableable {
    protected final String m_name;

    private enum OperationalState { MANUAL, NORMAL }

    private OperationalState m_goalOperationState;

    private StateMachine m_operationStatusStateMachine;

    private final NetworkTableEntry m_isEnabledEntry;
    private AtomicBoolean m_enabled = new AtomicBoolean(false);

    public GStemsSubsystem(String name) {
        m_name = name;
        m_isEnabledEntry = SmartDashboard.getEntry(m_name + " Enabled");

        m_isEnabledEntry.setBoolean(false);

        if (!Robot.isCompetition()) {
            var enableTrigger = new Trigger(() -> m_isEnabledEntry.getBoolean(false));

            enableTrigger.whenActive(new InstantCommand(this::enable) {
                @Override
                public boolean runsWhenDisabled() {
                    return true;
                }
            });
            enableTrigger.whenInactive(new InstantCommand(this::disable) {
                @Override
                public boolean runsWhenDisabled() {
                    return true;
                }
            });
        }

        m_goalOperationState = OperationalState.NORMAL;

        State disabledState = provideDisabledState();
        State manualState = provideManualState();
        State normalState = provideNormalState();

        manualState.addTransition(new StaticTransition(disabledState, () -> !m_enabled.get()));
        manualState.addTransition(
            new StaticTransition(normalState, () -> m_goalOperationState == OperationalState.NORMAL));

        normalState.addTransition(new StaticTransition(disabledState, () -> !m_enabled.get()));
        normalState.addTransition(
            new StaticTransition(manualState, () -> m_goalOperationState == OperationalState.MANUAL));

        // clang-format off
        disabledState.addTransition(
            new DynamicTransition(() -> m_goalOperationState == OperationalState.MANUAL ? manualState : normalState,
                                  m_enabled::get));
        // clang-format on

        m_operationStatusStateMachine = new StateMachine(disabledState);
        m_operationStatusStateMachine.start();
    }

    public String getName() {
        return m_name;
    }

    public State getCurrentState() {
        return m_operationStatusStateMachine.getCurrentState();
    }

    @Override
    public void periodic() {
        m_operationStatusStateMachine.step();
    }

    @Override
    public void disable() {
        m_enabled.set(false);
        m_isEnabledEntry.setBoolean(false);
    }

    @Override
    public void enable() {
        m_enabled.set(true);
        m_isEnabledEntry.setBoolean(true);
    }

    // To provide OnEnable functionality, subsystems should return a state
    // that executes an action upon stepping out of the disabledState
    protected abstract State provideDisabledState();
    protected abstract State provideManualState();
    protected abstract State provideNormalState();

    // Determine if subsystem is ready to begin a match
    public boolean isReadyToStartMatch(boolean mustBeEnabled) {
        return isReadyToStartMatch() && (!mustBeEnabled || m_enabled.get());
    }

    protected boolean isReadyToStartMatch() {
        return true;
    }

    public Command getManualCommand() {
        return new InstantCommand(() -> m_goalOperationState = OperationalState.MANUAL, this) {
            @Override
            public boolean runsWhenDisabled() {
                return true;
            }
        };
    }

    public Command getNormalCommand() {
        return new InstantCommand(() -> m_goalOperationState = OperationalState.NORMAL, this) {
            @Override
            public boolean runsWhenDisabled() {
                return true;
            }
        };
    }
}
