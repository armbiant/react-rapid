/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import frc.library.Length.LengthUnit;

import org.growingstems.math.Vector2d;

public class Position2d {
    private final Length k_x;
    private final Length k_y;

    public Position2d(Length x, Length y) {
        k_x = x;
        k_y = y;
    }

    public Length getX() {
        return k_x;
    }

    public Length getY() {
        return k_y;
    }

    public Vector2d getVector(LengthUnit unit) {
        return new Vector2d(k_x.getValue(unit), k_y.getValue(unit));
    }
}
