/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2/. */

package frc.library;

/**
 * Interface for classes that need to be updated periodically in order to
 * function. Generally if a class implements this interface, it is expected that
 * update() is called first at the beginning of every code cycle.
 */
public interface Periodic {
    /** Function that should be called at the beginning of every code cycle. */
    public void update();
}
