/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import edu.wpi.first.wpilibj.DriverStation;

public class RobotMatchState {
    public static enum MatchState {
        EMERGENCY_STOPPED,
        AUTO_ENABLED,
        AUTO_DISABLED,
        TELE_ENABLED,
        TELE_DISABLED,
        TEST_ENABLED,
        TEST_DISABLED,
        UNKNOWN_ENABLED,
        UNKNOWN_DISABLED
    }

    public static MatchState getMatchState() {
        // estop
        if (DriverStation.isEStopped()) {
            return MatchState.EMERGENCY_STOPPED;
        }

        // auto
        if (DriverStation.isAutonomousEnabled()) {
            return MatchState.AUTO_ENABLED;
        }
        if (DriverStation.isAutonomous()) {
            return MatchState.AUTO_DISABLED;
        }

        // teleop
        if (DriverStation.isTeleopEnabled()) {
            return MatchState.TELE_ENABLED;
        }
        if (DriverStation.isTeleop()) {
            return MatchState.TELE_DISABLED;
        }

        // test (has to expilicitly check isEnabled because there is not testEnabled())
        if (DriverStation.isTest() && DriverStation.isEnabled()) {
            return MatchState.TEST_ENABLED;
        }
        if (DriverStation.isTest()) {
            return MatchState.TEST_DISABLED;
        }

        // somehow in an unknown mode
        if (DriverStation.isEnabled()) {
            return MatchState.UNKNOWN_ENABLED;
        }
        return MatchState.UNKNOWN_DISABLED;
    }
}
