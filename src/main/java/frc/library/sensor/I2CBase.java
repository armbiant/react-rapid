/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.sensor;

public abstract class I2CBase implements I2CInterface {
    private final int m_deviceAddress;

    public I2CBase(int deviceAddress) {
        m_deviceAddress = deviceAddress;
    }

    public int getDeviceAddress() {
        return m_deviceAddress;
    }
}
