/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.sensor;

import java.nio.ByteBuffer;
import java.util.Vector;

import edu.wpi.first.wpilibj.DigitalOutput;

/**
 * Written based on TCA9548A chip.
 */
public class I2CMux {
    public enum AddressSelect {
        LLL(0x70),
        LLH(0x71),
        LHL(0x72),
        LHH(0x73),
        HLL(0x74),
        HLH(0x75),
        HHL(0x76),
        HHH(0x77);

        public final int address;

        AddressSelect(int address) {
            this.address = address;
        }
    }

    public enum Channel {
        CHANNEL_0(0),
        CHANNEL_1(1),
        CHANNEL_2(2),
        CHANNEL_3(3),
        CHANNEL_4(4),
        CHANNEL_5(5),
        CHANNEL_6(6),
        CHANNEL_7(7);

        public final int value;

        Channel(int channel) {
            this.value = channel;
        }
    }

    private final I2CInterface m_masterInterface;
    public static final int k_totalChannels = 8;
    private boolean m_channelStates[] = new boolean[k_totalChannels];
    private byte m_prevData = 0x0;
    private Vector<I2CMuxSlaveInterface> m_slaveInterfaces = new Vector<>();
    private final DigitalOutput m_resetDio;
    private static final double k_resetPulseTime_s = 2.0e-6;

    public I2CMux(I2CInterface masterInterface, DigitalOutput resetDio) {
        m_masterInterface = masterInterface;
        m_resetDio = resetDio;
        m_resetDio.set(true);

        for (int i=0; i<m_channelStates.length; i++) {
            m_channelStates[i] = false;
        }
    }

    public I2CInterface getMuxInterface(I2CBase slaveInterface, Channel muxChannel) {
        I2CMuxSlaveInterface slaveMuxInterface = new I2CMuxSlaveInterface(slaveInterface, muxChannel);
        m_slaveInterfaces.add(slaveMuxInterface);
        updateSlaveInterfaceConflictingChannels();
        return slaveMuxInterface;
    }

    public void reset() {
        m_resetDio.pulse(k_resetPulseTime_s);
    }

    public boolean verify() {
        byte testData = (byte)0x74;
        writeSetting(testData, true);
        if (testData != readSetting()) {
            commitChannelStates();
            return false;
        }

        testData = (byte)0xB5;
        writeSetting(testData, true);
        if (testData != readSetting()) {
            commitChannelStates();
            return false;
        }

        commitChannelStates();
        return true;
    }

    private boolean updateSlaveInterfaceConflictingChannels() {
        for (I2CMuxSlaveInterface slaveInterface : m_slaveInterfaces) {
            Vector<Channel> conflictingChannels = new Vector<>();
            for (I2CMuxSlaveInterface slaveInterfaceOther : m_slaveInterfaces) {
                // Ignore the interface if its the one we are searching off of
                if (slaveInterface == slaveInterfaceOther) {
                    continue;
                }

                if (slaveInterface.getDeviceAddress() == slaveInterfaceOther.getDeviceAddress()) {
                    if (slaveInterface.getChannel() == slaveInterfaceOther.getChannel()) {
                        // We can't have the same address on the same channel. That's the point of the Mux
                        return false;
                    }

                    boolean uniqueConflict = true;
                    for (Channel conflictingChannel : conflictingChannels) {
                        if (slaveInterfaceOther.getChannel() == conflictingChannel) {
                            // We already know that this channel is conflict
                            uniqueConflict = false;
                            break;
                        }
                    }

                    if (uniqueConflict) {
                        // Found a new unique channel that conflicts with our device on its channel
                        conflictingChannels.add(slaveInterfaceOther.getChannel());
                    }
                }
            }

            slaveInterface.setConflictingChannels(conflictingChannels);
        }

        return true;
    }

    private void stageEnabledChannel(Channel channel) {
        m_channelStates[channel.value] = true;
    }

    private void stageDisabledChannel(Channel channel) {
        m_channelStates[channel.value] = false;
    }

    private void commitChannelStates() {
        byte data = 0x0;

        for (int i=0; i<m_channelStates.length; i++) {
            data |= (m_channelStates[i] ? 1 : 0) << i;
        }

        writeSetting(data);
    }

    private void writeSetting(byte in) {
        writeSetting(in, false);
    }

    private void writeSetting(byte in, boolean force) {
        if (m_prevData != in || force) {
            byte data[] = new byte[1];
            data[0] = in;
            m_masterInterface.writeBulk(data, 1);
            m_prevData = in;
        }
    }

    public byte readSetting() {
        byte data[] = new byte[1];
        m_masterInterface.readOnly(1, data);
        return data[0];
    }

    public class I2CMuxSlaveInterface extends I2CBase {
        private final I2CBase m_slaveInterface;
        private final Channel m_slaveChannel;
        private Vector<Channel> m_conflictingChannels = new Vector<>();
        public I2CMuxSlaveInterface(I2CBase slaveInterface, Channel slaveChannel) {
            super(slaveInterface.getDeviceAddress());
            m_slaveInterface = slaveInterface;
            m_slaveChannel = slaveChannel;
        }

        public Channel getChannel() {
            return m_slaveChannel;
        }

        private void setConflictingChannels(Vector<Channel> conflictingChannels) {
            m_conflictingChannels = conflictingChannels;
        }

        private Vector<Channel> getConflictingChannels() {
            return m_conflictingChannels;
        }

        private void prepareSlaveChannel() {
            stageDisabledConflictingAddresses();
            stageEnabledChannel(getChannel());
            commitChannelStates();
        }

        private void stageDisabledConflictingAddresses() {
            for (Channel conflictingChannel : getConflictingChannels()) {
                stageDisabledChannel(conflictingChannel);
            }
        }


        @Override
        public void close() {
            return;
        }
        @Override
        public boolean read(int registerAddress, int count, byte[] buffer) {
            prepareSlaveChannel();
            return m_slaveInterface.read(registerAddress, count, buffer);
        }
        @Override
        public boolean read(int registerAddress, int count, ByteBuffer buffer) {
            prepareSlaveChannel();
            return m_slaveInterface.read(registerAddress, count, buffer);
        }
        @Override
        public boolean readOnly(int count, byte[] buffer) {
            prepareSlaveChannel();
            return m_slaveInterface.readOnly(count, buffer);
        }
        @Override
        public boolean readOnly(int count, ByteBuffer buffer) {
            prepareSlaveChannel();
            return m_slaveInterface.readOnly(count, buffer);
        }
        @Override
        public boolean transaction(byte[] dataToSend, int sendSize, byte[] dataReceived, int receiveSize) {
            prepareSlaveChannel();
            return m_slaveInterface.transaction(dataToSend, sendSize, dataReceived, receiveSize);
        }
        @Override
        public boolean transaction(ByteBuffer dataToSend, int sendSize, ByteBuffer dataReceived, int receiveSize) {
            prepareSlaveChannel();
            return m_slaveInterface.transaction(dataToSend, sendSize, dataReceived, receiveSize);
        }
        @Override
        public boolean write(int registerAddress, byte data) {
            prepareSlaveChannel();
            return m_slaveInterface.write(registerAddress, data);
        }
        @Override
        public boolean writeBulk(byte[] data) {
            prepareSlaveChannel();
            return m_slaveInterface.writeBulk(data);
        }
        @Override
        public boolean writeBulk(byte[] data, int size) {
            prepareSlaveChannel();
            return m_slaveInterface.writeBulk(data, size);
        }
        @Override
        public boolean writeBulk(ByteBuffer data, int size) {
            prepareSlaveChannel();
            return m_slaveInterface.writeBulk(data, size);
        }
    }
}
