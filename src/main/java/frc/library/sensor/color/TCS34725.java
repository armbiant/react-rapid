/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2/. */

package frc.library.sensor.color;

import java.util.Set;

import org.growingstems.math.Time;
import org.growingstems.math.Time.TimeUnit;

import frc.library.sensor.I2CInterface;

/**
 * https://www.adafruit.com/product/1334
 * https://cdn-shop.adafruit.com/datasheets/TCS34725.pdf
 * https://github.com/adafruit/Adafruit_TCS34725
 */
public class TCS34725 extends ColorSensor {
    public enum Register {
        ENABLE(0x00),
        ATIME(0x01),
        WTIME(0x03),

        // Interrupt Registers
        AILTL(0x04),
        AILTH(0x05),
        AIHTL(0x06),
        AIHTH(0x07),
        PERS(0x0C),

        /** Config for Wait Long Enable */
        CONFIG(0x0D),
        /** Gain Control on bits 1:0 */
        CONTROL(0x0F),
        ID(0x12),
        STATUS(0x13),

        CDATAL(0x14),
        CDATAH(0x15),
        RDATAL(0x16),
        RDATAH(0x17),
        GDATAL(0x18),
        GDATAH(0x19),
        BDATAL(0x1A),
        BDATAH(0x1B);

        private final byte m_address;

        private Register(int address) {
            m_address = (byte) (address & 0xFF);
        }

        public byte getAddress() {
            return m_address;
        }
    };

    public enum CommandType {
        REPEATED_BYTE_PROTOCOL(0b00),
        AUTO_INCREMENT_PROTOCOL(0b01),
        /**
         * Byte protocol will repeatedly read the same register with each data access.
         * <p>
         * Block protocol will provide auto-increment function to read successive bytes
         */
        SPECIAL(0b11);

        private final int m_data;

        private CommandType(int data) {
            m_data = data;
        }

        public int getData() {
            return m_data;
        }
    }

    public enum Gain {
        GAIN_1X(0b00),
        GAIN_4X(0b01),
        GAIN_16X(0b10),
        GAIN_60X(0b11);

        private final int m_data;

        private Gain(int data) {
            m_data = data;
        }

        public int getData() {
            return m_data;
        }
    }

    public enum HardwareChannel {
        CLEAR,
        RED,
        BLUE,
        GREEN;
    };

    private static class HardwareRegisterBase {
        protected int boolToInt(boolean bool) {
            return bool ? 1 : 0;
        }
    }

    private static class Command extends HardwareRegisterBase {
        private CommandType m_commandType = CommandType.REPEATED_BYTE_PROTOCOL;
        private boolean m_clearInterrupt = false;
        private static Register m_register;

        private static int k_commandBit = 0x80;
        private int k_clearChannelInterrupt = 0b00110;

        /**
         * Create normal command.
         *
         * @param commandType
         * @param register
         */
        public Command(CommandType commandType, Register register) {
            m_commandType = commandType;
            m_clearInterrupt = false;
            m_register = register;
        }

        public byte getByte() {
            int data = (m_commandType.getData() << 5) | k_commandBit;

            if (m_clearInterrupt) {
                data |= k_clearChannelInterrupt;
            } else {
                data |= m_register.getAddress();
            }

            return (byte) data;
        }
    }

    private static class EnableRegister extends HardwareRegisterBase {
        public boolean rgbcInterruptEnable = false;
        public boolean waitEnable = false;
        public boolean rgbcEnable = true;
        public boolean powerOn = true;

        public byte getByte() {
            int data = ((boolToInt(rgbcInterruptEnable) << 4) | (boolToInt(waitEnable) << 3)
                    | (boolToInt(rgbcEnable) << 1) | boolToInt(powerOn));
            return (byte) data;
        }
    }

    private static int k_singleByte = 1;
    private static int k_twoBytes = 2;

    private static byte k_deviceId = 0x44;
    private static CommandType k_defaultCommandType = CommandType.REPEATED_BYTE_PROTOCOL;
    private static int k_maxChannelAdcValue = (int) Math.pow(2, 13) - 1;
    private static double k_rgbcIntegrationLSB_ms = 2.4;

    private final I2CInterface m_i2cInterface;

    private EnableRegister m_enableRegister = new EnableRegister();

    public static final int k_deviceAddress = 0x29;

    public TCS34725(I2CInterface i2cInterface) {
        m_i2cInterface = i2cInterface;
        setEnableRegister();
    }

    @Override
    public double getColor(ColorChannel color) {
        HardwareChannel channel = getHardwareChannel(color);

        if (channel == null) {
            return Double.NaN;
        }

        return (double) readRawColor(channel) / (double) k_maxChannelAdcValue;
    }

    @Override
    public Set<ColorChannel> getSupportedChannels() {
        return Set.of(ColorChannel.RED, ColorChannel.GREEN, ColorChannel.BLUE, ColorChannel.CLEAR);
    }

    /**
     * Checks if the device is connected and communicating.
     *
     * @return true if the device is connected and communicating.
     */
    public boolean verify() {
        return readByte(Register.ID, k_defaultCommandType) == k_deviceId;
    }

    /**
     * Sets integration time of device. Time will be rounded down to the next
     * available value.
     *
     * @param time device integration time.
     */
    public void setRgbcIntegration(Time time) {
        double time_ms = time.getValue(TimeUnit.MILLISECONDS);
        byte timeRaw;

        if (time_ms < k_rgbcIntegrationLSB_ms) {
            timeRaw = (byte) 0xFF;
        } else if (time_ms > (k_rgbcIntegrationLSB_ms * 256)) {
            timeRaw = (byte) 0x00;
        } else {
            timeRaw = (byte) (256 - (int) Math.floor(time_ms / k_rgbcIntegrationLSB_ms));
        }

        setRgbcIntegration(timeRaw);
    }

    private void setRgbcIntegration(int time) {
        writeByte(Register.ATIME, k_defaultCommandType, (byte)time);
    }

    /**
     * Enables device.
     */
    public void enable() {
        m_enableRegister.rgbcEnable = true;
        m_enableRegister.powerOn = true;
        setEnableRegister();
    }

    /**
     * Disables device. Does not turn off LED on Adafruit's TCS34725.
     */
    public void disable() {
        m_enableRegister.rgbcEnable = false;
        m_enableRegister.powerOn = false;
        setEnableRegister();
    }

    private void setEnableRegister() {
        writeByte(Register.ENABLE, k_defaultCommandType, m_enableRegister.getByte());
    }

    /**
     * Set gain of all color channel ADCs.
     *
     * @param gain amount of gain applied to all color channel ADCs.
     */
    public void setGain(Gain gain) {
        byte data = (byte) gain.getData();
        writeByte(Register.CONTROL, k_defaultCommandType, data);
    }

    /**
     * Checks status of RGBC integrator. Indicates that the RGBC channels have
     * completed an integration cycle.
     *
     * @return true if RGBC is valid.
     */
    public boolean isDataReady() {
        return (readByte(Register.STATUS, k_defaultCommandType) & (byte) 0x1) > 0;
    }

    private HardwareChannel getHardwareChannel(ColorChannel color) {
        switch (color) {
            case CLEAR:
                return HardwareChannel.CLEAR;
            case RED:
                return HardwareChannel.RED;
            case GREEN:
                return HardwareChannel.GREEN;
            case BLUE:
                return HardwareChannel.BLUE;
            default:
                return null;
        }
    }

    /**
     * Reads the raw color device registers filled by the 16 bit ADCs.
     *
     * @param color The color channel to read.
     * @return the value returned by the device.
     */
    public int readRawColor(HardwareChannel color) {
        byte[] data = { 0, 0 };

        switch (color) {
            case CLEAR:
                data = readTwoBytes(Register.CDATAL, k_defaultCommandType);
                break;
            case RED:
                data = readTwoBytes(Register.RDATAL, k_defaultCommandType);
                break;
            case GREEN:
                data = readTwoBytes(Register.GDATAL, k_defaultCommandType);
                break;
            case BLUE:
                data = readTwoBytes(Register.BDATAL, k_defaultCommandType);
                break;
        }

        return (((int) data[1] << 8) & 0xFF00) | ((int) data[0] & 0xFF);
    }

    private byte readByte(Register register, CommandType commandType) {
        byte[] read_byte = new byte[1];
        Command commandAddress = new Command(commandType, register);

        m_i2cInterface.read(commandAddress.getByte(), k_singleByte, read_byte);

        return read_byte[0];
    }

    private byte[] readTwoBytes(Register register, CommandType commandType) {
        byte[] read_bytes = new byte[2];

        Command commandAddress = new Command(commandType, register);

        m_i2cInterface.read(commandAddress.getByte(), k_twoBytes, read_bytes);

        return read_bytes;
    }

    private void writeByte(Register register, CommandType commandType, byte data) {
        Command commandAddress = new Command(commandType, register);

        m_i2cInterface.write(commandAddress.getByte(), data);
    }
}
