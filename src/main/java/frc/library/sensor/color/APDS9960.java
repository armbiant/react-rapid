/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2/. */

package frc.library.sensor.color;

import java.util.Set;

import org.growingstems.math.Time;
import org.growingstems.math.Time.TimeUnit;

import frc.library.sensor.I2CInterface;

/**
 * Ambient Light Sensor with Gesture Detection
 *
 * https://www.adafruit.com/product/3595
 * https://cdn-learn.adafruit.com/assets/assets/000/045/848/original/Avago-APDS-9960-datasheet.pdf?1504034182
 * https://github.com/adafruit/Adafruit_APDS9960
 */
public class APDS9960 extends ColorSensor {
    public enum Register {
        ENABLE(0x80),
        ATIME(0x81),
        WTIME(0x83),

        // ALS Interrupt Registers
        AILTL(0x84),
        AILTH(0x85),
        AIHTL(0x86),
        AIHTH(0x87),
        // Proximity Interrupt Registers
        PILT(0x89),
        PIHT(0x8B),
        /** Interrupt Persistence Register */
        PERS(0x8C),

        /** Wait Long Config Register (bit 1) */
        CONFIG1(0x8D),
        /** Proximity Pulse Count Register */
        PPULSE(0x8E),
        CONTROL(0x8F),
        CONFIG2(0x90),
        ID(0x92),
        STATUS(0x93),

        // Color Registers
        CDATAL(0x94),
        CDATAH(0x95),
        RDATAL(0x96),
        RDATAH(0x97),
        GDATAL(0x98),
        GDATAH(0x99),
        BDATAL(0x9A),
        BDATAH(0x9B),

        PDATA(0x9C),
        POFFSET_UR(0x9D),
        POFFSET_DL(0x9E),
        CONFIG3(0x9F),
        GPENTH(0xA0),
        GEXTH(0xA1),
        GCONF1(0xA2),
        GCONF2(0xA3),
        GOFFSET_U(0xA4),
        GOFFSET_D(0xA5),
        GOFFSET_L(0xA7),
        GOFFSET_R(0xA9),
        GPULSE(0xA6),
        GCONF3(0xAA),
        GCONF4(0xAB),
        GFLVL(0xAE),
        GSTATUS(0xAF),
        IFORCE(0xE4),
        PICLEAR(0xE5),
        CICLEAR(0xE6),
        AICLEAR(0xE7),
        GFIFO_U(0xFC),
        GFIFO_D(0xFD),
        GFIFO_L(0xFE),
        GFIFO_R(0xFF);

        private final byte m_address;

        private Register(int address) {
            m_address = (byte)address;
        }

        public byte getAddress() {
            return m_address;
        }
    };

    public enum LedCurrent {
        LED_100MA(0b00),
        LED_50MA(0b01),
        LED_25MA(0b10),
        LED_12_5MA(0b11);

        public final int m_data;

        private LedCurrent(int data) {
            m_data = data;
        }
    }

    public enum ProximityGain {
        GAIN_1X(0b00),
        GAIN_2X(0b01),
        GAIN_4X(0b10),
        GAIN_8X(0b11);

        public final int m_data;

        private ProximityGain(int data) {
            m_data = data;
        }
    }

    /** ALS and Color Gain Control */
    public enum ColorGain {
        GAIN_1X(0b00),
        GAIN_4X(0b01),
        GAIN_16X(0b10),
        GAIN_64X(0b11);

        public final int m_data;

        private ColorGain(int data) {
            m_data = data;
        }
    }

    public enum LedBoost {
        LED_100_PERCENT(0b00),
        LED_150_PERCENT(0b01),
        LED_200_PERCENT(0b10),
        LED_300_PERCENT(0b11);

        public final int m_data;

        private LedBoost(int data) {
            m_data = data;
        }
    }

    public enum HardwareChannel {
        CLEAR,
        RED,
        BLUE,
        GREEN;
    };

    private static class HardwareRegisterBase {
        protected int boolToInt(boolean bool) {
            return bool ? 1 : 0;
        }
    }

    private static class EnableRegister extends HardwareRegisterBase {
        public boolean gestureEnable = false;
        public boolean proximityInterruptEnable = false;
        public boolean alsInterruptEnable = false;
        public boolean waitEnable = false;
        public boolean proximityDetectEnable = true;
        public boolean alsEnable = true;
        public boolean powerOn = true;

        public byte getByte() {
            int data = ((boolToInt(gestureEnable) << 6) | (boolToInt(proximityInterruptEnable) << 5)
                    | (boolToInt(alsInterruptEnable) << 4) | boolToInt(waitEnable) << 3
                    | boolToInt(proximityDetectEnable) << 2 | boolToInt(alsEnable) << 1 | boolToInt(powerOn));
            return (byte) data;
        }
    }

    private static class ControlOneRegister extends HardwareRegisterBase {
        public LedCurrent ledDriveCurrent = LedCurrent.LED_100MA;
        public ProximityGain proximityGain = ProximityGain.GAIN_8X;
        public ColorGain colorGain = ColorGain.GAIN_64X;

        public byte getByte() {
            int data = ((ledDriveCurrent.m_data << 6) | (proximityGain.m_data << 2)
                    | colorGain.m_data);
            return (byte) data;
        }
    }

    private static class ControlTwoRegister extends HardwareRegisterBase {
        public boolean proximitySaturationEnable = false;
        public boolean clearPhotoDiodeSaturationIntterupt = false;
        public LedBoost ledBoost = LedBoost.LED_100_PERCENT;

        public byte getByte() {
            int data = ((boolToInt(proximitySaturationEnable) << 7)
                    | (boolToInt(clearPhotoDiodeSaturationIntterupt) << 6) | (ledBoost.m_data << 4) | 0x1);
            return (byte) data;
        }
    }

    private static int k_singleByte = 1;
    private static int k_twoBytes = 2;

    private static byte k_deviceId = (byte)0xAB;
    private static int k_maxChannelAdcValue = (int) Math.pow(2, 16) - 1;
    private static double k_adcIntegrationLSB_ms = 2.78;

    private final I2CInterface m_i2cInterface;

    private EnableRegister m_enableRegister = new EnableRegister();
    private ControlOneRegister m_controlRegister = new ControlOneRegister();

    public static final int k_deviceAddress = 0x39;

    public APDS9960(I2CInterface i2cInterface) {
        m_i2cInterface = i2cInterface;
        setEnableRegister();
        setControlOneRegister();
    }

    @Override
    public double getColor(ColorChannel color) {
        HardwareChannel channel = getHardwareChannel(color);

        if (channel == null) {
            return Double.NaN;
        }

        return (double) readRawColor(channel) / (double) k_maxChannelAdcValue;
    }

    @Override
    public Set<ColorChannel> getSupportedChannels() {
        return Set.of(ColorChannel.RED, ColorChannel.GREEN, ColorChannel.BLUE, ColorChannel.CLEAR);
    }

    /**
     * Checks if the device is connected and communicating.
     *
     * @return true if the device is connected and communicating.
     */
    public boolean verify() {
        return readByte(Register.ID) == k_deviceId;
    }

    /**
     * Sets integration time of device. Time will be rounded down to the next
     * available value.
     *
     * @param time device integration time.
     */
    public void setAdcIntegration(Time time) {
        double time_ms = time.getValue(TimeUnit.MILLISECONDS);
        byte timeRaw;

        if (time_ms < k_adcIntegrationLSB_ms) {
            timeRaw = (byte) 0xFF;
        } else if (time_ms > (k_adcIntegrationLSB_ms * 256)) {
            timeRaw = (byte) 0x00;
        } else {
            timeRaw = (byte) (256 - (int) Math.floor(time_ms / k_adcIntegrationLSB_ms));
        }

        setAdcIntegration(timeRaw);
    }

    private void setAdcIntegration(int time) {
        writeByte(Register.ATIME, (byte)time);
    }

    /**
     * Enables device.
     */
    public void enable() {
        m_enableRegister.alsEnable = true;
        m_enableRegister.powerOn = true;
        setEnableRegister();
    }

    /**
     * Disables device. Does not turn off LED on Adafruit's APDS9960.
     */
    public void disable() {
        m_enableRegister.alsEnable = false;
        m_enableRegister.powerOn = false;
        setEnableRegister();
    }

    private void disableWaitTime() {
        m_enableRegister.waitEnable = false;
        setEnableRegister();
    }

    private void setEnableRegister() {
        writeByte(Register.ENABLE, m_enableRegister.getByte());
    }

    private void setWaitTime() {
        writeByte(Register.WTIME, (byte) 0xFF);
    }

    /**
     * Set gain of all color channel ADCs.
     *
     * @param gain amount of gain applied to all color channel ADCs.
     */
    public void setColorGain(ColorGain gain) {
        m_controlRegister.colorGain = gain;
        setControlOneRegister();
    }

    /**
     * Set gain of proximity ADC.
     *
     * @param gain amount of gain applied to proximity ADC.
     */
    public void setProximityGain(ProximityGain gain) {
        m_controlRegister.proximityGain = gain;
        setControlOneRegister();
    }

    public void setLedCurrent(LedCurrent current) {
        m_controlRegister.ledDriveCurrent = current;
        setControlOneRegister();
    }

    private void setControlOneRegister() {
        writeByte(Register.CONTROL, m_controlRegister.getByte());
    }

    private HardwareChannel getHardwareChannel(ColorChannel color) {
        switch (color) {
            case CLEAR:
                return HardwareChannel.CLEAR;
            case RED:
                return HardwareChannel.RED;
            case GREEN:
                return HardwareChannel.GREEN;
            case BLUE:
                return HardwareChannel.BLUE;
            default:
                return null;
        }
    }

    /**
     * Reads the raw color device registers filled by the 16 bit ADCs.
     *
     * @param color The color channel to read.
     * @return the value returned by the device.
     */
    public int readRawColor(HardwareChannel color) {
        byte[] data = { 0, 0 };

        switch (color) {
            case CLEAR:
                data = readTwoBytes(Register.CDATAL);
                break;
            case RED:
                data = readTwoBytes(Register.RDATAL);
                break;
            case GREEN:
                data = readTwoBytes(Register.GDATAL);
                break;
            case BLUE:
                data = readTwoBytes(Register.BDATAL);
                break;
        }

        return (((int) data[1] << 8) & 0xFF00) | ((int) data[0] & 0xFF);
    }

    public int readRawProximity() {
        return readByte(Register.PDATA);
    }

    private byte readByte(Register register) {
        byte[] read_byte = new byte[1];

        m_i2cInterface.read(register.getAddress(), k_singleByte, read_byte);

        return read_byte[0];
    }

    private byte[] readTwoBytes(Register register) {
        byte[] read_bytes = new byte[2];

        m_i2cInterface.read(register.getAddress(), k_twoBytes, read_bytes);

        return read_bytes;
    }

    private void writeByte(Register register, byte data) {
        m_i2cInterface.write(register.getAddress(), data);
    }
}
