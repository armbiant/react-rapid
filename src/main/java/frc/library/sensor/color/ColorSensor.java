/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2/. */

package frc.library.sensor.color;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class ColorSensor {
    public enum ColorChannel {
        RED,
        GREEN,
        BLUE,
        CLEAR,
        YELLOW,
        ORANGE,
        VIOLET,
        CYAN
    }

    /**
     * R, G, and B shall all be [0, 1]
     */
    public static class RGB {
        public final double red;
        public final double green;
        public final double blue;

        public RGB(double r, double g, double b) {
            red = r;
            green = g;
            blue = b;
        }

        public HSV toHsv() {
            double colorMax = Math.max(red, Math.max(green, blue));
            double colorMin = Math.min(red, Math.min(green, blue));
            double hue = 0;
            double saturation = 0;
            double value = 0;
            double delta = colorMax - colorMin;

            if (red > blue && red > green) {
                hue = (60.0 * ((green - blue) / delta) + 360.0) % 360.0;
            } else if (green > blue && green > red) {
                hue = (60.0 * ((blue - red) / delta) + 120.0) % 360.0;
            } else {
                hue = (60 * ((red - green) / delta) + 240.0) % 360.0;
            }
            hue /= 2.0;

            saturation = delta / colorMax * 255.0;
            value = colorMax * 255.0;
            return new HSV(hue, saturation, value);
        }
    }

    /**
     * H shall be [0, 180]
     * S and V shall both be [0, 255]
     */
    public static class HSV {
        public final double hue;
        public final double saturation;
        public final double value;

        public HSV(double h, double s, double v) {
            hue = h;
            saturation = s;
            value = v;
        }
    }

    /**
     * Gets the current Red Green Blue intensity measurement. Channel values are
     * normalized relative to the ADC size.
     *
     * @return returns RGB value. Returns null if there isn't one or the device
     *         isn't ready.
     */
    public RGB getRgb() {
        return new RGB(getColor(ColorChannel.RED), getColor(ColorChannel.GREEN), getColor(ColorChannel.BLUE));
    }

    /**
     * Get the HSV representation of the color sensor's RGB channels.
     *
     * @return the measured HSV values.
     */
    public HSV getHsv() {
        return getRgb().toHsv();
    }

    /**
     * Gets all of the color intensity channels that is supported by the device.
     * Channel values are normalized relative to the ADC size.
     *
     * @return a list of color intensity values as a ColorMap.
     */
    public Map<ColorChannel, Double> getAllColors() {
        return getSupportedChannels().stream()
            .collect(Collectors.toMap(c -> c, this::getColor));
    }

    /**
     * Determines if provided color channels are all supported
     * @param channels Channels to test against
     * @return true if all channels are supported
     */
    public boolean supportsChannels(ColorChannel...channels) {
        return supportsChannels(Arrays.asList(channels));
    }

    /**
     * Determines if provided color channels are all supported
     * @param channels Channels to test against
     * @return true if all channels are supported
     */
    public boolean supportsChannels(List<ColorChannel> channels) {
        return getSupportedChannels().containsAll(channels);
    }

    /**
     * Gets a specific color intensity from the device if the device supports the
     * color. Channel values are normalized relative to the ADC size.
     *
     * @param color The color channel to get.
     * @return the color intensity of the given color. Returns NaN if the color
     *         isn't supported.
     */
    public abstract double getColor(ColorChannel color);

    /**
     * Gets a set containing all channels supported by the device.
     *
     * @return a set of all supported channels
     */
    public abstract Set<ColorChannel> getSupportedChannels();
}
