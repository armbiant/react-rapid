/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2/. */

package frc.library.sensor.color;

import java.util.Set;

import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.math.Time;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.signals.EdgeDetector;
import org.growingstems.util.timer.TimerI;

import frc.library.Periodic;
import frc.library.sensor.I2CInterface;

/**
 * Based on Adafruit's AS7262 Color sensor.
 * <p>
 * This class is provided <b>AS IS</b> and is not fully supported currently. It proved
 * to be too difficult to provide a real time solution at a 50hz rate without
 * running into complications with device readiness. The issue lies in the fact
 * that this device utilizes an unconventional "virtual address" system on top of
 * the I2C protocol that requires checking for register readiness. This register
 * readiness can potentially cause issues on a single threaded robot project.
 * This data readiness is unpredictable and can either slow down code, or if
 * timeout period is too long, provide unstable data.
 * <p>
 * Resources:
 * <ul>
 * <li>https://www.adafruit.com/product/3779</li>
 * <li>https://cdn-learn.adafruit.com/assets/assets/000/052/623/original/AS7262_DS000486_2-00_%281%29.pdf?1522179774</li>
 * <li>https://github.com/adafruit/Adafruit_AS726x</li>
 * </ul>
 */
public class AS7262 extends ColorSensor implements Periodic {
    public enum VirtualRegister {
        HW_VERSION(0x00),
        FW_VERSION(0x02),
        CONTROL_SETUP(0x04),
        INT_T(0x05),
        DEVICE_TEMP(0x06),
        LED_CONTROL(0x07),

        V_HIGH(0x08),
        V_LOW(0x09),
        B_HIGH(0x0A),
        B_LOW(0x0B),
        G_HIGH(0x0C),
        G_LOW(0x0D),
        Y_HIGH(0x0E),
        Y_LOW(0x0F),
        O_HIGH(0x10),
        O_LOW(0x11),
        R_HIGH(0x12),
        R_LOW(0x13),

        V_CAL(0x14),
        B_CAL(0x18),
        G_CAL(0x1C),
        Y_CAL(0x20),
        O_CAL(0x24),
        R_CAL(0x28);

        private final byte m_address;

        private VirtualRegister(int address) {
            m_address = (byte) (address & 0xFF);
        }

        public byte getAddress() {
            return m_address;
        }
    };

    public enum HardwareRegister {
        STATUS(0x00),
        WRITE(0x01),
        READ(0x02);

        private final int m_address;

        private HardwareRegister(int address) {
            m_address = address;
        }

        public int getAddress() {
            return m_address;
        }
    };

    public enum HardwareChannel {
        VIOLET,
        BLUE,
        GREEN,
        YELLOW,
        ORANGE,
        RED;
    };

    public enum ConversionMode {
        /**
         * Data will be available in registers V, B, G and Y (O and R registers will be
         * zero) with conversions occurring continuously.
         */
        MODE_0(0b00),
        /**
         * Data will be available in registers G, Y, O and R (V and B registers will be
         * zero) with conversions occurring continuously.
         */
        MODE_1(0b01),
        /**
         * Hardware Default
         * <p>
         * Data will be available in registers V, B, G, Y, O and R with conversions
         * occurring continuously. When the bank setting is Mode 0, Mode 1, or Mode 2,
         * the spectral data conversion process operates continuously, with new data
         * available after each IT ms period. In the continuous modes, care should be
         * taken to assure prompt interrupt servicing so that integration values from
         * both banks are all derived from the same spectral conversion cycle.
         */
        MODE_2(0b10),
        /**
         * Data will be available in registers V, B, G, Y, O and R in One-Shot mode When
         * the bank setting is set to Mode 3 the device initiates One-Shot operation.
         * The DATA_RDY bit is set to 1 once data is available, indicating spectral
         * conversion is complete. One-Shot mode is intended for use when it is critical
         * to ensure spectral conversion results are obtained contemporaneously. An
         * example use for one-shot mode is when a digitally controlled illumination
         * source is briefly turned on for the purpose of taking a set of filter
         * readings.
         */
        ONE_SHOT(0b11);

        private int m_value;

        private ConversionMode(int value) {
            m_value = value;
        }
    };

    public enum ChannelGain {
        /** Hardware Default */
        GAIN_1X(0b00),
        GAIN_3_7X(0b01),
        GAIN_16X(0b10),
        GAIN_64X(0b11);

        private int m_value;

        private ChannelGain(int value) {
            m_value = value;
        }
    };

    public enum IndicatorLedCurrentLimit {
        /** Hardware Default */
        LIMIT_1MA(0b00),
        LIMIT_2MA(0b01),
        LIMIT_4MA(0b10),
        LIMIT_8MA(0b11);

        private int m_value;

        private IndicatorLedCurrentLimit(int value) {
            m_value = value;
        }
    };

    public enum DriverLedCurrentLimit {
        /** Hardware Default */
        LIMIT_12_5MA(0b00),
        LIMIT_25MA(0b01),
        LIMIT_50MA(0b10),
        LIMIT_100MA(0b11);

        private int m_value;

        private DriverLedCurrentLimit(int value) {
            m_value = value;
        }
    };

    private static class Status {
        public boolean txValid;
        public boolean rxValid;
    }

    private static class HardwareRegisterBase {
        protected int boolToInt(boolean bool) {
            return bool ? 1 : 0;
        }
    }

    private static class ControlSetup extends HardwareRegisterBase {
        public boolean dataReady = false;
        public ConversionMode conversionMode = ConversionMode.MODE_2;
        public ChannelGain channelGain = ChannelGain.GAIN_64X;
        public boolean enableInterrupt = true;
        public boolean reset = false;

        public byte getByte() {
            int data = (boolToInt(dataReady) << 1) | (conversionMode.m_value << 2) | (channelGain.m_value << 4)
                    | (boolToInt(enableInterrupt) << 6) | (boolToInt(reset) << 7);
            return (byte) (data);
        }
    }

    private static class LedControl extends HardwareRegisterBase {
        public boolean enableIndicator = false;
        public IndicatorLedCurrentLimit indicatorCurrentLimit = IndicatorLedCurrentLimit.LIMIT_1MA;
        public boolean enableDriver = false;
        public DriverLedCurrentLimit driverCurrentLimit = DriverLedCurrentLimit.LIMIT_25MA;

        public byte getByte() {
            int data = (boolToInt(enableIndicator) | (indicatorCurrentLimit.m_value << 1)
                    | (boolToInt(enableDriver) << 3) | (driverCurrentLimit.m_value << 4));
            return (byte) (data);
        }
    }

    private static int k_singleByte = 1;
    private static int k_rxValidMask = 0x01;
    private static int k_txValidMask = 0x02;
    private static Time k_timeoutTime = new Time(1.0, TimeUnit.MILLISECONDS);
    private static Time k_resetTime = new Time(1.0, TimeUnit.SECONDS);

    private static byte k_defaultHardwareVersion = 0x40;
    private static int k_maxChannelAdcValue = (int) Math.pow(2, 16) - 1;
    private static int k_maxIntegrationTime = (int) Math.pow(2, 8) - 1;
    private static double k_integrationScalar_ms = 2.8;

    private final I2CInterface m_i2cInterface;
    private final TimerI m_timer;
    private ControlSetup m_controlSetup = new ControlSetup();
    private LedControl m_ledControl = new LedControl();
    private byte m_intergrationTime = 17; // 47.6ms
    private boolean m_hasTimedout = false;
    private boolean m_localHasTimedout = false;
    private Time m_lastResetTime = new Time();
    private boolean m_ready = false;
    private final EdgeDetector m_becameReadyDetector = new EdgeDetector(Edge.RISING);
    private boolean m_becameReady = false;
    private boolean m_measurementReady = false;
    private boolean m_measuring = false;

    public static final int k_deviceAddress = 0x49;

    public AS7262(I2CInterface i2cInterface, TimerI timer) {
        m_i2cInterface = i2cInterface;
        m_timer = timer;
        m_timer.start();
        reset();
    }

    @Override
    public RGB getRgb() {
        if (isReady()) {
            double red = getColor(ColorChannel.RED);
            double green = getColor(ColorChannel.GREEN);
            double blue = getColor(ColorChannel.BLUE);
            return new RGB(red, green, blue);
        } else {
            return null;
        }
    }

    @Override
    public double getColor(ColorChannel color) {
        HardwareChannel deviceColor = getHardwareChannel(color);

        if (deviceColor != null) {
            return (double) readRawColor(deviceColor) / (double) k_maxChannelAdcValue;
        } else {
            return Double.NaN;
        }
    }

    @Override
    public Set<ColorChannel> getSupportedChannels() {
        return Set.of(ColorChannel.RED, ColorChannel.GREEN, ColorChannel.BLUE, ColorChannel.YELLOW, ColorChannel.ORANGE, ColorChannel.VIOLET);
    }

    @Override
    public void update() {
        m_ready = m_timer.get().subtract(m_lastResetTime).getValue(TimeUnit.SECONDS) > k_resetTime
                .getValue(TimeUnit.SECONDS);
        m_becameReady = m_becameReadyDetector.update(m_ready);

        if (m_becameReady) {
            setControlSetup();
            setLedControl();
            setIntegrationTime();
        }

        if (isReady()) {
            if (m_measuring) {
                m_measurementReady = dataReady();
                m_measuring = !m_measurementReady;
            }
        }
    }

    /**
     * Returns if the device is ready to be used after update has been called.
     *
     * @return ready status
     */
    public boolean isReady() {
        return m_ready;
    }

    /**
     * Returns if the device just became ready after update has been called.
     *
     * @return became ready status
     */
    public boolean becameReady() {
        return m_becameReady;
    }

    /**
     * Indicates if a new measurement has been made available in the channel
     * registers after update has been called.
     *
     * @return measurment ready status
     */
    public boolean measurementReady() {
        return m_measurementReady;
    }

    /**
     * Checks if the device is connected and communicating.
     *
     * @return true if the device is connected and communicating or if the driver
     *         timedout trying to read the virtual register.
     */
    public boolean verify() {
        if (!isReady()) {
            return false;
        }

        byte data = readVirtualByte(VirtualRegister.HW_VERSION);

        if (m_localHasTimedout) {
            m_localHasTimedout = false;
            return false;
        }

        return data == k_defaultHardwareVersion;
    }

    /**
     * Get's value from device temperature register.
     *
     * @return device temperature in degrees celsius. -1 if the driver timedout
     *         trying to read the virtual register.
     */
    public int getTemperature() {
        byte data = readVirtualByte(VirtualRegister.DEVICE_TEMP);

        if (m_localHasTimedout) {
            m_localHasTimedout = false;
            return -1;
        }

        return (int) data & 0xFF;
    }

    public void getMeasurement() {
        if (!isReady()) {
            return;
        }

        m_measuring = true;
        m_controlSetup.dataReady = false;
        setControlSetup();
        setConversionMode(ConversionMode.ONE_SHOT);
    }

    private boolean dataReady() {
        byte data = readVirtualByte(VirtualRegister.CONTROL_SETUP);

        if (m_localHasTimedout) {
            m_localHasTimedout = false;
            return false;
        }

        return (data & 0x02) > 0;
    }

    /**
     * Check if the device's driver timedout while waiting for the virtual register
     * to update the driver. Resets the timedout variable when this function is
     * called.
     *
     * @return whether or not the device driver has timedout since the last time
     *         this
     *         function was called.
     */
    public boolean hasTimedout() {
        boolean hasTimedout = m_hasTimedout;
        m_hasTimedout = false;
        return hasTimedout;
    }

    /**
     * Reads the raw color device registers filled by the 16 bit ADCs. Use
     * measurementReady() after update() is called to check if the values in this
     * register is new.
     *
     * @param color The calibrated color channel to read.
     * @return the value returned by the device. -1 if the device isn't ready. 0 can
     *         potentially mean that the driver timedout while trying to read the
     *         virtual register.
     */
    public int readRawColor(HardwareChannel color) {
        if (!isReady()) {
            return -1;
        }

        return readRawChannel(color);
    }

    /**
     * Reads the calibrated color device registers. Currently unknown what these
     * values are relative to. Use measurementReady() after update() is called to
     * check if the values in this register is new.
     *
     * @param color The calibrated color channel to read.
     * @return the value returned by the device
     */
    public double readCalibratedColor(HardwareChannel color) {
        if (!isReady()) {
            return Double.NaN;
        }

        return readCalibratedChannel(color);
    }

    /**
     * Enables the illuminating driver LED.
     */
    public void enableDriverLed() {
        m_ledControl.enableDriver = true;
        setLedControl();
    }

    /**
     * Disables the illuminating driver LED.
     */
    public void disableDriverLed() {
        m_ledControl.enableDriver = false;
        setLedControl();
    }

    /**
     * Sets the illuminating driver LED's current limit.
     *
     * @param current illuminating driver LED current limit
     */
    public void setDriverLedCurrent(DriverLedCurrentLimit current) {
        m_ledControl.driverCurrentLimit = current;
        setLedControl();
    }

    /**
     * Enables the indicator LED.
     */
    public void enableIndicatorLed() {
        m_ledControl.enableIndicator = true;
        setLedControl();
    }

    /**
     * Disables the indicator LED.
     */
    public void disableIndicatorLed() {
        m_ledControl.enableIndicator = false;
        setLedControl();
    }

    /**
     * Sets the indicator LED's current limit.
     *
     * @param current indicator LED current limit
     */
    public void setIndicatorLedCurrent(IndicatorLedCurrentLimit current) {
        m_ledControl.indicatorCurrentLimit = current;
        setLedControl();
    }

    private void setLedControl() {
        if (!isReady()) {
            return;
        }

        writeVirtualByte(VirtualRegister.LED_CONTROL, m_ledControl.getByte());
    }

    /**
     * Integration time is time * 2.8ms.
     *
     * @param time (desired integration time) / 2.8ms
     */
    public void setIntegrationTime(byte time) {
        if (!isReady()) {
            return;
        }

        m_intergrationTime = time;
    }

    /**
     * Sets integration time of device. Time will be rounded down to the next
     * available value.
     *
     * @param time device integration time.
     */
    public void setIntegrationTime(Time time) {
        if (!isReady()) {
            return;
        }

        if (time.getValue(TimeUnit.MILLISECONDS) > ((double) k_maxIntegrationTime) * k_integrationScalar_ms) {
            m_intergrationTime = (byte) k_maxIntegrationTime;
        } else if (time.getValue(TimeUnit.MILLISECONDS) < 0.0) {
            m_intergrationTime = 0;
        } else {
            m_intergrationTime = (byte) Math.floor(time.getValue(TimeUnit.MILLISECONDS) * k_integrationScalar_ms);
        }

        setIntegrationTime();
    }

    private void setIntegrationTime() {
        writeVirtualByte(VirtualRegister.INT_T, (byte) (m_intergrationTime));
    }

    /**
     * Resets the device via software. Check isReady() to see when device is ready
     * again after reset.
     */
    public void reset() {
        m_measuring = false;
        m_controlSetup.reset = true;
        setControlSetup();
        m_controlSetup.reset = false;
        m_lastResetTime = m_timer.get();
    }

    /**
     * Sets device color conversion mode. Refer to ConversionMode's Javadoc for more
     * info.
     *
     * @param mode color conversion mode
     */
    public void setConversionMode(ConversionMode mode) {
        m_controlSetup.conversionMode = ConversionMode.ONE_SHOT;
        setControlSetup();
    }

    /**
     * Sets gain of all color channel ADC's.
     *
     * @param gain ADC gain for all color channels
     */
    public void setGain(ChannelGain gain) {
        m_controlSetup.channelGain = gain;
        setControlSetup();
    }

    /**
     * Enables device interrupt.
     */
    public void enableInterrupt() {
        m_controlSetup.enableInterrupt = true;
        setControlSetup();
    }

    /**
     * Disables device interrupt.
     */
    public void disableInterrupt() {
        m_controlSetup.enableInterrupt = false;
        setControlSetup();
    }

    private void setControlSetup() {
        if (!isReady() && !m_controlSetup.reset) {
            return;
        }

        writeVirtualByte(VirtualRegister.CONTROL_SETUP, m_controlSetup.getByte());
    }

    private HardwareChannel getHardwareChannel(ColorChannel color) {
        switch (color) {
            case RED:
                return HardwareChannel.RED;
            case GREEN:
                return HardwareChannel.GREEN;
            case BLUE:
                return HardwareChannel.BLUE;
            case YELLOW:
                return HardwareChannel.YELLOW;
            case ORANGE:
                return HardwareChannel.ORANGE;
            case VIOLET:
                return HardwareChannel.VIOLET;
            default:
                return null;
        }
    }

    private int readRawChannel(HardwareChannel channel) {
        VirtualRegister low;
        VirtualRegister high;

        switch (channel) {
            case VIOLET:
                low = VirtualRegister.V_LOW;
                high = VirtualRegister.V_HIGH;
                break;
            case BLUE:
                low = VirtualRegister.B_LOW;
                high = VirtualRegister.B_HIGH;
                break;
            case GREEN:
                low = VirtualRegister.G_LOW;
                high = VirtualRegister.G_HIGH;
                break;
            case YELLOW:
                low = VirtualRegister.Y_LOW;
                high = VirtualRegister.Y_HIGH;
                break;
            case ORANGE:
                low = VirtualRegister.O_LOW;
                high = VirtualRegister.O_HIGH;
                break;
            case RED:
                low = VirtualRegister.R_LOW;
                high = VirtualRegister.R_HIGH;
                break;
            default:
                return -1;
        }

        byte lowRaw = readVirtualByte(low);
        byte highRaw = readVirtualByte(high);

        if (m_localHasTimedout) {
            m_localHasTimedout = false;
            return 0;
        }

        return (((int) highRaw & 0xFF) << 8) | ((int) lowRaw & 0xFF);
    }

    private float readCalibratedChannel(HardwareChannel channel) {
        VirtualRegister reg;

        switch (channel) {
            case VIOLET:
                reg = VirtualRegister.V_CAL;
                break;
            case BLUE:
                reg = VirtualRegister.B_CAL;
                break;
            case GREEN:
                reg = VirtualRegister.G_CAL;
                break;
            case YELLOW:
                reg = VirtualRegister.Y_CAL;
                break;
            case ORANGE:
                reg = VirtualRegister.O_CAL;
                break;
            case RED:
                reg = VirtualRegister.R_CAL;
                break;
            default:
                return -1;
        }

        byte raw0 = readVirtualByte(reg, 0);
        byte raw1 = readVirtualByte(reg, 1);
        byte raw2 = readVirtualByte(reg, 2);
        byte raw3 = readVirtualByte(reg, 3);

        if (m_localHasTimedout) {
            m_localHasTimedout = false;
            return Float.NaN;
        }

        int asInt = ((int) raw3 & 0xFF)
                | (((int) raw2 & 0xFF) << 8)
                | (((int) raw1 & 0xFF) << 16)
                | (((int) raw0 & 0xFF) << 24);

        return Float.intBitsToFloat(asInt);
    }

    private Status checkStatus() {
        Status status = new Status();
        byte raw = readByte(HardwareRegister.STATUS);
        status.txValid = !((raw & k_txValidMask) != 0);
        status.rxValid = ((raw & k_rxValidMask) != 0);
        return status;
    }

    private byte readVirtualByte(VirtualRegister register) {
        return readVirtualByte(register, 0);
    }

    private byte readVirtualByte(VirtualRegister register, int offset) {
        byte regAddress = (byte) ((register.m_address + offset) & 0xFF);
        Status status = checkStatus();
        Time start = m_timer.get();
        while (!status.txValid) {
            if (m_timer.get().subtract(start).getValue(TimeUnit.SECONDS) > k_timeoutTime.getValue(TimeUnit.SECONDS)) {
                m_hasTimedout = true;
                m_localHasTimedout = true;
                return 0x0;
            }
            status = checkStatus();
        }

        writeByte(HardwareRegister.WRITE, regAddress);

        status = checkStatus();
        start = m_timer.get();
        while (!status.rxValid) {
            if (m_timer.get().subtract(start).getValue(TimeUnit.SECONDS) > k_timeoutTime.getValue(TimeUnit.SECONDS)) {
                m_hasTimedout = true;
                m_localHasTimedout = true;
                return 0x0;
            }
            status = checkStatus();
        }

        return readByte(HardwareRegister.READ);
    }

    private void writeVirtualByte(VirtualRegister register, byte data) {
        Status status = checkStatus();
        Time start = m_timer.get();
        while (!status.txValid) {
            if (m_timer.get().subtract(start).getValue(TimeUnit.SECONDS) > k_timeoutTime.getValue(TimeUnit.SECONDS)) {
                m_hasTimedout = true;
                m_localHasTimedout = true;
                return;
            }
            status = checkStatus();
        }

        writeByte(HardwareRegister.WRITE, (byte) ((register.m_address | 0x80) & 0xFF));

        status = checkStatus();
        start = m_timer.get();
        while (!status.txValid) {
            if (m_timer.get().subtract(start).getValue(TimeUnit.SECONDS) > k_timeoutTime.getValue(TimeUnit.SECONDS)) {
                m_hasTimedout = true;
                m_localHasTimedout = true;
                return;
            }
            status = checkStatus();
        }

        writeByte(HardwareRegister.WRITE, data);
    }

    private byte readByte(HardwareRegister register) {
        return readByte((byte) register.getAddress());
    }

    private byte readByte(int address) {
        byte[] read_byte = new byte[1];

        m_i2cInterface.read(address, k_singleByte, read_byte);

        return read_byte[0];
    }

    private void writeByte(HardwareRegister register, byte data) {
        writeByte((byte) register.getAddress(), data);
    }

    private void writeByte(byte address, byte data) {
        m_i2cInterface.write(address, data);
    }
}
