/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.sensor;

import java.nio.ByteBuffer;

public interface I2CInterface {
    public abstract void close();

    public abstract boolean read(int registerAddress, int count, byte[] buffer);

    public abstract boolean read(int registerAddress, int count, ByteBuffer buffer);

    public abstract boolean readOnly(int count, byte[] buffer);

    public abstract boolean readOnly(int count, ByteBuffer buffer);

    public abstract boolean transaction(byte[] dataToSend, int sendSize, byte[] dataReceived, int receiveSize);

    public abstract boolean transaction(ByteBuffer dataToSend, int sendSize, ByteBuffer dataReceived, int receiveSize);

    public abstract boolean write(int registerAddress, byte data);

    public abstract boolean writeBulk(byte[] data);

    public abstract boolean writeBulk(byte[] data, int size);

    public abstract boolean writeBulk(ByteBuffer data, int size);
}
