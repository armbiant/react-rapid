/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.sensor;

import java.nio.ByteBuffer;

import edu.wpi.first.hal.I2CJNI;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.I2C.Port;

public class I2CRoboRIO extends I2CBase {
    protected I2C m_i2c;
    private static boolean m_initialClose = false;

    /**
     * Create RoboRio I2C device. MXP port is highly recommended as there is a known issue with the onboard I2C bus. Must call startI2C after devices have been initialized.
     *
     * @param port Which RoboRio I2C port to use. Highly recommend using MXP port.
     * @param deviceAddress The I2C device address.
     */
    public I2CRoboRIO(Port port, int deviceAddress) {
        super(deviceAddress);
        m_i2c = new I2C(port, deviceAddress);
        // Remove code if ability to reset I2C exists
        close();
        checkInitialClose();
    }

    public static void checkInitialClose() {
        if (!m_initialClose) {
            stopI2C(Port.kMXP);
            m_initialClose = true;
        }
    }

    /**
     * Starts the I2C bus. Currently, the HAL implementation of I2C has a counter
     * tracking how many devices opened/closed the MXP I2C bus, so this function does not
     * guarantee that the bus will be correctly started.
     *
     * @param port Which RoboRio I2C port to use.
     */
    public static void startI2C(Port port) {
        // https://github.com/wpilibsuite/allwpilib/blob/b6f44f98befa7943871de38d2e3c077e5979c13f/hal/src/main/native/athena/I2C.cpp#L29
        try {
            I2CJNI.i2CInitialize(port.value);
        } catch (edu.wpi.first.hal.util.AllocationException exception) {
            DriverStation.reportWarning("Caught exception Allocation Exception - " + exception.toString(), false);
        }
    }

    /**
     * Stops the I2C bus. Currently, the HAL implementation of I2C has a counter
     * tracking how many devices opened/closed the MXP I2C bus, so this function does not
     * guarantee that the bus will be correctly stopped.
     *
     * @param port Which RoboRio I2C port to use.
     */
    public static void stopI2C(Port port) {
        // https://github.com/wpilibsuite/allwpilib/blob/b6f44f98befa7943871de38d2e3c077e5979c13f/hal/src/main/native/athena/I2C.cpp#L29
        I2CJNI.i2CClose(port.value);
    }

    /**
     * Starts the I2C bus. Currently, the HAL implementation of I2C has a counter
     * tracking how many devices opened/closed the MXP I2C bus, so this function does not
     * guarantee that the bus will be correctly reset.
     *
     * @param port Which RoboRio I2C port to use.
     */
    public static void resetI2C(Port port) {
        stopI2C(port);
        startI2C(port);
    }

    public I2C getI2C() {
        return m_i2c;
    }

    @Override
    public void close() {
        m_i2c.close();
    }

    @Override
    public boolean read(int registerAddress, int count, byte[] buffer) {
        return m_i2c.read(registerAddress, count, buffer);
    }

    @Override
    public boolean read(int registerAddress, int count, ByteBuffer buffer) {
        return m_i2c.read(registerAddress, count, buffer);
    }

    @Override
    public boolean readOnly(int count, byte[] buffer) {
        return m_i2c.readOnly(buffer, count);
    }

    @Override
    public boolean readOnly(int count, ByteBuffer buffer) {
        return m_i2c.readOnly(buffer, count);
    }

    @Override
    public boolean transaction(byte[] dataToSend, int sendSize, byte[] dataReceived, int receiveSize) {
        return m_i2c.transaction(dataToSend, sendSize, dataReceived, receiveSize);
    }

    @Override
    public boolean transaction(ByteBuffer dataToSend, int sendSize, ByteBuffer dataReceived, int receiveSize) {
        return m_i2c.transaction(dataToSend, sendSize, dataReceived, receiveSize);
    }

    @Override
    public boolean write(int registerAddress, byte data) {
        return m_i2c.write(registerAddress, data);
    }

    @Override
    public boolean writeBulk(byte[] data) {
        return m_i2c.writeBulk(data);
    }

    @Override
    public boolean writeBulk(byte[] data, int size) {
        return m_i2c.writeBulk(data, size);
    }

    @Override
    public boolean writeBulk(ByteBuffer data, int size) {
        return m_i2c.writeBulk(data, size);
    }
}
