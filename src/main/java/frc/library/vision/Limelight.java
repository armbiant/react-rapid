/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.vision;

import java.util.ArrayList;
import java.util.List;

import org.growingstems.geometry.Vector3d;
import org.growingstems.math.Angle;
import org.growingstems.math.Angle.AngleUnit;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.DriverStation;

public class Limelight extends VisionProcessor {
    public enum Model {
        LL1,
        LL2,
        LL2_PLUS
    }

    public enum LightMode {
        PIPELINE(0),
        FORCE_OFF(1),
        FORCE_BLINK(2),
        FORCE_ON(3);

        private int m_value;
        private LightMode(int value) {
            m_value = value;
        }

        int getValue() {
            return m_value;
        }
    }

    // Limelight 1 Constants
    private static final Angle k_ll1HorizFov = new Angle(54.0, AngleUnit.DEGREES);
    private static final Angle k_ll1VertFov = new Angle(41.0, AngleUnit.DEGREES);
    private static final int k_ll1HorizRes = 320;
    private static final int k_ll1VertRes = 240;

    // Limelight 2 Constants
    private static final Angle k_ll2HorizFov = new Angle(59.6, AngleUnit.DEGREES);
    private static final Angle k_ll2VertFov = new Angle(49.7, AngleUnit.DEGREES);
    private static final int k_ll2HorizRes = 320;
    private static final int k_ll2VertRes = 240;

    // Limelight 2+ Constants
    private static final Angle k_ll2PHorizFov = new Angle(59.6, AngleUnit.DEGREES);
    private static final Angle k_ll2PVertFov = new Angle(49.7, AngleUnit.DEGREES);
    private static final int k_ll2PHorizRes = 320;
    private static final int k_ll2PVertRes = 240;

    private static final NetworkTable k_defaultNetworkTable  = NetworkTableInstance.getDefault().getTable("limelight");
    private static final NetworkTableEntry k_keyValidTarget  = k_defaultNetworkTable.getEntry("tv");
    private static final NetworkTableEntry k_keyTargetAngleX = k_defaultNetworkTable.getEntry("tx");
    private static final NetworkTableEntry k_keyTargetAngleY = k_defaultNetworkTable.getEntry("ty");
    private static final NetworkTableEntry k_keyCorners      = k_defaultNetworkTable.getEntry("tcornxy");
    private static final NetworkTableEntry k_ledMode         = k_defaultNetworkTable.getEntry("ledMode");
    private static final NetworkTableEntry k_camMode         = k_defaultNetworkTable.getEntry("camMode");
    private static final NetworkTableEntry k_stream          = k_defaultNetworkTable.getEntry("stream");
    protected Model m_model;

    public Limelight(Vector3d position, Angle pitch, Angle yaw) {
        this(position, pitch, yaw, Orientation.NORMAL, Model.LL2_PLUS);
    }

    public Limelight(Vector3d position, Angle pitch, Angle yaw, Orientation orientation) {
        this(position, pitch, yaw, orientation, Model.LL2_PLUS);
    }

    public Limelight(Vector3d position, Angle pitch, Angle yaw, Orientation orientation, Model model) {
        super(position, pitch, yaw, new Angle(), new Angle(), 0, 0, orientation);

        switch (model) {
            case LL1:
                m_horizontalFov = k_ll1HorizFov;
                m_verticalFov = k_ll1VertFov;
                m_horizontalResolution = k_ll1HorizRes;
                m_verticalResolution = k_ll1VertRes;
                break;
            case LL2:
                m_horizontalFov = k_ll2HorizFov;
                m_verticalFov = k_ll2VertFov;
                m_horizontalResolution = k_ll2HorizRes;
                m_verticalResolution = k_ll2VertRes;
                break;
            case LL2_PLUS:
                m_horizontalFov = k_ll2PHorizFov;
                m_verticalFov = k_ll2PVertFov;
                m_horizontalResolution = k_ll2PHorizRes;
                m_verticalResolution = k_ll2PVertRes;
                break;
        }

        m_model = model;

        calculateTans();

        k_stream.setNumber(2);
    }

    public void setZoom2X() {
        Angle horiFov = new Angle();
        Angle vertFov = new Angle();

        switch (m_model) {
            case LL1:
                horiFov = k_ll1HorizFov;
                vertFov = k_ll1VertFov;
                break;
            case LL2:
                horiFov = k_ll2HorizFov;
                vertFov = k_ll2VertFov;
                break;
            case LL2_PLUS:
                horiFov = k_ll2PHorizFov;
                vertFov = k_ll2PVertFov;
                break;
        }
        setFov(vertFov.scale(0.5), horiFov.scale(0.5));
    }

    public void setZoom1X() {
        Angle horiFov = new Angle();
        Angle vertFov = new Angle();

        switch (m_model) {
            case LL1:
                horiFov = k_ll1HorizFov;
                vertFov = k_ll1VertFov;
                break;
            case LL2:
                horiFov = k_ll2HorizFov;
                vertFov = k_ll2VertFov;
                break;
            case LL2_PLUS:
                horiFov = k_ll2PHorizFov;
                vertFov = k_ll2PVertFov;
                break;
        }
        setFov(vertFov, horiFov);
    }

    public void setLightMode(LightMode mode) {
        k_ledMode.setNumber(mode.getValue());
    }

    public void setSaMode(boolean enable) {
        int enableValue = enable ? 1 : 0;
        k_camMode.setNumber(enableValue);
    }

    @Override
    public boolean hasValidTarget() {
        return k_keyValidTarget.getNumber(0).intValue() != 0;
    }

    @Override
    public YawPitch getTargetAngles() {
        Angle yaw = new Angle(k_keyTargetAngleX.getDouble(Double.NaN), AngleUnit.DEGREES).negate();
        Angle pitch = new Angle(k_keyTargetAngleY.getDouble(Double.NaN), AngleUnit.DEGREES);
        return reorientCameraYawPitch(new YawPitch(yaw, pitch));
    }

    @Override
    public BoundingBox getTargetBoundingBox() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ScreenPos> getTargetCorners() {
        ArrayList<ScreenPos> result = new ArrayList<ScreenPos>();
        Number[] cornerCoords = k_keyCorners.getNumberArray(new Number[]{});

        int i = 0;
        while (i+1 < cornerCoords.length) {
            int x = cornerCoords[i++].intValue();
            int y = cornerCoords[i++].intValue();
            result.add(reorientCameraScreenPos(new ScreenPos(x, y)));
        }

        if (i != cornerCoords.length) {
            DriverStation.reportWarning("Limelight returned corner array that didn't fully map to x/y coordinates.", false);
        }

        return result;
    }

}
