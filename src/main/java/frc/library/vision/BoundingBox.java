/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.vision;

public class BoundingBox {
    protected ScreenPos m_position;
    protected int m_height;
    protected int m_width;

    public BoundingBox(ScreenPos position, int height, int width) {
        m_position = position;
        m_height = height;
        m_width = width;
    }
}
