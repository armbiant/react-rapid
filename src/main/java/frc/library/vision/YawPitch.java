/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.vision;

import org.growingstems.math.Angle;

public class YawPitch {
    protected Angle m_yaw;
    protected Angle m_pitch;

    public YawPitch(Angle yaw, Angle pitch) {
        m_yaw = yaw;
        m_pitch = pitch;
    }

    public Angle getYaw() {
        return m_yaw;
    }

    public Angle getPitch() {
        return m_pitch;
    }
}
