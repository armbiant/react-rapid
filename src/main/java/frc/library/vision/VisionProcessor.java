/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library.vision;

import java.util.ArrayList;
import java.util.List;

import org.growingstems.geometry.Vector3d;
import org.growingstems.math.Angle;
import org.growingstems.math.Range;
import org.growingstems.math.Angle.AngleUnit;

/**
 * Pixel coordinates start at (0,0) at the top-left corner of the screen.
 */
public class VisionProcessor {
    protected Vector3d m_position;
    protected Angle m_pitch;
    protected Angle m_yaw;
    protected Angle m_verticalFov;
    protected Angle m_horizontalFov;
    protected int m_verticalResolution;
    protected int m_horizontalResolution;
    Orientation m_orientation;
    private double m_verticalTan = 0.0;
    private double m_horizontalTan = 0.0;
    private Range m_verticalRange;
    private Range m_horizontalRange;

    /**
     * The orientation of the camera from the perspective of the camera's image.
     */
    public static enum Orientation { NORMAL, ROT_90_DEG_CW, ROT_90_DEG_CCW, ROT_180_DEG }

    public VisionProcessor(Vector3d position,
                           Angle pitch,
                           Angle yaw,
                           Angle verticalFov,
                           Angle horizontalFov,
                           int verticalResolution,
                           int horizontalResolution,
                           Orientation orientation) {
        m_position = position;
        m_pitch = pitch;
        m_yaw = yaw;
        setFov(verticalFov, horizontalFov);
        m_verticalResolution = verticalResolution;
        m_horizontalResolution = horizontalResolution;
        m_orientation = orientation;
        calculateTans();
        reorientMemberVariables();
    }

    public VisionProcessor(Angle verticalFov,
                           Angle horizontalFov,
                           int verticalResolution,
                           int horizontalResolution,
                           Orientation orientation) {
        this(new Vector3d(),
             new Angle(),
             new Angle(),
             verticalFov,
             horizontalFov,
             verticalResolution,
             horizontalResolution,
             orientation);
    }

    public VisionProcessor(Angle verticalFov, Angle horizontalFov, int verticalResolution, int horizontalResolution) {
        this(new Vector3d(),
             new Angle(),
             new Angle(),
             verticalFov,
             horizontalFov,
             verticalResolution,
             horizontalResolution,
             Orientation.NORMAL);
    }

    protected final void calculateTans() {
        m_verticalTan = m_verticalFov.scale(0.5).tan();
        m_horizontalTan = m_horizontalFov.scale(0.5).tan();
    }

    protected final void setFov(Angle vert, Angle hori) {
        m_verticalFov = vert;
        m_horizontalFov = hori;
        calculateTans();
        m_verticalRange = new Range(-m_verticalFov.getValue(AngleUnit.RADIANS), m_verticalFov.getValue(AngleUnit.RADIANS));
        m_horizontalRange = new Range(-m_horizontalFov.getValue(AngleUnit.RADIANS),m_horizontalFov.getValue(AngleUnit.RADIANS));
    }

    protected final double getVerticalTan() {
        return m_verticalTan;
    }

    protected final double getHorizontalTan() {
        return m_horizontalTan;
    }

    public Vector3d getPosition() {
        return m_position;
    }

    public Angle getPitch() {
        return m_pitch;
    }

    public Angle getYaw() {
        return m_yaw;
    }

    public Angle getVerticalFov() {
        return m_verticalFov;
    }

    public Angle getHorizontalFov() {
        return m_horizontalFov;
    }

    public int getVerticalResolution() {
        return m_verticalResolution;
    }

    public int getHorizontalResolution() {
        return m_horizontalResolution;
    }

    protected void reorientMemberVariables() {
        if (m_orientation == Orientation.ROT_90_DEG_CCW || m_orientation == Orientation.ROT_90_DEG_CW) {
            Angle tempAngle = m_horizontalFov;
            m_horizontalFov = m_verticalFov;
            m_verticalFov = tempAngle;

            int tempResolution = m_horizontalResolution;
            m_horizontalResolution = m_verticalResolution;
            m_verticalResolution = tempResolution;
        }
    }

    /**
     * The resolution and FOV member variables are already re-oriented. This should
     * only be used to re-orient the data coming from the camera, as it will be
     * outputting as if it is oriented normally.
     *
     * @param screenPos Screen position data coming straight from a camera processor.
     * @return Re-oriented screen position based on the orientation of the camera.
     */
    protected ScreenPos reorientCameraScreenPos(ScreenPos screenPos) {

        //TODO: Test this functionality more rigorously
        switch (m_orientation) {
            case NORMAL:
                return screenPos;
            case ROT_90_DEG_CW:
                return new ScreenPos(m_horizontalResolution - screenPos.getY(), screenPos.getX());
            case ROT_90_DEG_CCW:
                return new ScreenPos(screenPos.getY(), m_verticalResolution - screenPos.getX());
            case ROT_180_DEG:
                return new ScreenPos(m_horizontalResolution - screenPos.getX(),
                                     m_verticalResolution - screenPos.getY());
            default:
                return null;
        }
    }

    protected YawPitch reorientCameraYawPitch(YawPitch yawPitch) {
        switch (m_orientation) {
            case NORMAL:
                return yawPitch;
            case ROT_90_DEG_CW:
                return new YawPitch(yawPitch.getPitch().negate(), yawPitch.getYaw());
            case ROT_90_DEG_CCW:
                return new YawPitch(yawPitch.getPitch(), yawPitch.getYaw().negate());
            case ROT_180_DEG:
                return new YawPitch(yawPitch.getYaw().negate(), yawPitch.getPitch().negate());
            default:
                return null;
        }
    }

    //TODO test this more thoroughly
    public YawPitch getYawPitchFromPos(ScreenPos pos) {
        return new YawPitch(getYawFromPos(pos), getPitchFromPos(pos));
    }

    public Angle getPitchFromPos(ScreenPos pos) {
        double verticalRun = ((m_verticalResolution / 2.0) / m_verticalTan);
        Angle pitch = Angle.atan2((m_verticalResolution / 2.0) - pos.getY(), verticalRun);
        double verticalClamp = m_verticalRange.coerceValue(pitch.getValue(AngleUnit.RADIANS));

        return new Angle(verticalClamp, AngleUnit.RADIANS);
    }

    public Angle getYawFromPos(ScreenPos pos) {
        double horizontalRun = ((m_horizontalResolution / 2.0) / m_horizontalTan);
        Angle yaw = Angle.atan2((m_horizontalResolution / 2.0) - pos.getX(), horizontalRun);
        double horizontalClamp = m_horizontalRange.coerceValue(yaw.getValue(AngleUnit.RADIANS));

        return new Angle(horizontalClamp, AngleUnit.RADIANS);
    }

    public boolean hasValidTarget() {
        return false;
    }

    public YawPitch getTargetAngles() {
        return new YawPitch(new Angle(), new Angle());
    }

    public BoundingBox getTargetBoundingBox() {
        return new BoundingBox(new ScreenPos(0, 0), 0, 0);
    }

    public List<ScreenPos> getTargetCorners() {
        return new ArrayList<ScreenPos>();
    }
}
