/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2/. */

package frc.library;

import edu.wpi.first.wpilibj.DriverStation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.String;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.stream.Collectors;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.math.Time;
import org.growingstems.math.Time.TimeUnit;

public class Logger {
    private static final String k_timestampHeader = "Timestamp_s";

    private final WpiTimer m_timer;
    private final Map<String, Object> m_dataMap = new TreeMap<String, Object>();
    private final ConcurrentLinkedQueue<String> m_lines = new ConcurrentLinkedQueue<String>();
    private final ScheduledExecutorService m_scheduler = Executors.newScheduledThreadPool(1);

    private FileWriter m_log = null;
    private ScheduledFuture<?> m_future = null;
    private File m_logFile;

    public Logger(List<String> keys) {
        m_dataMap.put(k_timestampHeader, 0.0);
        for (var key : keys) {
            m_dataMap.put(key, "");
        }

        m_timer = new WpiTimer();
    }

    public void init(File logFile) {
        if (isInitialized()) {
            return;
        }

        try {
            //create the log file and parent directories
            logFile.getParentFile().mkdirs();
            m_log = new FileWriter(logFile);
            m_logFile = logFile;
        } catch (IOException e) {
            e.printStackTrace();
            DriverStation.reportError(e.toString(), true);
        }

        //add the header row for the CSV
        m_lines.add(String.join(",", m_dataMap.keySet()));
        flush();

        //start the timer
        m_timer.start();
    }

    public void startLogging(Time logPeriod) {
        if (isLogging() || !isInitialized()) {
            return;
        }
        long logPeriod_ms = Math.round(logPeriod.getValue(TimeUnit.MILLISECONDS));
        m_future = m_scheduler.scheduleAtFixedRate(this::flush, logPeriod_ms, logPeriod_ms, java.util.concurrent.TimeUnit.MILLISECONDS);
    }

    public void stopLogging() {
        if (!isLogging()) {
            return;
        }
        m_future.cancel(false);
        m_future = null;
    }

    public boolean isInitialized() {
        return m_log != null;
    }

    public boolean isLogging() {
        return m_future != null;
    }

    public boolean keyExists(String key) {
        return m_dataMap.containsKey(key);
    }

    public void setDataPoint(String key, Object val) {
        if (val instanceof Boolean) {
            m_dataMap.put(key, (Boolean)(val) ? 1 : 0);
        } else {
            m_dataMap.put(key, val.toString().replace(',', '_'));
        }
    }

    public void grabLine() {
        if (!isLogging()) {
            return;
        }

        var now = m_timer.get();
        m_dataMap.put(k_timestampHeader, now.getValue(TimeUnit.SECONDS));

        var valueStrings = m_dataMap.values().stream().map(Object::toString).collect(Collectors.toList());
        m_lines.add(String.join(",", valueStrings));
    }

    private void flush() {
        if (!isInitialized()) {
            return;
        }

        if (!m_logFile.exists()) {
            stopLogging();
            try {
                m_log.close();
            } catch (IOException e) {
                System.err.println("Log failed to close.");
            }
            m_log = null;
            return;
        }

        try {
            for (var line : m_lines) {
                m_log.write(line + "\n");
            }
            m_lines.clear();
            m_log.flush();
        } catch (IOException e) {
            DriverStation.reportError("\n\n\n" + e.getMessage() + "\n\n\n", true);
        }
    }
}
