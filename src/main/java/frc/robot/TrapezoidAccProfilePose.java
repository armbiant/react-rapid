/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import org.growingstems.math.Angle;
import org.growingstems.math.Pose2d;
import org.growingstems.math.Angle.AngleUnit;

public class TrapezoidAccProfilePose {
    private final TrapezoidAccProfileDouble m_angleTrapezoid;
    private final TrapezoidAccProfileVector m_translationTrapezoid;
    private final Angle m_startHeading;

    public TrapezoidAccProfilePose(double maxTranslationAccPercent,
                                   double maxTranslationVelPercent,
                                   double maxTurnAccPercent,
                                   double maxTurnVelPercent,
                                   Pose2d to,
                                   Pose2d from) {
        m_startHeading = from.getRotation();
        m_angleTrapezoid = new TrapezoidAccProfileDouble(maxTurnAccPercent,
                                                         maxTurnVelPercent,
                                                         to.getRotation().difference(from.getRotation()).getValue(AngleUnit.DEGREES));
        m_translationTrapezoid = new TrapezoidAccProfileVector(maxTranslationAccPercent,
                                                               maxTranslationVelPercent,
                                                               to.getVector(),
                                                               from.getVector());
    }

    public Pose2d getCurrentPose() {
        // Note: Angle may not be in the range of [-180, 180] degrees
        return new Pose2d(m_translationTrapezoid.getCurrentVector(),
                          new Angle(m_angleTrapezoid.getCurrentDistance(), AngleUnit.DEGREES).add(m_startHeading));
    }
}
