// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SelectCommand;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import edu.wpi.first.wpilibj2.command.button.Trigger;

import frc.library.Length.LengthUnit;
import frc.library.LogEntryPriority;
import frc.robot.constants.LogEntry;
import frc.robot.constants.RobotMap;
import frc.robot.subsystems.Climb;
import frc.robot.subsystems.Drive;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Serializer;
import frc.robot.subsystems.Shooter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.function.Supplier;

import org.growingstems.frc.command.NTrigger;
import org.growingstems.frc.util.WpiTimer;
import org.growingstems.math.Angle;
import org.growingstems.math.Pose2d;
import org.growingstems.math.Range;
import org.growingstems.math.Vector2d;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.signals.EdgeDetector;
import org.growingstems.util.timer.TimerI;

/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in
 * the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of
 * the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
    private final TimerI m_periodicTimer = new WpiTimer();

    private static final LogEntryPriority k_loggerPriorityLevel = LogEntryPriority.HIGH;

    private static final NetworkTableEntry k_maxTranslationAccPercentEntry = SmartDashboard.getEntry("Auto/MaxTranslationAccPercent");
    private static final NetworkTableEntry k_maxTranslationVelPercentEntry = SmartDashboard.getEntry("Auto/MaxTranslationVelPercent");
    private static final NetworkTableEntry k_maxTurnAccPercentEntry = SmartDashboard.getEntry("Auto/MaxTurnAccPercent");
    private static final NetworkTableEntry k_maxTurnVelPercentEntry = SmartDashboard.getEntry("Auto/MaxTurnVelPercent");
    private static final NetworkTableEntry k_autoStartDelay = SmartDashboard.getEntry("Auto/Start Delay");

    private static final double k_maxTranslationAccPercentDefault = 250.0;
    private static final double k_maxTranslationVelPercentDefault = 175.0;
    private static final double k_maxTurnAccPercentDefault = 700.0;
    private static final double k_maxTurnVelPercentDefault = 500.0;

    static {
        k_maxTranslationAccPercentEntry.setDefaultNumber(k_maxTranslationAccPercentDefault);
        k_maxTranslationVelPercentEntry.setDefaultNumber(k_maxTranslationVelPercentDefault);
        k_maxTurnAccPercentEntry.setDefaultNumber(k_maxTurnAccPercentDefault);
        k_maxTurnVelPercentEntry.setDefaultNumber(k_maxTurnVelPercentDefault);
        k_autoStartDelay.setDefaultNumber(0.0);
    }

    private static final Range k_powerRange = new Range(-1.0, 1.0);
    private static final double k_climbSlowDriveScale = 0.6;
    private static final double k_climbSlowTurnScale = 0.6;
    private final RobotState m_robotState;
    private final RobotLogger m_logger;

    private final Auto m_auto = new Auto();

    private final Drive m_drive;
    private final Intake m_intake;
    private final Serializer m_serializer;
    private final Shooter m_shooter;
    private final Climb m_climb;

    private final XboxController m_driver;
    private final XboxController m_operator;
    private final ControllerConfig m_mainControllerConfig;
    private final ManualControllerConfig m_manualControllerConfig;

    private final NetworkTableEntry m_manualIntakeInPower;
    private final NetworkTableEntry m_manualIntakeOutPower;
    private final NetworkTableEntry m_manualSerializerInPower;
    private final NetworkTableEntry m_manualSerializerOutPower;
    private final NetworkTableEntry m_manualShooterPrimaryRpm;
    private final NetworkTableEntry m_shooterRatio;
    private final SendableChooser<Command> m_manualClimbState;
    private final NetworkTableEntry m_manualClimbPower;
    private final NetworkTableEntry m_manualClimbPosition;

    public RobotContainer() {
        m_driver = new XboxController(RobotMap.k_driverUsbPort);
        m_mainControllerConfig = new DefaultControllerConfig(m_driver);
        if (Robot.isCompetition()) {
            m_operator = null;
            m_manualControllerConfig = null;
        } else {
            m_operator = new XboxController(RobotMap.k_operatorUsbPort);
            m_manualControllerConfig = new ManualControllerConfig(m_operator);
        }

        m_logger = new RobotLogger(k_loggerPriorityLevel);
        m_robotState = new RobotState(m_logger, RobotIdentifier.determineRobot(RobotIdentifier.STING_RAY));

        m_drive = new Drive(m_robotState, m_logger);

        m_intake = new Intake(m_robotState, m_logger);
        m_manualIntakeInPower = SmartDashboard.getEntry("Manual/Intake In Power");
        m_manualIntakeOutPower = SmartDashboard.getEntry("Manual/Intake Out Power");
        m_manualIntakeInPower.setDefaultNumber(0.0);
        m_manualIntakeOutPower.setDefaultNumber(0.0);

        m_serializer = new Serializer(m_robotState, m_logger);
        m_manualSerializerInPower = SmartDashboard.getEntry("Manual/Serializer In Power");
        m_manualSerializerOutPower = SmartDashboard.getEntry("Manual/Serializer Out Power");
        m_manualSerializerInPower.setDefaultNumber(0.0);
        m_manualSerializerOutPower.setDefaultNumber(0.0);

        m_shooter = new Shooter(m_robotState, m_logger);
        m_manualShooterPrimaryRpm = SmartDashboard.getEntry("Manual/Shooter Primary Rpm");
        m_shooterRatio = SmartDashboard.getEntry("Manual/Shooter Ratio");

        m_climb = new Climb(m_robotState,
                            m_logger,
                            new EdgeDetector().provide(m_mainControllerConfig.climbAdvance),
                            new EdgeDetector().provide(m_mainControllerConfig.climbRegress));
        m_manualClimbPower = SmartDashboard.getEntry("Manual/Climb Power");
        m_manualClimbPosition = SmartDashboard.getEntry("Manual/Climb Position");
        m_manualClimbState = new SendableChooser<>();
        m_manualClimbState.setDefaultOption("Stopped", m_climb.getManualStopCommand());
        m_manualClimbState.addOption(
            "Loaded Position",
            m_climb.getManualPositionLoadedCommand(() -> m_manualClimbPosition.getDouble(0.0)));
        m_manualClimbState.addOption(
            "Unloaded Forward Position",
            m_climb.getManualPositionUnloadedForwardCommand(() -> m_manualClimbPosition.getDouble(0.0)));
        m_manualClimbState.addOption(
            "Unloaded Reverse Position",
            m_climb.getManualPositionUnloadedReverseCommand(() -> m_manualClimbPosition.getDouble(0.0)));
        m_manualClimbState.addOption("Power", m_climb.getManualPowerCommand(() -> m_manualClimbPower.getDouble(0.0)));
        SmartDashboard.putData("Manual Climb State", m_manualClimbState);

        m_manualIntakeInPower.setDefaultNumber(0.0);
        m_manualIntakeOutPower.setDefaultNumber(0.0);
        m_manualSerializerInPower.setDefaultNumber(0.0);
        m_manualSerializerOutPower.setDefaultNumber(0.0);
        m_manualShooterPrimaryRpm.setDefaultNumber(0.0);
        m_shooterRatio.setDefaultNumber(0.0);
        m_manualClimbPosition.setDefaultNumber(Double.NaN);
        m_manualClimbPower.setDefaultNumber(0.0);

        m_robotState.init(m_drive, m_intake, m_serializer, m_shooter, m_climb, m_driver);

        var fullManualEntry = SmartDashboard.getEntry("Robot Manual Mode");
        fullManualEntry.setBoolean(false);
        var fullManualTrigger = new Trigger(() -> fullManualEntry.getBoolean(false));
        fullManualTrigger.whenActive(new ParallelCommandGroup(m_drive.getManualCommand(),
                                                              m_intake.getManualCommand(),
                                                              m_serializer.getManualCommand(),
                                                              m_shooter.getManualCommand(),
                                                              m_climb.getManualCommand()));
        fullManualTrigger.whenInactive(new ParallelCommandGroup(m_drive.getNormalCommand(),
                                                                m_intake.getNormalCommand(),
                                                                m_serializer.getNormalCommand(),
                                                                m_shooter.getNormalCommand(),
                                                                m_climb.getNormalCommand()));
        // Configure the button bindings
        configureButtonBindings();

        k_maxTranslationAccPercentEntry.setNumber(k_maxTranslationAccPercentEntry.getNumber(k_maxTranslationAccPercentDefault));
        k_maxTranslationVelPercentEntry.setNumber(k_maxTranslationVelPercentEntry.getNumber(k_maxTranslationVelPercentDefault));
        k_maxTurnAccPercentEntry.setNumber(k_maxTurnAccPercentEntry.getNumber(k_maxTurnAccPercentDefault));
        k_maxTurnVelPercentEntry.setNumber(k_maxTurnVelPercentEntry.getNumber(k_maxTurnVelPercentDefault));

        m_periodicTimer.start();

        //WRITE GIT INFO
        try {
            var gitInfoFile = new File(Filesystem.getDeployDirectory(), "commitInfo.txt");
            var reader = new BufferedReader(new FileReader(gitInfoFile));
            m_logger.setDataPoint(LogEntry.GIT_BRANCH_NAME, reader.readLine());
            m_logger.setDataPoint(LogEntry.GIT_COMMIT_HASH, reader.readLine());
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
            DriverStation.reportError(e.toString(), true);
        }
    }

    /**
     * Use this method to define your button->command mappings. Buttons can be
     * created by
     * instantiating a {@link GenericHID} or one of its subclasses ({@link
     * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing
     * it to a {@link
     * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
     */
    private void configureButtonBindings() {
        //// Main Controls ////

        // Drive
        Supplier<Vector2d> translationControl = () -> {
            var control = m_mainControllerConfig.defaultVelocitySupplier.get();
            if (control.getMagnitude() == 0.0 && m_manualControllerConfig != null) {
                control = m_manualControllerConfig.defaultVelocitySupplier.get();
            }
            if (m_mainControllerConfig.climbEnable.get()) {
                control = control.scale(k_climbSlowDriveScale);
            }
            return control;
        };
        Supplier<Double> rotationControl = () -> {
            var control = m_mainControllerConfig.defaultRotationSupplier.get();
            if (control == 0.0 && m_manualControllerConfig != null) {
                control = m_manualControllerConfig.defaultRotationSupplier.get();
            }
            if (m_mainControllerConfig.climbEnable.get()) {
                control *= k_climbSlowTurnScale;
            }
            return control;
        };

        // Drive
        m_drive.setDefaultCommand(m_drive.getDirectDriveCommand(translationControl, rotationControl));
        m_mainControllerConfig.trackCargo.whileActiveOnce(m_robotState.getTrackedCargoCommand(translationControl, rotationControl));
        m_mainControllerConfig.fullPowerTrigger.whenActive(m_drive.getCurrentLimitDisableCommand());
        m_mainControllerConfig.fullPowerTrigger.whenInactive(m_drive.getCurrentLimitEnableCommand());

        // IMU
        var resetImuTrigger = m_mainControllerConfig.resetImu;
        if (m_manualControllerConfig != null) {
            resetImuTrigger = resetImuTrigger.or(m_manualControllerConfig.resetImu);
        }
        resetImuTrigger.whenActive(m_robotState.getResetImuCommand()
            .alongWith(new InstantCommand(() -> m_drive.setSmartHeading(new Angle()))));

        // Intake
        var intakeTrigger = new NTrigger(m_mainControllerConfig.intakeTrigger, m_mainControllerConfig.outtakeTrigger);
        intakeTrigger.setCommands(m_intake.getIntakeCommand(), m_intake.getOuttakeCommand());
        intakeTrigger.setStopCommand(m_intake.getStopMotorCommand());

        m_mainControllerConfig.retractIntakeTrigger.whenActive(m_intake.getRetractCommand());

        // Serializer
        m_mainControllerConfig.intakeTrigger.whenActive(m_serializer.getIntakeCommand());
        m_mainControllerConfig.intakeTrigger.whenInactive(m_serializer.getIdleCommand());
        var rejectIntakeTrigger = m_mainControllerConfig.rejectIntakeTrigger;
        var rejectShooterTrigger = m_mainControllerConfig.rejectShooterTrigger;
        if (m_manualControllerConfig != null) {
            rejectIntakeTrigger = rejectIntakeTrigger.or(m_manualControllerConfig.rejectIntakeTrigger);
            rejectShooterTrigger = rejectShooterTrigger.or(m_manualControllerConfig.rejectShooterTrigger);
        }
        var rejectTrigger = new NTrigger(rejectIntakeTrigger, rejectShooterTrigger);
        rejectTrigger.setCommands(m_robotState.getRejectIntakeCommand(), m_serializer.getRejectShooterCommand());
        rejectTrigger.setStopCommand(m_robotState.getNoRejectCommand());

        // Shooter
        Supplier<Command> getTurnToHubCommand = () -> m_drive.getSmartTurnToHubCommand(translationControl);
        Supplier<Command> getDontTurnCommand = () -> m_drive.getDirectUnmanagedDriveCommand(translationControl, () -> 0.0);
        Supplier<Command> getSerializerShootCommand = () -> m_serializer.getShootCommand();
        Supplier<Command> getStopShootingCommand = () -> m_robotState.getStopShootingCommand();

        m_mainControllerConfig.shootAutoTrigger.whenActive(
            m_shooter.getFireCommand().alongWith(getSerializerShootCommand.get(), getTurnToHubCommand.get()));
        m_mainControllerConfig.shootAutoTrigger.whenInactive(getStopShootingCommand.get());
        m_mainControllerConfig.shootFenderTrigger.whenActive(
            m_shooter.getFenderShotCommand().alongWith(getSerializerShootCommand.get(), getDontTurnCommand.get()));
        m_mainControllerConfig.shootFenderTrigger.whenInactive(getStopShootingCommand.get());
        m_mainControllerConfig.shootDashboardTrigger.whenActive(
            m_shooter.getDashboardShotCommand().alongWith(getSerializerShootCommand.get(), getDontTurnCommand.get()));
        m_mainControllerConfig.shootDashboardTrigger.whenInactive(getStopShootingCommand.get());

        // Climb
        m_mainControllerConfig.climbEnable.whenActive(() -> {
            m_drive.setSmartHeading(new Angle(0.0, AngleUnit.DEGREES));
            m_climb.enable();
        });
        m_mainControllerConfig.climbEnable.whenInactive(m_climb::disable);

        /***********************
         *   Manual Controls   *
         ***********************/
        if (m_manualControllerConfig != null) {
            // Intake
            m_manualControllerConfig.intakeDeploy.whenActive(m_intake.getManualDeployCommand());
            m_manualControllerConfig.intakeStow.whenActive(m_intake.getManualRetractCommand());

            var manualIntakeTrigger = new NTrigger(m_manualControllerConfig.intakeIn, m_manualControllerConfig.intakeOut);
            var intakeInCommand =
                m_intake.getManualPowerCommand(() -> k_powerRange.coerceValue(m_manualIntakeInPower.getDouble(0.0)));
            var intakeOutCommand =
                m_intake.getManualPowerCommand(() -> k_powerRange.coerceValue(m_manualIntakeOutPower.getDouble(0.0)));
            manualIntakeTrigger.setCommands(intakeInCommand, intakeOutCommand);
            manualIntakeTrigger.setStopCommand(m_intake.getManualStopCommand());

            // Serializer
            var manualSerializerTrigger =
                new NTrigger(m_manualControllerConfig.serializerIn, m_manualControllerConfig.serializerOut);
            var serializerInCommand = m_serializer.getManualMotorSpeedCommand(
                () -> k_powerRange.coerceValue(m_manualSerializerInPower.getDouble(0.0)));
            var serializerOutCommand = m_serializer.getManualMotorSpeedCommand(
                () -> k_powerRange.coerceValue(m_manualSerializerOutPower.getDouble(0.0)));
            manualSerializerTrigger.setCommands(serializerInCommand, serializerOutCommand);
            manualSerializerTrigger.setStopCommand(m_serializer.getManualStopCommand());

            m_manualControllerConfig.serializerPistonEngage.whenActive(m_serializer.getManualEngageCommand());
            m_manualControllerConfig.serializerPistonDisengage.whenActive(m_serializer.getManualDisengageCommand());

            // Shooter
            // clang-format off
            m_manualControllerConfig.shooterSpinUp.toggleWhenActive(
                m_shooter.getManualVelocityCommand(() -> m_manualShooterPrimaryRpm.getDouble(0.0),
                                                () -> m_shooterRatio.getDouble(0.0)));
            // clang-format on

            m_manualControllerConfig.shooterOut.whenActive(m_shooter.getHoodUpCommand());
            m_manualControllerConfig.shooterUp.whenActive(m_shooter.getHoodDownCommand());

            // Climb
            var select = new SelectCommand(m_manualClimbState::getSelected);
            select.addRequirements(m_climb);
            m_manualControllerConfig.climb.whenActive(select);
            m_manualControllerConfig.climb.whenInactive(m_climb.getManualStopCommand());
        }
    }

    /**
     * Use this to pass the autonomous command to the main {@link Robot} class.
     *
     * @return the command to run in autonomous
     */
    public Command getAutonomousCommand() {
        return new InstantCommand(() -> {
            var auto = m_auto.getMode((in) -> new AutoBuilder(in));
            if (auto != null) {
                auto.schedule();
            }
        });
    }

    public class AutoBuilder {
        private final Pose2d m_startPose;
        private Pose2d m_previousPose = null;

        public AutoBuilder(Pose2d startPose) {
            m_startPose = startPose;
        }

        public Command getStartCommand() {
            m_previousPose = m_startPose;
            return new WaitCommand(k_autoStartDelay.getDouble(0.0))
                .andThen(new InstantCommand(() -> {
                    m_robotState.disableAutoLimelight();
                    m_robotState.setRobotPose(m_startPose);
                }));
        }

        public Command enableLimelight() {
            return new InstantCommand(() -> m_robotState.enableAutoLimelight());
        }

        @Deprecated(forRemoval = true)
        public Command driveToCargo(Vector2d cargoPosition, double stopShort_in) {
            var vectorToCargo = cargoPosition.subtract(m_previousPose.getVector());
            var driveToPos = new Vector2d(vectorToCargo.getMagnitude() - m_robotState.k_distanceCenterToBumper.getValue(LengthUnit.INCHES) - stopShort_in, vectorToCargo.getAngle());
            var fieldDrivePos = m_previousPose.getVector().add(driveToPos);
            var finalPosition = new Pose2d(fieldDrivePos, vectorToCargo.getAngle());
            return goTo(finalPosition).alongWith(intake());
        }

        public Command goTo(Pose2d pose) {
            var command = m_drive.getTwoPointWithProfileCommand(k_maxTranslationAccPercentEntry.getDouble(k_maxTranslationAccPercentDefault),
                                                                k_maxTranslationVelPercentEntry.getDouble(k_maxTranslationVelPercentDefault),
                                                                k_maxTurnAccPercentEntry.getDouble(k_maxTurnAccPercentDefault),
                                                                k_maxTurnVelPercentEntry.getDouble(k_maxTurnVelPercentDefault),
                                                                pose,
                                                                m_previousPose);
            m_previousPose = pose;
            return command;
        }

        public Command shootAll() {
            return shootAll(false);
        }

        public Command shootAll(boolean waitForOneCargo) {
            Command cargoWait = waitForCargoNoTimeout(0);
            if (waitForOneCargo) {
                cargoWait = waitForCargoNoTimeout(1).andThen(cargoWait);
            }
            return shoot()
                .raceWith(cargoWait)
                .andThen(ceaseFire());
        }

        public Command prepFender() {
            return m_shooter.getFenderShotCommand();
        }

        public Command shootFender() {
            return m_serializer.getShootCommand();
        }

        public Command shoot() {
            return m_shooter.getFireCommand().alongWith(m_serializer.getShootCommand(), turnToHub());
        }

        // Doesn't need to be private necessarily, but use with caution if you make it public
        private Command turnToHub() {
            return m_drive.getTurnToHeadingCommand(
                () -> new Vector2d(),
                () -> m_robotState.getShotVectorToHub_in().getAngle().add(new Angle(180.0, AngleUnit.DEGREES)));
        }

        public Command prepareShotFromRobotPose() {
            return m_shooter.getFireCommand();
        }

        public Command prepareShot(Vector2d shotLocation) {
            return m_shooter.getFirePoseCommand(shotLocation);
        }

        public Command onlyIntake() {
            return m_intake.getIntakeCommand();
        }

        public Command intake() {
            return onlyIntake().alongWith(m_serializer.getIntakeCommand());
        }

        public Command retract() {
            return m_intake.getRetractCommand().alongWith(m_serializer.getIdleCommand());
        }

        public Command reject() {
            return m_serializer.getRejectIntakeCommand()
                .andThen(m_serializer.getNoRejectCommand());
        }

        public Command ceaseFire() {
            return m_robotState.getStopShootingCommand();
        }

        public Command waitForCargo(int n, double timeout_s) {
            return waitForCargoNoTimeout(n).withTimeout(timeout_s);
        }

        public Command waitForCargoNoTimeout(int n) {
            return new WaitUntilCommand(() -> m_serializer.cargoCount() == n);
        }

        public Command stopEverything() {
            return m_serializer.getIdleCommand().andThen(
                m_serializer.getCeaseFireCommand(),
                m_intake.getStopMotorCommand(),
                m_intake.getRetractCommand(),
                m_drive.getDirectDriveCommand(() -> 0.0, () -> 0.0, () -> 0.0),
                m_shooter.getStopCommand());
        }

        public Command getDefaultAuto() {
            return null;
        }
    }

    public Command getTelopInitCommand() {
        return m_robotState.getStopShootingCommand()
            .alongWith(m_intake.getRetractCommand())
            .alongWith(m_serializer.getIdleCommand());
    }

    public RobotState getRobotState() {
        return m_robotState;
    }

    public void onDisable() {
        m_robotState.onDisable();
    }

    public RobotLogger getLogger() {
        return m_logger;
    }

    public void logControls() {
        m_periodicTimer.reset();
        // log driver joystick
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_LEFT_X, m_driver.getLeftX());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_LEFT_Y, m_driver.getLeftY());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_RIGHT_X, m_driver.getRightX());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_RIGHT_Y, m_driver.getRightY());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_A, m_driver.getAButton());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_B, m_driver.getBButton());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_X, m_driver.getXButton());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_Y, m_driver.getYButton());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_RB, m_driver.getRightBumper());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_LB, m_driver.getLeftBumper());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_RT, m_driver.getRightTriggerAxis());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_LT, m_driver.getLeftTriggerAxis());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_BACK, m_driver.getBackButton());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_START, m_driver.getStartButton());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_POV, m_driver.getPOV());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_L3, m_driver.getLeftStickButton());
        m_logger.setDataPoint(LogEntry.CONTROLLER_DRIVER_R3, m_driver.getRightStickButton());

        // log operator joystick
        if (m_operator != null) {
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_LEFT_X, m_operator.getLeftX());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_LEFT_Y, m_operator.getLeftY());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_RIGHT_X, m_operator.getRightX());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_RIGHT_Y, m_operator.getRightY());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_A, m_operator.getAButton());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_B, m_operator.getBButton());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_X, m_operator.getXButton());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_Y, m_operator.getYButton());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_RB, m_operator.getRightBumper());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_LB, m_operator.getLeftBumper());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_RT, m_operator.getRightTriggerAxis());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_LT, m_operator.getLeftTriggerAxis());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_BACK, m_operator.getBackButton());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_START, m_operator.getStartButton());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_POV, m_operator.getPOV());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_L3, m_operator.getLeftStickButton());
            m_logger.setDataPoint(LogEntry.CONTROLLER_OPERATOR_R3, m_operator.getRightStickButton());
        }

        m_logger.setDataPoint(LogEntry.TIME_CONTROLLER, m_periodicTimer.get().getValue(TimeUnit.MILLISECONDS));
    }
}
