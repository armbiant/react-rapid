/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import edu.wpi.first.wpilibj.DriverStation;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public enum RobotIdentifier {
    STING_RAY("00-80-2F-24-4D-56"),
    P_BOT("00-80-2F-22-6A-4F"),
    // P_BOT("00-80-2F-22-6A-50"), USB
    SWERVEX("00-80-2F-17-DB-F7"),
    MAVERICK("00-80-2F-16-17-F7"),
    BENCHTOP("00-80-2F-21-E2-91");

    private final String m_macAddress;

    private RobotIdentifier(String macAddress) {
        m_macAddress = macAddress;
    }

    public static RobotIdentifier determineRobot(RobotIdentifier defaultRobot) {
        RobotIdentifier identifier = null;
        if (Robot.isCompetition()) {
            identifier = STING_RAY;
            System.out.println("RUNNING COMPETITION CODE. Robot Assumed to be: " + identifier.toString());
            return identifier;
        }
        Enumeration<NetworkInterface> networkInterfaces;
        try {
            networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface ni = networkInterfaces.nextElement();
                byte[] hardwareAddress = ni.getHardwareAddress();
                if (hardwareAddress != null) {
                    String[] hexadecimalFormat = new String[hardwareAddress.length];
                    for (int i = 0; i < hardwareAddress.length; i++) {
                        hexadecimalFormat[i] = String.format("%02X", hardwareAddress[i]);
                    }
                    var address = String.join("-", hexadecimalFormat);
                    System.out.println("Found MAC Address: " + address);
                    for (var testIdentifier : RobotIdentifier.values()) {
                        if (testIdentifier.m_macAddress.equals(address)) {
                            if (identifier == null) {
                                identifier = testIdentifier;
                            } else if (!identifier.equals(testIdentifier)) {
                                DriverStation.reportWarning(
                                    "Found multiple robots' MAC Addresses. Using first address seen.",
                                    false);
                            }
                        }
                    }
                }
            }
        } catch (SocketException e) {
            DriverStation.reportError("Error determining MAC Addresses", true);
        }
        if (identifier == null) {
            DriverStation.reportError("No Robot MAC Address found! Using default.", false);
            identifier = defaultRobot;
        }
        System.out.println("Robot Determined to be: " + identifier.toString());
        return identifier;
    }
}
