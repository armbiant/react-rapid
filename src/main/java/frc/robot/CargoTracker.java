/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import java.util.ArrayList;
import java.util.List;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.math.Time;
import org.growingstems.math.Vector2d;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.util.timer.TimerI;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.library.CircularBuffer;
import frc.robot.RobotState.CargoTrackOption;
import frc.robot.constants.LogEntry;
import frc.robot.subsystems.CargoTrackerCamera;
import frc.robot.subsystems.CargoTrackerCamera.TrackedCargo;

public class CargoTracker {
    private RobotState m_robotState;
    private static final int k_closestCargoPosDequeSize = 5;
    private final CircularBuffer<Vector2d> m_closestCargoPosBuffer = new CircularBuffer<>(k_closestCargoPosDequeSize);
    private final TimerI m_cargoTrackerCooldownTimer = new WpiTimer();
    private final TimerI m_staleCargoTimer = new WpiTimer();
    private static final Time k_cargoTrackerCooldownTime = new Time(1.5, TimeUnit.SECONDS);
    private static final Time k_staleCargoTime = new Time(0.5, TimeUnit.SECONDS);
    private static final double k_cargoTrackTooCloseDistance_in = 30.0;

    private static final double k_fieldToolGhostTime_s = 5.0;
    private static final int k_fieldToolCargoRadius_px = 10;
    private int m_updateCount = 0;
    private static final NetworkTableEntry k_fieldToolUpdateCountEntry = SmartDashboard.getEntry("Field/UpdateCount");
    private static final NetworkTableEntry k_fieldToolGhostTimeEntry = SmartDashboard.getEntry("Field/CargoGhostingTime_s");
    private static final NetworkTableEntry k_fieldToolCargoRadiusEntry = SmartDashboard.getEntry("Field/CargoRadius_px");
    private static final NetworkTableEntry k_fieldToolXListEntry = SmartDashboard.getEntry("Field/CargoX");
    private static final NetworkTableEntry k_fieldToolYListEntry = SmartDashboard.getEntry("Field/CargoY");
    private static final NetworkTableEntry k_fieldToolIsRedListEntry = SmartDashboard.getEntry("Field/CargoIsRed");
    private static final NetworkTableEntry k_fieldToolRobotXEntry = SmartDashboard.getEntry("Field/RobotX");
    private static final NetworkTableEntry k_fieldToolRobotYEntry = SmartDashboard.getEntry("Field/RobotY");
    private static final NetworkTableEntry k_fieldToolRobotAngEntry = SmartDashboard.getEntry("Field/RobotAng");
    private static final NetworkTableEntry k_fieldToolRobotAltColorEntry = SmartDashboard.getEntry("Field/RobotAltColor");

    private final RobotLogger m_logger;

    private Vector2d m_averageClosestCargoPos_in = null;
    private Vector2d m_vectorToCargo = null;

    public CargoTracker(RobotState robotState, RobotLogger robotLogger) {
        m_robotState = robotState;
        m_cargoTrackerCooldownTimer.start();
        m_staleCargoTimer.start();
        m_logger = robotLogger;
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_0_X, 0.0);
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_0_Y, 0.0);
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_1_X, 0.0);
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_1_Y, 0.0);
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_2_X, 0.0);
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_2_Y, 0.0);
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_3_X, 0.0);
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_3_Y, 0.0);
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_4_X, 0.0);
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_4_Y, 0.0);

        k_fieldToolGhostTimeEntry.setNumber(k_fieldToolGhostTime_s);
        k_fieldToolCargoRadiusEntry.setNumber(k_fieldToolCargoRadius_px);
    }

    public void updateTracks(List<TrackedCargo> cargoList){
        int i = 0;
        List<Double> cargoXList = new ArrayList<>();
        ArrayList<Double> cargoYList = new ArrayList<>();
        ArrayList<Boolean> cargoIsRedList = new ArrayList<>();
        for (var cargo : cargoList) {
            // Set odometry-based field position
            var relativePos = cargo.getRelativePosition();
            var robotCentricVector = relativePos.add(CargoTrackerCamera.k_lensPositionFromRobotCenter_in);
            var relativeFieldVector = robotCentricVector.rotate(m_robotState.getOdometryRobotPose_in().getRotation());
            var cargoFieldPosition = relativeFieldVector.add(m_robotState.getOdometryRobotPose_in().getVector());
            cargo.setFieldPosition(cargoFieldPosition);

            // Calculate fused field position
            var cargoFieldPosition_fused = relativeFieldVector.add(m_robotState.getRobotPose_in().getVector());

            // Store fused field position for CargoViewer
            cargoXList.add(cargoFieldPosition_fused.getX());
            cargoYList.add(cargoFieldPosition_fused.getY());
            cargoIsRedList.add(cargo.isRed);

            // Log fused field position
            switch (i) {
                case 0:
                    m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_0_X, cargoFieldPosition_fused.getX());
                    m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_0_Y, cargoFieldPosition_fused.getY());
                    break;
                case 1:
                    m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_1_X, cargoFieldPosition_fused.getX());
                    m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_1_Y, cargoFieldPosition_fused.getY());
                    break;
                case 2:
                    m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_2_X, cargoFieldPosition_fused.getX());
                    m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_2_Y, cargoFieldPosition_fused.getY());
                    break;
                case 3:
                    m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_3_X, cargoFieldPosition_fused.getX());
                    m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_3_Y, cargoFieldPosition_fused.getY());
                    break;
                case 4:
                    m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_4_X, cargoFieldPosition_fused.getX());
                    m_logger.setDataPoint(LogEntry.CARGO_TRACKER_INDEX_4_Y, cargoFieldPosition_fused.getY());
                    break;
                default:
                    break;
            }
            i++;
        }

        k_fieldToolXListEntry.setNumberArray(cargoXList.toArray(new Double[cargoXList.size()]));
        k_fieldToolYListEntry.setNumberArray(cargoYList.toArray(new Double[cargoYList.size()]));
        k_fieldToolIsRedListEntry.setBooleanArray(cargoIsRedList.toArray(new Boolean[cargoIsRedList.size()]));
        k_fieldToolRobotXEntry.setNumber(m_robotState.getRobotPose_in().getX());
        k_fieldToolRobotYEntry.setNumber(m_robotState.getRobotPose_in().getY());
        k_fieldToolRobotAngEntry.setNumber(m_robotState.getRobotPose_in().getRotation().getValue(AngleUnit.RADIANS));
        k_fieldToolRobotAltColorEntry.setBoolean(m_robotState.getSensors().limelightHasTarget());
        k_fieldToolUpdateCountEntry.setNumber(++m_updateCount);

        if (shouldIgnoreCargo()) {
            var cargoToIgnore = getCargoToIgnore();
            cargoList = new ArrayList<>(cargoList); // Copy the list so we don't modify the original list
            cargoList.removeIf(c -> c.getColor() == cargoToIgnore);
        }

        if (cargoList.isEmpty()) {
            if (m_staleCargoTimer.hasElapsed(k_staleCargoTime)) {
                resetQueue();
            }
            return;
        } else {
            m_staleCargoTimer.reset();
        }

        Vector2d closestCargoPosition = null;
        double closestDistance = Double.POSITIVE_INFINITY;

        for (var cargo : cargoList) {
            var distanceToCargo = cargo.getFieldPosition().subtract(m_robotState.getOdometryRobotPose_in().getVector()).getMagnitude();
            if (distanceToCargo < closestDistance) {
                closestDistance = distanceToCargo;
                closestCargoPosition = cargo.getFieldPosition();
            }
        }

        if (closestCargoPosition != null) {
            m_closestCargoPosBuffer.add(closestCargoPosition);
            var averageCargoPos = m_closestCargoPosBuffer.stream().reduce(Vector2d::add).orElse(new Vector2d());
            m_averageClosestCargoPos_in = averageCargoPos.scale(1.0 / m_closestCargoPosBuffer.size());
        }

        calculateVector();
    }

    public void calculateVector() {
        var cargoPos = getAverageClosestCargoPos();
        if (m_cargoTrackerCooldownTimer.hasElapsed(k_cargoTrackerCooldownTime) && cargoPos != null) {
            m_vectorToCargo = cargoPos.subtract(m_robotState.getOdometryRobotPose_in().getVector());

            if (m_vectorToCargo.getMagnitude() < k_cargoTrackTooCloseDistance_in) {
                m_cargoTrackerCooldownTimer.reset();
                resetQueue();
            }
        } else {
            m_vectorToCargo = null;
        }
    }

    public Vector2d getAverageClosestCargoPos() {
        return m_averageClosestCargoPos_in;
    }

    public Vector2d getVectorToClosestCargo() {
        return m_vectorToCargo;
    }

    private void resetQueue() {
        m_closestCargoPosBuffer.clear();
        m_averageClosestCargoPos_in = null;
        m_vectorToCargo = null;
    }

    private Alliance getCargoToIgnore() {
        if (m_robotState.getCargoTrackSetting() == CargoTrackOption.THEIRS) {
            return DriverStation.getAlliance();
        }

        if (DriverStation.getAlliance() == Alliance.Red) {
            return Alliance.Blue;
        } else {
            return Alliance.Red;
        }
    }

    private boolean shouldIgnoreCargo() {
        return m_robotState.getCargoTrackSetting() != CargoTrackOption.BOTH;
    }
}
