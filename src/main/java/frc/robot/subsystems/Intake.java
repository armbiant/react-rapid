/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.StatusFrame;
import com.ctre.phoenix.motorcontrol.TalonSRXControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.FunctionalCommand;
import edu.wpi.first.wpilibj2.command.InstantCommand;

import frc.library.GStemsSubsystem;
import frc.robot.RobotLogger;
import frc.robot.RobotState;
import frc.robot.constants.LogEntry;
import frc.robot.constants.RobotMap;

import java.util.function.Supplier;

import org.growingstems.frc.drivers.TalonSRXUtils;
import org.growingstems.frc.util.WpiTimer;
import org.growingstems.math.Time;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.util.statemachine.DynamicTransition;
import org.growingstems.util.statemachine.ExecState;
import org.growingstems.util.statemachine.InitExecExitState;
import org.growingstems.util.statemachine.InitExecState;
import org.growingstems.util.statemachine.State;
import org.growingstems.util.statemachine.StateMachine;
import org.growingstems.util.statemachine.StaticTransition;
import org.growingstems.util.timer.TimerI;

public class Intake extends GStemsSubsystem {
    private static final double k_intakePower = 0.55;
    private static final double k_outtakePower = 0.6;
    private static final Time k_deployingTime = new Time(0.75, TimeUnit.SECONDS);
    private static final Time k_intakeDelayTime = new Time(0.1, TimeUnit.SECONDS);
    private static final Time k_retractingTime = new Time(0.25, TimeUnit.SECONDS);
    private static final double k_motorSecondsFromNeutralToFull = 0.1;

    private static final boolean k_forceRetractWhenFull = true;
    private static final int k_intakeContinuousCurrentLimit_A = 25;

    private final TimerI m_periodicTimer = new WpiTimer();
    private final RobotState m_robotState;
    private final RobotLogger m_logger;
    private final RetractionSolenoid m_retractionSolenoid;

    private Action m_commandedAction = Action.RETRACTED;
    private boolean m_manuallyDeployed = false;
    private double m_manualPower = 0.0;

    private boolean m_forceRetract = false;

    private final TalonSRX m_motor;

    private enum Action { RETRACTED, IDLE_OFF, INTAKING, OUTTAKING }

    private static class RetractionSolenoid {
        private static final boolean k_retractionState = false;
        private static final boolean k_extentionState = true;
        private final Solenoid m_retractionSolenoid;
        private final Solenoid m_extentionSolenoid;

        public RetractionSolenoid() {
            m_retractionSolenoid = new Solenoid(RobotMap.k_pcmCanId,
                                                PneumaticsModuleType.CTREPCM,
                                                RobotMap.k_intakeRetractSolenoidPcmChannel);
            m_extentionSolenoid = new Solenoid(RobotMap.k_pcmCanId,
                                               PneumaticsModuleType.CTREPCM,
                                               RobotMap.k_intakeExtendSolenoidPcmChannel);
        }

        public boolean isRetracted() {
            return m_retractionSolenoid.get() == k_retractionState && m_extentionSolenoid.get() == !k_extentionState;
        }

        public void retract() {
            m_retractionSolenoid.set(k_retractionState);
            m_extentionSolenoid.set(!k_extentionState);
        }

        public void intakeFloat() {
            m_retractionSolenoid.set(!k_retractionState);
            m_extentionSolenoid.set(!k_extentionState);
        }

        public void deploy() {
            m_retractionSolenoid.set(!k_retractionState);
            m_extentionSolenoid.set(k_extentionState);
        }
    }

    public Intake(RobotState robotState, RobotLogger logger) {
        super("intake");

        m_robotState = robotState;
        m_logger = logger;
        m_retractionSolenoid = new RetractionSolenoid();

        m_motor = TalonSRXUtils.createDefaultTalonSRX(RobotMap.k_intakeMotorCanId, false);
        m_motor.setNeutralMode(NeutralMode.Brake);
        m_motor.configOpenloopRamp(k_motorSecondsFromNeutralToFull);

        // Current Limiting
        var error = m_motor.configContinuousCurrentLimit(k_intakeContinuousCurrentLimit_A, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "enableCurrentLimit");

        // Status Rates
        error = m_motor.setStatusFramePeriod(StatusFrame.Status_2_Feedback0,
                                             TalonSRXUtils.k_maxPeriod_ms,
                                             TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "setStatusFramePeriod_Status_2_Feedback0");

        m_periodicTimer.start();
    }

    @Override
    public void periodic() {
        m_periodicTimer.reset();

        m_forceRetract = m_robotState.cargoFullyPrimed() && !DriverStation.isAutonomous() && k_forceRetractWhenFull;

        super.periodic();

        m_logger.setDataPoint(LogEntry.INTAKE_STATE, super.getCurrentState().getStateName());
        m_logger.setDataPoint(LogEntry.TIME_INTAKE, m_periodicTimer.get().getValue(TimeUnit.MILLISECONDS));
    }

    private void setIntakePower(double power) {
        m_motor.set(TalonSRXControlMode.PercentOutput, power);
    }

    private void stop() {
        m_motor.set(TalonSRXControlMode.PercentOutput, 0.0);
    }

    public Command getIntakeCommand() {
        return new InstantCommand(() -> m_commandedAction = Action.INTAKING);
    }

    public Command getOuttakeCommand() {
        return new InstantCommand(() -> m_commandedAction = Action.OUTTAKING);
    }

    public Command getRetractCommand() {
        return new InstantCommand(() -> m_commandedAction = Action.RETRACTED);
    }

    public Command getStopMotorCommand() {
        return new InstantCommand(() -> m_commandedAction = Action.IDLE_OFF);
    }

    public Command getManualPowerCommand(Supplier<Double> power) {
        // clang-format off
        return new FunctionalCommand(() -> {},
                                     () -> m_manualPower = power.get(),
                                     unused -> {},
                                     () -> false,
                                     this);
        // clang-format on
    }

    public Command getManualStopCommand() {
        return new InstantCommand(() -> m_manualPower = 0.0, this);
    }

    public Command getManualDeployCommand() {
        return new InstantCommand(() -> m_manuallyDeployed = true, this);
    }

    public Command getManualRetractCommand() {
        return new InstantCommand(() -> m_manuallyDeployed = false, this);
    }

    @Override
    protected State provideDisabledState() {
        return new ExecState("Disabled State", () -> stop());
    }
    @Override
    protected State provideManualState() {
        // clang-format off
        return new InitExecState("Manual State",
                                 () -> {
                                     m_manuallyDeployed = !m_retractionSolenoid.isRetracted();
                                     m_manualPower = 0.0;
                                 },
                                 () -> {
                                     setIntakePower(m_manualPower);
                                     if (m_manuallyDeployed) {
                                         m_retractionSolenoid.deploy();
                                     } else {
                                         m_retractionSolenoid.retract();
                                     }
                                 });
        // clang-format on
    }

    @Override
    protected State provideNormalState() {
        TimerI stateTimer = new WpiTimer();
        stateTimer.start();

        State retracted =
            new InitExecState("Retracted", () -> m_retractionSolenoid.retract(), () -> setIntakePower(0.0));
        // clang-format off
        State deploying = new InitExecExitState("Deploying",
                                                () -> {
                                                    m_retractionSolenoid.deploy();
                                                    stateTimer.reset();
                                                },
                                                () -> {
                                                    var pwr = 0.0;
                                                    if (stateTimer.hasElapsed(k_intakeDelayTime)) {
                                                        switch (m_commandedAction) {
                                                            case INTAKING:
                                                                pwr = k_intakePower;
                                                                break;
                                                            case OUTTAKING:
                                                                pwr = -k_outtakePower;
                                                                break;
                                                            default:
                                                                break;
                                                        }
                                                    }
                                                    setIntakePower(pwr);
                                                },
                                                () -> m_retractionSolenoid.intakeFloat());
        // clang-format on
        State extendedOff = new ExecState("Extended Stopped", () -> setIntakePower(0.0));
        State intaking = new ExecState("Intaking", () -> setIntakePower(k_intakePower));
        State outtaking = new ExecState("Outtaking", () -> setIntakePower(-k_outtakePower));
        State retracting = new InitExecState("Retracting", () -> {
            m_retractionSolenoid.retract();
            stateTimer.reset();
        }, () -> setIntakePower(0.0));

        var rejectOuttaking = new StaticTransition(outtaking, () -> m_robotState.rejectingOutIntake() && !m_forceRetract);

        retracted.addTransition(new StaticTransition(deploying, () -> {
            if (m_robotState.rejectingOutIntake() || m_forceRetract) {
                return false;
            }
            return m_commandedAction == Action.OUTTAKING || m_commandedAction == Action.INTAKING;
        }));

        retracting.addTransition(new StaticTransition(deploying, () -> m_commandedAction != Action.RETRACTED && m_commandedAction != Action.IDLE_OFF && !m_forceRetract));
        retracting.addTransition(new StaticTransition(retracted, () -> stateTimer.hasElapsed(k_retractingTime)));

        deploying.addTransition(new DynamicTransition(() -> {
            switch (m_commandedAction) {
                case INTAKING:
                    return intaking;
                case OUTTAKING:
                    return outtaking;
                default:
                    return extendedOff;
            }
        }, () -> stateTimer.hasElapsed(k_deployingTime) && !m_forceRetract));
        deploying.addTransition(new StaticTransition(retracting, () -> m_commandedAction == Action.RETRACTED || m_forceRetract));

        extendedOff.addTransition(new StaticTransition(intaking, () -> m_commandedAction == Action.INTAKING && !m_forceRetract));
        extendedOff.addTransition(new StaticTransition(outtaking, () -> m_commandedAction == Action.OUTTAKING && !m_forceRetract));
        extendedOff.addTransition(new StaticTransition(retracting, () -> m_commandedAction == Action.RETRACTED || m_forceRetract));
        extendedOff.addTransition(rejectOuttaking);

        intaking.addTransition(rejectOuttaking);
        intaking.addTransition(new StaticTransition(retracting, () -> m_commandedAction == Action.RETRACTED || m_forceRetract));
        intaking.addTransition(new StaticTransition(extendedOff, () -> (m_commandedAction == Action.IDLE_OFF || m_commandedAction == Action.OUTTAKING) && !m_forceRetract));

        outtaking.addTransition(new StaticTransition(retracting, () -> m_commandedAction == Action.RETRACTED || m_forceRetract));
        outtaking.addTransition(new StaticTransition(extendedOff, () -> {
            if (m_robotState.rejectingOutIntake()) {
                return false;
            }
            return m_commandedAction == Action.IDLE_OFF || m_commandedAction == Action.INTAKING && !m_forceRetract;
        }));

        return new StateMachine(retracted).getAsState("Normal State", true, false);
    }
}
