/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems;

import com.ctre.phoenix.ErrorCode;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.StatusFrame;
import com.ctre.phoenix.motorcontrol.VictorSPXControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.FunctionalCommand;
import edu.wpi.first.wpilibj2.command.InstantCommand;

import frc.library.drivers.VictorSPXUtils;
import frc.library.GStemsSubsystem;
import frc.robot.RobotState;
import frc.robot.RobotState.AutoRejectOption;
import frc.robot.constants.LogEntry;
import frc.robot.constants.RobotMap;
import frc.robot.constants.ShotCriteria;
import frc.robot.RobotLogger;

import java.util.function.Supplier;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.math.Time;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.signals.Debounce;
import org.growingstems.util.statemachine.ExecState;
import org.growingstems.util.statemachine.InitExecExitState;
import org.growingstems.util.statemachine.InitExecState;
import org.growingstems.util.statemachine.InitExitState;
import org.growingstems.util.statemachine.InitState;
import org.growingstems.util.statemachine.State;
import org.growingstems.util.statemachine.StateMachine;
import org.growingstems.util.statemachine.StaticTransition;
import org.growingstems.util.timer.TimerI;

public class Serializer extends GStemsSubsystem {
    private static final double k_intakeEngagedPower = 0.4;
    private static final double k_intakeEngagedFastPower = 0.8;
    private static final double k_intakeDisengagedPower = 0.7;
    private static final double k_rejectIntakePower = 0.5;
    private static final double k_shootPower = 0.7;
    private static final double k_rejectShooterPower = k_intakeEngagedPower;
    private static final double k_afterShotPower = -0.5;
    private static final Time k_engageTime = new Time(0.2, TimeUnit.SECONDS);
    private static final Time k_disengageTime = new Time(0.2, TimeUnit.SECONDS);
    private static final Time k_engagedIntakeTimeout = new Time(1.0, TimeUnit.SECONDS);
    private static final Time k_towerNotPrimedTimeout = new Time(2.0, TimeUnit.SECONDS);
    private static final Time k_reprimeTimeout = new Time(2.0, TimeUnit.SECONDS);
    private static final Time k_afterShotDelay = new Time(160.0, TimeUnit.MILLISECONDS);
    private static final Time k_rejectIntakeTime = new Time(1.0, TimeUnit.SECONDS);
    private static final Time k_rejectShooterTimeout = new Time(2.0, TimeUnit.SECONDS);

    private static final Time k_keepIntakingTime = new Time(2.0, TimeUnit.SECONDS);
    private static final Debounce k_keepIntakingDebounce = new Debounce(k_keepIntakingTime, new WpiTimer());
    private static final Debounce k_reprimeTowerDebounce = new Debounce(new Time(60, TimeUnit.MILLISECONDS), new WpiTimer());
    private static final Debounce k_stopAllDebounce = new Debounce(new Time(2.0, TimeUnit.SECONDS), new WpiTimer());
    private static final Debounce k_serializerLidarRejectDebounce = new Debounce(new Time(1.0, TimeUnit.SECONDS), new WpiTimer());

    private static final Time k_serializerKeepPrimingTime = new Time(0.2, TimeUnit.SECONDS);
    private boolean m_serializerStopKeepPriming = false;
    private static final Debounce k_serializerKeepPrimingDebounce = new Debounce(k_serializerKeepPrimingTime, new WpiTimer());

    private boolean m_keepIntaking = false;
    private boolean m_reprimeTower = false;
    private boolean m_tryingToRejectShooter = false;
    private boolean m_stopAllNoCargoTrig = false;
    private boolean m_serializerLidarReject = false;

    private final TimerI m_periodicTimer = new WpiTimer();
    private final RobotState m_robotState;
    private final RobotLogger m_logger;
    private final VictorSPX m_motor;
    private final EngagementSolenoid m_towerEngagement = new EngagementSolenoid(this);

    private boolean m_manuallyEngage = false;
    private class EngagementSolenoid {
        private static final boolean k_engagedState = false;
        private final Solenoid m_solenoid;
        private final Serializer m_parent;

        public EngagementSolenoid(Serializer serializer) {
            m_solenoid = new Solenoid(RobotMap.k_pcmCanId, PneumaticsModuleType.CTREPCM, RobotMap.k_towerEngagementPcmChannel);
            m_parent = serializer;
        }

        public void engage() {
            m_parent.m_logger.setDataPoint(LogEntry.SERIALIZER_SOLENOID_ENGAGED, true);
            m_solenoid.set(k_engagedState);
        }

        public void disengage() {
            m_parent.m_logger.setDataPoint(LogEntry.SERIALIZER_SOLENOID_ENGAGED, false);
            m_solenoid.set(!k_engagedState);
        }

        public boolean isEngaged() {
            return m_solenoid.get() == k_engagedState;
        }

        public boolean isDisengaged() {
            return !isEngaged();
        }
    }

    enum Action {
        INTAKE,
        IDLE
    }
    enum RejectAction {
        REJECT_SHOOTER,
        REJECT_INTAKE,
        NO_REJECT
    }

    private boolean m_commandedToShoot = false;
    private Action m_commandedAction = Action.IDLE;
    private RejectAction m_commandedRejectAction = RejectAction.NO_REJECT;
    private double m_manualMotorPower = 0.0;

    private double m_normalMotorPower = 0.0;
    private boolean m_normalEngaged = true;
    private boolean m_shouldShoot = false;
    private boolean m_isShooting = false;
    private boolean m_shotCooldown = false;

    private boolean m_isRejectingShooter = false;
    private boolean m_isRejectingIntake = false;

    // These are JUST statuses. They should NOT be used for any logic within Serializer
    private int m_cargoCount = 0;

    private final StateMachine m_primaryStateMachine;

    private final TimerI m_shotCooldownTimer = new WpiTimer();

    public Serializer(RobotState robotState, RobotLogger logger) {
        super("serializer");
        m_robotState = robotState;
        m_logger = logger;

        m_motor = VictorSPXUtils.createDefaultVictorSPX(RobotMap.k_serializerMotorCanId, false);
        m_motor.setNeutralMode(NeutralMode.Brake);
        m_motor.setInverted(InvertType.InvertMotorOutput);

        m_shotCooldownTimer.start();

        // Status Rates
        ErrorCode error = ErrorCode.OK;

        error = m_motor.setStatusFramePeriod(StatusFrame.Status_2_Feedback0,
                VictorSPXUtils.k_maxPeriod_ms, VictorSPXUtils.k_cfgTimeout_ms);
        VictorSPXUtils.checkError(error, "setStatusFramePeriod_Status_2_Feedback0");

        TimerI stateTimer = new WpiTimer();
        stateTimer.start();

        //Internal intaking state machine
        State engagedIdle = new InitState("Engaged Idle", () -> {
            setNormal(0.0, true);
            m_cargoCount = 0;
            stateTimer.reset();
        });
        State engagedFastIntake = new InitState("Engaged Fast Intaking", () -> {
            setNormal(k_intakeEngagedFastPower, true);
            // If we are intaking the tower, it means their isn't one in the tower, so we should reset that cargo status for it
            m_cargoCount = 0;
            stateTimer.reset();
        });
        State engagedFastIntakeDetectedCargo = new InitState("Engaged Fast Intaking - Detected Cargo", () -> {
            setNormal(k_intakeEngagedFastPower, true);
            // We saw a ball while actively trying to intake a ball via the serializer, set cargo count to 1
            m_cargoCount = 1;
            stateTimer.reset();
        });
        State engagedIntake = new InitState("Engaged Intaking", () -> {
            setNormal(k_intakeEngagedPower, true);
            stateTimer.reset();
        });
        State reprimeTower = new InitState("Engaged Re-Prime Tower", () -> {
            setNormal(k_intakeEngagedPower, true);
            stateTimer.reset();
        });
        State disengagedIdle = new InitState("Disengaged Idle", () -> {
            setNormal(0.0, false);
            // We disengaged the tower, which means we have a cargo in tower
            m_cargoCount = 1;
            stateTimer.reset();
        });
        State disengagedIntake = new InitState("Disengaged Intaking", () -> {
            setNormal(k_intakeDisengagedPower, false);
            // If we are intaking for just the serializer, it means we dont have a serializer cargo
            m_cargoCount = 1;
            stateTimer.reset();
        });
        State stopAll = new InitState("All Stopped", () -> {
            setNormal(0.0, true);
            // We have both balls
            m_cargoCount = 2;
            stateTimer.reset();
        });
        State shotMade = new InitState("Shot Made", () -> {
            setNormal(k_afterShotPower, true);
            m_cargoCount--;
            if (m_cargoCount < 0) {
                m_cargoCount = 0;
            }
            stateTimer.reset();
        });

        State rejectingOutShooter = new InitExecExitState("Rejecting Out Shooter", () -> {
            m_tryingToRejectShooter = true;
            stateTimer.reset();

            m_cargoCount--;
            if (m_cargoCount < 0) {
                m_cargoCount = 0;
            }
        }, () -> {
            if (m_robotState.isReadyToRejectShooter()) {
                m_isRejectingShooter = true;
                setNormal(k_rejectShooterPower, true);
            } else {
                m_isRejectingShooter = false;
                setNormal(0.0, true);
                stateTimer.reset();
            }
        }, () -> {
            m_isRejectingShooter = false;
            m_tryingToRejectShooter = false;
        });
        State rejectingOutIntake = new InitExitState("Rejecting Out Intake", () -> {
            m_isRejectingIntake = true;
            setNormal(-k_rejectIntakePower, false);

            m_cargoCount--;
            if (m_cargoCount < 0) {
                m_cargoCount = 0;
            }

            stateTimer.reset();
        }, () -> {
            m_isRejectingIntake = false;
        });

        var towerPrimeDetected = new StaticTransition(disengagedIntake, () -> m_robotState.getSensors().isTowerPrimedDirect());
        var serializerPrimeDetected = new StaticTransition(stopAll, () -> m_serializerStopKeepPriming);
        var towerNotPrimeTimeout = new StaticTransition(reprimeTower, () -> stateTimer.hasElapsed(k_towerNotPrimedTimeout) && !m_robotState.getSensors().isTowerPrimedDirect() && !m_commandedToShoot);
        var shotDetected = new StaticTransition(shotMade, () -> m_robotState.shotDetected());

        engagedIdle.addTransition(new StaticTransition(engagedFastIntake, () -> m_commandedAction == Action.INTAKE));
        engagedIdle.addTransition(new StaticTransition(engagedIntake, () -> m_robotState.getSensors().isSerializerPrimedDirect()));
        engagedIdle.addTransition(new StaticTransition(disengagedIdle, () -> m_robotState.getSensors().isTowerPrimedDirect()));
        engagedIdle.addTransition(new StaticTransition(engagedFastIntake, () -> m_robotState.getSensors().getSerializerLidarDetected() && DriverStation.isTeleopEnabled()));

        engagedFastIntake.addTransition(new StaticTransition(engagedFastIntakeDetectedCargo, () -> m_robotState.getSensors().isSerializerPrimedDirect()));
        engagedFastIntake.addTransition(new StaticTransition(engagedIdle, () -> m_keepIntaking));

        engagedFastIntakeDetectedCargo.addTransition(new StaticTransition(engagedIntake, () -> true));

        engagedIntake.addTransition(new StaticTransition(engagedIdle, () -> stateTimer.hasElapsed(k_engagedIntakeTimeout)));
        engagedIntake.addTransition(towerPrimeDetected);
        engagedIntake.addTransition(shotDetected);


        reprimeTower.addTransition(new StaticTransition(disengagedIdle, () -> m_reprimeTower));
        reprimeTower.addTransition(new StaticTransition(engagedIdle, () -> stateTimer.hasElapsed(k_reprimeTimeout)));
        reprimeTower.addTransition(shotDetected);

        disengagedIdle.addTransition(new StaticTransition(disengagedIntake, () -> m_commandedAction == Action.INTAKE));
        disengagedIdle.addTransition(serializerPrimeDetected);
        disengagedIdle.addTransition(towerNotPrimeTimeout);
        disengagedIdle.addTransition(shotDetected);
        disengagedIdle.addTransition(new StaticTransition(disengagedIntake, () -> m_robotState.getSensors().getSerializerLidarDetected()));

        disengagedIntake.addTransition(serializerPrimeDetected);
        disengagedIntake.addTransition(new StaticTransition(disengagedIdle, () -> m_keepIntaking));
        disengagedIntake.addTransition(towerNotPrimeTimeout);
        disengagedIntake.addTransition(shotDetected);

        stopAll.addTransition(shotDetected);
        stopAll.addTransition(towerNotPrimeTimeout);
        stopAll.addTransition(new StaticTransition(disengagedIntake, () -> m_stopAllNoCargoTrig));

        shotMade.addTransition(new StaticTransition(engagedIntake, () -> stateTimer.hasElapsed(k_afterShotDelay)));

        // Reject Transistions
        var rejectOutShooter = new StaticTransition(rejectingOutShooter, () -> m_commandedRejectAction == RejectAction.REJECT_SHOOTER);
        var rejectOutIntake = new StaticTransition(rejectingOutIntake, () -> m_commandedRejectAction == RejectAction.REJECT_INTAKE);
        var autoRejectShooter = new StaticTransition(rejectingOutShooter,
                () -> m_robotState.getSensors().getTowerCargoColor() == getAutoRejectColor() && shouldAutoReject());
        var autoRejectShooterNoCargo = new StaticTransition(rejectingOutShooter,
                () -> (m_robotState.getSensors().getTowerCargoColor() == getAutoRejectColor()
                        || m_robotState.getSensors().getSerializerCargoColor() == getAutoRejectColor())
                        && shouldAutoReject());
        var autoRejectIntake = new StaticTransition(rejectingOutIntake,
                () -> m_robotState.getSensors().getSerializerCargoColor() == getAutoRejectColor()
                        && shouldAutoReject());

        engagedIdle.addTransition(autoRejectShooterNoCargo);
        engagedIdle.addTransition(rejectOutShooter);
        engagedIdle.addTransition(rejectOutIntake);

        engagedFastIntake.addTransition(autoRejectShooterNoCargo);
        engagedFastIntake.addTransition(towerPrimeDetected);
        engagedFastIntake.addTransition(rejectOutShooter);

        engagedIntake.addTransition(autoRejectShooter);
        engagedIntake.addTransition(rejectOutShooter);
        engagedIntake.addTransition(rejectOutIntake);

        reprimeTower.addTransition(autoRejectShooter);
        reprimeTower.addTransition(rejectOutShooter);

        disengagedIdle.addTransition(autoRejectShooter);
        disengagedIdle.addTransition(autoRejectIntake);
        disengagedIdle.addTransition(rejectOutShooter);
        disengagedIdle.addTransition(rejectOutIntake);

        disengagedIntake.addTransition(autoRejectShooter);
        disengagedIntake.addTransition(autoRejectIntake);
        disengagedIntake.addTransition(rejectOutShooter);
        disengagedIntake.addTransition(rejectOutIntake);

        stopAll.addTransition(autoRejectShooter);
        stopAll.addTransition(autoRejectIntake);
        stopAll.addTransition(rejectOutShooter);
        stopAll.addTransition(rejectOutIntake);
        stopAll.addTransition(new StaticTransition(rejectingOutIntake, () -> m_serializerLidarReject));

        rejectingOutShooter.addTransition(new StaticTransition(engagedIntake, () -> m_commandedRejectAction != RejectAction.REJECT_SHOOTER && (m_robotState.shotDetected() || stateTimer.hasElapsed(k_rejectShooterTimeout))));
        rejectingOutShooter.addTransition(rejectOutIntake);

        rejectingOutIntake.addTransition(rejectOutShooter);
        rejectingOutIntake.addTransition(new StaticTransition(engagedIdle, () -> m_commandedRejectAction != RejectAction.REJECT_INTAKE && stateTimer.hasElapsed(k_rejectIntakeTime)));

        //Intake state machine
        m_primaryStateMachine = new StateMachine(engagedFastIntake);
        m_primaryStateMachine.start();
        m_periodicTimer.start();
    }

    private Alliance getAutoRejectColor() {
        if (m_robotState.getAutoRejectSetting() == AutoRejectOption.OURS) {
            return DriverStation.getAlliance();
        }

        if (DriverStation.getAlliance() == Alliance.Red) {
            return Alliance.Blue;
        } else {
            return Alliance.Red;
        }
    }

    private boolean shouldAutoReject() {
        return m_robotState.getAutoRejectSetting() != AutoRejectOption.NONE;
    }

    @Override
    public void periodic() {
        m_periodicTimer.reset();
        if (m_robotState.shotDetected()) {
            m_shotCooldownTimer.reset();
        }

        m_primaryStateMachine.step();

        if(!m_robotState.isShooterHoodUp()) {
            m_shotCooldown = !m_shotCooldownTimer.hasElapsed(ShotCriteria.k_serializerShotCooldownTimeHoodDown);
        } else {
            m_shotCooldown = !m_shotCooldownTimer.hasElapsed(ShotCriteria.k_serializerShotCooldownTimeHoodUp);
        }
        m_shouldShoot = m_commandedToShoot && (m_robotState.isReadyToShoot() || m_shouldShoot) && !m_shotCooldown && !isIntakeRejecting() && !isTryingToRejectShooter();

        m_reprimeTower = k_reprimeTowerDebounce.update(m_robotState.getSensors().isTowerPrimedDirect());
        m_stopAllNoCargoTrig = k_stopAllDebounce.update(!m_robotState.getSensors().isSerializerPrimedDirect());
        m_serializerLidarReject = k_serializerLidarRejectDebounce.update(m_robotState.getSensors().getSerializerLidarDetected());
        m_keepIntaking = k_keepIntakingDebounce.update(m_commandedAction == Action.IDLE && !m_robotState.getSensors().getSerializerLidarDetected());

        m_serializerStopKeepPriming = k_serializerKeepPrimingDebounce.update(m_robotState.getSensors().isSerializerPrimedDirect());

        m_primaryStateMachine.step();

        // Super's periodic runs the Normal state machine, which relies on the above
        // logic/state machines to update member variables used by the Normal state
        // machine
        super.periodic();

        m_logger.setDataPoint(LogEntry.SERIALIZER_NORMAL_POWER, m_normalMotorPower);
        m_logger.setDataPoint(LogEntry.SERIALIZER_NORMAL_ENGAGED, m_normalEngaged);
        m_logger.setDataPoint(LogEntry.SERIALIZER_CARGO_COUNT, m_cargoCount);
        m_logger.setDataPoint(LogEntry.SERIALIZER_COMMANDED_TO_SHOOT, m_commandedToShoot);
        m_logger.setDataPoint(LogEntry.SERIALIZER_SHOULD_SHOOT, m_shouldShoot);
        m_logger.setDataPoint(LogEntry.SERIALIZER_IS_SHOOTING, m_isShooting);
        m_logger.setDataPoint(LogEntry.SERIALIZER_SHOT_COOLDOWN, m_shotCooldown);
        m_logger.setDataPoint(LogEntry.SERIALIZER_NORMAL_STATE, m_primaryStateMachine.getCurrentState().getStateName());
        m_logger.setDataPoint(LogEntry.SERIALIZER_STATE, super.getCurrentState().getStateName());
        m_logger.setDataPoint(LogEntry.SERIALIZER_REPRIME, m_reprimeTower);

        m_logger.setDataPoint(LogEntry.SERIALIZER_PRIMED_ONE, m_cargoCount >= 1);
        m_logger.setDataPoint(LogEntry.SERIALIZER_PRIMED_BOTH, m_cargoCount >= 2);
        m_logger.setDataPoint(LogEntry.SERIALIZER_REJECTING, m_isRejectingIntake || m_isRejectingShooter);

        m_logger.setDataPoint(LogEntry.TIME_SERIALIZER, m_periodicTimer.get().getValue(TimeUnit.MILLISECONDS));
    }

    private void setNormal(double motorPower, Boolean engaged) {
        m_normalMotorPower = motorPower;
        if (engaged != null) {
            m_normalEngaged = engaged;
        }
    }

    private void setMotorPower(double power) {
        m_logger.setDataPoint(LogEntry.SERIALIZER_MOTOR_POWER, power);
        m_motor.set(VictorSPXControlMode.PercentOutput, power);
    }

    public double getMotorPower() {
        return m_motor.getMotorOutputPercent();
    }

    private void stop() {
        m_logger.setDataPoint(LogEntry.SERIALIZER_MOTOR_POWER, 0.0);
        m_motor.set(VictorSPXControlMode.PercentOutput, 0.0);
    }

    public int cargoCount() {
        return m_cargoCount;
    }

    public boolean isShooting() {
        return m_isShooting;
    }

    public boolean isShooterRejecting() {
        return m_isRejectingShooter;
    }

    public boolean isIntakeRejecting() {
        return m_isRejectingIntake;
    }

    public boolean isTryingToRejectShooter() {
        return m_tryingToRejectShooter;
    }

    @Override
    protected State provideDisabledState() {
        return new ExecState("Disabled State", () -> stop());
    }

    @Override
    protected State provideManualState() {
        return new ExecState("Manual State", () -> {
            setMotorPower(m_manualMotorPower);
            if (m_manuallyEngage) {
                m_towerEngagement.engage();
            }
            else {
                m_towerEngagement.disengage();
            }
        });
    }

    @Override
    protected State provideNormalState() {
        var stateTimer = new WpiTimer();
        stateTimer.start();

        Supplier<Boolean> shouldDisengage = () -> !m_normalEngaged && !m_shouldShoot;
        Supplier<Boolean> shouldEngage = () -> m_normalEngaged || m_shouldShoot;

        State engaged = new ExecState("Engaged", () -> {
            // New tower is reversible, though we don't utilize it that way still
            double motorPower = m_normalMotorPower;

            if (m_shouldShoot) {
                motorPower = k_shootPower;
                m_isShooting = true;
            } else {
                m_isShooting = false;
            }

            if (shouldDisengage.get()) {
                motorPower = 0.0;
            }

            setMotorPower(motorPower);
            m_towerEngagement.engage();
        });
        State engaging = new InitExecState("Engaging", stateTimer::reset, () -> {
            setMotorPower(0.0);
            m_towerEngagement.engage();
        });
        State disengaged = new ExecState("Disengaged", () -> {
            double motorPower = m_normalMotorPower;

            if (shouldEngage.get()) {
                motorPower = 0.0;
            }
            setMotorPower(motorPower);
            m_towerEngagement.disengage();
        });
        State disengaging = new InitExecState("Disengaging", stateTimer::reset, () -> {
            setMotorPower(0.0);
            m_towerEngagement.disengage();
        });

        engaged.addTransition(new StaticTransition(disengaging, shouldDisengage));
        disengaged.addTransition(new StaticTransition(engaging, shouldEngage));
        engaging.addTransition(new StaticTransition(engaged, () -> stateTimer.hasElapsed(k_engageTime)));
        disengaging.addTransition(new StaticTransition(disengaged, () -> stateTimer.hasElapsed(k_disengageTime)));

        return new StateMachine(engaging).getAsState("Normal", true, true);
    }

    public boolean isEngaged() {
        return m_towerEngagement.isEngaged();
    }

    public boolean isDisengaged() {
        return m_towerEngagement.isDisengaged();
    }

    // Normal Mode Commands
    private Command getNormalStateCommand(Action action) {
        return new InstantCommand(() -> m_commandedAction = action, this);
    }

    private Command getRejectCommand(RejectAction action) {
        return new InstantCommand(() -> m_commandedRejectAction = action, this);
    }

    public Command getIntakeCommand() {
        return getNormalStateCommand(Action.INTAKE);
    }

    public Command getIdleCommand() {
        return getNormalStateCommand(Action.IDLE);
    }

    public Command getShootCommand() {
        return new InstantCommand(() -> m_commandedToShoot = true);
    }

    public Command getCeaseFireCommand() {
        return new InstantCommand(() -> m_commandedToShoot = false);
    }

    public Command getRejectShooterCommand() {
        return getRejectCommand(RejectAction.REJECT_SHOOTER);
    }

    /**
     * Do not use directly. Use the RobotState version, getRejectIntakeCommand
     * @return Reject Intake Command
     */
    public Command getRejectIntakeCommand() {
        return getRejectCommand(RejectAction.REJECT_INTAKE);
    }

    /**
     * Do not use directly. Use the RobotState version, getNoRejectCommand
     * @return No Reject Command
     */
    public Command getNoRejectCommand() {
        return getRejectCommand(RejectAction.NO_REJECT);
    }

    // Manual Mode Commands
    public Command getManualMotorSpeedCommand(Supplier<Double> power) {
        // clang-format off
        return new FunctionalCommand(() -> {},
                                     () -> m_manualMotorPower = power.get(),
                                     unused -> {},
                                     () -> false,
                                     this);
        // clang-format on
    }

    public Command getManualStopCommand() {
        return new InstantCommand(() -> m_manualMotorPower = 0.0, this);
    }

    public Command getManualEngageCommand() {
        return new InstantCommand(() -> m_manuallyEngage = true, this);
    }

    public Command getManualDisengageCommand() {
        return new InstantCommand(() -> m_manuallyEngage = false, this);
    }
}
