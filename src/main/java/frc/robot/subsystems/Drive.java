/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems;

import com.ctre.phoenix.ErrorCode;
import com.ctre.phoenix.ParamEnum;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;
import com.ctre.phoenix.motorcontrol.TalonFXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.sensors.CANCoder;
import com.ctre.phoenix.sensors.SensorInitializationStrategy;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.FunctionalCommand;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.library.GStemsSubsystem;
import frc.library.SwerveDrive;
import frc.library.SwerveModuleFalconDrvFalconStr;
import frc.library.drivers.CANCoderUtils;
import frc.library.drivers.TalonFXUtils;
import frc.robot.TrapezoidAccProfilePose;
import frc.robot.RobotState;
import frc.robot.constants.LogEntry;
import frc.robot.constants.RobotMap;
import frc.robot.constants.ShotCriteria;
import frc.robot.constants.swerve.MaverickConstants;
import frc.robot.constants.swerve.CBotConstants;
import frc.robot.constants.swerve.PBotConstants;
import frc.robot.constants.swerve.SwerveConstants;
import frc.robot.constants.swerve.SwerveXConstants;
import frc.robot.RobotLogger;

import java.util.List;
import java.util.function.Supplier;

import org.growingstems.control.PidController;
import org.growingstems.frc.util.WpiTimer;
import org.growingstems.logic.JKLatch;
import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.math.Angle;
import org.growingstems.math.Pose2d;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.math.Range;
import org.growingstems.math.Time;
import org.growingstems.math.Vector2d;
import org.growingstems.util.statemachine.ExecExitState;
import org.growingstems.util.statemachine.ExecState;
import org.growingstems.util.statemachine.State;
import org.growingstems.util.timer.TimerI;

public class Drive extends GStemsSubsystem {
    private final TimerI m_periodicTimer = new WpiTimer();
    private final SwerveDrive<SwerveModuleFalconDrvFalconStr> m_swerveDrive;
    private final RobotState m_robotState;
    private final RobotLogger m_logger;
    private final SwerveConstants m_sc;
    private Angle m_smartHeadingSetpoint = new Angle();

    public final static int k_defaultAinTempVbatPeriod_ms = 100;

    private final TimerI m_drivePidChecker = new WpiTimer();
    private final Time m_drivePidCheckTime = new Time(5.0, TimeUnit.SECONDS);

    private static final int k_defaultPidIdx = 0;
    private static final int k_remoteSensor0 = 0;
    private static final Angle k_smartHeadingAutoThreshold = new Angle(4.0, AngleUnit.DEGREES);
    private static final Range k_zeroPowerRange = new Range(-1e-9, 1e-9);
    private static final double k_smartHeadingThreshold_radps = 0.15;
    private static final Range k_smartHeadingRange_radps = new Range(-k_smartHeadingThreshold_radps, k_smartHeadingThreshold_radps);
    private JKLatch m_smartHeadingEnabledLatch = new JKLatch(false, Edge.RISING);
    private boolean m_smartHeadingEnabled = false;
    private boolean m_hasPid = false;

    private static final double k_posThreshold_in = 3.0;

    private static final double k_headingP = 0.01;
    private static final double k_headingI = 0.0;
    private static final double k_headingD = 0.0;
    private static final double k_headingIZone = 4.0;
    private static final double k_headingIResetZone = 0.8;
    private static final double k_pGainToPose = 0.02;
    private static final double k_maxSpeedToPose = 1.0;
    private static final double k_smartHeadingVelocityFeedForward_power_per_rps = 0.0819; // 700 deg/s per power
    private final PidController m_headingController =
        new PidController(k_headingP, k_headingI, k_headingD, new WpiTimer());

    private Runnable m_driveExecution = () -> manualDrive(new Vector2d(0.0, 0.0), 0.0, false, true);

    private class LoggingSwerveModule extends SwerveModuleFalconDrvFalconStr {
        private final LogEntry m_forwardsXEntry;
        private final LogEntry m_forwardsYEntry;
        private final LogEntry m_sensorPosEntry;
        private final LogEntry m_driveReset;
        private final LogEntry m_steerReset;
        public LoggingSwerveModule(String name,
                                   TalonFX motorDrive,
                                   TalonFX motorSteer,
                                   CANCoder steerSensor,
                                   Vector2d modulePosition,
                                   double driveInchesPerSensorUnit,
                                   double steerRadiansPerSensorUnit,
                                   LogEntry forwardsXEntry,
                                   LogEntry forwardsYEntry,
                                   LogEntry sensorPosEntry,
                                   LogEntry driveReset,
                                   LogEntry steerReset) {
            super(name, motorDrive, motorSteer, steerSensor, modulePosition, driveInchesPerSensorUnit, steerRadiansPerSensorUnit);
            m_forwardsXEntry = forwardsXEntry;
            m_forwardsYEntry = forwardsYEntry;
            m_sensorPosEntry = sensorPosEntry;
            m_driveReset = driveReset;
            m_steerReset = steerReset;
        }

        @Override
        protected double getRawDrivePosition_su() {
            var rawDrivePosition = super.getRawDrivePosition_su();
            m_logger.setDataPoint(m_sensorPosEntry, rawDrivePosition);
            m_logger.setDataPoint(m_driveReset, m_motorDrive.hasResetOccurred());
            m_logger.setDataPoint(m_steerReset, m_motorSteer.hasResetOccurred());
            return rawDrivePosition;
        }

        @Override
        public Vector2d getForwardKinematics() {
            var forwardsKinematics = super.getForwardKinematics();
            m_logger.setDataPoint(m_forwardsXEntry, forwardsKinematics.getX());
            m_logger.setDataPoint(m_forwardsYEntry, forwardsKinematics.getY());
            return forwardsKinematics;
        }
    }

    private final SwerveModuleFalconDrvFalconStr m_frontRight;
    private final SwerveModuleFalconDrvFalconStr m_frontLeft;
    private final SwerveModuleFalconDrvFalconStr m_backLeft;
    private final SwerveModuleFalconDrvFalconStr m_backRight;

    public Drive(RobotState robotState, RobotLogger logger) {
        super("drive");
        m_robotState = robotState;
        m_logger = logger;
        m_logger.setDataPoint(LogEntry.DRIVE_SHOT_DTHETA_DT_RPS, 0.0);
        switch (m_robotState.robotId) {
            case SWERVEX:
                m_sc = new SwerveXConstants();
                break;
            case MAVERICK:
                m_sc = new MaverickConstants();
                break;
            case P_BOT:
                m_sc = new PBotConstants();
                break;
            default:
                DriverStation.reportWarning("Robot ID \"" + m_robotState.robotId.toString() +
                                            "\" has no linked Swerve Constants! Defaulting to CBOT",
                                            false);
            case STING_RAY:
                m_sc = new CBotConstants();
                break;
        }

        m_frontRight = new LoggingSwerveModule("front_right",
            TalonFXUtils.createDefaultTalonFX(RobotMap.k_swerveFrontRightPowerCanId, RobotMap.k_secondCAN, false),
            TalonFXUtils.createDefaultTalonFX(RobotMap.k_swerveFrontRightSteerCanId, RobotMap.k_secondCAN, false),
            CANCoderUtils.createDefaultCANCoder(RobotMap.k_swerveFrontRightCanCoderCanId, RobotMap.k_secondCAN),
            m_sc.fr.pos,
            m_sc.fr.ipsu,
            m_sc.steer_rpsu,
            LogEntry.DRIVE_FR_FORWARDS_KINEMATICS_X,
            LogEntry.DRIVE_FR_FORWARDS_KINEMATICS_Y,
            LogEntry.DRIVE_FR_SENSOR_POSITION,
            LogEntry.DRIVE_FR_DRIVE_RESET,
            LogEntry.DRIVE_FR_STEER_RESET);
        m_frontLeft = new LoggingSwerveModule("front_left",
            TalonFXUtils.createDefaultTalonFX(RobotMap.k_swerveFrontLeftPowerCanId, RobotMap.k_secondCAN, false),
            TalonFXUtils.createDefaultTalonFX(RobotMap.k_swerveFrontLeftSteerCanId, RobotMap.k_secondCAN, false),
            CANCoderUtils.createDefaultCANCoder(RobotMap.k_swerveFrontLeftCanCoderCanId, RobotMap.k_secondCAN),
            m_sc.fl.pos,
            m_sc.fl.ipsu,
            m_sc.steer_rpsu,
            LogEntry.DRIVE_FL_FORWARDS_KINEMATICS_X,
            LogEntry.DRIVE_FL_FORWARDS_KINEMATICS_Y,
            LogEntry.DRIVE_FL_SENSOR_POSITION,
            LogEntry.DRIVE_FL_DRIVE_RESET,
            LogEntry.DRIVE_FL_STEER_RESET);
        m_backLeft = new LoggingSwerveModule("back_left",
            TalonFXUtils.createDefaultTalonFX(RobotMap.k_swerveBackLeftPowerCanId, RobotMap.k_secondCAN, false),
            TalonFXUtils.createDefaultTalonFX(RobotMap.k_swerveBackLeftSteerCanId, RobotMap.k_secondCAN, false),
            CANCoderUtils.createDefaultCANCoder(RobotMap.k_swerveBackLeftCanCoderCanId, RobotMap.k_secondCAN),
            m_sc.bl.pos,
            m_sc.bl.ipsu,
            m_sc.steer_rpsu,
            LogEntry.DRIVE_BL_FORWARDS_KINEMATICS_X,
            LogEntry.DRIVE_BL_FORWARDS_KINEMATICS_Y,
            LogEntry.DRIVE_BL_SENSOR_POSITION,
            LogEntry.DRIVE_BL_DRIVE_RESET,
            LogEntry.DRIVE_BL_STEER_RESET);
        m_backRight = new LoggingSwerveModule("back_right",
            TalonFXUtils.createDefaultTalonFX(RobotMap.k_swerveBackRightPowerCanId, RobotMap.k_secondCAN, false),
            TalonFXUtils.createDefaultTalonFX(RobotMap.k_swerveBackRightSteerCanId, RobotMap.k_secondCAN, false),
            CANCoderUtils.createDefaultCANCoder(RobotMap.k_swerveBackRightCanCoderCanId, RobotMap.k_secondCAN),
            m_sc.br.pos,
            m_sc.br.ipsu,
            m_sc.steer_rpsu,
            LogEntry.DRIVE_BR_FORWARDS_KINEMATICS_X,
            LogEntry.DRIVE_BR_FORWARDS_KINEMATICS_Y,
            LogEntry.DRIVE_BR_SENSOR_POSITION,
            LogEntry.DRIVE_BR_DRIVE_RESET,
            LogEntry.DRIVE_BR_STEER_RESET);
        ErrorCode error;

        var baseSwerveModules = List.of(m_frontRight, m_frontLeft, m_backLeft, m_backRight);

        m_frontRight.getDriveController().setInverted(m_sc.fr.driveInvert);
        m_frontLeft.getDriveController().setInverted(m_sc.fl.driveInvert);
        m_backLeft.getDriveController().setInverted(m_sc.bl.driveInvert);
        m_backRight.getDriveController().setInverted(m_sc.br.driveInvert);

        m_frontRight.getDriveController().setSensorPhase(m_sc.fr.driveSensorPhase);
        m_frontLeft.getDriveController().setSensorPhase(m_sc.fl.driveSensorPhase);
        m_backLeft.getDriveController().setSensorPhase(m_sc.bl.driveSensorPhase);
        m_backRight.getDriveController().setSensorPhase(m_sc.br.driveSensorPhase);

        m_frontRight.setAbsSteerAngleOffset(new Angle(m_sc.fr.steerEncoderoffset, AngleUnit.DEGREES));
        m_frontLeft.setAbsSteerAngleOffset(new Angle(m_sc.fl.steerEncoderoffset, AngleUnit.DEGREES));
        m_backLeft.setAbsSteerAngleOffset(new Angle(m_sc.bl.steerEncoderoffset, AngleUnit.DEGREES));
        m_backRight.setAbsSteerAngleOffset(new Angle(m_sc.br.steerEncoderoffset, AngleUnit.DEGREES));

        for (var module : baseSwerveModules) {
            module.getDriveController().setNeutralMode(NeutralMode.Brake);
            error = module.getDriveController().configSelectedFeedbackSensor(TalonFXFeedbackDevice.IntegratedSensor,
                                                                             k_defaultPidIdx,
                                                                             TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "configSelectedFeedbackSensor");

            error =
                module.getSteerController().config_kP(m_sc.steerPid.slot, m_sc.steerPid.p, TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "config_kP");
            error =
                module.getSteerController().config_kI(m_sc.steerPid.slot, m_sc.steerPid.i, TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "config_kI");
            error =
                module.getSteerController().config_kD(m_sc.steerPid.slot, m_sc.steerPid.d, TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "config_kD");
            error =
                module.getSteerController().config_kF(m_sc.steerPid.slot, m_sc.steerPid.f, TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "config_kF");
            error = module.getSteerController().configAllowableClosedloopError(m_sc.steerPid.slot,
                                                                               m_sc.steerPid.allowedError,
                                                                               TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "configAllowableClosedloopError");
            error = module.getSteerController().config_IntegralZone(m_sc.steerPid.slot,
                                                                    m_sc.steerPid.izone,
                                                                    TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "config_IntegralZone");

            error = module.getSteerController().config_kP(m_sc.unloadedSteerPid.slot,
                                                          m_sc.unloadedSteerPid.p,
                                                          TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "config_kP");
            error = module.getSteerController().config_kI(m_sc.unloadedSteerPid.slot,
                                                          m_sc.unloadedSteerPid.i,
                                                          TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "config_kI");
            error = module.getSteerController().config_kD(m_sc.unloadedSteerPid.slot,
                                                          m_sc.unloadedSteerPid.d,
                                                          TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "config_kD");
            error = module.getSteerController().config_kF(m_sc.unloadedSteerPid.slot,
                                                          m_sc.unloadedSteerPid.f,
                                                          TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "config_kF");
            error = module.getSteerController().configAllowableClosedloopError(m_sc.unloadedSteerPid.slot,
                                                                               m_sc.unloadedSteerPid.allowedError,
                                                                               TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "configAllowableClosedloopError");
            error = module.getSteerController().config_IntegralZone(m_sc.unloadedSteerPid.slot,
                                                                    m_sc.unloadedSteerPid.izone,
                                                                    TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "config_IntegralZone");

            error =
                module.getSteerController().configMotionCruiseVelocity(m_sc.mm.cruiseVel, TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "configMotionCruiseVelocity");
            error = module.getSteerController().configMotionAcceleration(m_sc.mm.acceleration,
                                                                         TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "configMotionAcceleration");

            error = module.getSteerController().configNeutralDeadband(m_sc.steer.neutralDeadband,
                                                                         TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "configNeutralDeadband");

            error = module.getSteerEncoder().configMagnetOffset(0.0, CANCoderUtils.k_cfgTimeout_ms);
            CANCoderUtils.checkError(error, "configMagnetOffset");

            error = module.getSteerController().configRemoteFeedbackFilter(module.getSteerEncoder(),
                                                                           k_remoteSensor0,
                                                                           TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "configRemoteFeedbackFilter");
            error = module.getSteerController().configSelectedFeedbackSensor(TalonFXFeedbackDevice.RemoteSensor0,
                                                                             k_defaultPidIdx,
                                                                             TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "configSelectedFeedbackSensor");
            module.getSteerController().setSensorPhase(m_sc.steer.sensorPhase);
            module.getSteerController().setInverted(m_sc.steer.invert);
            error = module.getSteerController().configVoltageCompSaturation(m_sc.steer.vCompSat,
                                                                             TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "configVoltageCompSaturation");
            module.getSteerController().enableVoltageCompensation(true);
            module.getSteerController().setNeutralMode(NeutralMode.Brake);

            error = module.getSteerEncoder().configSensorInitializationStrategy(
                SensorInitializationStrategy.BootToAbsolutePosition,
                CANCoderUtils.k_cfgTimeout_ms);
            CANCoderUtils.checkError(error, "configSensorInitializationStrategy");

            error = module.getDriveController().configSelectedFeedbackSensor(TalonFXFeedbackDevice.IntegratedSensor, k_defaultPidIdx, TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "configSelectedFeedbackSensor");

            module.getSteerController().selectProfileSlot(m_sc.steerPid.slot, k_defaultPidIdx);

            error = module.getDriveController().setSelectedSensorPosition(0.0, k_defaultPidIdx, TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "setSelectedSensorPosition");

            // Set Temperature Status Rate
            error = module.getDriveController().setStatusFramePeriod(StatusFrameEnhanced.Status_4_AinTempVbat, k_defaultAinTempVbatPeriod_ms, TalonFXUtils.k_cfgTimeout_ms);
            TalonFXUtils.checkError(error, "getStatusFramePeriod_Status_4_AinTempVbat");
        }

        m_hasPid = false;
        m_swerveDrive = new SwerveDrive<>(baseSwerveModules);
        m_logger.setDataPoint(LogEntry.DRIVE_HAS_PID, m_hasPid);

        m_periodicTimer.start();
    }

    public void enableDriveCurrentLimiting(boolean enable){
        m_frontLeft.enableDriveCurrentLimiting(enable);
        m_frontRight.enableDriveCurrentLimiting(enable);
        m_backLeft.enableDriveCurrentLimiting(enable);
        m_backRight.enableDriveCurrentLimiting(enable);
    }

    @Override
    public void periodic() {
        m_periodicTimer.reset();
        super.periodic();
        var smartHeadingError = getSmartHeadingError().toString(AngleUnit.DEGREES);

        Vector2d velocityVector = getVelocityVector_inps();
        m_logger.setDataPoint(LogEntry.DRIVE_VEL_X, velocityVector.getX());
        m_logger.setDataPoint(LogEntry.DRIVE_VEL_Y, velocityVector.getY());
        m_logger.setDataPoint(LogEntry.DRIVE_SMART_HEADING_ERROR, smartHeadingError);

        var fr_v = m_frontRight.getVelocityVector();
        m_logger.setDataPoint(LogEntry.DRIVE_FR_VELOCITY_X, fr_v.getX());
        m_logger.setDataPoint(LogEntry.DRIVE_FR_VELOCITY_Y, fr_v.getY());
        m_logger.setDataPoint(LogEntry.DRIVE_FR_CURRENT, m_frontRight.getDriveController().getSupplyCurrent());
        m_logger.setDataPoint(LogEntry.DRIVE_FR_SELECTED_STEER_ANGLE, m_frontRight.getSteerAngle().getValue(AngleUnit.DEGREES));
        m_logger.setDataPoint(LogEntry.DRIVE_FR_CANCODER_ABS_POS, m_frontRight.getSteerEncoder().getAbsolutePosition());
        m_logger.setDataPoint(LogEntry.DRIVE_FR_CANCODER_POS, m_frontRight.getSteerEncoder().getPosition());

        var fl_v = m_frontLeft.getVelocityVector();
        m_logger.setDataPoint(LogEntry.DRIVE_FL_VELOCITY_X, fl_v.getX());
        m_logger.setDataPoint(LogEntry.DRIVE_FL_VELOCITY_Y, fl_v.getY());
        m_logger.setDataPoint(LogEntry.DRIVE_FL_CURRENT, m_frontLeft.getDriveController().getSupplyCurrent());
        m_logger.setDataPoint(LogEntry.DRIVE_FL_SELECTED_STEER_ANGLE, m_frontLeft.getSteerAngle().getValue(AngleUnit.DEGREES));
        m_logger.setDataPoint(LogEntry.DRIVE_FL_CANCODER_ABS_POS, m_frontLeft.getSteerEncoder().getAbsolutePosition());
        m_logger.setDataPoint(LogEntry.DRIVE_FL_CANCODER_POS, m_frontLeft.getSteerEncoder().getPosition());

        var bl_v = m_backLeft.getVelocityVector();
        m_logger.setDataPoint(LogEntry.DRIVE_BL_VELOCITY_X, bl_v.getX());
        m_logger.setDataPoint(LogEntry.DRIVE_BL_VELOCITY_Y, bl_v.getY());
        m_logger.setDataPoint(LogEntry.DRIVE_BL_CURRENT, m_backLeft.getDriveController().getSupplyCurrent());
        m_logger.setDataPoint(LogEntry.DRIVE_BL_SELECTED_STEER_ANGLE, m_backLeft.getSteerAngle().getValue(AngleUnit.DEGREES));
        m_logger.setDataPoint(LogEntry.DRIVE_BL_CANCODER_ABS_POS, m_backLeft.getSteerEncoder().getAbsolutePosition());
        m_logger.setDataPoint(LogEntry.DRIVE_BL_CANCODER_POS, m_backLeft.getSteerEncoder().getPosition());

        var br_v = m_backRight.getVelocityVector();
        m_logger.setDataPoint(LogEntry.DRIVE_BR_VELOCITY_X, br_v.getX());
        m_logger.setDataPoint(LogEntry.DRIVE_BR_VELOCITY_Y, br_v.getY());
        m_logger.setDataPoint(LogEntry.DRIVE_BR_CURRENT, m_backRight.getDriveController().getSupplyCurrent());
        m_logger.setDataPoint(LogEntry.DRIVE_BR_SELECTED_STEER_ANGLE, m_backRight.getSteerAngle().getValue(AngleUnit.DEGREES));
        m_logger.setDataPoint(LogEntry.DRIVE_BR_CANCODER_ABS_POS, m_backRight.getSteerEncoder().getAbsolutePosition());
        m_logger.setDataPoint(LogEntry.DRIVE_BR_CANCODER_POS, m_backRight.getSteerEncoder().getPosition());

        m_logger.setDataPoint(LogEntry.DRIVE_FR_TEMPERATURE, m_frontRight.getDriveController().getTemperature());
        m_logger.setDataPoint(LogEntry.DRIVE_FL_TEMPERATURE, m_frontLeft.getDriveController().getTemperature());
        m_logger.setDataPoint(LogEntry.DRIVE_BL_TEMPERATURE, m_backLeft.getDriveController().getTemperature());
        m_logger.setDataPoint(LogEntry.DRIVE_BR_TEMPERATURE, m_backRight.getDriveController().getTemperature());

        m_logger.setDataPoint(LogEntry.DRIVE_STATE, super.getCurrentState().getStateName());

        m_logger.setDataPoint(LogEntry.TIME_DRIVE, m_periodicTimer.get().getValue(TimeUnit.MILLISECONDS));
    }

    private boolean isAcceptable(double expected, double actual) {
        return (expected == 0.0) || (actual != 0.0);
    }

    @Override
    protected boolean isReadyToStartMatch() {
        int timeoutMs = 0;
        if (m_drivePidChecker.hasPeriodPassed(m_drivePidCheckTime) || !m_hasPid) {
            boolean hasPid = true;
            for (var module : m_swerveDrive.getModules()) {
                var steerController = module.getSteerController();
                m_hasPid &= isAcceptable(m_sc.steerPid.p, steerController.configGetParameter(ParamEnum.eProfileParamSlot_P, m_sc.steerPid.slot, timeoutMs)) &&
                          isAcceptable(m_sc.steerPid.i, steerController.configGetParameter(ParamEnum.eProfileParamSlot_I, m_sc.steerPid.slot, timeoutMs)) &&
                          isAcceptable(m_sc.steerPid.d, steerController.configGetParameter(ParamEnum.eProfileParamSlot_D, m_sc.steerPid.slot, timeoutMs)) &&
                          isAcceptable(m_sc.steerPid.f, steerController.configGetParameter(ParamEnum.eProfileParamSlot_F, m_sc.steerPid.slot, timeoutMs));
            }
            m_hasPid = hasPid;
            m_logger.setDataPoint(LogEntry.DRIVE_HAS_PID, m_hasPid);
        }
        return m_hasPid;
    }

    public boolean isReadyToShoot() {
        boolean speedIsAcceptable = true;
        if (!m_robotState.isVelocityCompensationEnabled()) {
            speedIsAcceptable = Math.abs(m_robotState.getSensors().getHeadingAngularVelocity_radps()) < ShotCriteria.k_driveAngularVelocityThreshold_radps &&
                                Math.abs(getVelocityVector_inps().getMagnitude()) < ShotCriteria.k_driveVelocityThreshold_inps;
        }

        boolean headingIsAcceptable = true;
        if (m_smartHeadingEnabled) {
            headingIsAcceptable = getSmartHeadingError().abs().getValue(AngleUnit.DEGREES) < ShotCriteria.k_driveAllowableHeadingError.getValue(AngleUnit.DEGREES);
        }

        return speedIsAcceptable && headingIsAcceptable;
    }

    /**
     * Gets the difference in position since the last time this function was called.
     * This delta in position is robot centric where +X is forwards relative to the
     * module and +Y is left relative to the module. The first time this function is
     * called, it always returns (0.0, 0.0).
     *
     * @return the difference in position since the last time this function was
     *         called.
     */
    public Vector2d getForwardKinematics() {
        return m_swerveDrive.getForwardKinematics();
    }

    /**
     * Gets the drive's current velocity as a vector relative to the robot.
     *
     * @return Curent velocity of the drive
     */
    public Vector2d getVelocityVector_inps() {
        return m_swerveDrive.getVelocityVector();
    }

    public void setSmartHeading(Angle target) {
        m_smartHeadingSetpoint = target;
    }

    private void manualDrive(Vector2d translation, double rotation, boolean forceSmartHeading, boolean useFoc) {
        if (useFoc) {
            translation = translation.rotate(m_robotState.getSensors().getImuReading().negate());
        }

        boolean set = k_zeroPowerRange.inRange(rotation) &&
                      k_smartHeadingRange_radps.inRange(m_robotState.getSensors().getHeadingAngularVelocity_radps());
        boolean reset = !k_zeroPowerRange.inRange(rotation);
        m_smartHeadingEnabled = forceSmartHeading || m_smartHeadingEnabledLatch.step(set, reset);
        double difference = getSmartHeadingError().getValue(AngleUnit.DEGREES);
        if (m_smartHeadingEnabled && Double.isFinite(difference)) {
            if (Math.abs(difference) <= k_headingIZone) {
                m_headingController.setIGain(k_headingI);
            } else {
                m_headingController.setIGain(0.0);
            }

            if (Math.abs(difference) <= k_headingIResetZone || !DriverStation.isEnabled()) {
                m_headingController.resetIntegral();
            }
            rotation -= m_headingController.update(difference);
        } else {
            m_headingController.resetIntegral();
            setSmartHeading(m_robotState.getSensors().getImuReading());
        }
        m_logger.setDataPoint(LogEntry.DRIVE_TRANSLATION_POWER_MAG, translation.getMagnitude());
        m_logger.setDataPoint(LogEntry.DRIVE_TRANSLATION_POWER_ANGLE, translation.getAngle().getValue(AngleUnit.DEGREES));
        m_logger.setDataPoint(LogEntry.DRIVE_ROTATION_POWER, rotation);
        m_swerveDrive.setOpenLoop(translation, rotation);
    }

    private void manualUnmanagedDrive(Vector2d translation, double rotation, boolean useFoc) {
        if (useFoc) {
            translation = translation.rotate(m_robotState.getSensors().getImuReading().negate());
        }
        m_headingController.resetIntegral();
        m_swerveDrive.setOpenLoop(translation, rotation);
        setSmartHeading(m_robotState.getSensors().getImuReading());
    }

    public Angle getSmartHeadingError() {
        return m_robotState.getSensors().getImuReading().difference(m_smartHeadingSetpoint);
    }

    private void manualRotate(double power, Vector2d origin) {
        m_swerveDrive.rotateOpenLoop(origin, power);
    }

    public Command getDirectUnmanagedDriveCommand(Supplier<Vector2d> velocity, Supplier<Double> rotation) {
        // clang-format off
        return new FunctionalCommand(() -> {},
                                     () -> m_driveExecution = () -> manualUnmanagedDrive(velocity.get(), rotation.get(), true),
                                     unused -> m_driveExecution = m_swerveDrive::stop,
                                     () -> false,
                                     this);
        // clang-format on
    }

    public Command getDirectDriveCommand(Supplier<Double> x, Supplier<Double> y, Supplier<Double> rotation) {
        return getDirectDriveCommand(() -> new Vector2d(x.get(), y.get()), rotation);
    }

    public Command getDirectDriveCommand(Supplier<Vector2d> velocity, Supplier<Double> rotation) {
        // clang-format off
        return new FunctionalCommand(() -> {},
                                     () -> m_driveExecution = () -> manualDrive(velocity.get(), rotation.get(), false, true),
                                     unused -> m_driveExecution = m_swerveDrive::stop,
                                     () -> false,
                                     this);
        // clang-format on
    }

    public Command getCurrentLimitEnableCommand(){
        return new InstantCommand(() -> enableDriveCurrentLimiting(true));
    }

    public Command getCurrentLimitDisableCommand(){
        return new InstantCommand(() -> enableDriveCurrentLimiting(false));
    }

    public Command getDirectDriveWithoutFocCommand(Supplier<Vector2d> velocity, Supplier<Double> rotation) {
        // clang-format off
        return new FunctionalCommand(() -> {},
                                     () -> m_driveExecution = () -> m_swerveDrive.setOpenLoop(velocity.get(), rotation.get()),
                                     unused -> m_driveExecution = m_swerveDrive::stop,
                                     () -> false,
                                     this);
        // clang-format on
    }

    public Command getManualRotateCommand(Supplier<Double> power, Supplier<Vector2d> origin) {
        // clang-format off
        return new FunctionalCommand(() -> {},
                                     () -> m_driveExecution = () -> manualRotate(power.get(), origin.get()),
                                     unused -> m_driveExecution = m_swerveDrive::stop,
                                     () -> false,
                                     this);
        // clang-format on
    }

    public Command getSmartTurnToHubCommand(Supplier<Vector2d> translationalControl) {
        return getTurnToHeadingCommand(translationalControl,
            () -> m_robotState.getShotVectorToHub_in().getAngle().add(new Angle(180.0, AngleUnit.DEGREES)),
            () -> {
                var shotVector = m_robotState.getShotVectorToHub_in();
                var velocityVector = m_robotState.getFieldVelocity_in().getVector();
                var dTheta_dT_rps = (shotVector.getY() * velocityVector.getX() - shotVector.getX() * velocityVector.getY())
                    / (shotVector.getX() * shotVector.getX() + shotVector.getY() * shotVector.getY());
                m_logger.setDataPoint(LogEntry.DRIVE_SHOT_DTHETA_DT_RPS, dTheta_dT_rps);
                return dTheta_dT_rps * k_smartHeadingVelocityFeedForward_power_per_rps;
            });
    }

    public Command getTurnToHeadingCommand(Supplier<Vector2d> translationalControl, Supplier<Angle> angleToFace) {
        return getTurnToHeadingCommand(translationalControl, angleToFace, () -> 0.0);
    }

    public Command getTurnToHeadingCommand(Supplier<Vector2d> translationalControl, Supplier<Angle> angleToFace, Supplier<Double> rotationFeedForward) {
        return new FunctionalCommand(
                                     () -> {},
                                     () -> m_driveExecution = () -> {
                                         setSmartHeading(angleToFace.get());
                                         manualDrive(translationalControl.get(), rotationFeedForward.get(), true, true);
                                     },
                                     unused -> m_driveExecution = m_swerveDrive::stop,
                                     () -> false,
                                     this);
    }

    public Command getTurnToHeadingWithManualBackupCommand(Supplier<Vector2d> translationalControl, Supplier<Angle> angleToFace, Supplier<Double> backupRotationControl) {
        return getTurnToHeadingWithManualBackupCommand(translationalControl, angleToFace, backupRotationControl, () -> {});
    }

    public Command getTurnToHeadingWithManualBackupCommand(Supplier<Vector2d> translationalControl, Supplier<Angle> angleToFace, Supplier<Double> backupRotationControl, Runnable onEnd) {
        return new FunctionalCommand(
                                     () -> {},
                                     () -> m_driveExecution = () -> {
                                         Angle angleToFaceLocal = angleToFace.get();
                                         if (Double.isFinite(angleToFaceLocal.getValue(AngleUnit.DEGREES))) {
                                            setSmartHeading(angleToFace.get());
                                            manualDrive(translationalControl.get(), 0.0, true, true);
                                         } else {
                                            manualDrive(translationalControl.get(), backupRotationControl.get(), false, true);
                                         }
                                     },
                                     unused -> {
                                         m_driveExecution = m_swerveDrive::stop;
                                         onEnd.run();
                                     },
                                     () -> false,
                                     this);
    }

    public Command getTurnToHeadingWithoutFocCommand(Supplier<Vector2d> translationalControl, Supplier<Angle> angleToFace) {
        return new FunctionalCommand(
            () -> {},
            () -> m_driveExecution = () -> {
                if (Double.isFinite(angleToFace.get().getValue(AngleUnit.DEGREES))) {
                    setSmartHeading(angleToFace.get());
                    manualDrive(translationalControl.get(), 0.0, true, false);
                } else {
                    manualDrive(translationalControl.get(), 0.0, false, false);
                }
            },
            unused -> m_driveExecution = m_swerveDrive::stop,
            () -> false,
            this);
    }

    public Command getGoToPoseCommand(Supplier<Pose2d> to, Pose2d finalPose) {
        return new FunctionalCommand(
                () -> {
                },
                () -> m_driveExecution = () -> {
                    var poseGoal = to.get();
                    setSmartHeading(poseGoal.getRotation());
                    var deltaVector = poseGoal.getVector().subtract(m_robotState.getOdometryRobotPose_in().getVector());
                    m_logger.setDataPoint(LogEntry.DRIVE_POSE_GOAL_VEC_ANG, deltaVector.getAngle());
                    m_logger.setDataPoint(LogEntry.DRIVE_POSE_GOAL_VEC_MAG, deltaVector.getMagnitude());
                    var deltaScaledVector = deltaVector.scale(k_pGainToPose);
                    if (deltaScaledVector.getMagnitude() > k_maxSpeedToPose) {
                        deltaScaledVector = new Vector2d(k_maxSpeedToPose, deltaScaledVector.getAngle());
                    }
                    manualDrive(deltaScaledVector, 0.0, true, true);
                },
                unused -> m_driveExecution = m_swerveDrive::stop,
                () -> {
                    var posDifference = m_robotState.getOdometryRobotPose_in().getVector()
                            .subtract(finalPose.getVector());
                    var angleDifference = m_robotState.getSensors().getImuReading().difference(finalPose.getRotation());
                    m_logger.setDataPoint(LogEntry.DRIVE_DIST_GOAL_POSE, posDifference.getMagnitude());
                    var condition = posDifference.getMagnitude() < k_posThreshold_in && angleDifference.abs()
                            .getValue(AngleUnit.DEGREES) < k_smartHeadingAutoThreshold.getValue(AngleUnit.DEGREES);
                    m_logger.setDataPoint(LogEntry.DRIVE_POSE_GOAL_AT_POSE, condition);
                    return condition;
                },
                this);
    }

    public Command getTwoPointWithProfileCommand(double maxTranslationAccPercent,
                                                 double maxTranslationVelPercent,
                                                 double maxTurnAccPercent,
                                                 double maxTurnVelPercent,
                                                 Pose2d to,
                                                 Pose2d from) {
        var profile = new TrapezoidAccProfilePose(maxTranslationAccPercent,
                                                  maxTranslationVelPercent,
                                                  maxTurnAccPercent,
                                                  maxTurnVelPercent,
                                                  to,
                                                  from);
        return getGoToPoseCommand(profile::getCurrentPose, to);
    }

    @Override
    protected State provideDisabledState() {
        return new ExecExitState("Disabled State", () -> m_swerveDrive.stop(), () -> m_swerveDrive.zeroSteerEncoders());
    }

    @Override
    protected State provideManualState() {
        return new ExecState("Manual State", () -> m_driveExecution.run());
    }

    @Override
    protected State provideNormalState() {
        return new ExecState("Normal State", () -> m_driveExecution.run());
    }
}
