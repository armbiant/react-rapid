/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import frc.library.GStemsSubsystem;
import frc.library.Length;
import frc.library.Length.LengthUnit;
import frc.library.vision.ScreenPos;
import frc.library.vision.VisionProcessor;
import frc.robot.constants.FieldDimensions;
import frc.robot.constants.LogEntry;
import frc.robot.constants.RobotMap;
import frc.robot.RobotLogger;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.growingstems.math.Angle;
import org.growingstems.math.Vector2d;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.frc.util.WpiTimer;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.util.statemachine.InitExitState;
import org.growingstems.util.statemachine.InitState;
import org.growingstems.util.statemachine.State;
import org.growingstems.util.timer.TimerI;

public class CargoTrackerCamera extends GStemsSubsystem {
    public static final Vector2d k_lensPositionFromRobotCenter_in = new Vector2d(7.5, 0.0);
    private static final Length k_lensHeightFromFloor = new Length(35.0, LengthUnit.INCHES);
    private static final Angle k_lensPitchAngle = new Angle(-33.5, AngleUnit.DEGREES);
    private static final Angle k_cameraVerticalFov = new Angle(52.5, AngleUnit.DEGREES);
    private static final Angle k_cameraHorizontalFov = new Angle(70.0, AngleUnit.DEGREES);
    private static final int k_cameraWidth = 320;
    private static final int k_cameraHeight = 240;
    private static VisionProcessor m_camera = new VisionProcessor(k_cameraVerticalFov, k_cameraHorizontalFov, k_cameraHeight, k_cameraWidth);

    private static final long k_sleepTime_ms = 1000;
    public static final int k_cameraPacketSizeBytes = 93;

    public static final int k_packetCargoCount = 5;
    private static final int k_headingByteCount = 2 + 4 * 2;
    private static final int k_roiMean = 4 * 3;
    private static final int k_cargoPacketSize =
        k_headingByteCount + k_roiMean + k_packetCargoCount * TrackedCargo.k_cargoSizeBytes;

    private final TimerI m_periodicTimer = new WpiTimer();
    private final RobotLogger m_logger;
    private final DatagramSocket m_socket;
    private final Thread m_serverThread;
    private final Queue<List<TrackedCargo>> m_cargo = new ConcurrentLinkedQueue<>();
    private final InetAddress m_piAddress;

    private final NetworkTableEntry m_brightnessEntry = SmartDashboard.getEntry("CargoTracker/Brightness");
    private final NetworkTableEntry m_contrastEntry = SmartDashboard.getEntry("CargoTracker/Contrast");
    private final NetworkTableEntry m_saturationEntry = SmartDashboard.getEntry("CargoTracker/Saturation");
    private final NetworkTableEntry m_redBalanceEntry = SmartDashboard.getEntry("CargoTracker/Red Balance");
    private final NetworkTableEntry m_blueBalanceEntry = SmartDashboard.getEntry("CargoTracker/Blue Balance");
    private final NetworkTableEntry m_sharpnessEntry = SmartDashboard.getEntry("CargoTracker/Sharpness");
    private final NetworkTableEntry m_exposureEntry = SmartDashboard.getEntry("CargoTracker/Exposure");
    private final NetworkTableEntry m_saQualityEntry = SmartDashboard.getEntry("CargoTracker/SA Quality");
    private final NetworkTableEntry m_yCutoff = SmartDashboard.getEntry("CargoTracker/YCutoff");
    private final NetworkTableEntry m_saHalfResolutionEntry = SmartDashboard.getEntry("CargoTracker/SA Half Resolution");
    private final SendableChooser<CameraView> m_viewEntry;
    private final HsvEntry m_lowRedThreshold = new HsvEntry(116, 156, 110, "Red Low");
    private final HsvEntry m_highRedThreshold = new HsvEntry(129, 219, 255, "Red High");
    private final HsvEntry m_lowBlueThreshold = new HsvEntry(94, 91, 36, "Blue Low");
    private final HsvEntry m_highBlueThreshold = new HsvEntry(131, 207, 214, "Blue High");
    private final NetworkTableEntry m_aspectRatioLow = SmartDashboard.getEntry("CargoTracker/AspectLow");
    private final NetworkTableEntry m_aspectRatioHigh = SmartDashboard.getEntry("CargoTracker/AspectHigh");
    private final NetworkTableEntry m_fillRatioLow = SmartDashboard.getEntry("CargoTracker/FillLow");
    private final NetworkTableEntry m_fillRatioHigh = SmartDashboard.getEntry("CargoTracker/FillHigh");
    private final NetworkTableEntry m_screenRatioLow = SmartDashboard.getEntry("CargoTracker/ScreenLow");
    private final NetworkTableEntry m_screenRatioHigh = SmartDashboard.getEntry("CargoTracker/ScreenHigh");
    private final NetworkTableEntry m_saFramerate = SmartDashboard.getEntry("CargoTracker/Sa Framerate");
    private final NetworkTableEntry m_smartScreenSecondDegreeTerm = SmartDashboard.getEntry("CargoTracker/SmartScreenSecondDegreeTerm");
    private final NetworkTableEntry m_smartScreenFirstDegreeTerm = SmartDashboard.getEntry("CargoTracker/SmartScreenFirstDegreeTerm");
    private final NetworkTableEntry m_smartScreenConstantTerm = SmartDashboard.getEntry("CargoTracker/SmartScreenConstantTerm");
    private final NetworkTableEntry m_smartScreenLow = SmartDashboard.getEntry("CargoTracker/SmartScreenLow");
    private final NetworkTableEntry m_smartScreenHigh = SmartDashboard.getEntry("CargoTracker/SmartScreenHigh");

    private static final int k_defaultBrightness = 61;
    private static final int k_defaultContrast = 15;
    private static final int k_defaultSaturation = 20;
    private static final int k_defaultRedBalance = 1218;
    private static final int k_defaultBlueBalance = 2300;
    private static final int k_defaultSharpness = 0;
    private static final int k_defaultExposure = 100;
    private static final int k_defaultSaQuality = 60;
    private static final int k_defaultYCutoff = 60;
    private static final boolean k_defaultSaHalfResolution = true;
    private static final CameraView k_defaultView = CameraView.BGR;
    private static final double k_defaultAspectRatioLow = 0.57;
    private static final double k_defaultAspectRatioHigh = 2.0;
    private static final double k_defaultFillRatioLow = 0.50;
    private static final double k_defaultFillRatioHigh = 0.88;
    private static final double k_defaultScreenRatioLow = 0.001;
    private static final double k_defaultScreenRatioHigh = 0.06;
    private static final double k_defaultSaFramerate = 30.0;
    private static final double k_defaultSmartScreenSecondDegreeTerm = 1.37524488e-06;
    private static final double k_defaultSmartScreenFirstDegreeTerm = -3.60564604e-05;
    private static final double k_defaultSmartScreenConstantTerm = -8.34749877e-05;
    private static final double k_defaultSmartScreenLow = -0.015;
    private static final double k_defaultSmartScreenHigh = 0.015;

    private volatile boolean m_runningState = false;
    private AtomicInteger m_frameTime_us = new AtomicInteger();
    private AtomicInteger m_pipelineLatency_us = new AtomicInteger();
    private AtomicInteger m_roiRed = new AtomicInteger();
    private AtomicInteger m_roiBlue = new AtomicInteger();
    private AtomicInteger m_roiGreen = new AtomicInteger();

    public static enum CameraView {
        BGR(0),
        HSV(1),
        RED(2),
        BLUE(3),
        LIVE(4),
        EARLY(5),
        SA_ONLY(6),
        SA_OFF(7);

        private final byte m_value;

        private CameraView(int value) {
            m_value = (byte)value;
        }

        public byte byteValue() {
            return m_value;
        }
    }

    public static class TrackedCargo {
        public static final int k_cargoSizeBytes = 29;

        public final int x;
        public final int y;
        public final int width;
        public final int height;
        public final double aspectRatio;
        public final double fillRatio;
        public final double screenRatio;
        public final double smartScreenRatio;
        public final boolean isRed;
        // Post Processed Data
        private Vector2d m_relativePosition;
        private Vector2d m_fieldPosition;

        private TrackedCargo(ByteBuffer bytes) {
            x = bytes.getInt();
            y = bytes.getInt();
            width = (int)bytes.getShort();
            height = (int)bytes.getShort();
            aspectRatio = (double)bytes.getFloat();
            fillRatio = (double)bytes.getFloat();
            screenRatio = (double)bytes.getFloat();
            smartScreenRatio = (double)bytes.getFloat();
            isRed = bytes.get() != 0;
        }

        public Angle horizontalAngle() {
            return m_camera.getYawFromPos(new ScreenPos(x, y));
        }

        public void setRelativePosition(Vector2d relativePos) {
            m_relativePosition = relativePos;
        }

        public Vector2d getRelativePosition() {
            return m_relativePosition;
        }

        public void setFieldPosition(Vector2d fieldPosition) {
            m_fieldPosition = fieldPosition;
        }

        public Vector2d getFieldPosition() {
            return m_fieldPosition;
        }

        public Alliance getColor() {
            if (isRed) {
                return Alliance.Red;
            } else {
                return Alliance.Blue;
            }
        }

        @Override
        public String toString() {
            return x + ", " + y + ", " + isRed;
        }
    }

    public static class HsvEntry {
        private final int m_hue;
        private final int m_saturation;
        private final int m_value;

        private final NetworkTableEntry m_hueEntry;
        private final NetworkTableEntry m_saturationEntry;
        private final NetworkTableEntry m_valueEntry;

        public HsvEntry(int h, int s, int v, String prefix) {
            m_hue = h;
            m_saturation = s;
            m_value = v;

            m_hueEntry = SmartDashboard.getEntry(prefix + " Hue");
            m_saturationEntry = SmartDashboard.getEntry(prefix + " Saturation");
            m_valueEntry = SmartDashboard.getEntry(prefix + " Value");

            m_hueEntry.setDefaultNumber(m_hue);
            m_saturationEntry.setDefaultNumber(m_saturation);
            m_valueEntry.setDefaultNumber(m_value);
        }

        public void fillByteBuffer(ByteBuffer b) {
            b.put(m_hueEntry.getNumber(m_hue).byteValue());
            b.put(m_saturationEntry.getNumber(m_saturation).byteValue());
            b.put(m_valueEntry.getNumber(m_value).byteValue());
        }
    }

    public CargoTrackerCamera(RobotLogger logger) {
        super("cargoTracker");
        m_logger = logger;

        NetworkTableInstance.getDefault().getTable("CameraPublisher").getEntry("CargoTracker/streams").setStringArray(new String[]{"mjpg:http://10.8.36.12:5809"});
        NetworkTableInstance.getDefault().getTable("CameraPublisher").getEntry("CargoTracker/source").setString("ip:http://10.8.36.12:5809");
        NetworkTableInstance.getDefault().getTable("CameraPublisher").getEntry("CargoTracker/description").setString("cargotracker");

        InetAddress piAddress = null;
        try {
            piAddress = InetAddress.getByName(RobotMap.k_piIpAddress);
        } catch (UnknownHostException e) {
            DriverStation.reportError("Cannot interpret Pi Address: " + RobotMap.k_piIpAddress, true);
        } finally {
            m_piAddress = piAddress;
        }

        DatagramSocket socket = null;
        try {
            socket = new DatagramSocket(RobotMap.k_cargoTrackerPort);
        } catch (SocketException e) {
            DriverStation.reportError("Cargo Tracker cannot open port!", true);
        } finally {
            m_socket = socket;
        }
        m_serverThread = new Thread(this::runServer);

        m_viewEntry = new SendableChooser<>();

        m_brightnessEntry.setDefaultNumber(k_defaultBrightness);
        m_contrastEntry.setDefaultNumber(k_defaultContrast);
        m_saturationEntry.setDefaultNumber(k_defaultSaturation);
        m_redBalanceEntry.setDefaultNumber(k_defaultRedBalance);
        m_blueBalanceEntry.setDefaultNumber(k_defaultBlueBalance);
        m_sharpnessEntry.setDefaultNumber(k_defaultSharpness);
        m_exposureEntry.setDefaultNumber(k_defaultExposure);
        m_yCutoff.setDefaultNumber(k_defaultYCutoff);
        m_aspectRatioLow.setDefaultNumber(k_defaultAspectRatioLow);
        m_aspectRatioHigh.setDefaultNumber(k_defaultAspectRatioHigh);
        m_fillRatioLow.setDefaultNumber(k_defaultFillRatioLow);
        m_fillRatioHigh.setDefaultNumber(k_defaultFillRatioHigh);
        m_screenRatioLow.setDefaultNumber(k_defaultScreenRatioLow);
        m_screenRatioHigh.setDefaultNumber(k_defaultScreenRatioHigh);
        m_smartScreenSecondDegreeTerm.setDefaultNumber(k_defaultSmartScreenSecondDegreeTerm);
        m_smartScreenFirstDegreeTerm.setDefaultNumber(k_defaultSmartScreenFirstDegreeTerm);
        m_smartScreenConstantTerm.setDefaultNumber(k_defaultSmartScreenConstantTerm);
        m_smartScreenLow.setDefaultNumber(k_defaultSmartScreenLow);
        m_smartScreenHigh.setDefaultNumber(k_defaultSmartScreenHigh);
        m_saFramerate.setDefaultNumber(k_defaultSaFramerate);

        m_saQualityEntry.setDefaultNumber(k_defaultSaQuality);
        m_saHalfResolutionEntry.setDefaultBoolean(k_defaultSaHalfResolution);
        m_viewEntry.setDefaultOption(k_defaultView.toString(), k_defaultView);
        for (var view : CameraView.values()) {
            m_viewEntry.addOption(view.toString(), view);
        }
        SmartDashboard.putData("Stream View", m_viewEntry);
        m_periodicTimer.start();
    }

    public synchronized List<TrackedCargo> tryGet() {
        List<TrackedCargo> cargoList = m_cargo.poll();
        // Clear out stale data
        m_cargo.clear();
        return cargoList;
    }

    @Override
    public void periodic() {
        m_periodicTimer.reset();
        super.periodic();

        if (m_socket != null && m_piAddress != null) {
            ByteBuffer b = ByteBuffer.allocate(k_cameraPacketSizeBytes);
            b.order(ByteOrder.LITTLE_ENDIAN);

            m_lowRedThreshold.fillByteBuffer(b);
            m_highRedThreshold.fillByteBuffer(b);
            m_lowBlueThreshold.fillByteBuffer(b);
            m_highBlueThreshold.fillByteBuffer(b);
            b.putFloat(m_aspectRatioLow.getNumber(k_defaultAspectRatioLow).floatValue());
            b.putFloat(m_aspectRatioHigh.getNumber(k_defaultAspectRatioHigh).floatValue());
            b.putFloat(m_fillRatioLow.getNumber(k_defaultFillRatioLow).floatValue());
            b.putFloat(m_fillRatioHigh.getNumber(k_defaultFillRatioHigh).floatValue());
            b.putFloat(m_screenRatioLow.getNumber(k_defaultScreenRatioLow).floatValue());
            b.putFloat(m_screenRatioHigh.getNumber(k_defaultScreenRatioHigh).floatValue());
            b.putFloat(m_smartScreenSecondDegreeTerm.getNumber(k_defaultSmartScreenSecondDegreeTerm).floatValue());
            b.putFloat(m_smartScreenFirstDegreeTerm.getNumber(k_defaultSmartScreenFirstDegreeTerm).floatValue());
            b.putFloat(m_smartScreenConstantTerm.getNumber(k_defaultSmartScreenConstantTerm).floatValue());
            b.putFloat(m_smartScreenLow.getNumber(k_defaultSmartScreenLow).floatValue());
            b.putFloat(m_smartScreenHigh.getNumber(k_defaultSmartScreenHigh).floatValue());
            b.putInt(m_brightnessEntry.getNumber(k_defaultBrightness).intValue());
            b.putInt(m_contrastEntry.getNumber(k_defaultContrast).intValue());
            b.putInt(m_saturationEntry.getNumber(k_defaultSaturation).intValue());
            b.putInt(m_redBalanceEntry.getNumber(k_defaultRedBalance).intValue());
            b.putInt(m_blueBalanceEntry.getNumber(k_defaultBlueBalance).intValue());
            b.putInt(m_sharpnessEntry.getNumber(k_defaultSharpness).intValue());
            b.putInt(m_exposureEntry.getNumber(k_defaultExposure).intValue());
            b.putShort(m_yCutoff.getNumber(k_defaultYCutoff).shortValue());
            b.put(m_saQualityEntry.getNumber(k_defaultSaQuality).byteValue());
            b.putFloat(m_saFramerate.getNumber(k_defaultSaFramerate).floatValue());
            b.put(m_saHalfResolutionEntry.getBoolean(k_defaultSaHalfResolution) ? (byte)1 : (byte)0);
            b.put(m_viewEntry.getSelected().byteValue());
            DatagramPacket p =
                new DatagramPacket(b.array(), k_cameraPacketSizeBytes, m_piAddress, RobotMap.k_cargoTrackerPort);
            try {
                m_socket.send(p);
            } catch (IOException e) {
                DriverStation.reportWarning("Failed to send Camera Settings to Pi", false);
            }
        }

        updateLogger();
        m_logger.setDataPoint(LogEntry.TIME_CARGO_TRACKER_CAMERA, m_periodicTimer.get().getValue(TimeUnit.MILLISECONDS));
    }

    private synchronized void updateLogger() {
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_STREAM_VIEW, m_viewEntry.getSelected().toString());
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_FPS, 1.0e6/m_frameTime_us.get());
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_LATENCY, m_pipelineLatency_us.get() / 1.0e3);
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_ROI_RED, m_roiRed.get());
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_ROI_GREEN, m_roiGreen.get());
        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_ROI_BLUE, m_roiBlue.get());

        m_logger.setDataPoint(LogEntry.CARGO_TRACKER_STATE, super.getCurrentState().getStateName());
    }

    private void runServer() {
        boolean errorReported = false;
        while (true) {
            if (m_runningState) {
                try {
                    tryReceivePacket();
                } catch (IOException e) {
                    if (!errorReported) {
                        DriverStation.reportError(
                            "IO Exception while receiving packet. This error will not be repeated.",
                            true);
                        errorReported = true;
                    }
                }
            } else {
                try {
                    Thread.sleep(k_sleepTime_ms);
                } catch (InterruptedException e) {
                    // Do Nothing
                }
            }
        }
    }

    private void tryReceivePacket() throws IOException {
        var bytes = new byte[k_cargoPacketSize];
        var packet = new DatagramPacket(bytes, bytes.length);
        m_socket.receive(packet);

        if (packet.getLength() == k_cargoPacketSize) {
            var buffer = ByteBuffer.wrap(packet.getData());
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            var count = buffer.get();

            m_frameTime_us.set(buffer.getInt());
            m_pipelineLatency_us.set(buffer.getInt());
            m_roiRed.set(buffer.getInt());
            m_roiGreen.set(buffer.getInt());
            m_roiBlue.set(buffer.getInt());

            var cargo = new TrackedCargo[count];
            for (int i = 0; i < count; i++) {
                cargo[i] = new TrackedCargo(buffer);
                cargo[i].setRelativePosition(getCargoRelativePosition(cargo[i]));
            }

            var raw = buffer.array();
            byte parity = raw[raw.length - 1];
            for (int i = 0; i < raw.length - 1; i++) {
                parity ^= raw[i];
            }

            if (parity == 0) {
                addCargoList(cargo);
            } else {
                DriverStation.reportError("Parity check failed for UDP packet", false);
            }
        }
    }

    private Vector2d getCargoRelativePosition(TrackedCargo cargo) {
        var cargoYawPitch = m_camera.getYawPitchFromPos(new ScreenPos(cargo.x, cargo.y));
        var pitchAngleToCargo = cargoYawPitch.getPitch().add(k_lensPitchAngle);
        var deltaHeight = FieldDimensions.k_cargoDiameter.scale(0.5).subtract(k_lensHeightFromFloor);
        var radialDistance = deltaHeight.scale(1.0 / pitchAngleToCargo.tan());

        var x = radialDistance.scale(cargoYawPitch.getYaw().cos());
        var y = radialDistance.scale(cargoYawPitch.getYaw().sin());
        return new Vector2d(x.getValue(LengthUnit.INCHES), y.getValue(LengthUnit.INCHES));
    }

    private synchronized void addCargoList(TrackedCargo[] cargo) {
        m_cargo.clear();
        m_cargo.add(Arrays.asList(cargo));
    }

    @Override
    protected State provideDisabledState() {
        return new InitExitState("Disabled State", () -> m_runningState = false, () -> {
            if (m_socket != null && !m_serverThread.isAlive()) {
                m_serverThread.start();
            }
        });
    }

    @Override
    protected State provideManualState() {
        return new InitState("Manual State", () -> m_runningState = true);
    }

    @Override
    protected State provideNormalState() {
        return new InitState("Normal State", () -> m_runningState = true);
    }
}
