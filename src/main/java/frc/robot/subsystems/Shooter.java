/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems;

import com.ctre.phoenix.ErrorCode;
import com.ctre.phoenix.ParamEnum;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;
import com.ctre.phoenix.motorcontrol.SupplyCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.sensors.SensorVelocityMeasPeriod;

import edu.wpi.first.math.Pair;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.FunctionalCommand;
import edu.wpi.first.wpilibj2.command.InstantCommand;

import frc.library.drivers.TalonFXUtils;
import frc.library.GStemsSubsystem;
import frc.library.InterpolatableDoublePair;
import frc.library.InterpolatableLength;
import frc.library.Length;
import frc.library.Length.LengthUnit;
import frc.library.FlywheelCharacterization;
import frc.robot.RobotState;
import frc.robot.constants.FieldDimensions;
import frc.robot.constants.LogEntry;
import frc.robot.constants.RobotMap;
import frc.robot.constants.ShotCriteria;
import frc.robot.RobotLogger;

import java.util.function.Supplier;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.logic.JKLatch;
import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.signals.Debounce;
import org.growingstems.signals.EdgeDetector;
import org.growingstems.signals.api.SignalModifier;
import org.growingstems.math.Range;
import org.growingstems.math.Time;
import org.growingstems.math.Vector2d;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.util.interpolation.InterpolatingTreeMap;
import org.growingstems.util.statemachine.ExecState;
import org.growingstems.util.statemachine.State;
import org.growingstems.util.timer.TimerI;

public class Shooter extends GStemsSubsystem {
    public static class Pidf {
        public double p;
        public double i;
        public double d;
        public double f;

        public Pidf(double p, double i, double d, double f) {
            this.p = p;
            this.i = i;
            this.d = d;
            this.f = f;
        }
    }

    private final TimerI m_periodicTimer = new WpiTimer();
    private final TimerI m_falconChecker = new WpiTimer();
    private final Time m_falconCheckTime = new Time(5.0, TimeUnit.SECONDS);

    private static final Length k_robotCenterToFrontBumper = Length.inches(17.25);
    private static final double k_fenderPrimaryRpm = 1500.0;
    private static final double k_fenderSecondaryRpm = k_fenderPrimaryRpm * 1.15; // Was 1540 when primary was 1400

    private static final double k_standbyPrimary_RPM = k_fenderPrimaryRpm;
    private static final double k_standbySecondary_RPM = 500.0;

    //TODO: Update Idle Constants
    public static final double k_idleAndRejectPrimary_RPM = 1500.0;
    public static final double k_idleAndRejectSecondary_RPM = k_standbySecondary_RPM;
    public static final double k_readyToRejectPrimaryThreshold_RPM = k_idleAndRejectPrimary_RPM * 1.4;
    public static final double k_readyToRejectSecondaryThreshold_RPM = k_idleAndRejectSecondary_RPM * 1.4;

    private static final Debounce k_halfPrimeDebounce = new Debounce(new Time(1.5, TimeUnit.SECONDS), new WpiTimer());
    private boolean k_halfPrimed = false;

    // All distances in the double[][] structures are from front of Bumper to Center of Hub.
    // All distances stores in the InterpolatingTreeMap are from Center of Robot to Center of Hub.
    private static final InterpolatingTreeMap<InterpolatableLength, InterpolatableDoublePair> k_downLookupTable = new InterpolatingTreeMap<>();
    // Distance from Bumper to Center of Hub in Inches, Primary RPM, Ratio of Secondary over Primary RPM
    private static final double[][] k_downLookupTableArray = {
        {FieldDimensions.k_hubDistanceToFender.add(k_robotCenterToFrontBumper).getValue(LengthUnit.INCHES), k_fenderPrimaryRpm, k_fenderSecondaryRpm / k_fenderPrimaryRpm},
        {61.0, 1425.0, 1.3},
        {77.25, 1100.0, 2.5},
        {89.25, 750.0, 5.0}
    };

    private static final InterpolatingTreeMap<InterpolatableLength, InterpolatableDoublePair> k_upLookupTable = new InterpolatingTreeMap<>();
    // Distance from Center of Robot to Center of Hub in Inches, Primary RPM, Ratio of Secondary over Primary RPM
    private static final double[][] k_upLookupTableArray = {
        {90.0, 3200.0, 0.05},
        {100.0, 2875.0, 0.125},
        {111.0, 2725.0, 0.2},
        {123.5, 2775.0, 0.2},
        {136.6, 2900.0, 0.25},
        {140.0, 2850.0, 0.26},
        {160.0, 2950.0, 0.3},
        {180.0, 3025.0, 0.35},
        {180.01, 1750.0, 1.5},
        {190.0, 1775.0, 1.5},
        {200.0, 1850.0, 1.5}
    };

    // Build up interpotable tables
    static {
        for (int i = 0; i < k_downLookupTableArray.length; i++) {
            InterpolatableLength key = new InterpolatableLength(new Length(k_downLookupTableArray[i][0], LengthUnit.INCHES));
            InterpolatableDoublePair value = new InterpolatableDoublePair(new Pair<Double, Double>(k_downLookupTableArray[i][1], k_downLookupTableArray[i][2]));
            k_downLookupTable.put(key, value);
        }
    };
    static {
        for (int i = 0; i < k_upLookupTableArray.length; i++) {
            InterpolatableLength key = new InterpolatableLength(new Length(k_upLookupTableArray[i][0], LengthUnit.INCHES));
            InterpolatableDoublePair value = new InterpolatableDoublePair(new Pair<Double, Double>(k_upLookupTableArray[i][1], k_upLookupTableArray[i][2]));
            k_upLookupTable.put(key, value);
        }
    };

    private final TalonFX m_primaryMotor;
    private final TalonFX m_secondaryMotor;

    private final RobotState m_robotState;
    private final RobotLogger m_logger;
    private final HoodSolenoid m_solenoid;

    // Sensor unit over 100 ms per RPM

    /**
     * su    1 Rot     10 hms   60 sec   Rot
     * --- * ------- * ------ * ------ = ---
     * hms   4096 su   1 sec    1 min    min
     *
     * 1 su/hms = (10*60/4096) RPM
     * 1 RPM = (4096/(10*60)) su/hms
     */
    private static final double k_sensorUnitPerRpm = 2048.0 / (10 * 60) * (22.0 / 16.0); // 22 teeth on wheel, 16 on motor
    /**
     * Valid Velocity Measurement Sample Period: 1, 5, 10, 20, 25, 50, 100(default)
     * <p>
     * https://docs.ctre-phoenix.com/en/stable/ch14_MCSensor.html?highlight=velocity#changing-velocity-measurement-parameters
     */
    private static final SensorVelocityMeasPeriod k_velocityMeasurementPeiod = SensorVelocityMeasPeriod.Period_10Ms;
    /**
     * Valid Velocity Measurement Rolling Average Window: 1, 2, 4, 8, 16, 32,
     * 64(default)
     * <p>
     * https://docs.ctre-phoenix.com/en/stable/ch14_MCSensor.html?highlight=velocity#changing-velocity-measurement-parameters
     */
    private static final int k_velocityMeasurementWindow = 32;

    private static final Pidf k_primarySpeedPidf = new Pidf(0.2, 0.002, 0.0, 0.04670186532400343);
    /** In Sensor Units per 100ms */
    private static final double k_primarySpeedIZone = 60.0;
    private static final double k_primarySpeedMaxIAcc = 0.2;
    private static final double k_primaryMaxVelocity = 21745.00850340134;
    private static final double k_primaryZeroPower = 0.00939584930230978;
    private static final double k_primaryMaxAcc_rpmps = 3000.0;

    private static final Pidf k_secondarySpeedPidf = new Pidf(0.2, 0.002, 0.0, 0.04626980847906384);
    /** In Sensor Units per 100ms */
    private static final double k_secondarySpeedIZone = 60.0;
    private static final double k_secondarySpeedMaxIAcc = 0.2;
    private static final double k_secondaryMaxVelocity = 21784.413265306102;
    private static final double k_secondaryZeroPower = 0.0054748733476966075;
    private static final double k_secondaryMaxAcc_rpmps = k_primaryMaxAcc_rpmps;

    private static final double k_peakOutputForward = 1.0;
    private static final double k_peakOutputReverse = -1.0;
    private static final int k_speedSlot = 0;

    private static final Length k_shootDownThreshold = Length.inches(90.0);
    private static final Length k_maxFiringRange = Length.inches(205.0);

    private double m_primaryManualPower = 0.0;
    private double m_secondaryManualPower = 0.0;
    private double m_primaryManualRpm = 0.0;
    private double m_secondaryManualRatio = 0.0;
    /** Update rate for Velocity measurment. */
    private static final int k_masterFeedback0Period_ms = 50;

    private double m_primaryGoalVelocity_RPM = 0.0;
    private double m_secondaryGoalVelocity_RPM = 0.0;
    private double m_primaryTempGoalVelocity_RPM = 0.0;
    private double m_secondaryTempGoalVelocity_RPM = 0.0;
    private TimerI m_setVelocityTimer = new WpiTimer();
    private boolean m_isTryingToShoot = false;

    private final NetworkTableEntry k_adjustPrimaryValue = SmartDashboard.getEntry("shooter/Primary Adjustment");
    private final double k_defaultPrimaryAdjustment_rpm = 0.0;

    private JKLatch m_readyToFire = new JKLatch();
    private SignalModifier<Double, Boolean> m_primaryRpmReadyToShoot;
    private SignalModifier<Double, Boolean> m_secondaryRpmReadyToShoot;
    private SignalModifier<Double, Boolean> m_primaryRpmNotReadyToShoot;
    private SignalModifier<Double, Boolean> m_secondaryRpmNotReadyToShoot;

    private static final double k_shotCurrentThreshold_A = 15.0;
    private final EdgeDetector m_shotDetector = new EdgeDetector(Edge.RISING);
    private boolean m_shotDetected = false;

    private enum HoodState { DOWN, UP }

    private HoodState m_manualHoodState = HoodState.DOWN;

    private Vector2d m_firePoseStored = new Vector2d();

    private enum NormalCommand { AUTO_FIRE, AUTO_CEASE_FIRE, FIRE_FENDER, FIRE_POSE, DASHBOARD }
    private NormalCommand m_normalCommand = NormalCommand.AUTO_CEASE_FIRE;

    private final NetworkTableEntry m_runCharacterizerInTestMode = SmartDashboard.getEntry("shooter/TEST_RunCharacterizer");
    private final EdgeDetector m_justEnabled = new EdgeDetector(Edge.RISING);
    private final FlywheelCharacterization m_primaryCharacterizer;
    private final FlywheelCharacterization m_secondaryCharacterizer;

    private final NetworkTableEntry m_dashboardHoodUp = SmartDashboard.getEntry("shooter/DBHoodUp");
    private final NetworkTableEntry m_dashboardPrimaryRpm = SmartDashboard.getEntry("shooter/DBPrimaryRPM");
    private final NetworkTableEntry m_dashboardRatio = SmartDashboard.getEntry("shooter/DBRatio");

    private boolean m_hasPid = false;

    private static class HoodSolenoid {
        private static final boolean k_hoodUpState = true;
        private final Solenoid m_solenoid;
        private WpiTimer m_solenoidTransition = new WpiTimer();

        public HoodSolenoid() {
            m_solenoid =
                new Solenoid(RobotMap.k_pcmCanId, PneumaticsModuleType.CTREPCM, RobotMap.k_shooterSolenoidPcmChannel);
            m_solenoidTransition.start();
        }

        public boolean isUp() {
            return m_solenoid.get() == k_hoodUpState;
        }

        public boolean isTransitioned(){
            return m_solenoidTransition.hasElapsed(isUp() ? ShotCriteria.k_shooterHoodUpTransition : ShotCriteria.k_shooterHoodDownTransition);
        }

        public void setUp() {
            if(!isUp()){
                m_solenoid.set(k_hoodUpState);
                m_solenoidTransition.reset();
            }
        }

        public void setDown() {
            if(isUp()){
                m_solenoid.set(!k_hoodUpState);
                m_solenoidTransition.reset();
            }
        }
    }

    public Shooter(RobotState robotState, RobotLogger logger) {
        super("shooter");
        ErrorCode error;

        m_robotState = robotState;
        m_logger = logger;
        m_solenoid = new HoodSolenoid();

        m_primaryRpmReadyToShoot = primaryRpm -> Math.abs(m_primaryGoalVelocity_RPM - primaryRpm) <= ShotCriteria.k_shooterPrimaryRpmRange;
        m_secondaryRpmReadyToShoot = secondaryRpm -> Math.abs(m_secondaryGoalVelocity_RPM - secondaryRpm) <= ShotCriteria.k_shooterSecondaryRpmRange;

        m_primaryRpmNotReadyToShoot = primaryRpm -> Math.abs(m_primaryGoalVelocity_RPM - primaryRpm) > ShotCriteria.k_shooterPrimaryRpmRange;
        m_secondaryRpmNotReadyToShoot = secondaryRpm -> Math.abs(m_secondaryGoalVelocity_RPM - secondaryRpm) > ShotCriteria.k_shooterSecondaryRpmRange;

        m_primaryRpmReadyToShoot = m_primaryRpmReadyToShoot.append(new Debounce(ShotCriteria.k_shooterWheelReadyDebounceTime, new WpiTimer()));
        m_secondaryRpmReadyToShoot = m_secondaryRpmReadyToShoot.append(new Debounce(ShotCriteria.k_shooterWheelReadyDebounceTime, new WpiTimer()));

        m_primaryRpmNotReadyToShoot = m_primaryRpmNotReadyToShoot.append(new Debounce(ShotCriteria.k_shooterWheelNotReadyDebounceTime, new WpiTimer()));
        m_secondaryRpmNotReadyToShoot = m_secondaryRpmNotReadyToShoot.append(new Debounce(ShotCriteria.k_shooterWheelNotReadyDebounceTime, new WpiTimer()));

        m_primaryMotor = TalonFXUtils.createDefaultTalonFX(RobotMap.k_primaryShooterMotorCanId, false);
        m_secondaryMotor = TalonFXUtils.createDefaultTalonFX(RobotMap.k_secondaryShooterMotorCanId, false);

        m_primaryMotor.setSensorPhase(false);
        m_secondaryMotor.setSensorPhase(true);

        error = m_primaryMotor.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor, k_speedSlot,
                TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "configSelectedFeedbackSensor");
        error = m_secondaryMotor.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor, k_speedSlot,
                TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "configSelectedFeedbackSensor");

        error = m_primaryMotor.config_kP(k_speedSlot, k_primarySpeedPidf.p, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "config_kP");
        error = m_primaryMotor.config_kI(k_speedSlot, k_primarySpeedPidf.i, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "config_kI");
        error = m_primaryMotor.config_kD(k_speedSlot, k_primarySpeedPidf.d, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "config_kD");
        error = m_primaryMotor.config_kF(k_speedSlot, k_primarySpeedPidf.f, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "config_kF");
        error = m_primaryMotor.config_IntegralZone(k_speedSlot, k_primarySpeedIZone, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "config_IntegralZone");
        error = m_primaryMotor.configMaxIntegralAccumulator(k_speedSlot, k_primarySpeedMaxIAcc, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "configMaxIntegralAccumulator");

        error = m_secondaryMotor.config_kP(k_speedSlot, k_secondarySpeedPidf.p, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "config_kP");
        error = m_secondaryMotor.config_kI(k_speedSlot, k_secondarySpeedPidf.i, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "config_kI");
        error = m_secondaryMotor.config_kD(k_speedSlot, k_secondarySpeedPidf.d, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "config_kD");
        error = m_secondaryMotor.config_kF(k_speedSlot, k_secondarySpeedPidf.f, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "config_kF");
        error = m_secondaryMotor.config_IntegralZone(k_speedSlot, k_secondarySpeedIZone, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "config_IntegralZone");
        error = m_secondaryMotor.configMaxIntegralAccumulator(k_speedSlot, k_secondarySpeedMaxIAcc, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "configMaxIntegralAccumulator");

        error = m_primaryMotor.configPeakOutputForward(k_peakOutputForward, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "configPeakOutputForward");
        error = m_secondaryMotor.configPeakOutputForward(k_peakOutputForward, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "configPeakOutputForward");

        error = m_primaryMotor.configPeakOutputReverse(k_peakOutputReverse, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "configPeakOutputReverse");
        error = m_secondaryMotor.configPeakOutputReverse(k_peakOutputReverse, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "configPeakOutputReverse");

        error = m_primaryMotor.configVelocityMeasurementPeriod(k_velocityMeasurementPeiod,
                TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "configVelocityMeasurementPeriod");
        error = m_secondaryMotor.configVelocityMeasurementPeriod(k_velocityMeasurementPeiod,
                TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "configVelocityMeasurementPeriod");

        error = m_primaryMotor.configVelocityMeasurementWindow(k_velocityMeasurementWindow,
                TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "configVelocityMeasurementPeriod");
        error = m_secondaryMotor.configVelocityMeasurementWindow(k_velocityMeasurementWindow,
                TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "configVelocityMeasurementPeriod");

        error = m_primaryMotor.configSupplyCurrentLimit(
                new SupplyCurrentLimitConfiguration(false, TalonFXUtils.k_defaultContCurrLimit_A,
                    TalonFXUtils.k_defaultPeakCurrLimit_A, TalonFXUtils.k_defaultPeakCurrDuration_ms),
                    TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "configSupplyCurrentLimit");
        error = m_secondaryMotor.configSupplyCurrentLimit(
                new SupplyCurrentLimitConfiguration(false, TalonFXUtils.k_defaultContCurrLimit_A,
                    TalonFXUtils.k_defaultPeakCurrLimit_A, TalonFXUtils.k_defaultPeakCurrDuration_ms),
                    TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "configSupplyCurrentLimit");

        m_primaryMotor.setInverted(InvertType.InvertMotorOutput);
        m_secondaryMotor.setInverted(InvertType.InvertMotorOutput);

        m_primaryMotor.setNeutralMode(NeutralMode.Coast);
        m_secondaryMotor.setNeutralMode(NeutralMode.Coast);

        // Status Rates
        error = m_primaryMotor.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0,
                k_masterFeedback0Period_ms, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "setStatusFramePeriod_Status_2_Feedback0");
        error = m_secondaryMotor.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0,
                k_masterFeedback0Period_ms, TalonFXUtils.k_cfgTimeout_ms);
        TalonFXUtils.checkError(error, "setStatusFramePeriod_Status_2_Feedback0");

        m_runCharacterizerInTestMode.setBoolean(false);
        m_primaryCharacterizer = new FlywheelCharacterization("Primary", m_primaryMotor);
        m_secondaryCharacterizer = new FlywheelCharacterization("Secondary", m_secondaryMotor);

        m_dashboardHoodUp.setDefaultBoolean(true);
        m_dashboardPrimaryRpm.setDefaultNumber(1000.0);
        m_dashboardRatio.setDefaultNumber(1.0);
        m_logger.setDataPoint(LogEntry.SHOOTER_HAS_PID, false);
        k_adjustPrimaryValue.setDefaultDouble(k_defaultPrimaryAdjustment_rpm);

        m_periodicTimer.start();
        m_falconChecker.start();
    }

    @Override
    public void periodic() {
        m_periodicTimer.reset();
        k_halfPrimed = k_halfPrimeDebounce.update(m_robotState.cargoHalfPrimed());
        super.periodic();
        var shooterReady = readyToFire();
        var primaryRPM = getPrimaryRpm();
        var secondaryRPM = getSecondaryRpm();

        var primaryCurrent_A = m_primaryMotor.getSupplyCurrent();
        var secondaryCurrent_A = m_secondaryMotor.getSupplyCurrent();

        m_shotDetected = m_shotDetector.update(primaryCurrent_A > k_shotCurrentThreshold_A);

        m_logger.setDataPoint(LogEntry.SHOOTER_TRYING_TO_SHOOT, m_isTryingToShoot);
        m_logger.setDataPoint(LogEntry.SHOOTER_SHOOTER_READY, shooterReady);
        m_logger.setDataPoint(LogEntry.SHOOTER_PRIMARY, primaryRPM);
        m_logger.setDataPoint(LogEntry.SHOOTER_SECONDARY, secondaryRPM);
        m_logger.setDataPoint(LogEntry.SHOOTER_PRIMARY_GOAL, m_primaryGoalVelocity_RPM);
        m_logger.setDataPoint(LogEntry.SHOOTER_SECONDARY_GOAL, m_secondaryGoalVelocity_RPM);
        m_logger.setDataPoint(LogEntry.SHOOTER_PRIMARY_DELTA, m_primaryGoalVelocity_RPM - primaryRPM);
        m_logger.setDataPoint(LogEntry.SHOOTER_SECONDARY_DELTA, m_secondaryGoalVelocity_RPM - secondaryRPM);
        m_logger.setDataPoint(LogEntry.SHOOTER_CURRENT_PRIMARY, primaryCurrent_A);
        m_logger.setDataPoint(LogEntry.SHOOTER_CURRENT_SECONDARY, secondaryCurrent_A);
        m_logger.setDataPoint(LogEntry.SHOOTER_PERCENT_OUT_PRIMARY, m_primaryMotor.getMotorOutputPercent());
        m_logger.setDataPoint(LogEntry.SHOOTER_PERCENT_OUT_SECONDARY, m_secondaryMotor.getMotorOutputPercent());
        m_logger.setDataPoint(LogEntry.SHOOTER_SHOT_DETECTED, m_shotDetected);
        m_logger.setDataPoint(LogEntry.SHOOTER_STATE, super.getCurrentState().getStateName());

        m_logger.setDataPoint(LogEntry.TIME_SHOOTER, m_periodicTimer.get().getValue(TimeUnit.MILLISECONDS));
    }

    private boolean isAcceptable(double expected, double actual) {
        return (expected == 0.0) || (actual != 0.0);
    }

    @Override
    protected boolean isReadyToStartMatch() {
        int timeoutMs = 0;
        if (m_falconChecker.hasPeriodPassed(m_falconCheckTime) || !m_hasPid) {
            m_hasPid = isAcceptable(k_primarySpeedPidf.p, m_primaryMotor.configGetParameter(ParamEnum.eProfileParamSlot_P, k_speedSlot, timeoutMs)) &&
                     isAcceptable(k_primarySpeedPidf.i, m_primaryMotor.configGetParameter(ParamEnum.eProfileParamSlot_I, k_speedSlot, timeoutMs)) &&
                     isAcceptable(k_primarySpeedPidf.d, m_primaryMotor.configGetParameter(ParamEnum.eProfileParamSlot_D, k_speedSlot, timeoutMs)) &&
                     isAcceptable(k_primarySpeedPidf.f, m_primaryMotor.configGetParameter(ParamEnum.eProfileParamSlot_F, k_speedSlot, timeoutMs)) &&
                     isAcceptable(k_secondarySpeedPidf.p, m_secondaryMotor.configGetParameter(ParamEnum.eProfileParamSlot_P, k_speedSlot, timeoutMs)) &&
                     isAcceptable(k_secondarySpeedPidf.i, m_secondaryMotor.configGetParameter(ParamEnum.eProfileParamSlot_I, k_speedSlot, timeoutMs)) &&
                     isAcceptable(k_secondarySpeedPidf.d, m_secondaryMotor.configGetParameter(ParamEnum.eProfileParamSlot_D, k_speedSlot, timeoutMs)) &&
                     isAcceptable(k_secondarySpeedPidf.f, m_secondaryMotor.configGetParameter(ParamEnum.eProfileParamSlot_F, k_speedSlot, timeoutMs));
            m_logger.setDataPoint(LogEntry.SHOOTER_HAS_PID, m_hasPid);
        }
        return m_hasPid;
    }

    @Override
    protected State provideDisabledState() {
        return new ExecState("Disabled State", () -> stop());
    }

    @Override
    protected State provideManualState() {
        return new ExecState("Manual State", () -> {
            setVelocity(m_primaryManualRpm, m_secondaryManualRatio * m_primaryManualRpm);
            switch (m_manualHoodState) {
                case DOWN:
                    setHoodDown();
                    break;
                case UP:
                    setHoodUp();
                    break;
                default:
                    break;
            }
        });
    }

    @Override
    protected State provideNormalState() {
        return new ExecState("Shooting", () -> {
            if (m_runCharacterizerInTestMode.getBoolean(false)) {
                setHoodUp();
                boolean justEnabled = m_justEnabled.update(DriverStation.isEnabled());
                if (DriverStation.isEnabled()) {
                    if (justEnabled) {
                        stop();
                        m_primaryCharacterizer.start();
                        m_secondaryCharacterizer.start();
                    }
                    m_primaryCharacterizer.tick();
                    m_secondaryCharacterizer.tick();
                } else {
                    m_primaryCharacterizer.stop();
                    m_secondaryCharacterizer.stop();
                }
            } else {
                if (DriverStation.isEnabled()) {
                    if (!m_robotState.isTryingToRejectShooter()) {
                        switch (m_normalCommand) {
                            case DASHBOARD:
                                if (m_dashboardHoodUp.getBoolean(false))
                                {
                                    setHood(HoodState.UP);
                                } else {
                                    setHood(HoodState.DOWN);
                                }
                                double primary = m_dashboardPrimaryRpm.getNumber(0.0).doubleValue();
                                double secondary = primary * m_dashboardRatio.getNumber(0.0).doubleValue();
                                setVelocity(primary, secondary);
                                m_isTryingToShoot = true;
                                break;
                            case FIRE_FENDER:
                                setHood(HoodState.DOWN);
                                setVelocity(k_fenderPrimaryRpm, k_fenderSecondaryRpm);
                                m_isTryingToShoot = true;
                                break;
                            case FIRE_POSE:
                            case AUTO_FIRE:
                                updateAutomaticMode();
                                break;
                            default:
                                DriverStation.reportError("Unknown Shoot Mode: " + m_normalCommand, false);
                            case AUTO_CEASE_FIRE:
                                Length distance = Length.inches(m_robotState.getShotVectorToHub_in().getMagnitude());
                                HoodState hoodState = evaluateHoodState(distance);
                                updateAutoNoShoot(hoodState);
                                break;
                        }
                    } else {
                        setHood(HoodState.DOWN);
                        setVelocity(k_idleAndRejectPrimary_RPM, k_idleAndRejectSecondary_RPM);
                    }
                } else {
                    setHood(HoodState.DOWN);
                    setVelocity(0.0, 0.0);
                }
            }
        });
    }

    private void updateAutoNoShoot(HoodState hoodState) {
        m_isTryingToShoot = false;
        if (m_robotState.cargoFullyPrimed() || k_halfPrimed) {
            if (hoodState == HoodState.UP) {
                setVelocity(k_standbyPrimary_RPM, k_standbySecondary_RPM);
            } else {
                setVelocity(k_fenderPrimaryRpm, k_fenderSecondaryRpm);
            }
        } else {
            setVelocity(k_idleAndRejectPrimary_RPM, k_idleAndRejectSecondary_RPM);
        }
    }

    private void updateAutomaticMode() {
        Length distance = Length.inches(m_robotState.getShotVectorToHub_in().getMagnitude());
        if (m_normalCommand == NormalCommand.FIRE_POSE) {
            distance = Length.inches(m_firePoseStored.getMagnitude());
            m_logger.setDataPoint(LogEntry.SHOOTER_SHOOT_POSE, distance.getValue(LengthUnit.INCHES));
        } else {
            m_logger.setDataPoint(LogEntry.SHOOTER_SHOOT_POSE, Double.NaN);
        }

        HoodState hoodState = evaluateHoodState(distance);
        if (distance != null && inFiringRange(distance)) {
            setHood(hoodState);
            setFiringVelocity(hoodState, distance);
            m_isTryingToShoot = true;
        } else {
            updateAutoNoShoot(hoodState);
        }
    }

    private void setFiringVelocity(HoodState state, Length distance) {
        Pair<Double, Double> rpmAndRatio = calculateRpmAndRatio(distance, state);
        var primary = rpmAndRatio.getFirst() + k_adjustPrimaryValue.getDouble(k_defaultPrimaryAdjustment_rpm);
        var secondary = primary * rpmAndRatio.getSecond();
        setVelocity(primary, secondary);
    }

    private Pair<Double, Double> calculateRpmAndRatio(Length distance, HoodState hood) {
        var lookupTable = hood == HoodState.DOWN ? k_downLookupTable : k_upLookupTable;
        return lookupTable.getValue(new InterpolatableLength(distance), false).getValue();
    }

    private static HoodState evaluateHoodState(Length distanceToHubCenter) {
        if(distanceToHubCenter.getValue(LengthUnit.INCHES) < k_shootDownThreshold.getValue(LengthUnit.INCHES)) {
            return HoodState.DOWN;
        } else {
            return HoodState.UP;
        }
    }

    private static boolean inFiringRange(Length distanceToHubCenter) {
        return distanceToHubCenter.getValue(LengthUnit.INCHES) < k_maxFiringRange.getValue(LengthUnit.INCHES);
    }

    private void setPower(double primary, double secondary) {
        m_primaryMotor.set(ControlMode.PercentOutput, primary);
        m_secondaryMotor.set(ControlMode.PercentOutput, secondary);
    }

    public double getPrimaryRpm() {
        return velocitySuToRpm(m_primaryMotor.getSelectedSensorVelocity());
    }

    public double getSecondaryRpm() {
        return velocitySuToRpm(m_secondaryMotor.getSelectedSensorVelocity());
    }

    public boolean isReadyToReject() {
        return getPrimaryRpm() < k_readyToRejectPrimaryThreshold_RPM &&
               getSecondaryRpm() < k_readyToRejectSecondaryThreshold_RPM &&
               m_solenoid.isTransitioned();
    }

    public boolean isHoodUp() {
        return m_solenoid.isUp();
    }

    private double velocityRpmToSu(double velocity_RPM) {
        return velocity_RPM * k_sensorUnitPerRpm;
    }

    private double velocitySuToRpm(double velocity_SU) {
        return velocity_SU / k_sensorUnitPerRpm;
    }

    private void setVelocity(double primary_RPM, double secondary_RPM) {
        if (!m_setVelocityTimer.isRunning()) {
            m_setVelocityTimer.reset();
            m_setVelocityTimer.start();
        }
        var dt_sec = m_setVelocityTimer.get().getValue(TimeUnit.SECONDS);

        if (primary_RPM > 1.0e-9) {
            var delta = dt_sec * k_primaryMaxAcc_rpmps;
            var primaryRange = new Range(m_primaryTempGoalVelocity_RPM - delta, m_primaryTempGoalVelocity_RPM + delta);
            m_primaryTempGoalVelocity_RPM = primaryRange.coerceValue(primary_RPM);
            m_logger.setDataPoint(LogEntry.SHOOTER_PRIMARY_MID_GOAL, m_primaryTempGoalVelocity_RPM);
            m_primaryMotor.set(ControlMode.Velocity, velocityRpmToSu(m_primaryTempGoalVelocity_RPM), DemandType.ArbitraryFeedForward, k_primaryZeroPower);
        } else {
            m_primaryTempGoalVelocity_RPM = 0.0;
            m_primaryMotor.neutralOutput();
        }

        if (secondary_RPM > 1.0e-9) {
            var delta = dt_sec * k_secondaryMaxAcc_rpmps;
            var secondaryRange = new Range(m_secondaryTempGoalVelocity_RPM - delta, m_secondaryTempGoalVelocity_RPM + delta);
            m_secondaryTempGoalVelocity_RPM = secondaryRange.coerceValue(secondary_RPM);
            m_logger.setDataPoint(LogEntry.SHOOTER_SECONDARY_MID_GOAL, m_secondaryTempGoalVelocity_RPM);
            m_secondaryMotor.set(ControlMode.Velocity, velocityRpmToSu(m_secondaryTempGoalVelocity_RPM), DemandType.ArbitraryFeedForward, k_secondaryZeroPower);
        } else {
            m_secondaryTempGoalVelocity_RPM = 0.0;
            m_secondaryMotor.neutralOutput();
        }

        m_setVelocityTimer.reset();

        m_primaryGoalVelocity_RPM = primary_RPM;
        m_secondaryGoalVelocity_RPM = secondary_RPM;
    }

    private void stop() {
        m_primaryMotor.neutralOutput();
        m_secondaryMotor.neutralOutput();
    }

    private void setHood(HoodState state) {
        if(state == HoodState.DOWN) {
            setHoodDown();
        } else {
            setHoodUp();
        }
    }

    private void setHoodUp() {
        m_solenoid.setUp();
    }

    private void setHoodDown() {
        m_solenoid.setDown();
    }

    public void enterClimb() {
        setHoodDown();
        disable();
    }

    public Command getManualPowerCommand(Supplier<Double> primary, Supplier<Double> secondary) {
        return new FunctionalCommand(() -> {}, () -> {
            m_primaryManualPower = primary.get();
            m_secondaryManualPower = secondary.get();
        }, unused -> {}, () -> false, this);
    }

    public Command getManualVelocityCommand(Supplier<Double> primary_RPM, Supplier<Double> secondary_ratio) {
        return new FunctionalCommand(() -> {
        }, () -> {
            m_primaryManualRpm = primary_RPM.get();
            m_secondaryManualRatio = secondary_ratio.get();
            m_primaryGoalVelocity_RPM = primary_RPM.get();
            m_secondaryGoalVelocity_RPM = secondary_ratio.get();
        }, unused -> {
        }, () -> false, this);
    }

    public boolean readyToFire() {
        var ready = m_primaryRpmReadyToShoot.update(getPrimaryRpm()) && m_secondaryRpmReadyToShoot.update(getSecondaryRpm());
        var notReady = m_primaryRpmNotReadyToShoot.update(getPrimaryRpm()) || m_secondaryRpmNotReadyToShoot.update(getSecondaryRpm());
        return m_solenoid.isTransitioned() && m_readyToFire.step(ready, notReady) && m_isTryingToShoot;
    }

    /**
     * Detects shots via current draw and is not 100% accurate.
     *
     * @return whether a shot is detected.
     */
    public boolean shotDetected() {
        return m_shotDetected && m_isTryingToShoot;
    }

    public boolean anyShotDetected() {
        return m_shotDetected;
    }

    public Command getStopCommand() {
        return new InstantCommand(() -> {
            m_primaryManualPower = 0.0;
            m_secondaryManualPower = 0.0;
            m_primaryGoalVelocity_RPM = 0.0;
            m_secondaryGoalVelocity_RPM = 0.0;
        }, this);
    }

    public Command getHoodDownCommand() {
        return new InstantCommand(() -> m_manualHoodState = HoodState.DOWN, this);
    }

    public Command getHoodUpCommand() {
        return new InstantCommand(() -> m_manualHoodState = HoodState.UP, this);
    }

    public Command getFireCommand() {
        return new InstantCommand(() -> m_normalCommand = NormalCommand.AUTO_FIRE, this);
    }

    public Command getFenderShotCommand() {
        return new InstantCommand(() -> m_normalCommand = NormalCommand.FIRE_FENDER, this);
    }

    public Command getDashboardShotCommand() {
        return new InstantCommand(() -> m_normalCommand = NormalCommand.DASHBOARD, this);
    }

    public Command getCeaseFireCommand() {
        return new InstantCommand(() -> m_normalCommand = NormalCommand.AUTO_CEASE_FIRE, this);
    }

    public Command getFirePoseCommand(Vector2d pose) {
        return new InstantCommand(() -> {
            m_normalCommand = NormalCommand.FIRE_POSE;
            m_firePoseStored = pose;
        }, this);
    }
}
