/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.TalonSRXControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

import com.ctre.phoenix.ErrorCode;
import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.FunctionalCommand;

import frc.library.drivers.TalonSRXUtils;
import frc.library.GStemsSubsystem;
import frc.library.Pidf;
import frc.robot.RobotState;
import frc.robot.constants.LogEntry;
import frc.robot.constants.RobotMap;
import frc.robot.RobotLogger;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.math.Angle;
import org.growingstems.math.Range;
import org.growingstems.math.Time;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.signals.EdgeDetector;
import org.growingstems.util.statemachine.ExecState;
import org.growingstems.util.statemachine.InitExecState;
import org.growingstems.util.statemachine.InitState;
import org.growingstems.util.statemachine.State;
import org.growingstems.util.statemachine.StateMachine;
import org.growingstems.util.statemachine.StaticTransition;
import org.growingstems.util.timer.TimerI;

public class Climb extends GStemsSubsystem {
    private static final Range k_allowablePassiveAngleStartRange_deg = new Range(-5.0, -3.0);
    private static final Range k_alternateAllowablePassiveAngleStartRange_deg = new Range(50.0, 53.0);
    private static final Range k_allowableActiveStartEncoderRange = new Range(-50.0, 200.0);
    private static final int k_acceptablePasses = 1;

    private static final Angle k_passiveAngleToSurpass = new Angle(-10.0, AngleUnit.DEGREES);
    private static final double k_allowableEncoderError = 150.0;
    private static final double k_fastLiftPower = 1.0;
    private static final double k_pinOnePos = 1000.0;
    private static final double k_pinTwoPos = 3371.0;
    private static final double k_extendedToMidPos = 19670.0;
    private static final double k_latchedMidPos = 19000.0;
    private static final double k_extendedToHighPos = 22440.0;
    private static final double k_latchedHighPos = 19600.0;
    private static final double k_pullupPos = 250.0;
    private static final double k_pullupTraversalPos = 625.0;
    private static final double k_pullupNearPos = 9500.0;
    private static final double k_unhookPos = 13000.0;
    private static final double k_readyToExtendPos = 17000.0;
    private static final double k_readyToReleaseMidPos = 7000.0;
    private static final double k_readyToReleaseHighPos = 3200.0;
    private static final Time k_fastLiftTime = new Time(0.35, TimeUnit.SECONDS);
    private static final Time k_releaseUnfoldTime = new Time(1.25, TimeUnit.SECONDS);
    private static final Time k_timeBeforeGetReadyToRaise = new Time(0.8, TimeUnit.SECONDS);
    private static final Time k_prePrepTime = new Time(0.25, TimeUnit.SECONDS);
    private static final double k_minimumPassiveSwingSpeed_dps = 0.0;
    private static final double k_maximumPassiveSwingSpeed_dps = 100.0;
    private static final double k_minPitchAdvance_deg = -8.0;
    private static final double k_maxPitchAdvance_deg = -2.0;
    private static final double k_maximumPitchSwingSpeed_dps = 80.0;

    private boolean m_forceDisable = false;

    private final Supplier<Boolean> m_shouldAdvance;
    private final Supplier<Boolean> m_shouldRegress;

    private double m_positionGoal_SU = 0.0;

    private static final int k_feedback0Period_ms = 100;
    private static final int k_defaultPidfIdx = 0;
    private static final int k_curveStrength = 4;
    /** Found by putting climb to reverse deadstop, power cycling speed controller
     * for climb master, and checking absolute encoder position */
    private final int k_encoderAbsZeroPosition_SU;
    private final int k_encoderPassiveArmZeroPosition_SU;

    private static final double k_encoderSuPerInches = (2988.0 - (-15198.0)) / 20.0; // TODO: Verify

    private static final Angle k_anglePerSuPassiveArm = new Angle((22.0 / 32.0) / 4096.0, AngleUnit.ROTATIONS);
    private static final double k_passiveArm_dps_per_su = k_anglePerSuPassiveArm.getValue(AngleUnit.DEGREES) * 10; // deg/100ms to deg/second

    // *********************
    //    Limit Constants
    // *********************
    private static final int k_reverseHardLimit_SU = 0;
    private static final int k_forwardHardLimit_SU = 23186;
    private static final int k_reverseSoftLimit_SU = k_reverseHardLimit_SU + 100;
    private static final int k_forwardSoftLimit_SU = k_forwardHardLimit_SU - 400;
    private static final Range k_softLimitRange_SU = new Range(k_reverseSoftLimit_SU, k_forwardSoftLimit_SU);
    private static final double k_neutralDeadband = 0.001;

    // ************************
    //    Unloaded Constants
    // ************************
    private static final int k_unloadedPidfSlot = 0;
    /** Unit: Sensor Units per 100ms^2 */
    private static final double k_accelerationUnloaded = 9000.0;
    /** Unit: Sensor Units per 100ms */
    private static final double k_maxUnloadedForwardSpeed = 2300.0;
    /** Unit: Sensor Units per 100ms^2 */
    private static final double k_accelerationUnloadedFast = 13000.0;
    /** Unit: Sensor Units per 100ms */
    private static final double k_maxUnloadedForwardSpeedFast = 4000.0;
    /** Unit: Sensor Units per 100ms */
    private static final double k_maxUnloadedReverseSpeed = 2650.0;
    private static final Pidf k_unloadedPidf = new Pidf(0.8, 0.0, 0.0, (1023.0 / k_maxUnloadedForwardSpeed));
    private static final double k_zeroPowerUnloaded = 0.042;

    // **********************
    //    Loaded Constants
    // **********************
    private static final int k_loadedPidfSlot = 1;
    /** Unit: Sensor Units per 100ms^2 */
    private static final double k_accelerationLoaded = 6000.0;
    /** Unit: Sensor Units per 100ms */
    private static final double k_maxLoadedSpeed = 1300.0;
    private static final Pidf k_loadedPidf = new Pidf(0.8, 0.0, 0.0, (1023.0 / k_maxLoadedSpeed));
    private static final double k_zeroPowerLoaded = -0.20;

    // **********************
    //    Slow Lift Constants
    // **********************
    private static final double k_slowLiftSpeed = 1000.0;
    private static final double k_accelerationSlow = 700.0;

    private enum MotorConfig {
        UNLOADED_FAST(k_maxUnloadedForwardSpeedFast, k_accelerationUnloadedFast, k_unloadedPidfSlot),
        UNLOADED_FORWARD(k_maxUnloadedForwardSpeed, k_accelerationUnloaded, k_unloadedPidfSlot),
        UNLOADED_REVERSE(k_maxUnloadedReverseSpeed, k_accelerationUnloaded, k_unloadedPidfSlot),
        LOADED(k_maxLoadedSpeed, k_accelerationLoaded, k_loadedPidfSlot),
        SLOW_LIFT(k_slowLiftSpeed, k_accelerationSlow, k_loadedPidfSlot);

        public final double acceleration;
        public final double velocity;
        public final int pidfSlot;

        private MotorConfig(double acceleration, double velocity, int pidfSlot) {
            this.acceleration = acceleration;
            this.velocity = velocity;
            this.pidfSlot = pidfSlot;
        }
    }

    private MotorConfig m_currentMotorConfig;

    private static class LockSolenoid {
        private static final Time k_unlockTime = new Time(0.2, TimeUnit.SECONDS);
        private static final Time k_lockTime = new Time(0.2, TimeUnit.SECONDS);

        private static final boolean k_lockedState = false;
        private final Solenoid m_solenoid;
        private final TimerI m_timer = new WpiTimer();

        public LockSolenoid() {
            m_timer.start();
            m_solenoid = new Solenoid(RobotMap.k_pcmCanId, PneumaticsModuleType.CTREPCM, RobotMap.k_climbLockSolenoid);
        }

        public boolean isLocked() {
            return m_solenoid.get() == k_lockedState;
        }

        public boolean isSteady() {
            return m_timer.hasElapsed(isLocked() ? k_lockTime : k_unlockTime);
        }

        public void lock() {
            if (!isLocked()) {
                m_timer.reset();
                m_solenoid.set(k_lockedState);
            }
        }

        public void unlock() {
            if (isLocked()) {
                m_timer.reset();
                m_solenoid.set(!k_lockedState);
            }
        }
    }

    private final TimerI m_periodicTimer = new WpiTimer();
    private final RobotState m_robotState;
    private final RobotLogger m_logger;
    private final TalonSRX m_motor;
    private final TalonSRX m_follower;
    private final LockSolenoid m_lock;

    // manual variables
    private static enum ManualCommand {
        STOPPED,
        POWER,
        POSITION_UNLOADED_FORWARD,
        POSITION_UNLOADED_REVERSE,
        POSITION_LOADED
    }
    private double m_manualValue = 0.0;
    private ManualCommand m_manualCommand = ManualCommand.STOPPED;

    public Climb(RobotState robotState, RobotLogger logger, Supplier<Boolean> shouldAdvance, Supplier<Boolean> shouldRegress) {
        super("climb");

        m_robotState = robotState;
        m_logger = logger;

        switch (m_robotState.robotId) {
            case P_BOT:
                k_encoderAbsZeroPosition_SU = 1500;
                k_encoderPassiveArmZeroPosition_SU = 2700;
                break;
            default:
                DriverStation.reportWarning("Robot ID \"" + m_robotState.robotId.toString() +
                                            "\" not matched for climb! Defaulting to CBOT",
                                            false);
            case STING_RAY:
                k_encoderAbsZeroPosition_SU = 672;
                k_encoderPassiveArmZeroPosition_SU = 394;
                break;

        }

        m_shouldAdvance = shouldAdvance;
        m_shouldRegress = shouldRegress;

        m_lock = new LockSolenoid();

        m_motor = TalonSRXUtils.createDefaultTalonSRX(RobotMap.k_climbMotorCanId, false);
        m_follower = TalonSRXUtils.createDefaultTalonSRX(RobotMap.k_climbFollowerMotorCanId, false);
        m_follower.follow(m_motor);

        m_motor.setNeutralMode(NeutralMode.Brake);
        m_follower.setNeutralMode(NeutralMode.Brake);

        m_motor.setSensorPhase(false);
        m_follower.setSensorPhase(true);
        m_motor.setInverted(InvertType.InvertMotorOutput);
        m_follower.setInverted(InvertType.FollowMaster);

        m_motor.enableCurrentLimit(false);
        m_follower.enableCurrentLimit(false);

        ErrorCode error = ErrorCode.OK;

        error = m_motor.configForwardSoftLimitEnable(true, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "configForwardSoftLimitEnable");
        error = m_motor.configReverseSoftLimitEnable(true, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "configReverseSoftLimitEnable");
        error = m_motor.configForwardSoftLimitThreshold(k_forwardSoftLimit_SU, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "configForwardSoftLimitThreshold");
        error = m_motor.configReverseSoftLimitThreshold(k_reverseSoftLimit_SU, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "configReverseSoftLimitThreshold");

        error = m_motor.configNeutralDeadband(k_neutralDeadband, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "configNeutralDeadband");

        error = m_motor.configMotionSCurveStrength(k_curveStrength, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "configMotionSCurveStrength");

        error = m_motor.config_kP(k_unloadedPidfSlot, k_unloadedPidf.p, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "config_kP");
        error = m_motor.config_kI(k_unloadedPidfSlot, k_unloadedPidf.i, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "config_kI");
        error = m_motor.config_kD(k_unloadedPidfSlot, k_unloadedPidf.d, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "config_kD");
        error = m_motor.config_kF(k_unloadedPidfSlot, k_unloadedPidf.f, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "config_kF");

        error = m_motor.config_kP(k_loadedPidfSlot, k_loadedPidf.p, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "config_kP");
        error = m_motor.config_kI(k_loadedPidfSlot, k_loadedPidf.i, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "config_kI");
        error = m_motor.config_kD(k_loadedPidfSlot, k_loadedPidf.d, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "config_kD");
        error = m_motor.config_kF(k_loadedPidfSlot, k_loadedPidf.f, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "config_kF");

        // *******************************************
        //    Config for Unloaded w/ Error Checking
        // *******************************************
        m_motor.selectProfileSlot(k_unloadedPidfSlot, k_defaultPidfIdx);
        error = m_motor.configMotionAcceleration(k_accelerationUnloaded);
        TalonSRXUtils.checkError(error, "configMotionAcceleration");
        error = m_motor.configMotionCruiseVelocity(k_maxUnloadedForwardSpeed);
        TalonSRXUtils.checkError(error, "configMotionCruiseVelocity");
        m_currentMotorConfig = MotorConfig.UNLOADED_FORWARD;

        // *************************************************************
        //    Configure encoder on startup based on absolute position
        // *************************************************************
        var sensorData = m_motor.getSensorCollection();
        double relativeOffset = -(sensorData.getPulseWidthPosition() - k_encoderAbsZeroPosition_SU);
        System.out.println("Sensor Data: " + sensorData.getPulseWidthPosition());
        System.out.println("Offset: " + relativeOffset);
        error = m_motor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, k_defaultPidfIdx, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "configSelectedFeedbackSensor");
        error = m_motor.setSelectedSensorPosition(relativeOffset, k_defaultPidfIdx, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "setSelectedSensorPosition");

        //PASSIVE ARM ENCODER
        error = m_follower.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, k_defaultPidfIdx, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "configSelectedFeedbackSensor");

        // ******************
        //    Status Rates
        // ******************
        error = m_motor.setStatusFramePeriod(StatusFrameEnhanced.Status_1_General, TalonSRXUtils.k_defaultMasterGeneralPeriod_ms, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "setStatusFramePeriod_Status_1_General");
        error = m_motor.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0, k_feedback0Period_ms, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "setStatusFramePeriod_Status_2_Feedback0");
        error = m_follower.setStatusFramePeriod(StatusFrameEnhanced.Status_1_General, TalonSRXUtils.k_defaultMasterGeneralPeriod_ms, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "setStatusFramePeriod_Status_1_General");
        error = m_follower.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0, k_feedback0Period_ms, TalonSRXUtils.k_cfgTimeout_ms);
        TalonSRXUtils.checkError(error, "setStatusFramePeriod_Status_2_Feedback0");

        m_periodicTimer.start();
    }

    @Override
    public void periodic() {
        m_periodicTimer.reset();
        super.periodic();
        var encoderPos = getActivePosition();

        if (m_motor.hasResetOccurred()) {
            m_forceDisable = true;
            disable();
            m_logger.setDataPoint(LogEntry.CLIMB_RESET_OCCURED, true);
        } else {
            m_logger.setDataPoint(LogEntry.CLIMB_RESET_OCCURED, false);
        }
        m_logger.setDataPoint(LogEntry.CLIMB_ENCODER_POSITION, encoderPos);
        m_logger.setDataPoint(LogEntry.CLIMB_GOAL_POSITION, m_positionGoal_SU);
        m_logger.setDataPoint(LogEntry.CLIMB_MASTER_CURRENT_PDP, m_robotState.getSensors().getCurrent_A(RobotMap.k_climbMotorPdpChannel));
        m_logger.setDataPoint(LogEntry.CLIMB_SLAVE_CURRENT_PDP, m_robotState.getSensors().getCurrent_A(RobotMap.k_climbFollowerPdpChannel));
        m_logger.setDataPoint(LogEntry.CLIMB_MASTER_CURRENT_TALON, m_motor.getSupplyCurrent());
        m_logger.setDataPoint(LogEntry.CLIMB_PERCENT_OUT, m_motor.getMotorOutputPercent());

        m_logger.setDataPoint(LogEntry.CLIMB_PASSIVE_ANGLE, getPassiveAngle().getValue(AngleUnit.DEGREES));

        var passiveAngleRate = getPassiveDegreePerSec();
        m_logger.setDataPoint(LogEntry.CLIMB_PASSIVE_ANGLE_RATE, passiveAngleRate);
        m_logger.setDataPoint(LogEntry.CLIMB_STATE, super.getCurrentState().getStateName());

        m_logger.setDataPoint(LogEntry.TIME_CLIMB, m_periodicTimer.get().getValue(TimeUnit.MILLISECONDS));
    }

    @Override
    protected boolean isReadyToStartMatch() {
        var passiveInRange = k_allowablePassiveAngleStartRange_deg.inRange(getPassiveAngle().getValue(AngleUnit.DEGREES))
            || k_alternateAllowablePassiveAngleStartRange_deg.inRange(getPassiveAngle().getValue(AngleUnit.DEGREES));
        var activeInRange = k_allowableActiveStartEncoderRange.inRange(getActivePosition());

        var isReady = passiveInRange && activeInRange;
        m_logger.setDataPoint(LogEntry.CLIMB_IS_READY, isReady);
        return isReady;
    }

    @Override
    public void enable() {
        if (!m_forceDisable) {
            super.enable();
        }
    }

    //=============================================================
    // GStems Subsystem State Machine
    //=============================================================
    @Override
    protected State provideDisabledState() {
        return new ExecState("Disabled State", () -> {
            m_lock.lock();
            stop();
        });
    }

    @Override
    protected State provideManualState() {
        var manualState = new InitExecState("Manual State", () -> m_manualCommand = ManualCommand.STOPPED, () -> {
            switch (m_manualCommand) {
                case POSITION_LOADED:
                    setLoadedPosition(m_manualValue);
                    break;
                case POSITION_UNLOADED_FORWARD:
                    setUnloadedForwardPosition(m_manualValue);
                    break;
                case POSITION_UNLOADED_REVERSE:
                    setUnloadedReversePosition(m_manualValue);
                    break;
                case POWER:
                    setPower(m_manualValue);
                    break;
                default:
                case STOPPED:
                    stop();
            }
        });

        var unlockState = new InitState("Unlocking", () -> m_lock.unlock());
        unlockState.addTransition(new StaticTransition(manualState, () -> m_lock.isSteady()));
        return new StateMachine(unlockState).getAsState("Manual SM", true, false);
    }

    @Override
    protected State provideNormalState() {
        var timer = new WpiTimer();
        timer.start();

        Runnable stopMotor = () -> stop();
        Runnable fastLift = () -> setPower(k_fastLiftPower);
        Runnable gotoUnhookPos = () -> setUnloadedForwardPosition(k_unhookPos);
        Runnable gotoReadyToExtendPos = () -> setUnloadedForwardPosition(k_readyToExtendPos);
        Runnable gotoPinOnePos = () -> {
            setUnloadedForwardPosition(k_pinOnePos);
            m_robotState.forceClimbMode_FirstPhase();
        };
        Runnable gotoPinTwoPos = () -> setUnloadedForwardPosition(k_pinTwoPos);
        Runnable gotoPrepReleaseMidPos = () -> setSlowPosition(k_readyToReleaseMidPos);
        Runnable gotoPrepReleaseHighPos = () -> setSlowPosition(k_readyToReleaseHighPos);
        Runnable extendToMid = () -> setUnloadedForwardPosition(k_extendedToMidPos);
        Runnable latchMid = () -> setLoadedPosition(k_latchedMidPos);
        Runnable extendToHigh = () -> setUnloadedFastPosition(k_extendedToHighPos);
        Runnable latchHigh = () -> setLoadedPosition(k_latchedHighPos);
        Runnable pullupNotTraversal = () -> setLoadedPosition(k_pullupPos);
        Runnable pullupTraversal = () -> setLoadedPosition(k_pullupTraversalPos);
        Runnable pullupNear = () -> setLoadedPosition(k_pullupNearPos);
        Runnable finishAndDisable = () -> {
            stop();
            disable();
        };
        Runnable pullupAndDisableDrive = () -> {
            pullupNotTraversal.run();
            m_robotState.forceClimbMode_SecondPhase();
        };

        State begin = new ExecState("At bottom", stopMotor);
        State releasePinOne = new ExecState("Releasing Pin One", gotoPinOnePos);
        State pinOneReleased = new InitExecState("Pin one released", timer::reset, stopMotor);
        State unfoldedPassiveHooks = new ExecState("Passive Hooks Unfolded", stopMotor);
        State releasePinTwo = new ExecState("Releasing pin two", gotoPinTwoPos);
        State pinTwoReleased = new ExecState("Pin two released", stopMotor);
        State raiseToMid = new ExecState("Raising to Mid", extendToMid);
        State extendedToMid = new ExecState("Extended to Mid", stopMotor);
        State latchingMid = new ExecState("Latching on Mid", latchMid);
        State latchedMid = new ExecState("Latched on Mid", latchMid);

        State pullupToHigh = new ExecState("Pulling up to High", pullupAndDisableDrive);
        State hookedMidHigh = new ExecState("Hooked on Mid and High", pullupNotTraversal);

        State pullupToHighRegressedPath = new ExecState("Pulling up to High - Regressed", pullupAndDisableDrive);
        State hookedMidHighRegressedPath = new ExecState("Hooked on Mid and High - Regressed", pullupNotTraversal);

        State almostPreparingToReleaseMid = new InitExecState("Waiting to Prep to Release Mid", timer::reset, pullupNotTraversal);
        State preparingToReleaseMid = new ExecState("Prepping to Release Mid", gotoPrepReleaseMidPos);
        State preparedToReleaseMid = new ExecState("Preped to Release Mid", gotoPrepReleaseMidPos);
        State unhookingMiddle = new InitExecState("Unhooking Mid Fast", timer::reset, fastLift);
        State gotoUnhookMiddlePos = new ExecState("Goto Unhook Mid Pos", gotoUnhookPos);
        State middleUnhooked = new InitExecState("Middle Unhooked", timer::reset, stopMotor);
        State getReadyToExtend = new ExecState("Getting Ready to Raise to High", gotoReadyToExtendPos);
        State readyToExtend = new ExecState("Ready to Raise to High", stopMotor);
        State raiseToHigh = new ExecState("Raising to High Bar", extendToHigh);
        State extendedToHigh = new ExecState("Extended to High", stopMotor);
        State latchingHigh = new ExecState("Latching on High", latchHigh);
        State latchedHigh = new ExecState("Latched on High", latchHigh);
        State pullupNearTraversal = new ExecState("Pulling up Near Traversal", pullupNear);
        State nearTraversal = new ExecState("Near Traversal", pullupNear);

        State pullupToTraversal = new ExecState("Pulling up to Traversal", pullupTraversal);
        State hookedHighTraversal = new ExecState("Hooked on High and Traversal", pullupTraversal);

        State pullupToTraversalRegressedPath = new ExecState("Pulling up to Traversal - Regressed", pullupTraversal);
        State hookedHighTraversalRegressedPath = new ExecState("Hooked on High and Traversal - Regressed", pullupTraversal);

        State preparingToReleaseHigh = new ExecState("Prepping to Release High", gotoPrepReleaseHighPos);
        State preparedToReleaseHigh = new ExecState("Preped to Release High", gotoPrepReleaseHighPos);
        State unhookingHigh = new InitExecState("Unhooking High", timer::reset, fastLift);
        State gotoUnhookHighPos = new ExecState("Goto Unhook High Pos", gotoUnhookPos);
        State finished = new ExecState("TRAVERSAL!", finishAndDisable);

        Supplier<Boolean> shouldAdvance = () -> m_shouldAdvance.get();
        Supplier<Boolean> shouldRegress = () -> m_shouldRegress.get();
        Supplier<Boolean> fastTimesUp = () -> timer.hasElapsed(k_fastLiftTime);
        Supplier<Boolean> unfoldTimeUp = () -> timer.hasElapsed(k_releaseUnfoldTime);
        Supplier<Boolean> getReadyTimeUp = () -> timer.hasElapsed(k_timeBeforeGetReadyToRaise);
        Supplier<Boolean> prePrepTimesUp = () -> timer.hasElapsed(k_prePrepTime);
        Supplier<Boolean> encoderBased = () -> Math.abs(getActivePosition() - m_motor.getClosedLoopTarget()) <= k_allowableEncoderError;
        Supplier<Boolean> skip = () -> true;

        var passiveSwingEdgeDetector = new EdgeDetector();
        Supplier<Boolean> shouldExtendToHigh = () -> {
            if (shouldAdvance.get()) {
                return true;
            }
            if (!passiveSwingEdgeDetector.update(getPassiveAngle().getValue(AngleUnit.DEGREES) > k_passiveAngleToSurpass.getValue(AngleUnit.DEGREES))) {
                return false;
            }
            var dps = getPassiveDegreePerSec();
            if (dps < k_minimumPassiveSwingSpeed_dps) {
                return false;
            }
            if (dps > k_maximumPassiveSwingSpeed_dps) {
                return false;
            }
            return true;
        };

        Supplier<Boolean> readyToHookTraversal = () -> {
            // TODO: If extremely stable, return true
            var pitch_deg = m_robotState.getSensors().getImuPitch().getValue(AngleUnit.DEGREES);
            if (pitch_deg < k_minPitchAdvance_deg) {
                return false;
            }
            if (pitch_deg > k_maxPitchAdvance_deg) {
                return false;
            }
            var pitchAngularVelocity_perSecond = new Angle(m_robotState.getSensors().getPitchAngularVelocity_radps(), AngleUnit.RADIANS);
            if (pitchAngularVelocity_perSecond.getValue(AngleUnit.DEGREES) < 0.0) {
                return false;
            }
            if (pitchAngularVelocity_perSecond.getValue(AngleUnit.DEGREES) > k_maximumPitchSwingSpeed_dps) {
                return false;
            }
            return true;
        };

        var readyEdgeDetector = new EdgeDetector();
        var counter = new AtomicInteger(0);
        Supplier<Boolean> readyToHookTraversalNTimes = () -> {
            if (shouldAdvance.get()) {
                return true;
            }
            if (readyEdgeDetector.update(readyToHookTraversal.get())) {
                if (counter.incrementAndGet() >= k_acceptablePasses) {
                    return true;
                }
            }
            return false;
        };

        begin.addTransition(new StaticTransition(releasePinOne, shouldAdvance));
        releasePinOne.addTransition(new StaticTransition(pinOneReleased, encoderBased));
        pinOneReleased.addTransition(new StaticTransition(unfoldedPassiveHooks, unfoldTimeUp));
        unfoldedPassiveHooks.addTransition(new StaticTransition(releasePinTwo, skip));
        releasePinTwo.addTransition(new StaticTransition(pinTwoReleased, encoderBased));
        pinTwoReleased.addTransition(new StaticTransition(raiseToMid, skip));
        raiseToMid.addTransition(new StaticTransition(extendedToMid, encoderBased));
        extendedToMid.addTransition(new StaticTransition(latchingMid, shouldAdvance));
        latchingMid.addTransition(new StaticTransition(latchedMid, encoderBased));
        latchedMid.addTransition(new StaticTransition(pullupToHigh, shouldAdvance));

        pullupToHigh.addTransition(new StaticTransition(hookedMidHigh, encoderBased));
        hookedMidHigh.addTransition(new StaticTransition(almostPreparingToReleaseMid, skip));

        pullupToHighRegressedPath.addTransition(new StaticTransition(hookedMidHighRegressedPath, encoderBased));
        hookedMidHighRegressedPath.addTransition(new StaticTransition(almostPreparingToReleaseMid, shouldAdvance));

        almostPreparingToReleaseMid.addTransition(new StaticTransition(preparingToReleaseMid, prePrepTimesUp));
        preparingToReleaseMid.addTransition(new StaticTransition(preparedToReleaseMid, encoderBased));
        preparedToReleaseMid.addTransition(new StaticTransition(unhookingMiddle, shouldAdvance));
        unhookingMiddle.addTransition(new StaticTransition(gotoUnhookMiddlePos, fastTimesUp));
        gotoUnhookMiddlePos.addTransition(new StaticTransition(middleUnhooked, encoderBased));
        middleUnhooked.addTransition(new StaticTransition(getReadyToExtend, getReadyTimeUp));
        getReadyToExtend.addTransition(new StaticTransition(readyToExtend, encoderBased));
        readyToExtend.addTransition(new StaticTransition(raiseToHigh, shouldExtendToHigh));
        raiseToHigh.addTransition(new StaticTransition(extendedToHigh, encoderBased));
        extendedToHigh.addTransition(new StaticTransition(latchingHigh, shouldAdvance));
        latchingHigh.addTransition(new StaticTransition(latchedHigh, encoderBased));
        latchedHigh.addTransition(new StaticTransition(pullupNearTraversal, shouldAdvance));
        pullupNearTraversal.addTransition(new StaticTransition(nearTraversal, encoderBased));
        nearTraversal.addTransition(new StaticTransition(pullupToTraversal, readyToHookTraversalNTimes));

        pullupToTraversal.addTransition(new StaticTransition(hookedHighTraversal, encoderBased));
        hookedHighTraversal.addTransition(new StaticTransition(preparingToReleaseHigh, shouldAdvance));

        pullupToTraversalRegressedPath.addTransition(new StaticTransition(hookedHighTraversalRegressedPath, encoderBased));
        hookedHighTraversalRegressedPath.addTransition(new StaticTransition(preparingToReleaseHigh, shouldAdvance));

        preparingToReleaseHigh.addTransition(new StaticTransition(preparedToReleaseHigh, encoderBased));
        preparedToReleaseHigh.addTransition(new StaticTransition(unhookingHigh, shouldAdvance));
        unhookingHigh.addTransition(new StaticTransition(gotoUnhookHighPos, fastTimesUp));
        gotoUnhookHighPos.addTransition(new StaticTransition(finished, encoderBased));

        // Reversals
        extendedToMid.addTransition(new StaticTransition(releasePinTwo, shouldRegress));
        latchingMid.addTransition(new StaticTransition(raiseToMid, shouldRegress));
        latchedMid.addTransition(new StaticTransition(raiseToMid, shouldRegress));
        preparedToReleaseMid.addTransition(new StaticTransition(pullupToHighRegressedPath, shouldRegress));
        raiseToHigh.addTransition(new StaticTransition(middleUnhooked, shouldRegress));
        extendedToHigh.addTransition(new StaticTransition(middleUnhooked, shouldRegress));
        preparedToReleaseHigh.addTransition(new StaticTransition(pullupToTraversalRegressedPath, shouldRegress));

        State normalState = new StateMachine(begin).getAsState("Normal State", true, true);

        var unlockState = new InitState("Unlocking", () -> m_lock.unlock());
        unlockState.addTransition(new StaticTransition(normalState, () -> m_lock.isSteady()));
        return new StateMachine(unlockState).getAsState("Normal SM", true, false);
    }

    private double getActivePosition() {
        return m_motor.getSelectedSensorPosition(k_defaultPidfIdx);
    }

    private Angle getPassiveAngle() {
        return k_anglePerSuPassiveArm.scale(-m_follower.getSelectedSensorPosition(k_defaultPidfIdx) + k_encoderPassiveArmZeroPosition_SU);
    }

    private double getPassiveDegreePerSec() {
        return k_passiveArm_dps_per_su * -m_follower.getSelectedSensorVelocity(k_defaultPidfIdx);
    }

    //=============================================================
    // private configuration functions
    //=============================================================

    private void configMotor(MotorConfig config) {
        if (config == m_currentMotorConfig) {
            return;
        }

        m_currentMotorConfig = config;
        m_motor.selectProfileSlot(config.pidfSlot, k_defaultPidfIdx);
        // Do not use timeout for non-blocking
        ErrorCode error = ErrorCode.OK;
        error = m_motor.configMotionAcceleration(config.acceleration);
        TalonSRXUtils.checkError(error, "configMotionAcceleration");
        error = m_motor.configMotionCruiseVelocity(config.velocity);
        TalonSRXUtils.checkError(error, "configMotionCruiseVelocity");
    }

    //=============================================================
    // private control functions
    //=============================================================
    private void stop() {
        m_motor.neutralOutput();
    }

    private void setPower(double power) {
        m_motor.set(TalonSRXControlMode.PercentOutput, power);
    }

    private void setUnloadedFastPosition(double position_SU) {
        configMotor(MotorConfig.UNLOADED_FAST);
        m_motor.set(TalonSRXControlMode.MotionMagic, k_softLimitRange_SU.coerceValue(position_SU), DemandType.ArbitraryFeedForward, k_zeroPowerUnloaded);
        m_positionGoal_SU = position_SU;
    }

    private void setUnloadedForwardPosition(double position_SU) {
        configMotor(MotorConfig.UNLOADED_FORWARD);
        m_motor.set(TalonSRXControlMode.MotionMagic, k_softLimitRange_SU.coerceValue(position_SU), DemandType.ArbitraryFeedForward, k_zeroPowerUnloaded);
        m_positionGoal_SU = position_SU;
    }

    private void setUnloadedReversePosition(double position_SU) {
        configMotor(MotorConfig.UNLOADED_REVERSE);
        m_motor.set(TalonSRXControlMode.MotionMagic, k_softLimitRange_SU.coerceValue(position_SU), DemandType.ArbitraryFeedForward, k_zeroPowerUnloaded);
        m_positionGoal_SU = position_SU;
    }

    private void setLoadedPosition(double position_SU) {
        configMotor(MotorConfig.LOADED);
        m_motor.set(TalonSRXControlMode.MotionMagic, k_softLimitRange_SU.coerceValue(position_SU), DemandType.ArbitraryFeedForward, k_zeroPowerLoaded);
        m_positionGoal_SU = position_SU;
    }

    private void setSlowPosition(double position_SU) {
        configMotor(MotorConfig.SLOW_LIFT);
        m_motor.set(TalonSRXControlMode.MotionMagic, k_softLimitRange_SU.coerceValue(position_SU), DemandType.ArbitraryFeedForward, k_zeroPowerLoaded);
        m_positionGoal_SU = position_SU;
    }

    //=============================================================
    // manual commands
    //=============================================================
    public Command getManualPowerCommand(Supplier<Double> power) {
        // clang-format off
        return new FunctionalCommand(() -> m_manualCommand = ManualCommand.POWER,
                                     () -> m_manualValue = power.get(),
                                     unused -> {},
                                     () -> false,
                                     this);
        // clang-format on
    }

    public Command getManualPositionUnloadedForwardCommand(Supplier<Double> position) {
        // clang-format off
        return new FunctionalCommand(
            () -> {
                configMotor(MotorConfig.UNLOADED_FORWARD);
                m_manualValue = getActivePosition();
                m_manualCommand = ManualCommand.POSITION_UNLOADED_FORWARD;
            },
            () -> m_manualValue = position.get(),
            unused -> {},
            () -> false,
            this);
        // clang-format on
    }

    public Command getManualPositionUnloadedReverseCommand(Supplier<Double> position) {
        // clang-format off
        return new FunctionalCommand(
            () -> {
                configMotor(MotorConfig.UNLOADED_REVERSE);
                m_manualValue = getActivePosition();
                m_manualCommand = ManualCommand.POSITION_UNLOADED_REVERSE;
            },
            () -> m_manualValue = position.get(),
            unused -> {},
            () -> false,
            this);
        // clang-format on
    }

    public Command getManualPositionLoadedCommand(Supplier<Double> position) {
        // clang-format off
        return new FunctionalCommand(
            () -> {
                configMotor(MotorConfig.LOADED);
                m_manualValue = getActivePosition();
                m_manualCommand = ManualCommand.POSITION_LOADED;
            },
            () -> m_manualValue = position.get(),
            unused -> {},
            () -> false,
            this);
        // clang-format on
    }

    public Command getManualStopCommand() {
        // clang-format off
        return new FunctionalCommand(() -> m_manualCommand = ManualCommand.STOPPED,
                                     () -> m_manualValue = 0.0,
                                     unused -> {},
                                     () -> false,
                                     this);
        // clang-format on
    }
}

