/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DriverStation;

import frc.library.GStemsSubsystem;
import frc.library.Length;
import frc.library.Length.LengthUnit;
import frc.library.vision.Limelight;
import frc.library.vision.ScreenPos;
import frc.library.vision.YawPitch;
import frc.library.vision.Limelight.LightMode;
import frc.robot.RobotIdentifier;
import frc.robot.RobotLogger;
import frc.robot.constants.FieldDimensions;
import frc.robot.constants.LogEntry;

import java.util.Collections;
import java.util.NoSuchElementException;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.geometry.Vector3d;
import org.growingstems.math.Angle;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.util.statemachine.InitExecState;
import org.growingstems.util.statemachine.InitState;
import org.growingstems.util.statemachine.State;
import org.growingstems.util.timer.TimerI;

public class Camera extends GStemsSubsystem {
    private final Length m_targetHeightToCameraHeightDelta;

    private final TimerI m_periodicTimer = new WpiTimer();
    private final Limelight m_limelight;
    private final RobotLogger m_logger;

    private YawPitch m_targetAngles;
    private Length m_radialDistance;
    private Vector3d m_cameraToTargetVector_in;
    private boolean m_isValid;
    private boolean m_visionProcessingEnabled;

    public Camera(RobotLogger logger, RobotIdentifier robotId) {
        super("limelight"); // WARNING: IF YOU CHANGE THIS NAME YOU MUST ALSO CHANGE THE LOGGER NAME IN ROBOT CONTAINER

        m_logger = logger;

        var yaw = new Angle(177.0, AngleUnit.DEGREES);
        var pos = new Vector3d(-10.596, 0.0, 30.483);
        // Double checked on C Bot by Nick. Adjusting for launch pad
        // ruined close up measurments, probably is distortion issues. - 3/2/22
        var pitch = new Angle(32.7, AngleUnit.DEGREES);
        switch (robotId) {
            case P_BOT:
                pitch = new Angle(34.3, AngleUnit.DEGREES);
            default:
                DriverStation.reportWarning("Robot ID \"" + robotId.toString() +
                                            "\" not matched for camera parameters",
                                            false);
            case STING_RAY:
                break;
        }
        m_limelight = new Limelight(pos, pitch, yaw);
        m_limelight.setLightMode(LightMode.FORCE_ON);
        m_limelight.setZoom2X();

        m_targetHeightToCameraHeightDelta = FieldDimensions.k_visionTargetHeightToTapeTop.subtract(Length.inches(pos.getZ()));
        m_periodicTimer.start();
    }

    public void update() {
        m_periodicTimer.reset();
        YawPitch maxCorner = new YawPitch(new Angle(Double.NaN, AngleUnit.RADIANS), new Angle(Double.NaN, AngleUnit.RADIANS));

        if (m_visionProcessingEnabled) {
            m_targetAngles = m_limelight.getTargetAngles();
            m_isValid = m_limelight.hasValidTarget();
            if (m_isValid) {
                maxCorner = getMaxCornerPitch();
                updateRadialDistance(maxCorner);
                updateCameraToTargetVector(maxCorner);
            }
        }
        m_logger.setDataPoint(LogEntry.LIMELIGHT_VALID, m_isValid);
        if (m_isValid) {
            m_logger.setDataPoint(LogEntry.LIMELIGHT_CORNER_PITCH, maxCorner.getPitch().getValue(AngleUnit.DEGREES));
            m_logger.setDataPoint(LogEntry.LIMELIGHT_CORNER_YAW, maxCorner.getYaw().getValue(AngleUnit.DEGREES));
            m_logger.setDataPoint(LogEntry.LIMELIGHT_TARGET_YAW, m_limelight.getPitch().getValue(AngleUnit.DEGREES));
            m_logger.setDataPoint(LogEntry.LIMELIGHT_RADIAL_DISTANCE, m_radialDistance.getValue(LengthUnit.INCHES));
            m_logger.setDataPoint(LogEntry.LIMELIGHT_POS_X, m_cameraToTargetVector_in.getX());
            m_logger.setDataPoint(LogEntry.LIMELIGHT_POS_Y, m_cameraToTargetVector_in.getY());
        } else {
            m_logger.setDataPoint(LogEntry.LIMELIGHT_CORNER_PITCH, "NaN");
            m_logger.setDataPoint(LogEntry.LIMELIGHT_CORNER_YAW, "NaN");
            m_logger.setDataPoint(LogEntry.LIMELIGHT_TARGET_YAW, "NaN");
            m_logger.setDataPoint(LogEntry.LIMELIGHT_RADIAL_DISTANCE, "NaN");
            m_logger.setDataPoint(LogEntry.LIMELIGHT_POS_X, "NaN");
            m_logger.setDataPoint(LogEntry.LIMELIGHT_POS_Y, "NaN");
        }

        m_logger.setDataPoint(LogEntry.TIME_LIMELIGHT, m_periodicTimer.get().getValue(TimeUnit.MILLISECONDS));
    }

    public Angle getYaw() {
        return m_limelight.getYaw();
    }

    public Vector3d getPosition() {
        return m_limelight.getPosition();
    }

    public boolean isValid() {
        return m_isValid;
    }

    public Length getRadialDistance() {
        return m_radialDistance;
    }

    public Vector3d getCameraToTargetVector_in() {
        return m_cameraToTargetVector_in;
    }

    private void updateRadialDistance(YawPitch maxCorner) {
        Angle angleToTarget;
        if (maxCorner == null) {
            angleToTarget = m_limelight.getTargetAngles().getPitch().add(m_limelight.getPitch());
        } else {
            Angle maxCornerPitch = maxCorner.getPitch();
            angleToTarget = maxCornerPitch.add(m_limelight.getPitch());
        }
        var deltaHeight = m_targetHeightToCameraHeightDelta;
        // Add radius of the hub
        m_radialDistance = deltaHeight.scale(1.0 / angleToTarget.tan())
                                      .add(FieldDimensions.k_visionTargetRadius);
    }

    private void updateCameraToTargetVector(YawPitch maxCorner) {
        Angle yawAngle;
        if (maxCorner == null) {
            yawAngle = m_limelight.getTargetAngles().getYaw();
        } else {
            yawAngle = maxCorner.getYaw();
        }
        var x = m_radialDistance.scale(yawAngle.cos());
        var y = m_radialDistance.scale(yawAngle.sin());
        var z = m_limelight.getPosition().getZ();

        m_cameraToTargetVector_in = new Vector3d(x.getValue(LengthUnit.INCHES), y.getValue(LengthUnit.INCHES), z);
    }

    /**
     * Finds the highest corner position in the Y direction on the screen, and returns its pitch angle relative to boresight.
     * @return pitch angle relative to boresight.
     */
    private YawPitch getMaxCornerPitch() {
        var points = m_limelight.getTargetCorners();

        if (!points.isEmpty()) {
            var highestScreenPos = Collections.min(points, (a, b) -> Integer.compare(a.getY(), b.getY()));
            return m_limelight.getYawPitchFromPos(highestScreenPos);
        } else {
            return null;
        }
    }

    private ScreenPos processPoints() {
        // More complex point processing can be implemented later
        var points = m_limelight.getTargetCorners();
        try {
            return Collections.max(points, (a, b) -> Integer.compare(a.getY(), b.getY()));
        } catch (NoSuchElementException e) {
            // No valid screen pos
            return new ScreenPos(0, 0);
        }
    }

    @Override
    protected State provideDisabledState() {
        return new InitState("Disabled State", () -> {
            m_limelight.setLightMode(LightMode.FORCE_OFF);
            m_visionProcessingEnabled = false;
        });
    }

    @Override
    protected State provideManualState() {
        return new InitExecState("Manual State", () -> {
            m_limelight.setLightMode(LightMode.FORCE_OFF);
            m_limelight.setSaMode(true);
            m_visionProcessingEnabled = false;
        }, this::update);
    }

    @Override
    protected State provideNormalState() {
        return new InitExecState("Normal State", () -> {
            m_limelight.setLightMode(LightMode.FORCE_ON);
            m_limelight.setSaMode(false);
            m_visionProcessingEnabled = true;
        }, this::update);
    }
}
