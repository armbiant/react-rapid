// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.math.Time;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.util.timer.TimerI;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.util.Color8Bit;

public class LedController {
    private static class Pixel {
        private static final double k_maxLedOutput = 0.6;
        private final int m_r;
        private final int m_g;
        private final int m_b;

        public Pixel(int r, int g, int b) {
            r = clamp(r);
            g = clamp(g);
            b = clamp(b);

            if ((r + g + b) > (int)(k_maxLedOutput * 255.0 * 3.0)) {
                r = (int)(r * k_maxLedOutput);
                g = (int)(g * k_maxLedOutput);
                b = (int)(b * k_maxLedOutput);
            }

            this.m_r = r;
            this.m_g = g;
            this.m_b = b;
        }

        public Color8Bit getColor() {
            return new Color8Bit(m_r, m_g, m_b);
        }

        public Pixel adjustSaturation(double value) {
            return new Pixel((int)Math.round(m_r * value), (int)Math.round(m_g * value), (int)Math.round(m_b * value));
        }

        private int clamp(int in) {
            if (in < 0) {
                return 0;
            }
            if (in > 255) {
                return 255;
            }
            return in;
        }
    }

    private static final int k_ledPort = 9;

    private static final int k_ledCollumnTotal = 40;
    private static final Time k_updateTime = new Time(150, TimeUnit.MILLISECONDS);
    private static final Time k_cargoTrackUpdateOnTime = new Time(300, TimeUnit.MILLISECONDS);
    private static final Time k_cargoTrackUpdateOffTime = new Time(80, TimeUnit.MILLISECONDS);
    private static final double k_cargoTrackDimValue = 0.4;
    private static final Pixel k_yellow = new Pixel(209, 80, 0);
    private static final Pixel k_purple = new Pixel(155, 0, 232);
    private static final Pixel k_cyan = new Pixel(120, 255, 0);
    private static final Pixel k_red = new Pixel(255, 0, 0);
    private static final Pixel k_off = new Pixel(0, 0, 0);

    private TimerI m_updateTimer = new WpiTimer();
    private TimerI m_cargoTrackUpdateTimer = new WpiTimer();
    private RobotState m_robotState;

    private boolean m_blinkState = false;
    private boolean m_cargoTrackBlinkState = false;

    private AddressableLED m_led = new AddressableLED(k_ledPort);
    private AddressableLEDBuffer m_buffer = new AddressableLEDBuffer(k_ledCollumnTotal);
    private Pixel m_currentColor = k_off;

    public LedController(RobotState robotState) {
        m_led.setLength(k_ledCollumnTotal);
        m_led.start();
        m_updateTimer.start();
        m_cargoTrackUpdateTimer.start();

        m_robotState = robotState;
        updateLeds();
    }

    private void setBufferColor(Pixel pixel) {
        if (pixel.equals(m_currentColor)) {
            return;
        }
        m_currentColor = pixel;

        updateLeds();
    }

    private void updateLeds() {
        var color = m_currentColor.getColor();
        for (int i = 0; i < k_ledCollumnTotal; i++) {
            m_buffer.setLED(i, color);
        }
        m_led.setData(m_buffer);
    }

    public void update() {
        if (m_updateTimer.hasPeriodPassed(k_updateTime)) {
            m_blinkState = !m_blinkState;
        }

        if (m_cargoTrackBlinkState && m_cargoTrackUpdateTimer.hasElapsed(k_cargoTrackUpdateOnTime)) {
            m_cargoTrackBlinkState = false;
            m_cargoTrackUpdateTimer.reset();
        }

        if (!m_cargoTrackBlinkState && m_cargoTrackUpdateTimer.hasElapsed(k_cargoTrackUpdateOffTime)) {
            m_cargoTrackBlinkState = true;
            m_cargoTrackUpdateTimer.reset();
        }

        if (m_robotState.getSensors().limelightHasTarget() && m_blinkState) {
            setBufferColor(k_off);
        } else if (m_robotState.isTryingToTrackCargo() && m_robotState.getCargoTracker().getVectorToClosestCargo() != null) {
            setBufferColor(k_cyan);
        } else if (m_robotState.cargoFullyPrimed()) {
            setBufferColor(k_yellow);
        } else if (m_robotState.cargoHalfPrimed()) {
            setBufferColor(k_purple);
        } else  if (m_robotState.isTryingToTrackCargo()) {
            setBufferColor(k_red);
        } else if (m_robotState.getCargoTracker().getVectorToClosestCargo() != null) {
            if (m_cargoTrackBlinkState) {
                setBufferColor(k_cyan);
            } else {
                setBufferColor(k_cyan.adjustSaturation(k_cargoTrackDimValue));
            }
        } else {
            setBufferColor(k_off);
        }
    }
}
