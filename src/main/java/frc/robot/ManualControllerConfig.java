/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.button.Trigger;

import frc.library.RateLimiter;

import org.growingstems.control.SymmetricDeadzone;
import org.growingstems.math.Vector2d;
import org.growingstems.signals.api.SignalModifier;

public class ManualControllerConfig extends ControllerConfig {
    public Trigger intakeDeploy;
    public Trigger intakeStow;
    public Trigger intakeIn;
    public Trigger intakeOut;
    public Trigger serializerIn;
    public Trigger serializerOut;
    public Trigger serializerPistonEngage;
    public Trigger serializerPistonDisengage;
    public Trigger shooterSpinUp;
    public Trigger shooterUp;
    public Trigger shooterOut;
    public Trigger climb;

    private final static double triggerThreshold = 0.5;

    private SignalModifier<Double, Double> translationDeadzoneModifier =
        new SymmetricDeadzone(1.0, 0.1)
            .append(in -> Math.pow(in, 3.0))
            .append(in -> in * (fullPowerTrigger.get() ? k_fullSpeed : driveThrottle.getDouble(k_defaultMaxDriveSpeed)));
    private SignalModifier<Double, Double> rotationDeadzoneModifier =
        new SymmetricDeadzone(1.0, 0.1).append(in -> turnThrottle.getDouble(k_defaultMaxTurnSpeed) * Math.pow(in, 3.0));

    private SignalModifier<Double, Double> inputRampX = translationDeadzoneModifier.append(new RateLimiter());
    private SignalModifier<Double, Double> inputRampY = translationDeadzoneModifier.append(new RateLimiter());

    private SignalModifier<Vector2d, Vector2d> inputVectorModifier = in -> {
        // Input space is a square, want it to be a circle
        // Scale input vector up to square perimiter
        double largest = Math.max(Math.abs(in.getX()), Math.abs(in.getY()));
        if (largest == 0.0) {
            return new Vector2d();
        }
        // Get the magnitude of the scaled perimiter vector, then scale by 1/mag
        return in.scale(1.0 / in.scale(1.0 / largest).getMagnitude());
    };

    public ManualControllerConfig(XboxController controller) {
        // Drive
        defaultVelocitySupplier =
            inputVectorModifier.provide(() -> new Vector2d(inputRampY.update(-controller.getLeftY()), inputRampX.update(-controller.getLeftX())));
        fullPowerTrigger = new Trigger(() -> false);

        // clang-format off
        defaultRotationSupplier = rotationDeadzoneModifier.provide(() -> -controller.getRightX());
        // clang-format on

        resetImu = new Trigger(controller::getBackButton);

        intakeDeploy = new Trigger(controller::getLeftBumper);
        intakeStow = new Trigger(controller::getRightBumper);
        intakeIn = new Trigger(() -> controller.getRightTriggerAxis() > triggerThreshold);
        intakeOut = new Trigger(() -> controller.getLeftTriggerAxis() > triggerThreshold);
        serializerIn = new Trigger(controller::getAButton);
        serializerOut = new Trigger(controller::getBButton);
        serializerPistonEngage = new Trigger(() -> controller.getPOV() == dPadRight);
        serializerPistonDisengage = new Trigger(() -> controller.getPOV() == dPadLeft);
        shooterSpinUp = new Trigger(controller::getXButton);
        shooterUp = new Trigger(() -> controller.getYButton() && controller.getPOV() == dPadDown);
        shooterOut = new Trigger(() -> controller.getYButton() && controller.getPOV() == dPadUp);
        climb = new Trigger(controller::getStartButton);

        // Serializer
        rejectIntakeTrigger = new Trigger(() -> !controller.getYButton() && controller.getPOV() == dPadDown);
        rejectShooterTrigger = new Trigger(() -> !controller.getYButton() && controller.getPOV() == dPadUp);
    }
}
