/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.util.timer.TimerI;

public class TrapezoidAccProfileDouble {
    private final TimerI m_timer = new WpiTimer();
    private final double m_endtime_s;
    private final double m_maxAcc_per_s2;
    private final double m_maxVel_per_s;
    private final double m_timeToAccToMax_s;
    private final double m_accelerationDistance;
    private final double m_from;
    private final double m_totalDistance;
    private final boolean m_isBackwards;

    public TrapezoidAccProfileDouble(double maxAcc_per_s2, double maxVel_per_s, double distance) {
        this(maxAcc_per_s2, maxVel_per_s, distance, 0.0);
    }

    public TrapezoidAccProfileDouble(double maxAcc_per_s2, double maxVel_per_s, double to, double from) {
        m_maxAcc_per_s2 = maxAcc_per_s2;
        m_isBackwards = to < from;
        m_totalDistance = Math.abs(to - from);
        m_maxVel_per_s = Math.min(maxVel_per_s, Math.sqrt(m_maxAcc_per_s2 * m_totalDistance));
        m_from = from;
        m_accelerationDistance = m_maxVel_per_s * m_maxVel_per_s / (2.0 * m_maxAcc_per_s2);
        m_timeToAccToMax_s = m_maxVel_per_s / m_maxAcc_per_s2;
        m_endtime_s = (2 * m_timeToAccToMax_s) + (m_totalDistance - 2.0 * m_accelerationDistance) / m_maxVel_per_s;
    }

    public double getCurrentDistance() {
        var unscaled = getUnscaledDistance();
        var result = unscaled;
        if (m_isBackwards) {
            result = -result;
        }
        result += m_from;
        return result;
    }

    private double getUnscaledDistance() {
        if (!m_timer.isRunning()) {
            m_timer.start();
        }
        var passed_s = m_timer.get().getValue(TimeUnit.SECONDS);
        var passed_perc = passed_s / m_endtime_s;
        if (passed_perc > 1.0) {
            return m_totalDistance;
        } else if (passed_s < m_timeToAccToMax_s && passed_perc <= 0.5) {
            // Accelerating
            // Velocity: m_maxAcc_per_s2 * passed_s;
            return m_maxAcc_per_s2 * passed_s * passed_s * 0.5;
        } else if (m_endtime_s - passed_s < m_timeToAccToMax_s) {
            // Decelerating
            // Velocity: (m_endtime_s - passed_s) * m_maxAcc_per_s2;
            return m_maxVel_per_s * (m_endtime_s - m_timeToAccToMax_s) - Math.pow(m_endtime_s - passed_s, 2.0) * m_maxAcc_per_s2 / 2.0;
        } else {
            // At max speed
            // Velocity: m_maxVel_per_s;
            return m_maxVel_per_s * passed_s - m_accelerationDistance;
        }
    }
}
