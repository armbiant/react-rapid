/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import static frc.library.RobotMatchState.getMatchState;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.library.GStemsSubsystem;
import frc.library.Length;
import frc.robot.constants.LogEntry;
import frc.robot.constants.RobotMap;
import frc.robot.constants.ShotCriteria;
import frc.robot.subsystems.Climb;
import frc.robot.subsystems.Drive;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Serializer;
import frc.robot.subsystems.Shooter;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.math.Angle;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.math.Pose2d;
import org.growingstems.math.PwmSignal;
import org.growingstems.math.Range;
import org.growingstems.math.Time;
import org.growingstems.math.Vector2d;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.signals.Debounce;
import org.growingstems.signals.EdgeDetector;
import org.growingstems.signals.api.SignalModifier;
import org.growingstems.util.timer.TimerI;

/**
 * This class holds all the calulations and resources needed to maintain the
 * robot's state. Generally these calculations are made at the beginning of
 * every loop so that all of the robot's subsystems can get access to the most
 * up-to-date state data. To initiate these calculations, call updateState().
 */

public class RobotState {
    private static final Range k_flightTimeEstimateRange_s = new Range(0.3, 1.0);
    private static final PwmSignal k_cargoTrackerRumble_s = PwmSignal.fromDutyCycleAndPeriod(0.65, 0.25);
    private static final Time k_cargoRumblePeriod = new Time(k_cargoTrackerRumble_s.getPeriod(), TimeUnit.SECONDS);
    private static final Time k_cargoRumbleRumbleTime = new Time(k_cargoTrackerRumble_s.getHigh(), TimeUnit.SECONDS);
    public final RobotIdentifier robotId;
    private final RobotLogger m_logger;
    private Sensors m_sensors;

    private final TimerI m_periodicTimer = new WpiTimer();
    private final TimerI m_poseFusionPeriodicTimer = new WpiTimer();
    private final TimerI m_cargoTrackerPeriodicTimer = new WpiTimer();

    public final Length k_distanceCenterToBumper = Length.inches(17);

    private final NetworkTableEntry m_enableVelocityCompensationEntry = SmartDashboard.getEntry("robotState/Enable Velocity Compensation");

    private static final double k_cargoTrackerTranslationalScale = 0.65;
    private static final double k_cargoTrackerRotationalScale = 0.6;

    private Drive m_drive;
    private Intake m_intake;
    private Serializer m_serializer;
    private Shooter m_shooter;
    private Climb m_climb;
    private XboxController m_driver;
    private Compressor m_compressor;
    private PoseFusion m_poseFusion;

    // LED
    private LedController m_ledController;

    // Shooter
    private final NetworkTableEntry k_extraDistanceEntry = SmartDashboard.getEntry("robotState/Extra Shot Distance");
    private final double k_defaultExtraDistance = 0.0;

    // Cargo Tracker
    private static final double k_cargoTrackInnerRumbleDistance_in = 30.0;
    private static final double k_cargoTrackOuterRumbleDistance_in = 80.0;
    private final CargoTracker m_cargoTracker;
    private boolean m_isTryingTrackCargo = false;
    private double m_cargoTrackerFeedback = 0.0;

    private List<GStemsSubsystem> m_subsystemsEnabledAtStart = new ArrayList<>();
    private List<GStemsSubsystem> m_subsystemsDisabledAtStart = new ArrayList<>();

    // Rumble Timer
    private final TimerI m_cargoRumbleTimer = new WpiTimer();

    private static final SignalModifier<Boolean, Boolean> m_notSignalModifier = in -> !in;
    private final SignalModifier<Boolean, Boolean> m_shotDetectedSensorFilter =
        m_notSignalModifier
        .append(new Debounce(new Time(20.0, TimeUnit.MILLISECONDS), new WpiTimer()))
        .append(m_notSignalModifier)
        .append(new EdgeDetector(Edge.FALLING));
    private boolean m_shotDetected = false;
    private int m_shots = 0;

    private Debounce m_readyToShootDebounce = new Debounce(ShotCriteria.k_readyToShootTimeRequirementTime, new WpiTimer());

    public enum AutoRejectOption {
        NONE,
        OURS,
        THEIRS
    }

    private static final SendableChooser<AutoRejectOption> m_autoRejectSetting = new SendableChooser<>();
    static {
        m_autoRejectSetting.addOption(AutoRejectOption.NONE.toString(), AutoRejectOption.NONE);
        // m_autoRejectSetting.addOption(AutoRejectOption.OURS.toString(), AutoRejectOption.OURS);
        m_autoRejectSetting.addOption(AutoRejectOption.THEIRS.toString(), AutoRejectOption.THEIRS);
        m_autoRejectSetting.setDefaultOption(AutoRejectOption.THEIRS.toString(), AutoRejectOption.THEIRS);
        SmartDashboard.putData("robotState/AutoRejectSetting", m_autoRejectSetting);
    }
    private AutoRejectOption m_autonomousAutoRejectSetting = AutoRejectOption.NONE;

    public enum CargoTrackOption {
        OURS,
        THEIRS,
        BOTH
    }

    private static final SendableChooser<CargoTrackOption> m_cargoTrackSetting = new SendableChooser<>();
    static {
        m_cargoTrackSetting.addOption(CargoTrackOption.OURS.toString(), CargoTrackOption.OURS);
        m_cargoTrackSetting.addOption(CargoTrackOption.THEIRS.toString(), CargoTrackOption.THEIRS);
        m_cargoTrackSetting.addOption(CargoTrackOption.BOTH.toString(), CargoTrackOption.BOTH);
        m_cargoTrackSetting.setDefaultOption(CargoTrackOption.OURS.toString(), CargoTrackOption.OURS);
        SmartDashboard.putData("robotState/CargoTrackSetting", m_cargoTrackSetting);
    }
    private CargoTrackOption m_autonomousCargoTrackSetting = CargoTrackOption.OURS;

    public RobotState(RobotLogger logger, RobotIdentifier robotId) {
        m_logger = logger;
        this.robotId = robotId;

        m_enableVelocityCompensationEntry.setBoolean(true);
        m_cargoTracker = new CargoTracker(this, m_logger);

        k_extraDistanceEntry.setDouble(k_defaultExtraDistance);

        m_periodicTimer.start();
        m_poseFusionPeriodicTimer.start();
        m_cargoTrackerPeriodicTimer.start();
        m_cargoRumbleTimer.start();
    }

    public void init(Drive drive,
                     Intake intake,
                     Serializer serializer,
                     Shooter shooter,
                     Climb climb,
                     XboxController driver) {
        m_drive = drive;
        m_subsystemsEnabledAtStart.add(m_drive);
        m_intake = intake;
        m_subsystemsEnabledAtStart.add(m_intake);
        m_serializer = serializer;
        m_subsystemsEnabledAtStart.add(m_serializer);
        m_shooter = shooter;
        m_subsystemsEnabledAtStart.add(m_shooter);
        m_climb = climb;
        m_subsystemsDisabledAtStart.add(m_climb);
        m_driver = driver;
        m_compressor = new Compressor(RobotMap.k_pcmCanId, RobotMap.k_pcmType);
        m_sensors = new Sensors(m_logger, this.robotId);

        m_subsystemsEnabledAtStart.forEach(GStemsSubsystem::enable);
        m_subsystemsDisabledAtStart.forEach(GStemsSubsystem::disable);
        m_compressor.enableDigital();

        m_sensors.setHeading(new Angle());
        m_poseFusion = new PoseFusion(this.m_logger, this.m_drive, this.m_sensors);

        m_ledController = new LedController(this);

        setRobotPose(new Pose2d());
    }

    public void forceClimbMode_FirstPhase() {
        m_shooter.enterClimb();
        m_serializer.disable();
        m_intake.getRetractCommand().schedule();
        m_sensors.turnOffLimelight();
        m_compressor.disable();
    }

    public void forceClimbMode_SecondPhase() {
        m_drive.disable();
    }

    /**
     * This method initiates the collection of external sensor data and calculates
     * all the relevent RobotState data. This method generally will be first thing
     * to run before any other robot code.
     */
    public void updateState() {
        m_periodicTimer.reset();

        // Test if ready to begin match
        boolean isReady = true;
        for (var subsystem : m_subsystemsEnabledAtStart) {
            if (!subsystem.isReadyToStartMatch(true)) { // Don't short-circuit
                isReady = false;
            }
        }
        for (var subsystem : m_subsystemsDisabledAtStart) {
            if (!subsystem.isReadyToStartMatch(false)) { // Don't short-circuit
                isReady = false;
            }
        }
        boolean isStingRay = robotId == RobotIdentifier.STING_RAY;
        m_logger.setDataPoint(LogEntry.ROBOT_STATE_IS_STING_RAY, isStingRay);
        isReady &= isStingRay;
        m_logger.setDataPoint(LogEntry.ROBOT_STATE_READY_TO_BEGIN_MATCH, isReady);

        m_poseFusionPeriodicTimer.reset();
        m_poseFusion.updateRobotPose();
        m_logger.setDataPoint(LogEntry.TIME_POSE_FUSION, m_poseFusionPeriodicTimer.get().getValue(TimeUnit.MILLISECONDS));

        var shotVectorToHub_in = getShotVectorToHub_in();

        var sensorShotDetected = m_shotDetectedSensorFilter.update(m_sensors.getShotDetectedSensor());
        var serializerCouldBeShooting = m_serializer.getMotorPower() > 0.0 && m_serializer.isEngaged();

        m_shotDetected = sensorShotDetected && serializerCouldBeShooting;
        if (m_shotDetected) {
            m_shots++;
        }

        // Cargo Tracker
        m_cargoTrackerPeriodicTimer.reset();
        var cargoList = m_sensors.getTrackedCargoList();
        m_cargoTracker.updateTracks(cargoList);
        m_logger.setDataPoint(LogEntry.TIME_CARGO_TRACKER, m_cargoTrackerPeriodicTimer.get().getValue(TimeUnit.MILLISECONDS));

        // Update rumble timer
        m_cargoRumbleTimer.hasPeriodPassed(k_cargoRumblePeriod);

        double rumblePower = 0.0;
        var cargoVector = m_cargoTracker.getVectorToClosestCargo();
        if (cargoVector != null)  {
            m_cargoTrackerFeedback = 1.0 - (cargoVector.getMagnitude() - k_cargoTrackInnerRumbleDistance_in) /
                                        (k_cargoTrackOuterRumbleDistance_in - k_cargoTrackInnerRumbleDistance_in);
            m_cargoTrackerFeedback = Math.min(Math.max(0.0, m_cargoTrackerFeedback), 1.0); // Clamp from 0.0 to 1.0
            // Don't rumble when disabled or in auto
            if (DriverStation.isTeleopEnabled()) {
                if (cargoFullyPrimed()) {
                    // Always rumble when we have two cargo
                    rumblePower =  0.5;
                } else if (!m_cargoRumbleTimer.hasElapsed(k_cargoRumbleRumbleTime)) {
                    // Rumble based on cargo tracker if cargo rumble timer says so
                    if (DriverStation.isEnabled() && m_isTryingTrackCargo) {
                        rumblePower = (m_cargoTrackerFeedback * 0.75) + 0.25; // Scale from 0.0-1.0 to 0.25-1.0
                    }
                }
            }
        } else {
            m_cargoTrackerFeedback = 0.0;
        }
        m_driver.setRumble(RumbleType.kLeftRumble, rumblePower);
        m_driver.setRumble(RumbleType.kRightRumble, rumblePower);

        //led
        m_ledController.update();

        // Logging
        m_logger.setDataPoint(LogEntry.ROBOT_STATE_ID, robotId.toString());

        var canStatus = RobotController.getCANStatus();
        m_logger.setDataPoint(LogEntry.CAN_STATUS_PERCENT_BUS_UTILIZATION, canStatus.percentBusUtilization);
        m_logger.setDataPoint(LogEntry.CAN_STATUS_BUS_OFF_COUNT, canStatus.busOffCount);
        m_logger.setDataPoint(LogEntry.CAN_STATUS_TX_FULL_COUNT, canStatus.txFullCount);
        m_logger.setDataPoint(LogEntry.CAN_STATUS_RECEIVE_ERROR_COUNT, canStatus.receiveErrorCount);
        m_logger.setDataPoint(LogEntry.CAN_STATUS_TRANSMIT_ERROR_COUNT, canStatus.transmitErrorCount);

        m_logger.setDataPoint(LogEntry.DRIVER_STATION_MATCH_TIME, DriverStation.getMatchTime());
        m_logger.setDataPoint(LogEntry.DRIVER_STATION_MATCH_TYPE, DriverStation.getMatchType());
        m_logger.setDataPoint(LogEntry.DRIVER_STATION_ALLIANCE, DriverStation.getAlliance());
        m_logger.setDataPoint(LogEntry.DRIVER_STATION_EVENT_NAME, DriverStation.getEventName());
        m_logger.setDataPoint(LogEntry.DRIVER_STATION_GAME_SPECIFIC_MSG, DriverStation.getGameSpecificMessage());
        m_logger.setDataPoint(LogEntry.DRIVER_STATION_REPLAY_NUMBER, DriverStation.getReplayNumber());
        m_logger.setDataPoint(LogEntry.DRIVER_STATION_LOCATION, DriverStation.getLocation());
        m_logger.setDataPoint(LogEntry.DRIVER_STATION_MATCH_NUMBER, DriverStation.getMatchNumber());
        m_logger.setDataPoint(LogEntry.DRIVER_STATION_FMS_ATTACHED, DriverStation.isFMSAttached());
        m_logger.setDataPoint(LogEntry.DRIVER_STATION_DS_ATTACHED, DriverStation.isDSAttached());
        m_logger.setDataPoint(LogEntry.DRIVER_STATION_MATCH_STATE, getMatchState());

        m_logger.setDataPoint(LogEntry.ROBOT_STATE_SHOT_VECTOR_ANG, shotVectorToHub_in.getAngle());
        m_logger.setDataPoint(LogEntry.ROBOT_STATE_SHOT_VECTOR_DIST, shotVectorToHub_in.getMagnitude());

        m_logger.setDataPoint(LogEntry.ROBOT_STATE_SERIALIZER_COULD_BE_SHOOTING, serializerCouldBeShooting);
        m_logger.setDataPoint(LogEntry.ROBOT_STATE_SHOT_SENSOR_FILTERED, sensorShotDetected);
        m_logger.setDataPoint(LogEntry.ROBOT_STATE_SHOT_DETECTED, m_shotDetected);
        m_logger.setDataPoint(LogEntry.ROBOT_STATE_READY_TO_SHOOT, isReadyToShoot());
        m_logger.setDataPoint(LogEntry.ROBOT_STATE_TOTAL_SHOTS, m_shots);

        if (cargoVector != null) {
            m_logger.setDataPoint(LogEntry.ROBOT_STATE_CARGO_DIST, cargoVector.getMagnitude());
            m_logger.setDataPoint(LogEntry.ROBOT_STATE_CARGO_ANGLE, cargoVector.getAngle().getValue(AngleUnit.DEGREES));
        } else {
            m_logger.setDataPoint(LogEntry.ROBOT_STATE_CARGO_DIST, 0.0);
            m_logger.setDataPoint(LogEntry.ROBOT_STATE_CARGO_ANGLE, 0.0);
        }

        m_logger.setDataPoint(LogEntry.TIME_ROBOT_STATE, m_periodicTimer.get().getValue(TimeUnit.MILLISECONDS));
    }

    public Sensors getSensors() {
        return m_sensors;
    }

    public void setRobotPose(Pose2d pose) {
        m_poseFusion.setRobotPose_in(pose);
    }

    public Pose2d getRobotPose_in() {
        return m_poseFusion.getRobotPose_in();
    }

    public Pose2d getOdometryRobotPose_in() {
        return m_poseFusion.getOdometryRobotPose_in();
    }

    public Vector2d getVectorToHubCenter_in() {
        return m_poseFusion.getVectorToHubCenter_in();
    }

    public Pose2d getFieldVelocity_in() {
        return m_poseFusion.getFieldVelocity_in();
    }

    public boolean isVelocityCompensationEnabled() {
        return m_enableVelocityCompensationEntry.getBoolean(true) && DriverStation.isTeleopEnabled();
    }

    public Vector2d getShotVectorToHub_in() {
        var shotVector = getVectorToHubCenter_in();
        if (isVelocityCompensationEnabled()) {
            var velocity_in = getFieldVelocity_in().getVector();
            var offset_in = velocity_in.scale(getShotFlightTimeEstimate().getValue(TimeUnit.SECONDS)).negate();
            shotVector = shotVector.add(offset_in);
        }
        return new Vector2d(shotVector.getMagnitude() + k_extraDistanceEntry.getDouble(k_defaultExtraDistance), shotVector.getAngle());
    }

    private Time getShotFlightTimeEstimate() {
        var distance = getVectorToHubCenter_in().getMagnitude();
        double lowerPosBound = 70.0;
        double upperPosBound = 200.0;
        var seconds = ((k_flightTimeEstimateRange_s.getLow() - k_flightTimeEstimateRange_s.getHigh()) /
                      (upperPosBound - lowerPosBound))*(distance - lowerPosBound)
                      + k_flightTimeEstimateRange_s.getHigh();
        return new Time(k_flightTimeEstimateRange_s.coerceValue(seconds), TimeUnit.SECONDS);
    }

    public boolean isReadyToShoot() {
        return m_readyToShootDebounce.update(m_drive.isReadyToShoot() && m_shooter.readyToFire());
    }

    public boolean isTryingToRejectShooter() {
        return m_serializer.isTryingToRejectShooter();
    }

    public boolean isReadyToRejectShooter() {
        return m_shooter.isReadyToReject();
    }

    public boolean cargoPrimed() {
        return m_sensors.isTowerPrimedDirect();
    }

    public boolean cargoHalfPrimed() {
        return m_serializer.cargoCount() >= 1;
    }

    public boolean cargoFullyPrimed() {
        return m_serializer.cargoCount() >= 2;
    }

    public boolean rejectingOutIntake() {
        return m_serializer.isIntakeRejecting();
    }

    public boolean shotDetected() {
        return m_shotDetected;
    }

    public boolean isShooterHoodUp() {
        return m_shooter.isHoodUp();
    }

    public boolean isTryingToTrackCargo() {
        return m_isTryingTrackCargo;
    }

    public CargoTracker getCargoTracker() {
        return m_cargoTracker;
    }

    public void setAutonomousAutoRejectSetting(AutoRejectOption setting) {
        m_autonomousAutoRejectSetting = setting;
    }

    public AutoRejectOption getAutoRejectSetting() {
        if (!DriverStation.isAutonomousEnabled()){
            return m_autoRejectSetting.getSelected();
        } else {
            return m_autonomousAutoRejectSetting;
        }
    }

    public void setAutonomousCargoTrackSetting(CargoTrackOption setting) {
        m_autonomousCargoTrackSetting = setting;
    }

    public CargoTrackOption getCargoTrackSetting() {
        if (!DriverStation.isAutonomousEnabled()){
            return m_cargoTrackSetting.getSelected();
        } else {
            return m_autonomousCargoTrackSetting;
        }
    }

    /**
     * Actions to perform when robot is disabled.
     *
     * This may be used to clean resources that are constantly used while enabled,
     * such as odometry.
     */
    public void onDisable() {
    }

    public Command getResetImuCommand() {
        return new InstantCommand(() -> m_sensors.setHeading(new Angle()));
    }

    public Command getStopShootingCommand() {
        return m_shooter.getCeaseFireCommand().alongWith(m_serializer.getCeaseFireCommand());
    }

    public Command getRejectIntakeCommand() {
        return m_serializer.getRejectIntakeCommand();
    }

    public Command getNoRejectCommand() {
        return m_serializer.getNoRejectCommand();
    }

    public void enableAutoLimelight() {
        m_poseFusion.enableAutoLimelight();
    }

    public void disableAutoLimelight() {
        m_poseFusion.disableAutoLimelight();
    }

    public Command getTrackedCargoCommand(Supplier<Vector2d> translationalControl, Supplier<Double> rotationControl) {
        var trackingCargo = new InstantCommand(() -> m_isTryingTrackCargo = true);
        return m_drive.getTurnToHeadingWithManualBackupCommand(() -> {
            var translationScale = ((1.0 - m_cargoTrackerFeedback) * (1.0 - k_cargoTrackerTranslationalScale)) + k_cargoTrackerTranslationalScale;
            var control = translationalControl.get().scale(translationScale);
            var cargoVector = m_cargoTracker.getVectorToClosestCargo();
            if (cargoVector != null) {
                control = new Vector2d(control.getMagnitude(), cargoVector.getAngle());
            }
            return control;
        }, () -> {
            var cargoVector = m_cargoTracker.getVectorToClosestCargo();
            if (cargoVector != null) {
                return cargoVector.getAngle();
            } else {
                return new Angle(Double.NaN, AngleUnit.DEGREES);
            }
        }, () -> rotationControl.get() * k_cargoTrackerRotationalScale,
        () -> m_isTryingTrackCargo = false).alongWith(trackingCargo);
    }
}
