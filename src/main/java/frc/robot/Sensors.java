/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import java.util.ArrayList;
import java.util.List;

import com.ctre.phoenix.sensors.Pigeon2;

import frc.library.Length;
import frc.library.Length.LengthUnit;
import frc.library.drivers.PigeonUtils;
import frc.library.sensor.I2CMux;
import frc.library.sensor.I2CRoboRIO;
import frc.library.sensor.color.TCS34725;
import frc.library.sensor.color.TCS34725.HardwareChannel;
import frc.library.sensor.color.ColorSensor.ColorChannel;
import frc.library.sensor.color.ColorSensor.RGB;
import frc.library.sensor.color.TCS34725.Gain;
import frc.robot.constants.FieldDimensions;
import frc.robot.constants.LogEntry;
import frc.robot.constants.RobotMap;
import frc.robot.subsystems.Camera;
import frc.robot.subsystems.CargoTrackerCamera;
import frc.robot.subsystems.CargoTrackerCamera.TrackedCargo;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.math.Angle;
import org.growingstems.math.Range;
import org.growingstems.math.Time;
import org.growingstems.math.Vector2d;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.util.timer.TimerI;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DigitalOutput;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.PowerDistribution;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.PowerDistribution.ModuleType;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Sensors {
    private final TimerI m_periodicTimer = new WpiTimer();
    private final TimerI m_i2cPeriodicTimer = new WpiTimer();

    private final Pigeon2 m_imu = new Pigeon2(RobotMap.k_pigeonImuCanId, RobotMap.k_secondCAN);
    private Angle m_currentHeading = new Angle();
    private Angle m_currentPitch = new Angle();
    private Angle m_currentRoll = new Angle();

    private Vector2d m_robotPositionLimelight_in;
    private Length m_limelightRadialDistance;

    private WpiTimer m_angularVelocityTimer = new WpiTimer();
    private Angle m_prevHeading = new Angle();
    private double m_headingAngularVelocity_radps;
    private Angle m_prevPitch = new Angle();
    private double m_pitchAngularVelocity_radps;
    private Angle m_prevRoll = new Angle();
    private double m_rollAngularVelocity_radps;

    private RobotLogger m_logger;

    // I2C Disable
    private static final boolean k_disableI2c = false;

    // I2C Mux
    private final I2CMux m_i2cMux = new I2CMux(new I2CRoboRIO(RobotMap.k_i2cPort, RobotMap.k_muxAddress.address), new DigitalOutput(RobotMap.k_muxResetDio));
    private final static TimerI m_i2cResetWindowTimer = new WpiTimer();
    private final static Time k_i2cResetWindowTime = new Time(10.0, TimeUnit.SECONDS);
    private final static TimerI m_i2cResetCooldownTimer = new WpiTimer();
    private final static Time k_i2cResetCooldownTime = new Time(100, TimeUnit.MILLISECONDS);
    private final static int k_i2cResetAllowedAttempts = 10;
    private int m_i2cResetCurrentAttempt = 0;

    // Color Sensor
    private final TCS34725 m_serializerColorSensor;
    private final TCS34725 m_towerColorSensor;
    private Alliance m_serializerCargoColor = Alliance.Invalid;
    private Alliance m_towerCargoColor = Alliance.Invalid;
    public static final double k_ambientSaturation = 98.1;
    public static final double k_redSaturation = 190.7;
    public static final double k_blueSaturation = 172.9;
    public static final double k_ambientValue = 4.6;
    public static final double k_redValue = 144.0;
    public static final double k_blueValue = 127.5;
    public static final double k_redHue = 180.0;
    public static final double k_blueHue = 102.21;
    public static final double k_saturationThreshold = ((Math.min(k_redSaturation, k_blueSaturation) - k_ambientSaturation) * 0.15) + k_ambientSaturation;
    public static final double k_valueThreshold = ((Math.min(k_redValue, k_blueValue) - k_ambientValue) * 0.15) + k_ambientValue;
    public static final double k_hueTolerance = 15.0;
    public static final Range k_blueHueRange = new Range(k_blueHue - k_hueTolerance, k_blueHue + k_hueTolerance);
    public static final double k_redHueThresholdHigh = k_redHue - k_hueTolerance;
    public static final double k_redHueThresholdLow = k_redHue + k_hueTolerance - 180.0;

    // Prime Sensor
    private final DigitalInput m_towerPrimeSensor = new DigitalInput(RobotMap.k_towerPrimedDioChannel);
    private final DigitalInput m_serializerPrimeSensor = new DigitalInput(RobotMap.k_serializerPrimedDioChannel);
    private boolean m_towerPrimed = false;
    private boolean m_serializerPrimed = false;

    // Shot Sensor
    private final DigitalInput m_shotDetector = new DigitalInput(RobotMap.k_shotIrDioChannel);
    private boolean m_shotDetectedSensor = false;

    // Serializer LIDAR
    private final AnalogInput m_serializerLidar = new AnalogInput(RobotMap.k_serializerLidarAnalogChannel);
    private double m_serializerLidarRaw_V = 0.0;
    private static final double k_serializerLidarDetectedUpperThreshold_V = 0.8;
    private boolean m_serializerLidarDetected = false;

    // PDP
    private final PowerDistribution m_pdp = new PowerDistribution(RobotMap.k_pdpCanId, ModuleType.kCTRE);

    // Limelight
    private Camera m_limelightCamera;
    private boolean m_limelightValid = false;

    // Cargo Tracker
    private final CargoTrackerCamera m_cargoTrackerCamera;
    private List<TrackedCargo> m_cargoList;
    private boolean m_newCargoTrackerData = false;
    private final NetworkTableEntry m_reportedCargoIndex = SmartDashboard.getEntry("sensors/reportedCargoIndex");

    public Sensors(RobotLogger log, RobotIdentifier identifier) {
        m_logger = log;

        PigeonUtils.configAsDefaultPigeon(m_imu);

        // Cargo Tracker
        m_cargoTrackerCamera = new CargoTrackerCamera(log);
        m_cargoTrackerCamera.enable();
        m_reportedCargoIndex.setNumber(0);

        // Limelight
        m_limelightCamera = new Camera(m_logger, identifier);
        m_limelightCamera.enable();

        // I2C Color Sensors
        if (!k_disableI2c) {
            // I2C Timers
            m_i2cResetCooldownTimer.start();
            m_i2cResetWindowTimer.start();

            m_serializerColorSensor = new TCS34725(m_i2cMux.getMuxInterface(new I2CRoboRIO(RobotMap.k_i2cPort, TCS34725.k_deviceAddress), RobotMap.k_serializerColorSensorMuxChannel));
            m_towerColorSensor = new TCS34725(m_i2cMux.getMuxInterface(new I2CRoboRIO(RobotMap.k_i2cPort, TCS34725.k_deviceAddress), RobotMap.k_towerColorSensorMuxChannel));
            I2CRoboRIO.startI2C(RobotMap.k_i2cPort);
            configureColorSensors();
        } else {
            m_serializerColorSensor = null;
            m_towerColorSensor = null;
        }

        m_periodicTimer.start();
        m_i2cPeriodicTimer.start();
    }

    private void configureColorSensors() {
        if (!k_disableI2c) {
            m_serializerColorSensor.setGain(Gain.GAIN_16X);
            m_serializerColorSensor.setRgbcIntegration(new Time(20.0, TimeUnit.MILLISECONDS));
            m_serializerColorSensor.enable();
            m_towerColorSensor.setGain(Gain.GAIN_16X);
            m_towerColorSensor.setRgbcIntegration(new Time(20.0, TimeUnit.MILLISECONDS));
            m_towerColorSensor.enable();
        }
    }

    public void turnOffLimelight() {
        m_limelightCamera.disable();
    }

    public void update() {
        m_periodicTimer.reset();
        // Update IMU
        double[] ypr_deg = new double[3];
        m_imu.getYawPitchRoll(ypr_deg);
        m_currentHeading = new Angle(ypr_deg[0], AngleUnit.DEGREES);
        m_currentRoll = new Angle(ypr_deg[1], AngleUnit.DEGREES);
        m_currentPitch = new Angle(ypr_deg[2], AngleUnit.DEGREES);

        // Rotation Velocity
        Angle deltaHeading = m_currentHeading.subtract(m_prevHeading);
        Angle deltaPitch = m_currentPitch.subtract(m_prevPitch);
        Angle deltaRoll = m_currentRoll.subtract(m_prevRoll);
        m_prevHeading = m_currentHeading;
        m_prevPitch = m_currentPitch;
        m_prevRoll = m_currentRoll;
        if (m_angularVelocityTimer.isRunning()) {
            Time timeElapsed = m_angularVelocityTimer.get();
            m_angularVelocityTimer.reset();
            m_headingAngularVelocity_radps = deltaHeading.getValue(AngleUnit.RADIANS) / timeElapsed.getValue(TimeUnit.SECONDS);
            m_pitchAngularVelocity_radps = deltaPitch.getValue(AngleUnit.RADIANS) / timeElapsed.getValue(TimeUnit.SECONDS);
            m_rollAngularVelocity_radps = deltaRoll.getValue(AngleUnit.RADIANS) / timeElapsed.getValue(TimeUnit.SECONDS);
        } else {
            m_angularVelocityTimer.start();
            m_angularVelocityTimer.reset();
            m_headingAngularVelocity_radps = 0.0;
            m_pitchAngularVelocity_radps = 0.0;
            m_rollAngularVelocity_radps = 0.0;
        }

        // Shot Detector
        m_shotDetectedSensor = !m_shotDetector.get();

        // Tower Detector
        m_towerPrimed = !m_towerPrimeSensor.get();
        m_serializerPrimed = !m_serializerPrimeSensor.get();

        // Serializer LIDAR
        m_serializerLidarRaw_V = m_serializerLidar.getVoltage();
        m_serializerLidarDetected = m_serializerLidarRaw_V < k_serializerLidarDetectedUpperThreshold_V;

        m_i2cPeriodicTimer.reset();
        if (!k_disableI2c) {
            //I2C Device Verification and Reset
            var muxValid = m_i2cMux.verify();
            var serializerColorSensorValid = m_serializerColorSensor.verify();
            var towerColorSensorValid = m_towerColorSensor.verify();

            // Check if we have elapsed our allotted amount of time to reset our attempts counter
            if (m_i2cResetWindowTimer.hasElapsed(k_i2cResetWindowTime)) {
                DriverStation.reportWarning("Reset I2C reset attempts counter.", false);
                m_i2cResetWindowTimer.reset();
                m_i2cResetWindowTimer.stop();
                m_i2cResetCurrentAttempt = 0;
            }

            var i2cBusInvalid = !muxValid || !serializerColorSensorValid || !towerColorSensorValid;

            var i2cJustGotReset = false;
            if (i2cBusInvalid) {
                if (!m_i2cResetWindowTimer.isRunning()) {
                    m_i2cResetWindowTimer.reset();
                    m_i2cResetWindowTimer.start();
                }

                if (m_i2cResetCooldownTimer.hasElapsed(k_i2cResetCooldownTime) && m_i2cResetCurrentAttempt < k_i2cResetAllowedAttempts) {
                    DriverStation.reportWarning("Attempting to reset I2C: " + m_i2cResetCurrentAttempt, false);
                    I2CRoboRIO.resetI2C(RobotMap.k_i2cPort);
                    m_i2cResetCurrentAttempt++;
                    m_i2cResetCooldownTimer.reset();
                    i2cJustGotReset = true;
                    m_logger.setDataPoint(LogEntry.SENSORS_RESET_I2C, true);
                } else {
                    m_logger.setDataPoint(LogEntry.SENSORS_RESET_I2C, false);
                }
            } else {
                m_logger.setDataPoint(LogEntry.SENSORS_RESET_I2C, false);
            }

            // Check if we just got reset and if we need to reconfigure sensors
            if (i2cJustGotReset) {
                muxValid = m_i2cMux.verify();
                serializerColorSensorValid = m_serializerColorSensor.verify();
                towerColorSensorValid = m_towerColorSensor.verify();

                if (muxValid && serializerColorSensorValid && towerColorSensorValid) {
                    DriverStation.reportWarning("Recovered I2C devices after reset. Reconfiguring sensors now.", false);
                    configureColorSensors();
                    m_i2cResetWindowTimer.stop();
                    m_i2cResetCurrentAttempt = 0;
                }
            }

            m_logger.setDataPoint(LogEntry.SENSORS_I2C_MUX_VERIFY, muxValid);

            // Serializer Color Sensor
            var serializerRgb = m_serializerColorSensor.getRgb();
            var serializerHsv = serializerRgb.toHsv();
            m_serializerCargoColor = getColorSensorDetectedAlliance(serializerRgb, !i2cBusInvalid && !i2cJustGotReset);

            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_SENSOR_VALID, serializerColorSensorValid);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_RED, serializerRgb.red);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_GREEN, serializerRgb.green);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_BLUE, serializerRgb.blue);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_CLEAR, () -> m_serializerColorSensor.getColor(ColorChannel.CLEAR));
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_RAW_RED, () -> m_serializerColorSensor.readRawColor(HardwareChannel.RED));
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_RAW_GREEN, () -> m_serializerColorSensor.readRawColor(HardwareChannel.GREEN));
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_RAW_BLUE, () -> m_serializerColorSensor.readRawColor(HardwareChannel.BLUE));
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_RAW_CLEAR, () -> m_serializerColorSensor.readRawColor(HardwareChannel.CLEAR));
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_HUE, serializerHsv.hue);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_VALUE, serializerHsv.value);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_SATURATION, serializerHsv.saturation);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_DETECTED_CARGO_COLOR, m_serializerCargoColor);

            // Tower Color Sensor
            var towerRgb = m_towerColorSensor.getRgb();
            var towerHsv = towerRgb.toHsv();
            m_towerCargoColor = getColorSensorDetectedAlliance(towerRgb, !i2cBusInvalid && !i2cJustGotReset);

            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_SENSOR_VALID, towerColorSensorValid);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_RED, towerRgb.red);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_GREEN, towerRgb.green);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_BLUE, towerRgb.blue);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_CLEAR, () -> m_towerColorSensor.getColor(ColorChannel.CLEAR));
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_RAW_RED, () -> m_towerColorSensor.readRawColor(HardwareChannel.RED));
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_RAW_GREEN, () -> m_towerColorSensor.readRawColor(HardwareChannel.GREEN));
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_RAW_BLUE, () -> m_towerColorSensor.readRawColor(HardwareChannel.BLUE));
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_RAW_CLEAR, () -> m_towerColorSensor.readRawColor(HardwareChannel.CLEAR));
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_HUE, towerHsv.hue);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_VALUE, towerHsv.value);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_SATURATION, towerHsv.saturation);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_DETECTED_CARGO_COLOR, m_towerCargoColor);
        } else {
            m_serializerCargoColor = Alliance.Invalid;
            m_towerCargoColor = Alliance.Invalid;

            m_logger.setDataPoint(LogEntry.SENSORS_I2C_MUX_VERIFY, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_RESET_I2C, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_SENSOR_VALID, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_RED, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_GREEN, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_BLUE, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_CLEAR, () -> Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_RAW_RED, () -> Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_RAW_GREEN, () -> Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_RAW_BLUE, () -> Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_COLOR_RAW_CLEAR, () -> Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_HUE, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_VALUE, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_SATURATION, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_DETECTED_CARGO_COLOR, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_SENSOR_VALID, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_RED, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_GREEN, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_BLUE, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_CLEAR, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_RAW_RED, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_RAW_GREEN, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_RAW_BLUE, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_COLOR_RAW_CLEAR, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_HUE, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_VALUE, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_SATURATION, Double.NaN);
            m_logger.setDataPoint(LogEntry.SENSORS_TOWER_DETECTED_CARGO_COLOR, Double.NaN);
        }
        m_logger.setDataPoint(LogEntry.TIME_I2C, m_i2cPeriodicTimer.get().getValue(TimeUnit.MILLISECONDS));

        // Cargo Tracker
        m_cargoList = m_cargoTrackerCamera.tryGet();
        m_newCargoTrackerData = (m_cargoList != null);
        if (!m_newCargoTrackerData) {
            m_cargoList = new ArrayList<TrackedCargo>();
        }

        // Limelight
        m_limelightCamera.update();
        m_limelightValid = m_limelightCamera.isValid();
        if (m_limelightValid) {
            Vector2d cameraCentricVector = m_limelightCamera.getCameraToTargetVector_in().getXY();
            Vector2d robotCentricVector = m_limelightCamera.getPosition().getXY().add(cameraCentricVector.rotate(m_limelightCamera.getYaw()));
            Vector2d fieldCentricVector = robotCentricVector.rotate(m_currentHeading);

            m_robotPositionLimelight_in = FieldDimensions.k_hubPos.getVector(LengthUnit.INCHES).subtract(fieldCentricVector);
            m_limelightRadialDistance = m_limelightCamera.getRadialDistance();

            m_logger.setDataPoint(LogEntry.SENSORS_LL_ROBOT_POS_X, m_robotPositionLimelight_in.getX());
            m_logger.setDataPoint(LogEntry.SENSORS_LL_ROBOT_POS_Y, m_robotPositionLimelight_in.getY());
        } else {
            m_robotPositionLimelight_in = null;
            m_limelightRadialDistance = null;

            m_logger.setDataPoint(LogEntry.SENSORS_LL_ROBOT_POS_X, "NaN");
            m_logger.setDataPoint(LogEntry.SENSORS_LL_ROBOT_POS_Y, "NaN");
        }

        // Logging
        m_logger.setDataPoint(LogEntry.SENSORS_PITCH, m_currentPitch.getValue(AngleUnit.DEGREES));
        m_logger.setDataPoint(LogEntry.SENSORS_ROLL, m_currentRoll.getValue(AngleUnit.DEGREES));
        m_logger.setDataPoint(LogEntry.SENSORS_HEADING, m_currentHeading.getValue(AngleUnit.DEGREES));

        m_logger.setDataPoint(LogEntry.SENSORS_SERIALZIER_PRIMED, m_serializerPrimed);
        m_logger.setDataPoint(LogEntry.SENSORS_TOWER_PRIME, m_towerPrimed);
        m_logger.setDataPoint(LogEntry.SENSORS_SHOT_DETECTED_SENSOR, m_shotDetectedSensor);

        m_logger.setDataPoint(LogEntry.SENSORS_HEADING_ANGULAR_VELOCITY, m_headingAngularVelocity_radps);
        m_logger.setDataPoint(LogEntry.SENSORS_PITCH_ANGULAR_VELOCITY, m_pitchAngularVelocity_radps);
        m_logger.setDataPoint(LogEntry.SENSORS_ROLL_ANGULAR_VELOCITY, m_rollAngularVelocity_radps);

        m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_LIDAR_RAW, m_serializerLidarRaw_V);
        m_logger.setDataPoint(LogEntry.SENSORS_SERIALIZER_LIDAR_DETECT, m_serializerLidarDetected);

        m_logger.setDataPoint(LogEntry.SENSORS_CARGO_NEW_DATA, m_newCargoTrackerData);
        m_logger.setDataPoint(LogEntry.SENSORS_CARGO_COUNT, m_cargoList.size());
        int cargoIndex = Math.max(Math.min(m_reportedCargoIndex.getNumber(0).intValue(),  m_cargoList.size() - 1), 0);
        m_logger.setDataPoint(LogEntry.SENSORS_CARGO_INDEX, cargoIndex);
        if (!m_cargoList.isEmpty()) {
            var entry = m_cargoList.get(cargoIndex);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_X, entry.x);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_Y, entry.y);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_WIDTH, entry.width);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_HEIGHT, entry.height);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_ASPECT, entry.aspectRatio);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_FILL, entry.fillRatio);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_SCREEN, entry.screenRatio);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_SMARTSCREEN, entry.smartScreenRatio);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_IS_RED, entry.isRed);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_RELATIVE_X, entry.getRelativePosition().getX());
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_RELATIVE_Y, entry.getRelativePosition().getY());
        } else {
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_X, 0.0);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_Y, 0.0);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_WIDTH, 0.0);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_HEIGHT, 0.0);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_ASPECT, 0.0);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_FILL, 0.0);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_SCREEN, 0.0);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_SMARTSCREEN, 0.0);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_IS_RED, 0.0);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_RELATIVE_X, 0.0);
            m_logger.setDataPoint(LogEntry.SENSORS_CARGO_RELATIVE_Y, 0.0);
        }

        m_logger.setDataPoint(LogEntry.TIME_SENSORS, m_periodicTimer.get().getValue(TimeUnit.MILLISECONDS));
    }

    private Alliance getColorSensorDetectedAlliance(RGB colorSensorRgb, boolean sensorValid) {
        if (!sensorValid || (Math.abs(colorSensorRgb.red) < 1.0e-6 && Math.abs(colorSensorRgb.green) < 1.0e-6 && Math.abs(colorSensorRgb.blue) < 1.0e-6)) {
            return Alliance.Invalid;
        }

        var hsv = colorSensorRgb.toHsv();
        if (hsv.value > k_valueThreshold) {
            if (k_blueHueRange.inRange(hsv.hue)) {
                return Alliance.Blue;
            } else if (hsv.hue > k_redHueThresholdHigh || hsv.hue < k_redHueThresholdLow) {
                return Alliance.Red;
            }
        }

        return Alliance.Invalid;
    }

    public void setHeading(Angle heading) {
        m_imu.setYaw(heading.getValue(AngleUnit.DEGREES));
        m_currentHeading = heading;
    }

    public Angle getImuReading() {
        return m_currentHeading;
    }

    public Angle getImuPitch() {
        return m_currentPitch;
    }

    public Angle getImuRoll() {
        return m_currentRoll;
    }

    public double getHeadingAngularVelocity_radps() {
        return m_headingAngularVelocity_radps;
    }

    public double getPitchAngularVelocity_radps() {
        return m_pitchAngularVelocity_radps;
    }

    public double getRollAngularVelocity_radps() {
        return m_rollAngularVelocity_radps;
    }

    /**
     * Returns if the tower prime sensor detects a cargo.
     * @return true if the tower prime sensor detects a cargo.
     */
    public boolean isTowerPrimed() {
        return m_towerPrimed;
    }

    public boolean isTowerPrimedDirect() {
        return !m_towerPrimeSensor.get();
    }

    /**
     * Returns if the serializer prime sensor detects a cargo.
     * @return true if the serializer prime sensor detects a cargo.
     */
    public boolean isSerializerPrimed() {
        return m_serializerPrimed;
    }

    public boolean isSerializerPrimedDirect() {
        return !m_serializerPrimeSensor.get();
    }

    public boolean getShotDetectedSensor() {
        return m_shotDetectedSensor;
    }

    public Alliance getSerializerCargoColor() {
        return m_serializerCargoColor;
    }

    public Alliance getTowerCargoColor() {
        return m_towerCargoColor;
    }

    public Length getClimbExtension() {
        return new Length();
    }

    public double getCurrent_A(int pdpChannel) {
        return m_pdp.getCurrent(pdpChannel);
    }

    public Vector2d getRobotPositionLimelight_in() {
        return m_robotPositionLimelight_in;
    }

    public Length getLimelightRadialDistance() {
        return m_limelightRadialDistance;
    }

    public double getSerializerLidarRaw_V() {
        return m_serializerLidarRaw_V;
    }

    public boolean getSerializerLidarDetected() {
        return m_serializerLidarDetected;
    }

    public boolean limelightHasTarget() {
        return m_limelightValid;
    }

    /**
     * Returns most current version of tracked cargo from cargo tracker.
     * @return list of currently tracked cargo.
     */
    public List<TrackedCargo> getTrackedCargoList() {
        return m_cargoList;
    }

    public boolean isCargoTrackerDataNew() {
        return m_newCargoTrackerData;
    }
}
