/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import edu.wpi.first.wpilibj.DriverStation;

import frc.library.CircularBuffer;
import frc.library.Length.LengthUnit;
import frc.robot.constants.FieldDimensions;
import frc.robot.constants.LogEntry;
import frc.robot.subsystems.Drive;

import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

import org.growingstems.math.Angle;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.math.Pose2d;
import org.growingstems.math.Vector2d;

public class PoseFusion {
    private static final int k_offsetDequeSize = 30;

    private int m_skippedOffsetCount = 0;
    private boolean m_limelightEnabledDuringAuto = false;

    private RobotLogger m_logger;
    private Drive m_drive;
    private Sensors m_sensors;

    private Pose2d m_odometryRobotPose_in = new Pose2d();
    private Pose2d m_adjustedRobotPose_in = new Pose2d();
    private Vector2d m_vectorToHubCenter_in = new Vector2d();
    // X and Y in inches/second. Angle is also per-second
    private Pose2d m_fieldVelocityPose_inps = new Pose2d();

    private final OffsetHistory m_offsetHistory = new OffsetHistory();

    private static class OffsetHistory {
        private final CircularBuffer<Vector2d> m_offsets = new CircularBuffer<>(k_offsetDequeSize);
        private final static StandardDeviation m_stdDevCalculator = new StandardDeviation();
        private Vector2d m_average = null;
        private Double m_stdDev = null;

        private void add(Vector2d offset) {
            m_offsets.add(offset);
            m_average = null;
            m_stdDev = null;
        }

        private boolean isFull() {
            return m_offsets.size() == m_offsets.getMaxSize();
        }

        private void clear() {
            m_offsets.clear();
            m_average = new Vector2d();
        }

        private Vector2d averageOffset() {
            if (m_average != null) {
                return m_average;
            }

            m_average = new Vector2d();
            if (m_offsets.size() == 0) {
                return m_average;
            }

            for (Vector2d vec : m_offsets) {
                m_average = m_average.add(vec);
            }

            return m_average = m_average.scale(1.0 / m_offsets.size());
        }

        private double standardDeviation() {
            if (m_stdDev != null) {
                return m_stdDev;
            }

            Vector2d stdDev = new Vector2d();
            double[] values = new double[m_offsets.size()];
            int i = 0;
            for (Vector2d offset : m_offsets) {
                values[i] = offset.getX();
                i++;
            }
            double xDeviation = m_stdDevCalculator.evaluate(values);

            i = 0;
            for (Vector2d offset : m_offsets) {
                values[i] = offset.getY();
                i++;
            }
            double yDeviation = m_stdDevCalculator.evaluate(values);

            stdDev = new Vector2d(xDeviation, yDeviation);
            m_stdDev = stdDev.getMagnitude();
            return m_stdDev;
        }
    }

    public PoseFusion(RobotLogger logger, Drive drive, Sensors sensors) {
        m_logger = logger;
        m_drive = drive;
        m_sensors = sensors;
    }

    public void setRobotPose_in(Pose2d pose_in) {
        m_odometryRobotPose_in = pose_in;
        m_sensors.setHeading(pose_in.getRotation());
        m_offsetHistory.clear();
    }

    /**
     * Returns the lastest field-oriented pose of the robot.
     *
     * @return The latest field-oriented pose of the robot.
     */
    public Pose2d getRobotPose_in() {
        return shouldUseFusedPose() ? m_adjustedRobotPose_in : m_odometryRobotPose_in;
    }

    private boolean shouldUseFusedPose() {
        return m_limelightEnabledDuringAuto || !DriverStation.isAutonomous();
    }

    public Vector2d getVectorToHubCenter_in() {
        return m_vectorToHubCenter_in;
    }

    public Pose2d getOdometryRobotPose_in() {
        return m_odometryRobotPose_in;
    }

    public Pose2d getFieldVelocity_in() {
        return m_fieldVelocityPose_inps;
    }

    public void updateRobotPose() {
        calculateOdometry();
        calculateOffset();
        adjustRobotPose();
        calculateHubCenterVector();
    }

    private void calculateOffset() {
        var limelightPosition = m_sensors.getRobotPositionLimelight_in();
        if (limelightPosition != null) {
            Vector2d offset = limelightPosition.subtract(m_odometryRobotPose_in.getVector());
            double mag = offset.subtract(m_offsetHistory.averageOffset()).getMagnitude();

            m_logger.setDataPoint(LogEntry.POSE_FUSION_LL_OFFSET_ERROR, mag);

            // TODO Determine how offset is overwritted after long periods of no limelight
            var skipOffset = false;
            if (!skipOffset) {
                m_offsetHistory.add(offset);
                m_logger.setDataPoint(LogEntry.POSE_FUSION_LL_STD, m_offsetHistory.standardDeviation());
            } else {
                m_skippedOffsetCount++;
                m_logger.setDataPoint(LogEntry.POSE_FUSION_SKIPPED_OFFSET, m_skippedOffsetCount);
            }
        }
    }

    private void calculateOdometry() {
        var heading = m_sensors.getImuReading();
        var kinematicsVec = m_drive.getForwardKinematics();

        var planeDelta = kinematicsVec.rotate(heading);
        m_odometryRobotPose_in = new Pose2d(m_odometryRobotPose_in.getVector().add(planeDelta), heading);

        var velocityVec = m_drive.getVelocityVector_inps().rotate(heading);
        var velocityAng = new Angle(m_sensors.getHeadingAngularVelocity_radps(), AngleUnit.RADIANS);
        m_fieldVelocityPose_inps = new Pose2d(velocityVec, velocityAng);

        m_logger.setDataPoint(LogEntry.POSE_FUSION_ODOM_POSE_X, m_odometryRobotPose_in.getX());
        m_logger.setDataPoint(LogEntry.POSE_FUSION_ODOM_POSE_Y, m_odometryRobotPose_in.getY());
        m_logger.setDataPoint(LogEntry.POSE_FUSION_FIELD_VEL_X, m_fieldVelocityPose_inps.getX());
        m_logger.setDataPoint(LogEntry.POSE_FUSION_FIELD_VEL_Y, m_fieldVelocityPose_inps.getY());
        m_logger.setDataPoint(LogEntry.POSE_FUSION_FIELD_VEL_THETA,
                              m_fieldVelocityPose_inps.getRotation().getValue(AngleUnit.DEGREES));
    }

    private void calculateHubCenterVector() {
        // Using fused pose with limelight
        m_vectorToHubCenter_in =
            FieldDimensions.k_hubPos.getVector(LengthUnit.INCHES).subtract(getRobotPose_in().getVector());

        m_logger.setDataPoint(LogEntry.POSE_FUSION_TO_HUB_ANG, m_vectorToHubCenter_in.getAngle());
        m_logger.setDataPoint(LogEntry.POSE_FUSION_TO_HUB_DIST, m_vectorToHubCenter_in.getMagnitude());
    }

    public void adjustRobotPose() {
        m_adjustedRobotPose_in = m_odometryRobotPose_in.transform(m_offsetHistory.averageOffset(), new Angle());

        m_logger.setDataPoint(LogEntry.POSE_FUSION_ROBOT_POSE_X, m_adjustedRobotPose_in.getX());
        m_logger.setDataPoint(LogEntry.POSE_FUSION_ROBOT_POSE_Y, m_adjustedRobotPose_in.getY());
    }

    public void enableAutoLimelight() {
        m_limelightEnabledDuringAuto = true;
    }

    public void disableAutoLimelight() {
        m_limelightEnabledDuringAuto = false;
    }
}
