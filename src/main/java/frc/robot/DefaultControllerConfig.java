/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.library.RateLimiter;

import org.growingstems.control.SymmetricDeadzone;
import org.growingstems.math.Vector2d;
import org.growingstems.signals.SchmittTrigger;
import org.growingstems.signals.api.SignalModifier;

public class DefaultControllerConfig extends ControllerConfig {
    private static final double k_defaultShootSlowdownFactor = 0.5;

    private static final double k_schmittLow = 0.4;
    private static final double k_schmittHigh = 0.6;

    private final NetworkTableEntry k_shootSlowdownFactorEntry = SmartDashboard.getEntry("Shoot Slowdown Factor");

    private SignalModifier<Double, Double> translationDeadzoneModifier =
        new SymmetricDeadzone(1.0, 0.1)
            .append(in -> Math.pow(in, 3.0))
            .append(in -> in * (fullPowerTrigger.get() ? k_fullSpeed : driveThrottle.getDouble(k_defaultMaxDriveSpeed)))
            .append(in -> in * (shootAutoTrigger.get() ? getShootSlowdownFactor() : 1.0));
    private SignalModifier<Double, Double> rotationDeadzoneModifier =
        new SymmetricDeadzone(1.0, 0.1).append(in -> turnThrottle.getDouble(k_defaultMaxTurnSpeed) * Math.pow(in, 3.0));

    private SignalModifier<Double, Double> inputRampX = translationDeadzoneModifier.append(new RateLimiter());
    private SignalModifier<Double, Double> inputRampY = translationDeadzoneModifier.append(new RateLimiter());

    private SignalModifier<Vector2d, Vector2d> inputVectorModifier = in -> {
        // Input space is a square, want it to be a circle
        // Scale input vector up to square perimiter
        double largest = Math.max(Math.abs(in.getX()), Math.abs(in.getY()));
        if (largest == 0.0) {
            return new Vector2d();
        }
        // Get the magnitude of the scaled perimiter vector, then scale by 1/mag
        return in.scale(1.0 / in.scale(1.0 / largest).getMagnitude());
    };

    public DefaultControllerConfig(XboxController driver) {
        // Drive
        defaultVelocitySupplier =
            inputVectorModifier.provide(() -> new Vector2d(inputRampY.update(-driver.getLeftY()), inputRampX.update(-driver.getLeftX())));
        fullPowerTrigger = new Trigger(driver::getXButton);

        // clang-format off
        defaultRotationSupplier = rotationDeadzoneModifier.provide(() -> -driver.getRightX());
        // clang-format on

        // IMU
        resetImu = new Trigger(driver::getYButton);

        // Intake
        intakeTrigger = new Trigger(new SchmittTrigger(k_schmittLow, k_schmittHigh).provide(driver::getRightTriggerAxis)::get);
        outtakeTrigger = new Trigger(() -> false);
        // outtakeTrigger = new Trigger(new SchmittTrigger(k_schmittLow, k_schmittHigh).provide(driver::getLeftTriggerAxis)::get);
        retractIntakeTrigger = new Trigger(driver::getRightBumper);

        // Serializer
        rejectIntakeTrigger = new Trigger(() -> driver.getPOV() == dPadDown);
        rejectShooterTrigger = new Trigger(() -> driver.getPOV() == dPadUp);

        // Shooter
        shootAutoTrigger = new Trigger(driver::getAButton);
        shootFenderTrigger = new Trigger(driver::getBButton);
        shootDashboardTrigger = new Trigger(() -> driver.getPOV() == dPadLeft);

        // Climb
        climbEnable = new Trigger(driver::getLeftBumper);
        climbAdvance = driver::getStartButton;
        climbRegress = driver::getBackButton;

        // CargoTracker
        trackCargo = new Trigger(new SchmittTrigger(k_schmittLow, k_schmittHigh).provide(driver::getLeftTriggerAxis)::get);

        // Combine Controls
        intakeTrigger = intakeTrigger.or(trackCargo);

        k_shootSlowdownFactorEntry.setNumber(k_defaultShootSlowdownFactor);
    }

    private double getShootSlowdownFactor() {
        return k_shootSlowdownFactorEntry.getDouble(k_defaultShootSlowdownFactor);
    }
}
