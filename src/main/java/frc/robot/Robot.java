// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.DriverStation.MatchType;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import frc.robot.constants.LogEntry;
import frc.robot.constants.ShotCriteria;

import java.io.File;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Supplier;

import org.growingstems.frc.util.WpiTimer;
import org.growingstems.logic.LogicCore.Edge;
import org.growingstems.math.Time;
import org.growingstems.math.Time.TimeUnit;
import org.growingstems.signals.EdgeDetector;
import org.growingstems.util.timer.TimerI;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the
 * name of this class or
 * the package after creating this project, you must also update the
 * build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
    private static final Time k_logPeriod = new Time(1.0, TimeUnit.SECONDS);
    private static final String k_defaultLogName = "RobotLog";
    private static final int k_maxRetries = 5;

    private final NetworkTableEntry m_logEnabledEntry = SmartDashboard.getEntry("Log");
    private final NetworkTableEntry m_logFileNameEntry = SmartDashboard.getEntry("Log File Name");

    private RobotContainer m_robotContainer;

    private Command m_autonomousCommand;

    private final EdgeDetector m_logStateChanged = new EdgeDetector(Edge.ANY);

    private final TimerI m_robotPeriodicTimer = new WpiTimer();

    public static boolean isCompetition() {
        return false;
    }

    /**
     * This function is run when the robot is first started up and should be used
     * for any initialization code.
     */
    @Override
    public void robotInit() {
        LiveWindow.disableAllTelemetry();

        // Instantiate our RobotContainer. This will perform all our button bindings,
        // and put our autonomous chooser on the dashboard.

        m_robotContainer = new RobotContainer();
        m_logFileNameEntry.setString("Logging Not Started");
        if (isCompetition()) {
            m_logEnabledEntry.setBoolean(true);
        } else {
            m_logEnabledEntry.setDefaultBoolean(true);
        }
        Supplier<String> logNameSupplier;
        if (isCompetition()) {
            logNameSupplier = () -> {
                if (DriverStation.isFMSAttached()) {
                    var eventName = DriverStation.getEventName();
                    var matchType = DriverStation.getMatchType();
                    var matchNumber = DriverStation.getMatchNumber();
                    var replayNumber = DriverStation.getReplayNumber();
                    if (!DriverStation.isEnabled() && (eventName == "" || matchType == MatchType.None || matchNumber == 0 || replayNumber == 0)) {
                        return null;
                    }

                    return eventName + "_" + matchType.toString() + "_" + matchNumber + "_" + replayNumber;
                } else if (DriverStation.isDSAttached()) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
                    return dateFormat.format(new Date());
                } else if (DriverStation.isEnabled()) {
                    return k_defaultLogName;
                }
                return null;
            };
        } else {
            logNameSupplier = () -> k_defaultLogName;
        }
        new CommandBase() {
            private String m_logName = null;

            @Override
            public void execute() {
                if (m_logEnabledEntry.getBoolean(true)) {
                    m_logName = logNameSupplier.get();
                }
            }

            @Override
            public boolean isFinished() {
                return m_logName != null;
            }

            private File genLog(int attempts) {
                String logName = m_logName;
                if (attempts == k_maxRetries) {
                    logName += "(MAX)";
                } else if (attempts > 1) {
                    logName += "(" + attempts + ")";
                }
                return Paths.get(Filesystem.getOperatingDirectory().getPath(), "logs", logName + ".csv").toFile();
            }

            @Override
            public void end(boolean interrupted) {
                if (interrupted) {
                    m_logFileNameEntry.setString("Log creation cancelled");
                    return;
                }

                File logFile = genLog(1);
                if (!m_logName.equals(k_defaultLogName)) {
                    for (int i = 2; logFile.exists() && i <= k_maxRetries; i++) {
                        logFile = genLog(i);
                    }
                }
                var logger = m_robotContainer.getLogger();
                logger.init(logFile);
                logger.startLogging(k_logPeriod);

                if (logger.isLogging()) {
                    m_logFileNameEntry.setString(logFile.toString());
                } else {
                    m_logFileNameEntry.setString("Failed to create " + logFile.toString());
                }
            }

            @Override
            public boolean runsWhenDisabled() {
                return true;
            }
        }.schedule();
        m_robotPeriodicTimer.start();
    }

    /**
     * This function is called every robot packet, no matter the mode. Use this for
     * items like
     * diagnostics that you want ran during disabled, autonomous, teleoperated and
     * test.
     *
     * <p>
     * This runs after the mode specific periodic functions, but before LiveWindow
     * and
     * SmartDashboard integrated updating.
     */
    @Override
    public void robotPeriodic() {
        var logger = m_robotContainer.getLogger();
        // Record time since last reset and reset timer for robotPeriodic measuring
        logger.setDataPoint(LogEntry.TIME_TOTAL, m_robotPeriodicTimer.get().getValue(TimeUnit.MILLISECONDS));
        m_robotPeriodicTimer.reset();

        // Uncomment to tune the shot criteria using SmartDashboard
        ShotCriteria.updateDashboard();

        // log the driver and operator controlls that will be used for the fram
        // IDK, maybe these should go somewhere else, but they're here for now.
        m_robotContainer.logControls();

        /**
         * Runs RobotState. This is responsible for polling the robot's sensors and then
         * calculates the robot's state based on this sensor data. This is ran first so
         * that all the subsystems can get access to the most up to date state of the
         * robot.
         */
        m_robotContainer.getRobotState().getSensors().update();
        m_robotContainer.getRobotState().updateState();

        // Runs the Scheduler. This is responsible for polling buttons, adding
        // newly-scheduled
        // commands, running already-scheduled commands, removing finished or
        // interrupted commands,
        // and running subsystem periodic() methods. This must be called from the
        // robot's periodic
        // block in order for anything in the Command-based framework to work.
        CommandScheduler.getInstance().run();

        boolean shouldLog = m_logEnabledEntry.getBoolean(true);
        if (m_logStateChanged.update(shouldLog)) {
            if (shouldLog) {
                logger.startLogging(k_logPeriod);
            } else {
                logger.stopLogging();
            }
        }
        if (shouldLog) {
            var timeBefore = m_robotPeriodicTimer.get();
            logger.grabLine();
            logger.setDataPoint(LogEntry.TIME_LOG_GRABLINE, m_robotPeriodicTimer.get().subtract(timeBefore).getValue(TimeUnit.MILLISECONDS));
        }
        logger.setDataPoint(LogEntry.TIME_ROBOT_PERIOD, m_robotPeriodicTimer.get().getValue(TimeUnit.MILLISECONDS));
    }
    /** This function is called once each time the robot enters Disabled mode. */
    @Override
    public void disabledInit() {
        if (m_robotContainer != null) {
            m_robotContainer.onDisable();
        }
    }

    @Override
    public void disabledPeriodic() {
    }

    /**
     * This autonomous runs the autonomous command selected by your
     * {@link RobotContainer} class.
     */
    @Override
    public void autonomousInit() {
        m_autonomousCommand = m_robotContainer.getAutonomousCommand();

        // schedule the autonomous command (example)
        if (m_autonomousCommand != null) {
            m_autonomousCommand.schedule();
        }
    }

    /** This function is called periodically during autonomous. */
    @Override
    public void autonomousPeriodic() {
    }

    @Override
    public void teleopInit() {
        // This makes sure that the autonomous stops running when
        // teleop starts running. If you want the autonomous to
        // continue until interrupted by another command, remove
        // this line or comment it out.
        if (m_autonomousCommand != null) {
            m_autonomousCommand.cancel();
        }

        var teleopInitCommand = m_robotContainer.getTelopInitCommand();

        if (teleopInitCommand != null) {
            teleopInitCommand.schedule();
        }
    }

    /** This function is called periodically during operator control. */
    @Override
    public void teleopPeriodic() {
    }

    @Override
    public void testInit() {
        // Cancels all running commands at the start of test mode.
        CommandScheduler.getInstance().cancelAll();
    }

    /** This function is called periodically during test mode. */
    @Override
    public void testPeriodic() {
    }
}
