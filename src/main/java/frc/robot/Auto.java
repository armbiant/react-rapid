// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.util.function.Function;

import org.growingstems.math.Angle;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.math.Time.TimeUnit;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.RobotContainer.AutoBuilder;

import org.growingstems.math.Pose2d;
import org.growingstems.math.Time;
import org.growingstems.math.Vector2d;

public class Auto {
    /**
     * Offset distance for far cargo. Positive value means the robot drives further to its right (Human Player's left)
     */
    private static final double k_farCargoDistToRight_in = 6.0;
    private static final Vector2d k_farCargoPos = new Vector2d(-274.0, -108.0);
    private static final Vector2d k_farCargoAdj = new Vector2d(k_farCargoDistToRight_in, new Angle(135.0, AngleUnit.DEGREES));
    private static final Pose2d k_farCargoPose = new Pose2d(k_farCargoPos.add(k_farCargoAdj), new Angle(-135.0, AngleUnit.DEGREES));

    /**
     * 34 inches from Hub Center to Fender Edge
     * 14 inches from Robot Center to Frame Edge
     * 3.5 inch thick bumper
     */
    private static final double k_fenderDistance_in = 34.0 + 14.0 + 3.5;

    private static final NetworkTableEntry k_testAutoX = SmartDashboard.getEntry("Test Auto/X (In)");
    private static final NetworkTableEntry k_testAutoY = SmartDashboard.getEntry("Test Auto/Y (In)");
    private static final NetworkTableEntry k_testAutoTheta = SmartDashboard.getEntry("Test Auto/Theta (Deg)");
    private static final NetworkTableEntry k_autoModeIndicator = SmartDashboard.getEntry("Auto/Auto Indicator");
    private static final Time k_timWaitTime = new Time(3.0, TimeUnit.SECONDS);

    private static final Pose2d k_leftStartPose = new Pose2d(-63.0, 68.0, new Angle(136.5, AngleUnit.DEGREES));
    private static final Pose2d k_midLeftStartPose = new Pose2d(-92.0, -8.0, new Angle(-178.5, AngleUnit.DEGREES));
    private static final Pose2d k_midRightStartPose = new Pose2d(-67.67, -63.13, new Angle(-133.5, AngleUnit.DEGREES));
    private static final Pose2d k_rightStartPose = new Pose2d(8.0, -92.0, new Angle(-88.5, AngleUnit.DEGREES));

    static {
        k_testAutoX.setDefaultNumber(120.0);
        k_testAutoY.setDefaultNumber(0.0);
        k_testAutoTheta.setDefaultNumber(0.0);
    }

    public static enum AutoMode {
        TEST_AUTO,
        FENDER_LEFT,
        FENDER_RIGHT,
        TWO_LEFT,
        TWO_MIDLEFT_CARGOLEFT,
        TWO_MIDLEFT_CARGORIGHT,
        TWO_MIDRIGHT,
        TWO_RIGHT,
        ONE_LEFT,
        ONE_MIDLEFT,
        ONE_MIDRIGHT,
        ONE_RIGHT,
        THREE_RIGHT,
        FOUR_MIDRIGHT,
        FOUR_MIDLEFT_CARGORIGHT,
        FOUR_MIDLEFT_CARGOLEFT,
        FOUR_LEFT,
        FIVE_RIGHT,
        SIX_LEFT,
        TIMS_AUTO_LEFT,
        TIMS_AUTO_RIGHT,
        NO_AUTO
    }

    private final SendableChooser<AutoMode> m_autoModes;

    public Auto() {
        m_autoModes = new SendableChooser<>();
        for (var pos : AutoMode.values()) {
            if (Robot.isCompetition()) {
                if (pos == AutoMode.TEST_AUTO || pos == AutoMode.TIMS_AUTO_RIGHT) {
                    continue;
                }
            }
            m_autoModes.addOption(pos.toString(), pos);
        }
        m_autoModes.setDefaultOption(AutoMode.NO_AUTO.toString(), AutoMode.NO_AUTO);
        SmartDashboard.putData("robotState/robotstartingconfig", m_autoModes);
        var autoIndicatorCommand = new RunCommand(() -> k_autoModeIndicator.setString(m_autoModes.getSelected().toString())) {
            @Override
            public boolean runsWhenDisabled() {
                return true;
            }
        };
        autoIndicatorCommand.schedule();
    }

    public Command getMode(Function<Pose2d, AutoBuilder> constructorCallback) {
        return getMode(m_autoModes.getSelected(), constructorCallback);
    }

    public static Command buildOneCargoAuto(AutoBuilder builder) {
        return builder.getStartCommand()
            .andThen(builder.intake())
            .andThen(builder.retract())
            .andThen(builder.shootAll(true))
            .andThen(builder.stopEverything());
    }

    public static Command buildFenderAuto(AutoBuilder builder) {
        return builder.getStartCommand()
            .andThen(builder.prepFender())
            .andThen(new WaitCommand(3.0))
            .andThen(builder.shootFender())
            .andThen(new WaitCommand(5.0))
            .andThen(builder.stopEverything());
    }

    private static Command getMode(AutoMode autoMode, Function<Pose2d, AutoBuilder> getBuilder) {
        switch(autoMode){
            case TEST_AUTO:
            {
                var builder = getBuilder.apply(new Pose2d());
                return builder.getStartCommand()
                    .andThen(builder.goTo(new Pose2d(k_testAutoX.getDouble(0.0),
                                                     k_testAutoY.getDouble(0.0),
                                                     new Angle(k_testAutoTheta.getDouble(0.0), AngleUnit.DEGREES))));

            }
            case NO_AUTO:
            {
                return new InstantCommand();
            }
            case FENDER_LEFT:
            {
                var startHeading = new Angle(159.0, AngleUnit.DEGREES);
                var startPose = new Pose2d(new Vector2d(k_fenderDistance_in, startHeading), startHeading);
                return buildFenderAuto(getBuilder.apply(startPose));
            }
            case FENDER_RIGHT:
            {
                var startHeading = new Angle(-111.0, AngleUnit.DEGREES);
                var startPose = new Pose2d(new Vector2d(k_fenderDistance_in, startHeading), startHeading);
                return buildFenderAuto(getBuilder.apply(startPose));
            }
            case ONE_LEFT:
            {
                return buildOneCargoAuto(getBuilder.apply(k_leftStartPose));
            }
            case ONE_MIDLEFT:
            {
                return buildOneCargoAuto(getBuilder.apply(k_midLeftStartPose));
            }
            case ONE_MIDRIGHT:
            {
                return buildOneCargoAuto(getBuilder.apply(k_midRightStartPose));
            }
            case ONE_RIGHT:
            {
                return buildOneCargoAuto(getBuilder.apply(k_rightStartPose));
            }
            case TWO_LEFT:
            {
                var startPose = new Pose2d(-63.0, 68.0, new Angle(136.5, AngleUnit.DEGREES));
                var firstShotVector = new Vector2d(-122.0, 72.0);
                var firstShotPose = new Pose2d(firstShotVector, firstShotVector.getAngle());
                var rightOutOfTarmacVector = new Vector2d(142.0, firstShotVector.getAngle());
                var rightOutOfTarmacPose = new Pose2d(rightOutOfTarmacVector, rightOutOfTarmacVector.getAngle());
                var builder = getBuilder.apply(startPose);
                return builder.getStartCommand()
                    .andThen(builder.prepareShot(firstShotVector))
                    .andThen(builder.intake()
                        .alongWith(builder.goTo(firstShotPose)))
                    .andThen(builder.prepareShotFromRobotPose()
                        .alongWith(builder.waitForCargo(2, 3.0)))
                    .andThen(builder.shootAll())
                    .andThen(builder.goTo(rightOutOfTarmacPose))
                    .andThen(builder.stopEverything());
            }
            case TWO_MIDLEFT_CARGOLEFT:
            {
                var startPose = new Pose2d(-92.0, -8.0, new Angle(-178.5, AngleUnit.DEGREES));
                var cargoPose = new Pose2d(-115.0, 70.0, new Angle(154.0, AngleUnit.DEGREES));
                var firstShotVector = new Vector2d(-122.0, 77.0);
                var firstShotPose = new Pose2d(firstShotVector, firstShotVector.getAngle());
                var rightOutOfTarmacVector = new Vector2d(142.0, firstShotVector.getAngle());
                var rightOutOfTarmacPose = new Pose2d(rightOutOfTarmacVector, rightOutOfTarmacVector.getAngle());
                var builder = getBuilder.apply(startPose);
                return builder.getStartCommand()
                    .andThen(builder.prepareShot(firstShotPose.getVector()))
                    .andThen(builder.intake())
                    .andThen(builder.goTo(cargoPose))
                    .andThen(builder.prepareShotFromRobotPose())
                    .andThen(builder.waitForCargo(2, 4.0).deadlineWith(builder.goTo(firstShotPose)))
                    .andThen(builder.shootAll())
                    .andThen(builder.goTo(rightOutOfTarmacPose))
                    .andThen(builder.stopEverything());
            }
            case TWO_MIDLEFT_CARGORIGHT:
            {
                var startPose = new Pose2d(-92.0, -8.0, new Angle(-178.5, AngleUnit.DEGREES));
                var firstShotVector = new Vector2d(-110.0, -96.0);
                var firstShotPose = new Pose2d(firstShotVector, firstShotVector.getAngle());
                var rightOutOfTarmacVector = new Vector2d(142.0, firstShotVector.getAngle());
                var rightOutOfTarmacPose = new Pose2d(rightOutOfTarmacVector, rightOutOfTarmacVector.getAngle());
                var builder = getBuilder.apply(startPose);
                return builder.getStartCommand()
                    .andThen(builder.prepareShot(firstShotPose.getVector()))
                    .andThen(builder.intake()
                        .alongWith(builder.goTo(firstShotPose)))
                    .andThen(builder.prepareShotFromRobotPose()
                        .alongWith(builder.waitForCargo(2, 4.0)))
                    .andThen(builder.shootAll())
                    .andThen(builder.goTo(rightOutOfTarmacPose))
                    .andThen(builder.stopEverything());
            }
            case TWO_MIDRIGHT:
            {
                var startPose = new Pose2d(-67.67, -63.13, new Angle(-133.5, AngleUnit.DEGREES));
                var firstShotVector = new Vector2d(-125.0, -88.0);
                var firstShotPose = new Pose2d(firstShotVector, firstShotVector.getAngle());
                var rightOutOfTarmacVector = new Vector2d(142.0, firstShotVector.getAngle());
                var rightOutOfTarmacPose = new Pose2d(rightOutOfTarmacVector, rightOutOfTarmacVector.getAngle());
                var builder = getBuilder.apply(startPose);
                return builder.getStartCommand()
                    .andThen(builder.prepareShot(firstShotVector)
                        .alongWith(builder.intake(),
                                   builder.goTo(firstShotPose)))
                    .andThen(builder.prepareShotFromRobotPose()
                        .alongWith(builder.waitForCargo(2, 3.0)))
                    .andThen(builder.shootAll())
                    .andThen(builder.goTo(rightOutOfTarmacPose))
                    .andThen(builder.stopEverything());
            }
            case TWO_RIGHT:
            {
                var startPose = new Pose2d(8.0, -92.0, new Angle(-88.5, AngleUnit.DEGREES));
                var firstShotVec = new Vector2d(-25.0, -129.0);
                var firstShotPose = new Pose2d(firstShotVec, firstShotVec.getAngle());
                var rightOutOfTarmac = new Pose2d(firstShotVec.getX(), -142.0, new Angle(-90.0, AngleUnit.DEGREES));
                var builder = getBuilder.apply(startPose);
                return builder.getStartCommand()
                    .andThen(builder.prepareShot(firstShotVec)
                        .alongWith(builder.intake(),
                                   builder.goTo(firstShotPose)))
                    .andThen(builder.prepareShotFromRobotPose()
                        .alongWith(builder.waitForCargo(2, 3.0)))
                    .andThen(builder.shootAll()
                        .alongWith(builder.retract()))
                    .andThen(builder.goTo(rightOutOfTarmac))
                    .andThen(builder.stopEverything());
            }
            case THREE_RIGHT:
            {
                var startPose = new Pose2d(8.0, -92.0, new Angle(-88.5, AngleUnit.DEGREES));
                var firstShotPose = new Pose2d(-25.0, -129.0, new Angle(-101.0, AngleUnit.DEGREES));
                var betweenShotsPose = new Pose2d(-103.0, -75.0, new Angle(-145.0, AngleUnit.DEGREES));
                var secondShotPose = new Pose2d(-130.0, -75.0, new Angle(-145.0, AngleUnit.DEGREES));
                var builder = getBuilder.apply(startPose);
                return builder.getStartCommand()
                    .andThen(builder.prepareShot(firstShotPose.getVector()))
                    .andThen(builder.intake())
                    .andThen(builder.goTo(firstShotPose))
                    .andThen(builder.waitForCargo(2, 4.0))
                    .andThen(builder.prepareShotFromRobotPose())
                    .andThen(builder.retract())
                    .andThen(builder.shootAll())
                    .andThen(builder.prepareShot(secondShotPose.getVector()))
                    .andThen(builder.intake())
                    .andThen(builder.goTo(betweenShotsPose))
                    .andThen(builder.prepareShotFromRobotPose())
                    .andThen(builder.waitForCargo(1, 4.0).deadlineWith(builder.goTo(secondShotPose)))
                    .andThen(builder.retract())
                    .andThen(builder.shootAll());
            }
            case FOUR_MIDRIGHT:
            {
                var startPose = new Pose2d(-67.67, -63.13, new Angle(-133.5, AngleUnit.DEGREES));
                var firstShotVector = new Vector2d(-125.0, -88.0);
                var firstShotPose = new Pose2d(firstShotVector, firstShotVector.getAngle());
                var builder = getBuilder.apply(startPose);
                return builder.getStartCommand()
                    .andThen(builder.prepareShot(firstShotVector)
                        .alongWith(builder.intake(),
                                   builder.goTo(firstShotPose)))
                    .andThen(builder.prepareShotFromRobotPose()
                        .alongWith(builder.waitForCargo(2, 3.0)))
                    .andThen(builder.shootAll())
                    .andThen(builder.intake()
                        .alongWith(builder.prepareShot(firstShotPose.getVector()),
                                   builder.goTo(k_farCargoPose),
                                   builder.enableLimelight()))
                    .andThen(builder.waitForCargo(2, 4.0))
                    .andThen(builder.goTo(firstShotPose))
                    .andThen(builder.shoot());
            }
            case FOUR_MIDLEFT_CARGORIGHT:
            {
                var startPose = new Pose2d(-92.0, -8.0, new Angle(-178.5, AngleUnit.DEGREES));
                var firstShotVector = new Vector2d(-110.0, -96.0);
                var firstShotPose = new Pose2d(firstShotVector, firstShotVector.getAngle());
                var builder = getBuilder.apply(startPose);
                return builder.getStartCommand()
                    .andThen(builder.prepareShot(firstShotPose.getVector()))
                    .andThen(builder.intake()
                        .alongWith(builder.goTo(firstShotPose)))
                    .andThen(builder.prepareShotFromRobotPose()
                        .alongWith(builder.waitForCargo(2, 4.0)))
                    .andThen(builder.shootAll())
                    .andThen(builder.intake()
                        .alongWith(builder.prepareShot(firstShotPose.getVector()),
                                   builder.goTo(k_farCargoPose),
                                   builder.enableLimelight()))
                    .andThen(builder.waitForCargo(2, 4.0))
                    .andThen(builder.goTo(firstShotPose))
                    .andThen(builder.shoot());
            }
            case FOUR_MIDLEFT_CARGOLEFT:
            {
                var startPose = new Pose2d(-92.0, -8.0, new Angle(-178.5, AngleUnit.DEGREES));
                var cargoPose = new Pose2d(-115.0, 70.0, new Angle(154.0, AngleUnit.DEGREES));
                var firstShotVector = new Vector2d(-122.0, 77.0);
                var firstShotPose = new Pose2d(firstShotVector, firstShotVector.getAngle());
                var secondShotVector = new Vector2d(-150.0, -34.0);
                var secondShotPose = new Pose2d(secondShotVector, secondShotVector.getAngle());
                var builder = getBuilder.apply(startPose);
                return builder.getStartCommand()
                    .andThen(builder.prepareShot(firstShotPose.getVector()))
                    .andThen(builder.intake())
                    .andThen(builder.goTo(cargoPose))
                    .andThen(builder.prepareShotFromRobotPose())
                    .andThen(builder.waitForCargo(2, 4.0).deadlineWith(builder.goTo(firstShotPose)))
                    .andThen(builder.shootAll())
                    .andThen(builder.intake()
                        .alongWith(builder.prepareShot(secondShotPose.getVector()),
                                   builder.goTo(k_farCargoPose),
                                   builder.enableLimelight()))
                    .andThen(builder.waitForCargo(2, 4.0))
                    .andThen(builder.goTo(secondShotPose))
                    .andThen(builder.shoot());
            }
            case FOUR_LEFT:
            {
                var startPose = new Pose2d(-63.0, 68.0, new Angle(136.5, AngleUnit.DEGREES));
                var firstShotVector = new Vector2d(-122.0, 73.0);
                var firstShotPose = new Pose2d(firstShotVector, firstShotVector.getAngle());
                var secondShotVector = new Vector2d(-150.0, -34.0);
                var secondShotPose = new Pose2d(secondShotVector, secondShotVector.getAngle());

                var farAdjustement = new Vector2d(2.0, new Angle(135.0, AngleUnit.DEGREES));

                var builder = getBuilder.apply(startPose);
                return builder.getStartCommand()
                    .andThen(builder.prepareShot(firstShotVector))
                    .andThen(builder.intake()
                        .alongWith(builder.goTo(firstShotPose)))
                    .andThen(builder.prepareShotFromRobotPose()
                        .alongWith(builder.waitForCargo(2, 3.0)))
                    .andThen(builder.shootAll())
                    .andThen(builder.intake()
                        .alongWith(builder.prepareShot(secondShotVector),
                                   builder.goTo(k_farCargoPose.transform(farAdjustement, new Angle())),
                                   builder.enableLimelight()))
                    .andThen(builder.waitForCargo(2, 4.0))
                    .andThen(builder.goTo(secondShotPose))
                    .andThen(builder.shoot());
            }
            case FIVE_RIGHT:
            {
                var startPose = new Pose2d(8.0, -92.0, new Angle(-88.5, AngleUnit.DEGREES));
                var firstShotVec = new Vector2d(-25.0, -129.0);
                var firstShotPose = new Pose2d(firstShotVec, firstShotVec.getAngle());
                var secondShotVec = new Vector2d(-130.0, -75.0);
                var midVec = secondShotVec.scale(0.92);
                var midPose = new Pose2d(midVec, midVec.getAngle());
                var secondShotPose = new Pose2d(secondShotVec, new Angle(-160.0, AngleUnit.DEGREES));
                var thirdShotVec = new Vector2d(-130.0, -75.0);
                var thirdShotPose = new Pose2d(thirdShotVec, thirdShotVec.getAngle());
                var builder = getBuilder.apply(startPose);
                return builder.getStartCommand()
                    .andThen(builder.prepareShot(firstShotVec)
                        .alongWith(builder.intake(),
                                   builder.goTo(firstShotPose)))
                    .andThen(builder.prepareShotFromRobotPose()
                        .alongWith(builder.waitForCargo(2, 3.0)))
                    .andThen(builder.shootAll()
                        .alongWith(builder.retract()))
                    .andThen(builder.prepareShot(secondShotVec)
                        .alongWith(builder.intake(),
                                   builder.goTo(midPose)))
                    .andThen(builder.goTo(secondShotPose))
                    .andThen(builder.shootAll(true))
                    .andThen(builder.prepareShot(thirdShotVec)
                        .alongWith(builder.goTo(k_farCargoPose),
                                   builder.enableLimelight()))
                    .andThen(builder.waitForCargo(2, 1.0))
                    .andThen(builder.goTo(thirdShotPose))
                    .andThen(builder.shoot());
            }
            case SIX_LEFT:
            {
                var startPose = new Pose2d(-63.0, 68.0, new Angle(136.5, AngleUnit.DEGREES));
                var firstShotVector = new Vector2d(-122.0, 77.0);
                var firstShotPose = new Pose2d(firstShotVector, firstShotVector.getAngle());
                var secondShotVector = new Vector2d(-150.0, -34.0);
                var secondShotPose = new Pose2d(secondShotVector, secondShotVector.getAngle());
                var turnBeforeGoingPose = new Pose2d(new Vector2d(-150.0, -60.0), new Angle(-60.0, AngleUnit.DEGREES));
                var getLastTwoPose = new Pose2d(-25.0, -139.0, new Angle(-87.0, AngleUnit.DEGREES));
                var thirdShootPose = new Pose2d(-25.0, -139.0, new Angle(-101.0, AngleUnit.DEGREES));

                var farAdjustement = new Vector2d(2.0, new Angle(135.0, AngleUnit.DEGREES));

                var builder = getBuilder.apply(startPose);
                return builder.getStartCommand()
                    .andThen(builder.prepareShot(firstShotVector))
                    .andThen(builder.intake()
                        .alongWith(builder.goTo(firstShotPose)))
                    .andThen(builder.prepareShotFromRobotPose()
                        .alongWith(builder.waitForCargo(2, 3.0)))
                    .andThen(builder.shootAll())
                    .andThen(builder.intake()
                        .alongWith(builder.prepareShot(secondShotVector),
                                   builder.goTo(k_farCargoPose.transform(farAdjustement, new Angle())),
                                   builder.enableLimelight()))
                    .andThen(builder.waitForCargo(2, 4.0))
                    .andThen(builder.goTo(secondShotPose))
                    .andThen(builder.shootAll())
                    .andThen(builder.intake()
                        .alongWith(builder.prepareShot(thirdShootPose.getVector()),
                                   builder.goTo(turnBeforeGoingPose)))
                    .andThen(builder.goTo(getLastTwoPose))
                    .andThen(builder.prepareShotFromRobotPose()
                        .alongWith(builder.waitForCargo(2, 4.0)
                            .deadlineWith(builder.goTo(thirdShootPose))))
                    .andThen(builder.shoot());
            }
            case TIMS_AUTO_LEFT:
            {
                var startPose = new Pose2d(-63.0, 68.0, new Angle(136.5, AngleUnit.DEGREES));
                var startTurnedPose = new Pose2d(-26.0, 120.0, new Angle(105.0, AngleUnit.DEGREES));

                double yPos_in = 138.0;
                var enemySideCargoPose = new Pose2d(-26.0, yPos_in, startTurnedPose.getRotation());
                var enemySideSlidePose = new Pose2d(-50.0, yPos_in, enemySideCargoPose.getRotation());

                var firstShotVector = new Vector2d(-88.0, 125.0);
                var firstShotPose = new Pose2d(firstShotVector, firstShotVector.getAngle());

                var builder = getBuilder.apply(startPose);
                return builder.getStartCommand()
                    .andThen(builder.prepareShot(firstShotVector))
                    .andThen(new WaitCommand(k_timWaitTime.getValue(TimeUnit.SECONDS)))
                    .andThen(builder.goTo(startTurnedPose))
                    .andThen(builder.reject())
                    .andThen(builder.intake())
                    .andThen(new WaitCommand(1.25))
                    .andThen(builder.waitForCargoNoTimeout(2)
                        .raceWith(builder.goTo(enemySideCargoPose)
                            .andThen(new WaitCommand(1.0))
                            .andThen(builder.goTo(enemySideSlidePose))
                            .andThen(new WaitCommand(1.25))))
                    .andThen(builder.goTo(firstShotPose))
                    .andThen(new WaitCommand(2.0))
                    .andThen(builder.shoot());
            }
            case TIMS_AUTO_RIGHT:
            {
                var startPose = new Pose2d(8.0, -92.0, new Angle(-88.5, AngleUnit.DEGREES));
                var startTurnedPose = new Pose2d(102.0, -95.72, new Angle(-105.0, AngleUnit.DEGREES));

                double yPos_in = -138.0;
                var enemySideCargoPose = new Pose2d(110.0, yPos_in, startTurnedPose.getRotation());
                var enemySideSlidePose = new Pose2d(79.0, yPos_in, enemySideCargoPose.getRotation());

                var firstShotVector = new Vector2d(8.0, -135.0);
                var firstShotPose = new Pose2d(firstShotVector, firstShotVector.getAngle());

                var builder = getBuilder.apply(startPose);
                return builder.getStartCommand()
                    .andThen(builder.prepareShot(firstShotVector))
                    .andThen(new WaitCommand(k_timWaitTime.getValue(TimeUnit.SECONDS)))
                    .andThen(builder.goTo(startTurnedPose))
                    .andThen(builder.reject())
                    .andThen(builder.intake())
                    .andThen(new WaitCommand(1.25))
                    .andThen(builder.waitForCargoNoTimeout(2)
                        .raceWith(builder.goTo(enemySideCargoPose)
                            .andThen(new WaitCommand(1.0))
                            .andThen(builder.goTo(enemySideSlidePose))
                            .andThen(new WaitCommand(1.25))))
                    .andThen(builder.retract())
                    .andThen(builder.goTo(firstShotPose))
                    .andThen(new WaitCommand(2.0))
                    .andThen(builder.shoot());
            }
            default:
                return getBuilder.apply(new Pose2d()).getDefaultAuto();
        }
    }
}
