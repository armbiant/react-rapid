/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import org.growingstems.math.Angle;
import org.growingstems.math.Vector2d;

public class TrapezoidAccProfileVector {
    private final TrapezoidAccProfileDouble m_distanceTrapezoid;
    private final Angle m_direction;
    private final Vector2d m_from;

    public TrapezoidAccProfileVector(double maxAccPercent, double maxVelPercent, Vector2d to, Vector2d from) {
        m_from = from;
        var translation = to.subtract(from);
        m_direction = translation.getAngle();
        m_distanceTrapezoid = new TrapezoidAccProfileDouble(maxAccPercent, maxVelPercent, translation.getMagnitude());
    }

    public Vector2d getCurrentVector() {
        return new Vector2d(m_distanceTrapezoid.getCurrentDistance(), m_direction).add(m_from);
    }
}
