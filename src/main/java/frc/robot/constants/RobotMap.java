/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.constants;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import frc.library.sensor.I2CMux.AddressSelect;
import frc.library.sensor.I2CMux.Channel;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other
 * purpose. All constants should be declared globally (i.e. public static). Do
 * not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the constants are needed, to reduce verbosity.
 */
public final class RobotMap {
    // ===================================================================
    // Ethernet
    // ===================================================================

    public static final int k_cargoTrackerPort = 5810;
    public static final String k_piIpAddress = "10.8.36.12";

    // ===================================================================
    // USB Devices
    // ===================================================================
    // controllers
    public static final int k_driverUsbPort = 0;
    public static final int k_operatorUsbPort = 1;

    // ===================================================================
    // CAN Devices
    // ===================================================================
    // Second CAN Bus
    public static final String k_secondCAN = "CANivore";

    // PDP
    public static final int k_pdpCanId = 50;

    // PCM
    public static final int k_pcmCanId = 15;
    public static final PneumaticsModuleType k_pcmType = PneumaticsModuleType.CTREPCM;

    // Swerve Drive
    public static final int k_swerveFrontRightPowerCanId = 1;
    public static final int k_swerveFrontRightSteerCanId = 2;
    public static final int k_swerveFrontLeftPowerCanId = 3;
    public static final int k_swerveFrontLeftSteerCanId = 4;
    public static final int k_swerveBackLeftPowerCanId = 5;
    public static final int k_swerveBackLeftSteerCanId = 6;
    public static final int k_swerveBackRightPowerCanId = 7;
    public static final int k_swerveBackRightSteerCanId = 8;
    public static final int k_swerveFrontRightCanCoderCanId = 9;
    public static final int k_swerveFrontLeftCanCoderCanId = 10;
    public static final int k_swerveBackLeftCanCoderCanId = 11;
    public static final int k_swerveBackRightCanCoderCanId = 12;

    // Intake
    public static final int k_intakeMotorCanId = 17;

    // Serializer
    public static final int k_serializerMotorCanId = 18;

    // Climber
    public static final int k_climbMotorCanId = 21;
    public static final int k_climbFollowerMotorCanId = 22;

    // IMU
    public static final int k_pigeonImuCanId = 17;

    // I2C
    public static final I2C.Port k_i2cPort = I2C.Port.kMXP;
    public static final AddressSelect k_muxAddress = AddressSelect.LLL;
    public static final int k_muxResetDio = 6;

    // Color Sensor
    public static final Channel k_serializerColorSensorMuxChannel = Channel.CHANNEL_2;
    public static final Channel k_towerColorSensorMuxChannel = Channel.CHANNEL_4;

    // ===================================================================
    // Pneumatics
    // ===================================================================
    public static final int k_towerEngagementPcmChannel = 1;

    // Intake
    public static final int k_intakeRetractSolenoidPcmChannel = 4;
    public static final int k_intakeExtendSolenoidPcmChannel = 5;

    // Shooter
    public static final int k_shooterSolenoidPcmChannel = 0;
    // Climb
    public static final int k_climbLockSolenoid = 3;

    // ===================================================================
    // Digital IO
    // ===================================================================

    // Serializer/ Tower
    public static final int k_towerPrimedDioChannel = 8;
    public static final int k_serializerPrimedDioChannel = 9;
    public static final int k_shotIrDioChannel = 7;

    // ===================================================================
    // Analog
    // ===================================================================

    // Grid IR
    public static final int k_serializerLidarAnalogChannel = 0;

    // ===================================================================
    // PWM
    // ===================================================================

    // ===================================================================
    // Relay
    // ===================================================================

    // ===================================================================
    // PDB Channels
    // ===================================================================

    public static final int k_climbMotorPdpChannel = 3;
    public static final int k_climbFollowerPdpChannel = 2;

    // ===================================================================
    // Shooter Wheels
    // ===================================================================
    public static final int k_primaryShooterMotorCanId = 19;
    public static final int k_secondaryShooterMotorCanId = 20;
}
