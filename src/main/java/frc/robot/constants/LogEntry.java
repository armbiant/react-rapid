/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.constants;

import frc.library.LogEntryPriority;

public enum LogEntry {
    DRIVER_STATION_MATCH_TIME("driverStation", "MatchTime", LogEntryPriority.HIGH, false),
    DRIVER_STATION_MATCH_TYPE("driverStation", "MatchType", LogEntryPriority.MEDIUM, false),
    DRIVER_STATION_ALLIANCE("driverStation", "Alliance", LogEntryPriority.MEDIUM, false),
    DRIVER_STATION_EVENT_NAME("driverStation", "EventName", LogEntryPriority.LOW, false),
    DRIVER_STATION_GAME_SPECIFIC_MSG("driverStation", "GameSpecificMsg", LogEntryPriority.LOWEST, false),
    DRIVER_STATION_REPLAY_NUMBER("driverStation", "ReplayNumber", LogEntryPriority.MEDIUM, false),
    DRIVER_STATION_LOCATION("driverStation", "Location", LogEntryPriority.MEDIUM, false),
    DRIVER_STATION_MATCH_NUMBER("driverStation", "MatchNumber", LogEntryPriority.MEDIUM, false),
    DRIVER_STATION_FMS_ATTACHED("driverStation", "isFMSAttached", LogEntryPriority.MEDIUM, false),
    DRIVER_STATION_DS_ATTACHED("driverStation", "isDSAttached", LogEntryPriority.MEDIUM, false),
    DRIVER_STATION_MATCH_STATE("driverStation", "MatchState", LogEntryPriority.HIGH, false),
    // Robot State
    ROBOT_STATE_ID("robotState", "id", LogEntryPriority.LOW, false),
    ROBOT_STATE_SHOT_VECTOR_ANG("robotState", "shotVectorAng_deg", LogEntryPriority.HIGH, false),
    ROBOT_STATE_SHOT_VECTOR_DIST("robotState", "shotVectorDist_in", LogEntryPriority.HIGH, false),
    ROBOT_STATE_SHOT_DETECTED("robotState", "shotDetected", LogEntryPriority.HIGH, false),
    ROBOT_STATE_READY_TO_SHOOT("robotState", "readyToShoot", LogEntryPriority.HIGH, false),
    ROBOT_STATE_TOTAL_SHOTS("robotState", "totalShots", LogEntryPriority.LOW, false),
    ROBOT_STATE_SERIALIZER_COULD_BE_SHOOTING("robotState", "serializerCouldBeShooting", LogEntryPriority.MEDIUM, false),
    ROBOT_STATE_SHOT_SENSOR_FILTERED("robotState", "shotSensorFiltered", LogEntryPriority.MEDIUM, false),
    ROBOT_STATE_CARGO_DIST("robotState", "cargoDist_In", LogEntryPriority.HIGH, LogEntry.extraLogging),
    ROBOT_STATE_CARGO_ANGLE("robotState", "cargoAng_deg", LogEntryPriority.HIGH, false),
    ROBOT_STATE_READY_TO_BEGIN_MATCH("robotState", "readyToBeginMatch", LogEntryPriority.LOWEST, true),
    ROBOT_STATE_IS_STING_RAY("robotState", "isStingRay", LogEntryPriority.LOWEST, true),
    // Intake
    INTAKE_STATE("intake", "state", LogEntryPriority.MEDIUM, false),
    // Climb
    CLIMB_STATE("climb", "state", LogEntryPriority.HIGH, LogEntry.extraLogging),
    CLIMB_RESET_OCCURED("climb", "resetOccurred", LogEntryPriority.HIGH, LogEntry.extraLogging),
    CLIMB_ENCODER_POSITION("climb", "encoderPosition_SU", LogEntryPriority.HIGH, true),
    CLIMB_GOAL_POSITION("climb", "goalPosition_SU", LogEntryPriority.HIGH, false),
    CLIMB_MASTER_CURRENT_PDP("climb", "masterCurrentPDP_A", LogEntryPriority.LOW, false),
    CLIMB_SLAVE_CURRENT_PDP("climb", "slaveCurrentPDP_A", LogEntryPriority.MEDIUM, false),
    CLIMB_MASTER_CURRENT_TALON("climb", "masterCurrentTalon_A", LogEntryPriority.MEDIUM, false),
    CLIMB_PERCENT_OUT("climb", "percentOut", LogEntryPriority.MEDIUM, false),
    CLIMB_PASSIVE_ANGLE("climb", "passiveAngle", LogEntryPriority.HIGH, true),
    CLIMB_PASSIVE_ANGLE_RATE("climb", "passiveAngleRate", LogEntryPriority.HIGH, LogEntry.extraLogging),
    CLIMB_IS_READY("climb", "isReady", LogEntryPriority.LOWEST, true),
    // CAN Status
    CAN_STATUS_PERCENT_BUS_UTILIZATION("canStatus", "percentBusUtilization", LogEntryPriority.HIGH, false),
    CAN_STATUS_BUS_OFF_COUNT("canStatus", "busOffCount", LogEntryPriority.LOW, false),
    CAN_STATUS_TX_FULL_COUNT("canStatus", "txFullCount", LogEntryPriority.LOW, false),
    CAN_STATUS_RECEIVE_ERROR_COUNT("canStatus", "receiveErrorCount", LogEntryPriority.LOW, false),
    CAN_STATUS_TRANSMIT_ERROR_COUNT("canStatus", "transmitErrorCount", LogEntryPriority.LOW, false),
    // Cargo Tracker
    CARGO_TRACKER_STATE("cargoTracker", "state", LogEntryPriority.LOWEST, false),
    CARGO_TRACKER_FPS("cargoTracker", "fps", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    CARGO_TRACKER_LATENCY("cargoTracker", "pipelineLatencyMs", LogEntryPriority.LOW, LogEntry.extraLogging),
    CARGO_TRACKER_ROI_RED("cargoTracker", "roiRed", LogEntryPriority.LOWEST, LogEntry.extraLogging),
    CARGO_TRACKER_ROI_GREEN("cargoTracker", "roiGreen", LogEntryPriority.LOWEST, LogEntry.extraLogging),
    CARGO_TRACKER_ROI_BLUE("cargoTracker", "roiBlue", LogEntryPriority.LOWEST, LogEntry.extraLogging),
    CARGO_TRACKER_STREAM_VIEW("cargoTracker", "streamView", LogEntryPriority.LOW, LogEntry.extraLogging),
    CARGO_TRACKER_INDEX_0_X("cargoTracker", "cargoIdx0x", LogEntryPriority.HIGH, false),
    CARGO_TRACKER_INDEX_0_Y("cargoTracker", "cargoIdx0y", LogEntryPriority.HIGH, false),
    CARGO_TRACKER_INDEX_1_X("cargoTracker", "cargoIdx1x", LogEntryPriority.HIGH, false),
    CARGO_TRACKER_INDEX_1_Y("cargoTracker", "cargoIdx1y", LogEntryPriority.HIGH, false),
    CARGO_TRACKER_INDEX_2_X("cargoTracker", "cargoIdx2x", LogEntryPriority.LOW, false),
    CARGO_TRACKER_INDEX_2_Y("cargoTracker", "cargoIdx2y", LogEntryPriority.LOW, false),
    CARGO_TRACKER_INDEX_3_X("cargoTracker", "cargoIdx3x", LogEntryPriority.LOW, false),
    CARGO_TRACKER_INDEX_3_Y("cargoTracker", "cargoIdx3y", LogEntryPriority.LOW, false),
    CARGO_TRACKER_INDEX_4_X("cargoTracker", "cargoIdx4x", LogEntryPriority.LOW, false),
    CARGO_TRACKER_INDEX_4_Y("cargoTracker", "cargoIdx4y", LogEntryPriority.LOW, false),
    // Sensors
    SENSORS_LL_ROBOT_POS_X("sensors", "llRobotPosX_In", LogEntryPriority.HIGH, false),
    SENSORS_LL_ROBOT_POS_Y("sensors", "llRobotPosY_In", LogEntryPriority.HIGH, false),
    SENSORS_HEADING("sensors", "heading_deg", LogEntryPriority.HIGH, false),
    SENSORS_PITCH("sensors", "pitch_deg", LogEntryPriority.HIGH, false),
    SENSORS_ROLL("sensors", "roll_deg", LogEntryPriority.LOW, false),
    SENSORS_RESET_I2C("sensors", "resetI2C", LogEntryPriority.HIGH, false),
    SENSORS_I2C_MUX_VERIFY("sensors", "i2cMuxVerify", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SENSORS_SERIALIZER_COLOR_SENSOR_VALID("sensors", "serializerColorSensorValid", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SENSORS_SERIALIZER_COLOR_RED("sensors", "serializerColorRed", LogEntryPriority.LOWEST, false),
    SENSORS_SERIALIZER_COLOR_BLUE("sensors", "serializerColorBlue", LogEntryPriority.LOWEST, false),
    SENSORS_SERIALIZER_COLOR_GREEN("sensors", "serializerColorGreen", LogEntryPriority.LOWEST, false),
    SENSORS_SERIALIZER_COLOR_CLEAR("sensors", "serializerColorClear", LogEntryPriority.LOWEST, false),
    SENSORS_SERIALIZER_COLOR_RAW_RED("sensors", "serializerColorRawRed", LogEntryPriority.LOW, false),
    SENSORS_SERIALIZER_COLOR_RAW_BLUE("sensors", "serializerColorRawBlue", LogEntryPriority.LOW, false),
    SENSORS_SERIALIZER_COLOR_RAW_GREEN("sensors", "serializerColorRawGreen", LogEntryPriority.LOW, false),
    SENSORS_SERIALIZER_COLOR_RAW_CLEAR("sensors", "serializerColorRawClear", LogEntryPriority.LOW, false),
    SENSORS_SERIALIZER_HUE("sensors", "serializerHue", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SENSORS_SERIALIZER_VALUE("sensors", "serializerValue", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SENSORS_SERIALIZER_SATURATION("sensors", "serializerSaturation", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SENSORS_SERIALIZER_DETECTED_CARGO_COLOR("sensors", "serializerDetectedCargoColor", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SENSORS_TOWER_COLOR_SENSOR_VALID("sensors", "towerColorSensorValid", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SENSORS_TOWER_COLOR_RED("sensors", "towerColorRed", LogEntryPriority.LOWEST, false),
    SENSORS_TOWER_COLOR_BLUE("sensors", "towerColorBlue", LogEntryPriority.LOWEST, false),
    SENSORS_TOWER_COLOR_GREEN("sensors", "towerColorGreen", LogEntryPriority.LOWEST, false),
    SENSORS_TOWER_COLOR_CLEAR("sensors", "towerColorClear", LogEntryPriority.LOWEST, false),
    SENSORS_TOWER_COLOR_RAW_RED("sensors", "towerColorRawRed", LogEntryPriority.LOW, false),
    SENSORS_TOWER_COLOR_RAW_BLUE("sensors", "towerColorRawBlue", LogEntryPriority.LOW, false),
    SENSORS_TOWER_COLOR_RAW_GREEN("sensors", "towerColorRawGreen", LogEntryPriority.LOW, false),
    SENSORS_TOWER_COLOR_RAW_CLEAR("sensors", "towerColorRawClear", LogEntryPriority.LOW, false),
    SENSORS_TOWER_HUE("sensors", "towerHue", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SENSORS_TOWER_VALUE("sensors", "towerValue", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SENSORS_TOWER_SATURATION("sensors", "towerSaturation", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SENSORS_TOWER_DETECTED_CARGO_COLOR("sensors", "towerDetectedCargoColor", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SENSORS_SERIALZIER_PRIMED("sensors", "serializerPrime", LogEntryPriority.HIGH, false),
    SENSORS_TOWER_PRIME("sensors", "towerPrime", LogEntryPriority.HIGH, false),
    SENSORS_SHOT_DETECTED_SENSOR("sensors", "shotDetectedSensor", LogEntryPriority.HIGH, false),
    SENSORS_HEADING_ANGULAR_VELOCITY("sensors", "headingAngularVelocity_radps", LogEntryPriority.HIGH, false),
    SENSORS_PITCH_ANGULAR_VELOCITY("sensors", "pitchAngularVelocity_radps", LogEntryPriority.HIGH, false),
    SENSORS_ROLL_ANGULAR_VELOCITY("sensors", "rollAngularVelocity_radps", LogEntryPriority.LOW, false),
    SENSORS_SERIALIZER_LIDAR_RAW("sensors", "serializerLidarRaw_V", LogEntryPriority.LOW, false),
    SENSORS_SERIALIZER_LIDAR_DETECT("sensors", "serializerLidarDetect", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SENSORS_CARGO_NEW_DATA("sensors", "cargoTrkNewData", LogEntryPriority.MEDIUM, false),
    SENSORS_CARGO_COUNT("sensors", "cargoTrkCount", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SENSORS_CARGO_INDEX("sensors", "cargoTrkIndex", LogEntryPriority.LOWEST, LogEntry.extraLogging),
    SENSORS_CARGO_X("sensors", "cargoTrkX", LogEntryPriority.LOW, LogEntry.extraLogging),
    SENSORS_CARGO_Y("sensors", "cargoTrkY", LogEntryPriority.LOW, LogEntry.extraLogging),
    SENSORS_CARGO_WIDTH("sensors", "cargoTrkWidth", LogEntryPriority.LOW, LogEntry.extraLogging),
    SENSORS_CARGO_HEIGHT("sensors", "cargoTrkHeight", LogEntryPriority.LOW, LogEntry.extraLogging),
    SENSORS_CARGO_ASPECT("sensors", "cargoTrkAspect", LogEntryPriority.LOW, LogEntry.extraLogging),
    SENSORS_CARGO_FILL("sensors", "cargoTrkFill", LogEntryPriority.LOW, LogEntry.extraLogging),
    SENSORS_CARGO_SCREEN("sensors", "cargoTrkScreen", LogEntryPriority.LOW, LogEntry.extraLogging),
    SENSORS_CARGO_IS_RED("sensors", "cargoTrkIsRed", LogEntryPriority.LOW, LogEntry.extraLogging),
    SENSORS_CARGO_SMARTSCREEN("sensors", "cargoTrkSmartScreen", LogEntryPriority.LOW, LogEntry.extraLogging),
    SENSORS_CARGO_RELATIVE_X("sensors", "cargoTrkRelativeX", LogEntryPriority.LOW, LogEntry.extraLogging),
    SENSORS_CARGO_RELATIVE_Y("sensors", "cargoTrkRelativeY", LogEntryPriority.LOW, LogEntry.extraLogging),
    // Limelight
    LIMELIGHT_STATE("limelight", "state", LogEntryPriority.MEDIUM, false),
    LIMELIGHT_VALID("limelight", "valid", LogEntryPriority.HIGH, true),
    LIMELIGHT_CORNER_PITCH("limelight", "cornerPitch_deg", LogEntryPriority.MEDIUM, false),
    LIMELIGHT_CORNER_YAW("limelight", "cornerYaw_deg", LogEntryPriority.MEDIUM, false),
    LIMELIGHT_TARGET_YAW("limelight", "targetYaw_deg", LogEntryPriority.MEDIUM, false),
    LIMELIGHT_RADIAL_DISTANCE("limelight", "radialDistance_In", LogEntryPriority.LOW, false),
    LIMELIGHT_RADIAL_ANGLE("limelight", "radialAngle_deg", LogEntryPriority.LOW, false),
    LIMELIGHT_POS_X("limelight", "posX_In", LogEntryPriority.LOW, false),
    LIMELIGHT_POS_Y("limelight", "posY_In", LogEntryPriority.LOW, false),
    // Controller - Driver
    CONTROLLER_DRIVER_LEFT_X("controller_driver", "LeftX", LogEntryPriority.HIGH, false),
    CONTROLLER_DRIVER_LEFT_Y("controller_driver", "LeftY", LogEntryPriority.HIGH, false),
    CONTROLLER_DRIVER_RIGHT_X("controller_driver", "RightX", LogEntryPriority.HIGH, false),
    CONTROLLER_DRIVER_RIGHT_Y("controller_driver", "RightY", LogEntryPriority.LOW, false),
    CONTROLLER_DRIVER_A("controller_driver", "A", LogEntryPriority.HIGH, false),
    CONTROLLER_DRIVER_B("controller_driver", "B", LogEntryPriority.HIGH, false),
    CONTROLLER_DRIVER_X("controller_driver", "X", LogEntryPriority.HIGH, false),
    CONTROLLER_DRIVER_Y("controller_driver", "Y", LogEntryPriority.HIGH, false),
    CONTROLLER_DRIVER_RB("controller_driver", "RightBumper", LogEntryPriority.HIGH, false),
    CONTROLLER_DRIVER_LB("controller_driver", "LeftBumper", LogEntryPriority.HIGH, false),
    CONTROLLER_DRIVER_RT("controller_driver", "RightTrigger", LogEntryPriority.HIGH, false),
    CONTROLLER_DRIVER_LT("controller_driver", "LeftTrigger", LogEntryPriority.HIGH, false),
    CONTROLLER_DRIVER_BACK("controller_driver", "Back", LogEntryPriority.HIGH, false),
    CONTROLLER_DRIVER_START("controller_driver", "Start", LogEntryPriority.HIGH, false),
    CONTROLLER_DRIVER_POV("controller_driver", "POV", LogEntryPriority.HIGH, false),
    CONTROLLER_DRIVER_L3("controller_driver", "L3", LogEntryPriority.LOW, false),
    CONTROLLER_DRIVER_R3("controller_driver", "R3", LogEntryPriority.LOW, false),
    // Controller - Operator
    CONTROLLER_OPERATOR_LEFT_X("controller_operator", "LeftX", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_LEFT_Y("controller_operator", "LeftY", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_RIGHT_X("controller_operator", "RightX", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_RIGHT_Y("controller_operator", "RightY", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_A("controller_operator", "A", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_B("controller_operator", "B", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_X("controller_operator", "X", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_Y("controller_operator", "Y", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_RB("controller_operator", "RightBumper", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_LB("controller_operator", "LeftBumper", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_RT("controller_operator", "RightTrigger", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_LT("controller_operator", "LeftTrigger", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_BACK("controller_operator", "Back", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_START("controller_operator", "Start", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_POV("controller_operator", "POV", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_L3("controller_operator", "L3", LogEntryPriority.LOW, false),
    CONTROLLER_OPERATOR_R3("controller_operator", "R3", LogEntryPriority.LOW, false),
    // Drive
    DRIVE_STATE("drive", "state", LogEntryPriority.LOW, false),
    DRIVE_VEL_X("drive", "velocityX_inps", LogEntryPriority.MEDIUM, false),
    DRIVE_VEL_Y("drive", "velocityY_inps", LogEntryPriority.MEDIUM, false),
    DRIVE_DIST_GOAL_POSE("drive", "distanceToGoalPose", LogEntryPriority.MEDIUM, false),
    DRIVE_POSE_GOAL_VEC_ANG("drive", "poseGoalVectorAng", LogEntryPriority.MEDIUM, false),
    DRIVE_POSE_GOAL_VEC_MAG("drive", "poseGoalVectorMag", LogEntryPriority.MEDIUM, false),
    DRIVE_POSE_GOAL_AT_POSE("drive", "poseGoalAtPose", LogEntryPriority.MEDIUM, false),
    DRIVE_SMART_HEADING_ERROR("drive", "smartHeadingError", LogEntryPriority.HIGH, false),
    DRIVE_FR_SELECTED_STEER_ANGLE("drive", "front_right_selected_steer_angle_deg", LogEntryPriority.MEDIUM, false),
    DRIVE_FR_CANCODER_ABS_POS("drive", "front_right_cancoder_abs_pos_deg", LogEntryPriority.LOW, false),
    DRIVE_FR_CANCODER_POS("drive", "front_right_cancoder_pos_deg", LogEntryPriority.LOW, false),
    DRIVE_FR_CURRENT("drive", "front_right_current_A", LogEntryPriority.MEDIUM, false),
    DRIVE_FR_VELOCITY_X("drive", "front_right_velocity_x", LogEntryPriority.LOWEST, false),
    DRIVE_FR_VELOCITY_Y("drive", "front_right_velocity_y", LogEntryPriority.LOWEST, false),
    DRIVE_FR_FORWARDS_KINEMATICS_X("drive", "front_right_forwards_kinematics_x", LogEntryPriority.MEDIUM, false),
    DRIVE_FR_FORWARDS_KINEMATICS_Y("drive", "front_right_forwards_kinematics_y", LogEntryPriority.MEDIUM, false),
    DRIVE_FR_SENSOR_POSITION("drive", "front_right_sensor_position", LogEntryPriority.MEDIUM, false),
    DRIVE_FR_DRIVE_RESET("drive", "front_right_drive_reset", LogEntryPriority.MEDIUM, false),
    DRIVE_FR_STEER_RESET("drive", "front_right_steer_reset", LogEntryPriority.MEDIUM, false),
    DRIVE_FL_SELECTED_STEER_ANGLE("drive", "front_left_selected_steer_angle_deg", LogEntryPriority.HIGH, false),
    DRIVE_FL_CANCODER_ABS_POS("drive", "front_left_cancoder_abs_pos_deg", LogEntryPriority.LOW, false),
    DRIVE_FL_CANCODER_POS("drive", "front_left_cancoder_pos_deg", LogEntryPriority.LOW, false),
    DRIVE_FL_CURRENT("drive", "front_left_current_A", LogEntryPriority.HIGH, false),
    DRIVE_FL_VELOCITY_X("drive", "front_left_velocity_x", LogEntryPriority.LOWEST, false),
    DRIVE_FL_VELOCITY_Y("drive", "front_left_velocity_y", LogEntryPriority.LOWEST, false),
    DRIVE_FL_FORWARDS_KINEMATICS_X("drive", "front_left_forwards_kinematics_x", LogEntryPriority.HIGH, false),
    DRIVE_FL_FORWARDS_KINEMATICS_Y("drive", "front_left_forwards_kinematics_y", LogEntryPriority.HIGH, false),
    DRIVE_FL_SENSOR_POSITION("drive", "front_left_sensor_position", LogEntryPriority.HIGH, false),
    DRIVE_FL_DRIVE_RESET("drive", "front_left_drive_reset", LogEntryPriority.HIGH, false),
    DRIVE_FL_STEER_RESET("drive", "front_left_steer_reset", LogEntryPriority.HIGH, false),
    DRIVE_BL_SELECTED_STEER_ANGLE("drive", "back_left_selected_steer_angle_deg", LogEntryPriority.MEDIUM, false),
    DRIVE_BL_CANCODER_ABS_POS("drive", "back_left_cancoder_abs_pos_deg", LogEntryPriority.LOW, false),
    DRIVE_BL_CANCODER_POS("drive", "back_left_cancoder_pos_deg", LogEntryPriority.LOW, false),
    DRIVE_BL_CURRENT("drive", "back_left_current_A", LogEntryPriority.MEDIUM, false),
    DRIVE_BL_VELOCITY_X("drive", "back_left_velocity_x", LogEntryPriority.LOWEST, false),
    DRIVE_BL_VELOCITY_Y("drive", "back_left_velocity_y", LogEntryPriority.LOWEST, false),
    DRIVE_BL_FORWARDS_KINEMATICS_X("drive", "back_left_forwards_kinematics_x", LogEntryPriority.MEDIUM, false),
    DRIVE_BL_FORWARDS_KINEMATICS_Y("drive", "back_left_forwards_kinematics_y", LogEntryPriority.MEDIUM, false),
    DRIVE_BL_SENSOR_POSITION("drive", "back_left_sensor_position", LogEntryPriority.MEDIUM, false),
    DRIVE_BL_DRIVE_RESET("drive", "back_left_drive_reset", LogEntryPriority.MEDIUM, false),
    DRIVE_BL_STEER_RESET("drive", "back_left_steer_reset", LogEntryPriority.MEDIUM, false),
    DRIVE_BR_SELECTED_STEER_ANGLE("drive", "back_right_selected_steer_angle_deg", LogEntryPriority.MEDIUM, false),
    DRIVE_BR_CANCODER_ABS_POS("drive", "back_right_cancoder_abs_pos_deg", LogEntryPriority.LOW, false),
    DRIVE_BR_CANCODER_POS("drive", "back_right_cancoder_pos_deg", LogEntryPriority.LOW, false),
    DRIVE_BR_CURRENT("drive", "back_right_current_A", LogEntryPriority.MEDIUM, false),
    DRIVE_BR_VELOCITY_X("drive", "back_right_velocity_x", LogEntryPriority.LOWEST, false),
    DRIVE_BR_VELOCITY_Y("drive", "back_right_velocity_y", LogEntryPriority.LOWEST, false),
    DRIVE_BR_FORWARDS_KINEMATICS_X("drive", "back_right_forwards_kinematics_x", LogEntryPriority.MEDIUM, false),
    DRIVE_BR_FORWARDS_KINEMATICS_Y("drive", "back_right_forwards_kinematics_y", LogEntryPriority.MEDIUM, false),
    DRIVE_BR_SENSOR_POSITION("drive", "back_right_sensor_position", LogEntryPriority.MEDIUM, false),
    DRIVE_BR_DRIVE_RESET("drive", "back_right_drive_reset", LogEntryPriority.MEDIUM, false),
    DRIVE_BR_STEER_RESET("drive", "back_right_steer_reset", LogEntryPriority.MEDIUM, false),
    DRIVE_FR_TEMPERATURE("drive", "front_right_temp_C", LogEntryPriority.HIGH, false), // Left HIGH because one would potentially be enough for all
    DRIVE_FL_TEMPERATURE("drive", "front_left_temp_C", LogEntryPriority.HIGH, false), // To compare since front_left is the problematic one
    DRIVE_BL_TEMPERATURE("drive", "back_left_temp_C", LogEntryPriority.MEDIUM, false),
    DRIVE_BR_TEMPERATURE("drive", "back_right_temp_C", LogEntryPriority.MEDIUM, false),
    DRIVE_TRANSLATION_POWER_MAG("drive", "translational_power_magnitude", LogEntryPriority.MEDIUM, false),
    DRIVE_TRANSLATION_POWER_ANGLE("drive", "translational_power_angle_deg", LogEntryPriority.MEDIUM, false),
    DRIVE_ROTATION_POWER("drive", "rotational_power", LogEntryPriority.MEDIUM, false),
    DRIVE_SHOT_DTHETA_DT_RPS("drive", "shot_dtheta_dt_rps", LogEntryPriority.MEDIUM, false),
    DRIVE_HAS_PID("drive", "hasPid", LogEntryPriority.LOW, true),
    // Shooter
    SHOOTER_STATE("shooter", "state", LogEntryPriority.LOW, false),
    SHOOTER_TRYING_TO_SHOOT("shooter", "tryingToShoot", LogEntryPriority.HIGH, false),
    SHOOTER_SHOOTER_READY("shooter", "shooterReady", LogEntryPriority.HIGH, false),
    SHOOTER_PRIMARY("shooter", "primary_RPM", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SHOOTER_SECONDARY("shooter", "secondary_RPM", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SHOOTER_PRIMARY_GOAL("shooter", "primaryGoal_RPM", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SHOOTER_SECONDARY_GOAL("shooter", "secondaryGoal_RPM", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SHOOTER_PRIMARY_MID_GOAL("shooter", "primaryMidGoal_RPM", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SHOOTER_SECONDARY_MID_GOAL("shooter", "secondaryMidGoal_RPM", LogEntryPriority.HIGH, LogEntry.extraLogging),
    SHOOTER_PRIMARY_DELTA("shooter", "primaryDelta_RPM", LogEntryPriority.MEDIUM, false),
    SHOOTER_SECONDARY_DELTA("shooter", "secondaryDelta_RPM", LogEntryPriority.MEDIUM, false),
    SHOOTER_CURRENT_PRIMARY("shooter", "currentPrim_A", LogEntryPriority.MEDIUM, false),
    SHOOTER_CURRENT_SECONDARY("shooter", "currentSec_A", LogEntryPriority.MEDIUM, false),
    SHOOTER_PERCENT_OUT_PRIMARY("shooter", "percentOutPrim", LogEntryPriority.MEDIUM, false),
    SHOOTER_PERCENT_OUT_SECONDARY("shooter", "percentOutSec", LogEntryPriority.MEDIUM, false),
    SHOOTER_SHOT_DETECTED("shooter", "shotDetected", LogEntryPriority.LOW, false),
    SHOOTER_SHOOT_POSE("shooter", "shootPose", LogEntryPriority.HIGH, false),
    SHOOTER_HAS_PID("shooter", "hasPid", LogEntryPriority.LOW, true),
    // Serializer
    SERIALIZER_STATE("serializer", "state", LogEntryPriority.HIGH, false),
    SERIALIZER_SOLENOID_ENGAGED("serializer", "solenoidEngaged", LogEntryPriority.HIGH, false),
    SERIALIZER_NORMAL_POWER("serializer", "power", LogEntryPriority.LOW, false),
    SERIALIZER_NORMAL_ENGAGED("serializer", "engaged", LogEntryPriority.LOW, false),
    SERIALIZER_CARGO_COUNT("serializer", "cargoCount", LogEntryPriority.HIGH, false),
    SERIALIZER_COMMANDED_TO_SHOOT("serializer", "commandedToShoot", LogEntryPriority.HIGH, false),
    SERIALIZER_SHOULD_SHOOT("serializer", "shouldShoot", LogEntryPriority.HIGH, false),
    SERIALIZER_IS_SHOOTING("serializer", "isShooting", LogEntryPriority.HIGH, false),
    SERIALIZER_SHOT_COOLDOWN("serializer", "shotCooldown", LogEntryPriority.HIGH, false),
    SERIALIZER_MOTOR_POWER("serializer", "motorPower", LogEntryPriority.HIGH, false),
    SERIALIZER_NORMAL_STATE("serializer", "normalState", LogEntryPriority.HIGH, false),
    SERIALIZER_REPRIME("serializer", "reprime", LogEntryPriority.HIGH, false),
    SERIALIZER_PRIMED_ONE("serializer", "primedOne", LogEntryPriority.LOWEST, false),
    SERIALIZER_PRIMED_BOTH("serializer", "primedBoth", LogEntryPriority.LOWEST, true),
    SERIALIZER_REJECTING("serializer", "rejecting", LogEntryPriority.LOW, false),
    // Timing
    TIME_TOTAL("time", "total", LogEntryPriority.HIGH, true),
    TIME_ROBOT_PERIOD("time", "robotPeriod", LogEntryPriority.HIGH, true),
    TIME_LOG_GRABLINE("time", "logGrabLine", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    TIME_SERIALIZER("time", "serializer", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    TIME_SHOOTER("time", "shooter", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    TIME_DRIVE("time", "drive", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    TIME_INTAKE("time", "intake", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    TIME_CLIMB("time", "climb", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    TIME_LIMELIGHT("time", "limelight", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    TIME_CARGO_TRACKER_CAMERA("time", "cargoTrackerCamera", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    TIME_SENSORS("time", "sensors", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    TIME_I2C("time", "i2c", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    TIME_ROBOT_STATE("time", "robotState", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    TIME_POSE_FUSION("time", "poseFusion", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    TIME_CARGO_TRACKER("time", "cargoTracker", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    TIME_CONTROLLER("time", "controller", LogEntryPriority.LOW, LogEntry.extraLogging),
    // Pose Fusion
    POSE_FUSION_LL_STD("poseFusion", "STD", LogEntryPriority.MEDIUM, false),
    POSE_FUSION_LL_OFFSET_ERROR("poseFusion", "offsetError", LogEntryPriority.MEDIUM, false),
    POSE_FUSION_ODOM_POSE_X("poseFusion", "odomPoseX", LogEntryPriority.HIGH, LogEntry.extraLogging),
    POSE_FUSION_ODOM_POSE_Y("poseFusion", "odomPoseY", LogEntryPriority.HIGH, LogEntry.extraLogging),
    POSE_FUSION_ROBOT_POSE_X("poseFusion", "robotPoseX", LogEntryPriority.HIGH, LogEntry.extraLogging),
    POSE_FUSION_ROBOT_POSE_Y("poseFusion", "robotPoseY", LogEntryPriority.HIGH, LogEntry.extraLogging),
    POSE_FUSION_TO_HUB_ANG("poseFusion", "toHubAng_deg", LogEntryPriority.MEDIUM, false),
    POSE_FUSION_TO_HUB_DIST("poseFusion", "toHubDist_In", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    POSE_FUSION_FIELD_VEL_X("poseFusion", "velocityX_inps", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    POSE_FUSION_FIELD_VEL_Y("poseFusion", "velocityY_inps", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    POSE_FUSION_FIELD_VEL_THETA("poseFusion", "velocityTheta_dps", LogEntryPriority.MEDIUM, LogEntry.extraLogging),
    POSE_FUSION_SKIPPED_OFFSET("poseFusion", "skippedOffset", LogEntryPriority.MEDIUM, false),
    //Git Information
    GIT_BRANCH_NAME("git", "gitBranch", LogEntryPriority.MEDIUM, false),
    GIT_COMMIT_HASH("git", "gitCommit", LogEntryPriority.HIGH, true);

    private static final boolean extraLogging = false;

    public final String subsystem;
    public final String name;
    public final LogEntryPriority priority;
    public final boolean sendToDashboard;

    LogEntry(String subsystem, String name, LogEntryPriority priority, boolean sendToDashboard) {
        this.subsystem = subsystem;
        this.name = name;
        this.priority = priority;
        this.sendToDashboard = sendToDashboard;
    }
}
