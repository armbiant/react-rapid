/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.constants;

import frc.library.Length;
import frc.library.Position2d;

import org.growingstems.math.Angle;
import org.growingstems.math.Angle.AngleUnit;

/**
 * The FieldDimensions class is used to store all of the dimensional data of the
 * field
 */
public final class FieldDimensions {
    // =============================================================================================
    // general field
    // =============================================================================================
    public static final Length k_fieldWidth = Length.feet(27.0);
    public static final Length k_fieldLength = Length.feet(54.0);

    // =============================================================================================
    // hangar
    // =============================================================================================
    // rung dimensions
    public static final Length k_rungWidth = Length.feet(7.0);
    public static final Length k_rungDiameter = Length.inches(1.66);

    // all rung heights are measured to top of the rung
    public static final Length k_lowRungHeight = Length.feet(4.0).add(Length.inches(0.75));
    public static final Length k_midRungHeight = Length.feet(5.0).add(Length.inches(0.75));
    public static final Length k_highRungHeight = Length.feet(6.0).add(Length.inches(3.625));
    public static final Length k_traversalRungHeight = Length.feet(7.0).add(Length.inches(7.0));

    // =============================================================================================
    // terminal
    // =============================================================================================
    public static final Length k_terminalWidth = Length.feet(7.0).add(Length.inches(8.5));
    public static final Length k_terminalHeight = Length.feet(3.0).add(Length.inches(7.0)); // measured from carpet to
                                                                                            // edge the cargo rolls over

    // =============================================================================================
    // hub
    // =============================================================================================
    public static final Position2d k_hubPos = new Position2d(Length.meters(0.0), Length.meters(0.0)); // intentionally
                                                                                                      // define center
                                                                                                      // of the field as
                                                                                                      // the center of
                                                                                                      // the hub

    public static final Length k_hubDistanceToFender = Length.inches(34.0); //Distance from center of Hub to Fender

    // upper hub
    public static final Length k_upperHubHeight = Length.feet(8.0).add(Length.inches(8.0)); // from floor to upper edge
                                                                                            // of the upper hub goal
    public static final Length k_upperHubDiameter = Length.feet(4.0);
    public static final Length k_upperExitHeight = Length.inches(67.375); // distance from base to the bottom tip of the
                                                                          // exit
    public static final Length k_upperHubExitTrackWidth = Length.inches(10.0);
    public static final Length k_upperHubExitTrapezoidLength = Length.inches(14.0);
    public static final Length k_upperHubExitTrapezoidWidth = Length.inches(31.0);
    public static final Length k_upperHubExitTrackLength = Length.inches(58.5); // from cone exit to end of trapezoid
    // According to the game manual, these are all approxiamte measures
    // Measured from center of robot with north being away from the driver station,
    // rotating counterclockwise around the circle formed by the center of the hub
    // Quadrants correspond to those on the Cartesian coordinate plane
    public static final Angle k_yawOfHubExitQuadOne = new Angle(-66.0, AngleUnit.DEGREES);
    public static final Angle k_yawOfHubExitQuadTwo = new Angle(24.0, AngleUnit.DEGREES);
    public static final Angle k_yawOfHubExitQuadThree = new Angle(114.0, AngleUnit.DEGREES);
    public static final Angle k_yawOfHubExitQuadFour = new Angle(-156.0, AngleUnit.DEGREES);

    // lower hub
    public static final Length k_lowerHubHeight = Length.feet(3.0).add(Length.inches(5.0)); // measured from floor to
                                                                                            // upper edge of the lower
                                                                                            // hub goal
    public static final Length k_lowerHubDiameter = Length.feet(5.0).add(Length.inches(0.125));

    // vision target
    public static final Length k_visionTargetDiameter = Length.feet(4.0).add(Length.inches(5.375));
    public static final Length k_visionTargetRadius = k_visionTargetDiameter.scale(0.5);
    public static final Length k_tapeWidth = Length.inches(5.0);
    public static final int k_numTapes = 16;
    public static final Length k_gapWidth =
        k_visionTargetDiameter.scale(Math.PI).scale(1.0 / k_numTapes).subtract(k_tapeWidth);

    public static final Length k_visionTargetHeightToTapeBottom = Length.feet(8.0).add(Length.inches(5.625));
    public static final Length k_visionTargetHeightToTapeTop = k_visionTargetHeightToTapeBottom.add(Length.inches(2.0));

    // =============================================================================================
    // game elements
    // =============================================================================================
    public static final Length k_cargoDiameter = Length.inches(9.5);

    // =============================================================================================
    // starting cargo positions (inches)
    // =============================================================================================
    // ABOVE HORIZONTAL
    // 149.23, 33.77
    // 124.95, 88.30
    // 25.91, 150.79
    // -33.77, 149.23
    // -88.30, 124.95
    // -129.40, 81.64

    // BELOW HORIZONTAL
    // -149.23, -33.77
    // -124.95, -88.30
    // -25.91, -150.79
    // 33.77, -149.23
    // 88.30, -124.95
    // 129.40, -81.64

    // TERMINAL CARGO
    // 282.12, 177.78
    // -282.12, -177.78

    // Cargo positions are defined in counterclockwise order from positive X
    // ┌─────────────────xxxxxxxxxxx─────────────────────│
    // │               -4-         -3---                 │
    // │             xxx               xx                │
    // │            xx                   2-              │
    // │           5-                     xxx            │
    // │          xx                        x            │
    // │          6                          1           │
    // │         xx                          x  positive x
    // ├─────────xx──────────────────────────x──>>>──────┤
    // │          7                          x           │
    // │          xx                        12           │
    // │           xx                       x            │
    // │             8-                    11            │
    // │               xx                 xx             │
    // │                -9-            -10-              │
    // │                  xxxxxx    xxxx                 │
    // └───────────────────────xxxxx─────────────────────┘

    public static final Position2d k_circleCargoOne = new Position2d(Length.inches(149.23), Length.inches(33.77));
    public static final Position2d k_circleCargoTwo = new Position2d(Length.inches(124.95), Length.inches(88.30));
    public static final Position2d k_circleCargoThree = new Position2d(Length.inches(25.91), Length.inches(150.79));
    public static final Position2d k_circleCargoFour = new Position2d(Length.inches(-33.77), Length.inches(149.23));
    public static final Position2d k_circleCargoFive = new Position2d(Length.inches(-88.30), Length.inches(124.95));
    public static final Position2d k_circleCargoSix = new Position2d(Length.inches(-129.40), Length.inches(81.64));
    public static final Position2d k_circleCargoSeven = new Position2d(Length.inches(-149.23), Length.inches(-33.77));
    public static final Position2d k_circleCargoEight = new Position2d(Length.inches(-124.95), Length.inches(-88.30));
    public static final Position2d k_circleCargoNine = new Position2d(Length.inches(-25.91), Length.inches(-150.79));
    public static final Position2d k_circleCargoTen = new Position2d(Length.inches(33.77), Length.inches(-149.23));
    public static final Position2d k_circleCargoEleven = new Position2d(Length.inches(88.30), Length.inches(-124.95));
    public static final Position2d k_circleCargoTwelve = new Position2d(Length.inches(129.40), Length.inches(-81.64));

    public static final Position2d k_terminalCargoClose =
        new Position2d(Length.inches(-282.12), Length.inches(-177.78));
    public static final Position2d k_terminalCargoFar = new Position2d(Length.inches(282.12), Length.inches(177.78));
}
