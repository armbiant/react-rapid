/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.constants;

import org.growingstems.math.Angle;
import org.growingstems.math.Time;
import org.growingstems.math.Angle.AngleUnit;
import org.growingstems.math.Time.TimeUnit;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class ShotCriteria {
    // Robot State
    private static final double k_defaultReadyToShootTimeRequirementTime = 0.1;
    public static Time k_readyToShootTimeRequirementTime = new Time(k_defaultReadyToShootTimeRequirementTime, TimeUnit.SECONDS);
    // Drive
    private static final double k_defaultDriveAllowableHeadingError = 10.0;
    public static Angle k_driveAllowableHeadingError = new Angle(k_defaultDriveAllowableHeadingError, AngleUnit.DEGREES);
    public static double k_driveAngularVelocityThreshold_radps = 0.15;
    public static double k_driveVelocityThreshold_inps = 6.0;
    // Shooter
    public static double k_shooterPrimaryRpmRange = 40.0;
    public static double k_shooterSecondaryRpmRange = 40.0;
    private static final double k_defaultShooterWheelReadyDebounceTime = 0.3;
    public static Time k_shooterWheelReadyDebounceTime = new Time(k_defaultShooterWheelReadyDebounceTime, TimeUnit.SECONDS);
    private static final double k_defaultShooterWheelNotReadyDebounceTime = 0.1;
    public static Time k_shooterWheelNotReadyDebounceTime = new Time(k_defaultShooterWheelNotReadyDebounceTime, TimeUnit.SECONDS);
    private static final double k_defaultShooterHoodDownTransition = 0.1;
    public static Time k_shooterHoodDownTransition = new Time(k_defaultShooterHoodDownTransition, TimeUnit.SECONDS);
    private static final double k_defaultShooterHoodUpTransition = 0.2;
    public static Time k_shooterHoodUpTransition = new Time(k_defaultShooterHoodUpTransition, TimeUnit.SECONDS);
    // Serializer
    private static final double k_hoodUpSerializerShotCooldownTime = 0.3;
    private static final double k_hoodDownSerializerShotCooldownTime = 0.3;
    public static Time k_serializerShotCooldownTimeHoodUp = new Time(k_hoodUpSerializerShotCooldownTime, TimeUnit.SECONDS);
    public static Time k_serializerShotCooldownTimeHoodDown = new Time(k_hoodDownSerializerShotCooldownTime, TimeUnit.SECONDS);

    // Network Table Entries
    // Robot State
    private static final NetworkTableEntry k_readyToShootTimeRequirementTimeEntry = SmartDashboard.getEntry("ShotCriteria/Ready To Shoot Time");
    // Drive
    private static final NetworkTableEntry k_driveAllowableHeadingErrorEntry = SmartDashboard.getEntry("ShotCriteria/Drive Allowed Heading Error Deg");
    private static final NetworkTableEntry k_driveAngularVelocityThresholdEntry = SmartDashboard.getEntry("ShotCriteria/Drive Angular Velocity Threshold radps");
    private static final NetworkTableEntry k_driveVelocityThresholdEntry = SmartDashboard.getEntry("ShotCriteria/Drive Velocity Threshold inps");
    // Shooter
    private static final NetworkTableEntry k_shooterPrimaryRpmRangeEntry = SmartDashboard.getEntry("ShotCriteria/Shooter Primary RPM Range");
    private static final NetworkTableEntry k_shooterSecondaryRpmRangeEntry = SmartDashboard.getEntry("ShotCriteria/Shooter Secondary RPM Range");
    private static final NetworkTableEntry k_shooterWheelReadyDebounceTimeEntry = SmartDashboard.getEntry("ShotCriteria/Ready Debounce Time Sec");
    private static final NetworkTableEntry k_shooterWheelNotReadyDebounceTimeEntry = SmartDashboard.getEntry("ShotCriteria/Not Ready Debounce Time Sec");
    private static final NetworkTableEntry k_shooterHoodDownTransitionEntry = SmartDashboard.getEntry("ShotCriteria/Hood Down Transition Time Sec");
    private static final NetworkTableEntry k_shooterHoodUpTransitionEntry = SmartDashboard.getEntry("ShotCriteria/Hood Up Transition Time Sec");
    // Serializer
    private static final NetworkTableEntry k_serializerHoodUpShotCooldownTimeEntry = SmartDashboard.getEntry("ShotCriteria/Serializer Shot Hood Up Cooldown Time Sec");
    private static final NetworkTableEntry k_serializerHoodDownShotCooldownTimeEntry = SmartDashboard.getEntry("ShotCriteria/Serializer Shot Hood Down Cooldown Time Sec");

    static {
        k_readyToShootTimeRequirementTimeEntry.setDefaultDouble(k_defaultReadyToShootTimeRequirementTime);
        k_driveAllowableHeadingErrorEntry.setDefaultDouble(k_defaultDriveAllowableHeadingError);
        k_driveAngularVelocityThresholdEntry.setDefaultDouble(k_driveAngularVelocityThreshold_radps);
        k_driveVelocityThresholdEntry.setDefaultDouble(k_driveVelocityThreshold_inps);
        k_shooterPrimaryRpmRangeEntry.setDefaultDouble(k_shooterPrimaryRpmRange);
        k_shooterSecondaryRpmRangeEntry.setDefaultDouble(k_shooterSecondaryRpmRange);
        k_shooterWheelReadyDebounceTimeEntry.setDefaultDouble(k_defaultShooterWheelReadyDebounceTime);
        k_shooterWheelNotReadyDebounceTimeEntry.setDefaultDouble(k_defaultShooterWheelNotReadyDebounceTime);
        k_shooterHoodDownTransitionEntry.setDefaultDouble(k_defaultShooterHoodDownTransition);
        k_shooterHoodUpTransitionEntry.setDefaultDouble(k_defaultShooterHoodUpTransition);
        k_serializerHoodUpShotCooldownTimeEntry.setDefaultDouble(k_hoodUpSerializerShotCooldownTime);
        k_serializerHoodDownShotCooldownTimeEntry.setDefaultDouble(k_hoodDownSerializerShotCooldownTime);
    }

    public static void updateDashboard() {
        // Robot State
        k_readyToShootTimeRequirementTime = new Time(k_readyToShootTimeRequirementTimeEntry.getDouble(k_defaultReadyToShootTimeRequirementTime), TimeUnit.SECONDS);
        // Drive
        k_driveAllowableHeadingError = new Angle(k_driveAllowableHeadingErrorEntry.getDouble(k_defaultDriveAllowableHeadingError), AngleUnit.DEGREES);
        k_driveAngularVelocityThreshold_radps = k_driveAngularVelocityThresholdEntry.getDouble(k_driveAngularVelocityThreshold_radps);
        k_driveVelocityThreshold_inps = k_driveVelocityThresholdEntry.getDouble(k_driveVelocityThreshold_inps);
        // Shooter
        k_shooterPrimaryRpmRange = k_shooterPrimaryRpmRangeEntry.getDouble(k_shooterPrimaryRpmRange);
        k_shooterSecondaryRpmRange = k_shooterSecondaryRpmRangeEntry.getDouble(k_shooterSecondaryRpmRange);
        k_shooterWheelReadyDebounceTime = new Time(k_shooterWheelReadyDebounceTimeEntry.getDouble(k_defaultShooterWheelReadyDebounceTime), TimeUnit.SECONDS);
        k_shooterWheelNotReadyDebounceTime = new Time(k_shooterWheelNotReadyDebounceTimeEntry.getDouble(k_defaultShooterWheelNotReadyDebounceTime), TimeUnit.SECONDS);
        k_shooterHoodDownTransition = new Time(k_shooterHoodDownTransitionEntry.getDouble(k_defaultShooterHoodDownTransition), TimeUnit.SECONDS);
        k_shooterHoodUpTransition = new Time(k_shooterHoodUpTransitionEntry.getDouble(k_defaultShooterHoodUpTransition), TimeUnit.SECONDS);
        // Serializer
        k_serializerShotCooldownTimeHoodUp = new Time(k_serializerHoodUpShotCooldownTimeEntry.getDouble(k_hoodUpSerializerShotCooldownTime), TimeUnit.SECONDS);
        k_serializerShotCooldownTimeHoodDown = new Time(k_serializerHoodDownShotCooldownTimeEntry.getDouble(k_hoodDownSerializerShotCooldownTime), TimeUnit.SECONDS);
    }
}
