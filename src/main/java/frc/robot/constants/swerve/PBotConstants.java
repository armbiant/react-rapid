/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.constants.swerve;

public class PBotConstants extends CBotConstants {
    public PBotConstants() {
        super();

        fr.steerEncoderoffset = -158.115;
        fl.steerEncoderoffset = -168.926;
        bl.steerEncoderoffset = -149.414;
        br.steerEncoderoffset = -147.656;

        fr.ipsu = (142.5 - 9.0) / Math.abs(-140156);
        fl.ipsu = (142.5 - 9.0) / Math.abs(-139626);
        br.ipsu = (142.5 - 9.0) / Math.abs(-139614);
        bl.ipsu = (142.5 - 9.0) / Math.abs(-141102);
    }
}
