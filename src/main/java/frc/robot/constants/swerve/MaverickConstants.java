/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.constants.swerve;

import org.growingstems.math.Vector2d;

public class MaverickConstants extends SwerveXConstants {
    public MaverickConstants() {
        super();

        fr.pos = new Vector2d(10.125, -10.125);
        fr.steerEncoderoffset = 19.687;

        fl.pos = new Vector2d(10.125, 10.125);
        fl.steerEncoderoffset = -175.693;

        bl.pos = new Vector2d(-10.125, 10.125);
        bl.steerEncoderoffset = 73.916;

        br.pos = new Vector2d(-10.125, -10.125);
        br.steerEncoderoffset = 112.764;
    }
}
