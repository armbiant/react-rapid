/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.constants.swerve;

import com.ctre.phoenix.motorcontrol.TalonFXInvertType;

import org.growingstems.math.Vector2d;

public abstract class SwerveConstants {
    public static class PidConstants {
        public int slot;
        public double p;
        public double i;
        public double d;
        public double f;
        public double izone;
        public double allowedError;
    }

    public static class ModuleConstants {
        public Vector2d pos;
        /*
         * If the robot already has code that configures the steer encoder and motor, skip stpes 1-3.
         *
         * TO CALIBRATE
         * 1. Upload code that configures the Steer Encoder/Motor
         * 2. Wait for robot code to initialize
         * 3. Power Cycle entire Robot
         * 4. Connect to the Robot using Phoenix Tuner
         * 5. Set each Encoder's Offset according to the formula below.
         * Encoder Offset = Current Offset + Current Position - Phoenix Tuner's Absolute Position
         * 6. Re-Upload Code
         * 7. Wait for robot code to initialize
         * 8. Power Cycle entire Robot
         */
        public double steerEncoderoffset;
        public double ipsu;
        public TalonFXInvertType driveInvert;
        public boolean driveSensorPhase;
    }

    public static class MotionMagicConstants {
        public double cruiseVel;
        public double acceleration;
    }

    public static class SteerConfigConstants {
        public double neutralDeadband;
        public boolean sensorPhase;
        public TalonFXInvertType invert;
        public double vCompSat;
    }

    public ModuleConstants fr = new ModuleConstants();
    public ModuleConstants fl = new ModuleConstants();
    public ModuleConstants bl = new ModuleConstants();
    public ModuleConstants br = new ModuleConstants();

    public PidConstants steerPid = new PidConstants();
    public PidConstants unloadedSteerPid = new PidConstants();
    public PidConstants headingPid = new PidConstants();

    // global swerve values
    public double steer_rpsu;
    public double driveStableGain;

    public MotionMagicConstants mm = new MotionMagicConstants();
    public SteerConfigConstants steer = new SteerConfigConstants();
}
