/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.constants.swerve;

import com.ctre.phoenix.motorcontrol.TalonFXInvertType;

import org.growingstems.math.Vector2d;

public class SwerveXConstants extends SwerveConstants {
    public SwerveXConstants() {
        fr.pos = new Vector2d(9.125, -9.125);
        fr.steerEncoderoffset = -41.836;
        fr.ipsu = 362.005 / 366819.0;
        fr.driveInvert = TalonFXInvertType.Clockwise;
        fr.driveSensorPhase = false;

        fl.pos = new Vector2d(9.125, 9.125);
        fl.steerEncoderoffset = -84.023;
        fl.ipsu = 362.005 / 365948.0;
        fl.driveInvert = TalonFXInvertType.CounterClockwise;
        fl.driveSensorPhase = true;

        bl.pos = new Vector2d(-10.125, 10.125);
        bl.steerEncoderoffset = -159.170;
        bl.ipsu = 362.005 / 365414.0;
        bl.driveInvert = TalonFXInvertType.Clockwise;
        bl.driveSensorPhase = false;

        br.pos = new Vector2d(-10.125, -10.125);
        br.steerEncoderoffset = -159.082;
        br.ipsu = 362.005 / 366498.0;
        br.driveInvert = TalonFXInvertType.CounterClockwise;
        br.driveSensorPhase = false;

        steerPid.slot = 0;
        steerPid.p = 1.0;
        steerPid.i = 0.005;
        steerPid.d = 15.0;
        steerPid.f = 0.366225481;
        steerPid.allowedError = 15.0;
        steerPid.izone = 50.0;

        unloadedSteerPid.p = 0.1;
        unloadedSteerPid.i = 0.0;
        unloadedSteerPid.d = 0.0;
        unloadedSteerPid.f = 0.366225481;
        unloadedSteerPid.allowedError = 15.0;
        unloadedSteerPid.slot = 1;
        unloadedSteerPid.izone = 50.0;

        // TODO: tune these gains
        headingPid.p = 0.02;
        headingPid.i = 0;
        headingPid.d = 0.001;

        steer_rpsu = (2.0 * Math.PI) / 4096.0;
        driveStableGain = 0.002;

        mm.cruiseVel = 2654;
        mm.acceleration = 15922;

        steer.neutralDeadband = 0.0001;
        steer.sensorPhase = true;
        steer.invert = TalonFXInvertType.Clockwise;
        steer.vCompSat = 10.0;
    }
}
