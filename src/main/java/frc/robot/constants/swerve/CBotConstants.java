/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.constants.swerve;

import com.ctre.phoenix.motorcontrol.TalonFXInvertType;

import org.growingstems.math.Vector2d;

public class CBotConstants extends SwerveConstants {
    public CBotConstants() {
        fr.pos = new Vector2d(11.25, -11.25);
        fr.steerEncoderoffset = -6.768;
        fr.ipsu = (142.5 - 9.0) / Math.abs(-140156);
        fr.driveInvert = TalonFXInvertType.CounterClockwise;
        fr.driveSensorPhase = false;

        fl.pos = new Vector2d(11.25, 11.25);
        fl.steerEncoderoffset = -59.854;
        fl.ipsu = (142.5 - 9.0) / Math.abs(-139626);
        fl.driveInvert = TalonFXInvertType.Clockwise;
        fl.driveSensorPhase = true;

        bl.pos = new Vector2d(-11.25, 11.25);
        bl.steerEncoderoffset = -153.896;
        bl.ipsu = (142.5 - 9.0) / Math.abs(-141102);
        bl.driveInvert = TalonFXInvertType.Clockwise;
        bl.driveSensorPhase = false;

        br.pos = new Vector2d(-11.25, -11.25);
        br.steerEncoderoffset = -163.213;
        br.ipsu = (142.5 - 9.0) / Math.abs(-139614);
        br.driveInvert = TalonFXInvertType.Clockwise;
        br.driveSensorPhase = false;

        steerPid.slot = 0;
        steerPid.p = 1.0;
        steerPid.i = 0.005;
        steerPid.d = 15.0;
        steerPid.f = 0.366225481;
        steerPid.allowedError = 15.0;
        steerPid.izone = 50.0;

        unloadedSteerPid.p = 0.1;
        unloadedSteerPid.i = 0.0;
        unloadedSteerPid.d = 0.0;
        unloadedSteerPid.f = 0.366225481;
        unloadedSteerPid.allowedError = 15.0;
        unloadedSteerPid.slot = 1;
        unloadedSteerPid.izone = 50.0;

        // TODO: tune these gains
        headingPid.p = 0.02;
        headingPid.i = 0;
        headingPid.d = 0.001;

        steer_rpsu = (2.0 * Math.PI) / 4096.0;
        driveStableGain = 0.002;

        mm.cruiseVel = 2654;
        mm.acceleration = 15922;

        steer.neutralDeadband = 0.0001;
        steer.sensorPhase = true;
        steer.invert = TalonFXInvertType.Clockwise;
        steer.vCompSat = 10.0;
    }
}
