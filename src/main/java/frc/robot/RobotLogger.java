/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import edu.wpi.first.util.sendable.Sendable;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.growingstems.math.Time;

import frc.library.LogEntryPriority;
import frc.library.Logger;
import frc.robot.constants.LogEntry;

public class RobotLogger {
    private final Logger m_logger;
    private final LogEntryPriority m_priority;

    public RobotLogger() {
        this(LogEntryPriority.HIGH);
    }

    public RobotLogger(LogEntryPriority priority) {
        m_priority = priority;
        m_logger = new Logger(generateKeys(priority));
    }

    public void init(File logFile) {
        m_logger.init(logFile);
    }

    public void startLogging(Time logPeriod) {
        m_logger.startLogging(logPeriod);
    }

    public void stopLogging() {
        m_logger.stopLogging();
    }

    public void setDataPoint(LogEntry entry, Supplier<Object> val) {
        if (entry.sendToDashboard || isLogged(entry, m_priority)) {
            setDataPoint(entry, val.get());
        }
    }

    public void setDataPoint(LogEntry entry, Object val) {
        if (entry.sendToDashboard) {
            String dashboardKey = entry.subsystem + "/" + entry.name;
            if (val instanceof Double) {
                SmartDashboard.putNumber(dashboardKey, (Double)val);
            } else if (val instanceof Float) {
                SmartDashboard.putNumber(dashboardKey, (Float)val);
            } else if (val instanceof Long) {
                SmartDashboard.putNumber(dashboardKey, (Long)val);
            } else if (val instanceof Integer) {
                SmartDashboard.putNumber(dashboardKey, (Integer)val);
            } else if (val instanceof Short) {
                SmartDashboard.putNumber(dashboardKey, (Short)val);
            } else if (val instanceof Byte) {
                SmartDashboard.putNumber(dashboardKey, (Byte)val);
            } else if (val instanceof Double[]) {
                SmartDashboard.putNumberArray(dashboardKey, (Double[])val);
            } else if (val instanceof double[]) {
                SmartDashboard.putNumberArray(dashboardKey, (double[])val);
            } else if (val instanceof Boolean) {
                SmartDashboard.putBoolean(dashboardKey, (Boolean)val);
            } else if (val instanceof Boolean[]) {
                SmartDashboard.putBooleanArray(dashboardKey, (Boolean[])val);
            } else if (val instanceof boolean[]) {
                SmartDashboard.putBooleanArray(dashboardKey, (boolean[])val);
            } else if (val instanceof String) {
                SmartDashboard.putString(dashboardKey, (String)val);
            } else if (val instanceof String[]) {
                SmartDashboard.putStringArray(dashboardKey, (String[])val);
            } else if (val instanceof Sendable) {
                SmartDashboard.putData(dashboardKey, (Sendable)val);
            } else if (val instanceof byte[]) {
                SmartDashboard.putRaw(dashboardKey, (byte[])val);
            } else {
                SmartDashboard.putString(dashboardKey, val.toString());
            }
        }

        if (isLogged(entry, m_priority)) {
            m_logger.setDataPoint(generateKeyName(entry), val);
        }
    }

    public boolean isLogging() {
        return m_logger.isLogging();
    }

    public boolean keyExists(String key) {
        return m_logger.keyExists(key);
    }

    public void grabLine() {
        m_logger.grabLine();
    }

    private static String generateKeyName(LogEntry entry) {
        return entry.subsystem + "_" + entry.name;
    }

    private static boolean isLogged(LogEntry entry, LogEntryPriority priority) {
        return entry.priority.value <= priority.value;
    }

    private static List<String> generateKeys(LogEntryPriority priority) {
        ArrayList<String> keys = new ArrayList<>();
        for (LogEntry entry : LogEntry.values()){
            if (isLogged(entry, priority)) {
                keys.add(generateKeyName(entry));
            }
        }
        return keys;
    }
}
