/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.button.Trigger;

import java.util.function.Supplier;

import org.growingstems.math.Vector2d;

public abstract class ControllerConfig {
    // These are the correct values
    protected final static int dPadUp = 0;
    protected final static int dPadRight = 90;
    protected final static int dPadDown = 180;
    protected final static int dPadLeft = 270;

    protected static final double k_defaultMaxDriveSpeed = 0.6;
    protected static final double k_defaultMaxTurnSpeed = 0.5;
    protected static final double k_fullSpeed = 1.0;

    public Trigger resetImu;
    public Supplier<Vector2d> defaultVelocitySupplier;
    public Supplier<Double> defaultRotationSupplier;
    public NetworkTableEntry driveThrottle = SmartDashboard.getEntry("Max Drive Speed");
    public NetworkTableEntry turnThrottle = SmartDashboard.getEntry("Max Turn Speed");

    public Trigger fullPowerTrigger;

    public Trigger intakeTrigger;
    public Trigger outtakeTrigger;
    public Trigger retractIntakeTrigger;

    public Trigger shootAutoTrigger;
    public Trigger shootFenderTrigger;
    public Trigger shootDashboardTrigger;

    public Trigger rejectShooterTrigger;
    public Trigger rejectIntakeTrigger;

    public Trigger climbEnable;
    public Supplier<Boolean> climbAdvance;
    public Supplier<Boolean> climbRegress;

    public Trigger trackCargo;

    protected ControllerConfig() {
        driveThrottle.setDouble(k_defaultMaxDriveSpeed);
        turnThrottle.setDouble(k_defaultMaxTurnSpeed);
    }
}
