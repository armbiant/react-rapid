/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.library;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.wpi.first.wpilibj2.command.CommandScheduler;

import org.growingstems.util.statemachine.ExecState;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class ScheduledStateMachine_Test {
    private ScheduledStateMachine sm;
    private int execCounter;

    private void increment() {
        execCounter++;
    }

    @BeforeEach
    public void setupStateMachine() {
        execCounter = 0;
        var incrementer = new ExecState("Increment", this::increment);
        sm = new ScheduledStateMachine(incrementer);
        sm.start();
    }

    @AfterEach
    public void removeStateMachine() {
        sm.stop();
        CommandScheduler.getInstance().unregisterSubsystem(sm);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3})
    public void stepFromCommandScheduler(int count) {
        assertEquals(0, execCounter);
        for (int i = 1; i <= count; i++) {
            CommandScheduler.getInstance().run();
            assertEquals(i, execCounter);
        }
        assertEquals(count, execCounter);
    }
}
