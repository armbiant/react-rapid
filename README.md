# 836 RoboBees - 2022 Rapid React Robot Code

We got an award at Houston! - [Roebling Innovation in Control Award](https://www.thebluealliance.com/event/2022roe#awards)

## Other Repos

### [Growing Stems Common](https://gitlab.com/growingstems/gstems-common)
### [Growing Stems FRC Common](https://gitlab.com/growingstems/frc-836-the-robobees/gstems-frc-common)
### [Cargo Tracker Raspberry Pi Code](https://gitlab.com/growingstems/frc-836-the-robobees/2022-vision-code)
### [Cargo Tracker Viewer](https://gitlab.com/growingstems/frc-836-the-robobees/2022-cargo-viewer)

## Sensor 3D Printed Parts (Yes, programmers can do mechanical stuff too sometimes)

### TCS34725 - RGBW Color Sensor
- [Adafruit TCS34725](https://www.adafruit.com/product/1334)
- [TCS34725_v2.step](uploads/f535d8b665450608c0db19203fc47bae/TCS34725_v2.step)
- [TCS34725_Case_Bottom_v16.stl](uploads/aec1a9eac388cda66d08733134b669ba/TCS34725_Case_Bottom_v16.stl)
- [TCS34725_Case_Top_v19.stl](uploads/5e2c33a5d9cef957bf09682277fb4ba9/TCS34725_Case_Top_v19.stl)

### VL53L1X - ToF Distance Sensor
- [Adafruit VL53L1X](https://www.adafruit.com/product/3967)
- [Seeeduino Xiao](https://www.seeedstudio.com/Seeeduino-XIAO-Arduino-Microcontroller-SAMD21-Cortex-M0+-p-4426.html)
- [Adafruit_VL53L1X_v6.step](uploads/722ef9bb5945ad044fb5f5deedfc176e/Adafruit_VL53L1X_v6.step)
- [Seeeduino_Xiao_v1.step](uploads/fd86ef87cb946780fa859b150c4321b9/Seeeduino_Xiao_v1.step)
- [Lidar_Case_Top_v19.stl](uploads/d5d738ffd7c72a72ea55e05d0458b4dc/Lidar_Case_Top_v19.stl)
- [LIDAR_Case_Bottom_v14.stl](uploads/31ff1535676bda539bce6d86614741d9/LIDAR_Case_Bottom_v14.stl)
- [LIDAR_Xiao_Code.ino](uploads/de802427f6bef52b9c1d6c8b35552f8a/LIDAR_Xiao_Code.ino)

### TCA9548A - I2C Multiplexer
- [Adafruit TCA9548A](https://www.adafruit.com/product/2717)
- [TCA95648A_v1.step](uploads/4c16a412d68437ed400d6b30e426fe9d/TCA95648A_v1.step)
- [I2C_Mux_Top_v19.stl](uploads/10b4df24ce49aef923d21e0fc2565402/I2C_Mux_Top_v19.stl)
- [I2C_Mux_Bottom_v8.stl](uploads/b4d0e853f47b8f4ac22062ebaa41ae43/I2C_Mux_Bottom_v8.stl)

